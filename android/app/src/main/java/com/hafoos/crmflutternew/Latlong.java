package com.hafoos.crmflutternew;

public class Latlong {
    int _id;
    String _lat;
    String _long;
    String _time;
    public Latlong(){   }
    public Latlong(int id, String _lat, String _long, String _time){
        this._id = id;
        this._lat = _lat;
        this._long = _long;
        this._time = _time;

    }
    public Latlong(String _lat, String _long, String _time){
        this._lat = _lat;
        this._long = _long;
        this._time = _time;

    }

    public int get_id() {
        return _id;
    }

    public String get_lat() {
        return _lat;
    }

    public String get_long() {
        return _long;
    }

    public String get_time() {
        return _time;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_lat(String _lat) {
        this._lat = _lat;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    public void set_time(String _time) {
        this._time = _time;
    }
}
