package com.hafoos.crmflutternew.data;

import android.content.Context;
import com.hafoos.crmflutternew.data.pref.PreferencesHelper;

public class AppDataManager implements DataManager {

    private Context context;
    private ApiHelper apiHelper;
    //    private DbHelper dbHelper;
    private PreferencesHelper mPreferencesHelper;

    //
    public AppDataManager(Context context,
                          ApiHelper apiHelper,
//                          DbHelper dbHelper,
                          PreferencesHelper mPreferencesHelper
    ) {
        this.context = context;
        this.apiHelper = apiHelper;
//        this.dbHelper = dbHelper;
        this.mPreferencesHelper = mPreferencesHelper;
    }

    @Override
    public void getTrackingapi(String token, String deviceid, String visitid, double lat, double lng, Callback<String> callback) {
        apiHelper.getTrackingapi(token, deviceid, visitid, lat, lng, callback);
    }


//    @Override
//    public void getLoginProfile(String lpg, Callback<String> callback) {
//        apiHelper.getLoginProfile(lpg,callback);
//    }

//    @Override
//    public void doSignup(String language, String email, String password, String name, String mobile, String nickname, int is_termstrue, int is_privacypolicy, int is_newsSubsribe, Callback<LoginDataModel> callback) {
//        apiHelper.doSignup(language, email, password, nickname, mobile, name, is_termstrue, is_privacypolicy, is_newsSubsribe, callback);
//    }
//
//    @Override
//    public void getLoginProfile(String encData) {
//        apiHelper.getLoginProfile(encData);
//    }
//
//    @Override
//    public void getUserProfileAlwasys(String language, String user_id, String user_type, Callback<LoginDataModel> callback) {
//        apiHelper.getUserProfileAlwasys(language, user_id, user_type, callback);
//
//    @Override
//    public void getSearchResult(String language, String searchTerm, Pagging<SearchList> pagging, Callback<Pagging<SearchList>> onApiCallback) {
//        apiHelper.getSearchResult(language, searchTerm, pagging, onApiCallback);
//    }
//
//    @Override
//    public void getUpdateUserProfile(String language, String user_id, String userType, String email, String mobile, String name, String nick_name, String current_password, String password, Callback<String> callback) {
//        apiHelper.getUpdateUserProfile(language, user_id, userType, email, mobile, name, nick_name, current_password, password, callback);
//    }
//
//
//    @Override
//    public void setBooleanAppPref(String key, boolean value) {
//        mPreferencesHelper.setBooleanAppPref(key, value);
//    }
//
//    @Override
//    public boolean getBooleanAppPref(String key, boolean defaultValue) {
//        return mPreferencesHelper.getBooleanAppPref(key, defaultValue);
//    }
//
//    @Override
//    public void saveUserToPref(LoginProfile user) {
//        mPreferencesHelper.saveUserToPref(user);
//    }
//
//    @Override
//    public LoginProfile getUserFromPref() {
//        return mPreferencesHelper.getUserFromPref();
//    }

}
