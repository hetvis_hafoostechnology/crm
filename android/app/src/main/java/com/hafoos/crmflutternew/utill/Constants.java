package com.hafoos.crmflutternew.utill;

public class Constants {
    public static final String SUCCESS_CODE_STRING = "success";
    public static final int FAIL_CODE = 0;
    public static final int SUCCESS_CODE = 1;
    public static final int CANCEL_CODE = -1;
    public static final int FAIL_INTERNET_CODE = -2;
    public static final int PERMISSION_CODE = 101;
    public static final String OS_ANDROID = "android";
    public static final String RES_MSG_KEY = "res_message";
    public static final String RES_CODE_KEY = "res_code";
    public static final String RES_DATA_KEY = "res_data";
    public static final String RES_HAS_MORE = "has_more_page";

    public static final String KEY_USER_ARRAY = "UserDataPhone";
    public static final String KEY_USERLOGINDATA = "UserPhoneLoginData";
    public static final String KEY_ConfTPINPhoneLoginData = "ConfTPINPhoneLoginData";
    public static final String KEY_TPINResponseData = "TPINResponseData";
    public static final String KEY_CONFIRM_TPIN = "ConfirmationTPINPhone";
    public static final String KEY_GPNResponse = "GPNResponse";
    public static final String KEY_LIST_ARRAY = "ListArray";


    public static final String KEY_USER_UserID = "UserID";
    public static final String KEY_LoginID = "LoginID";
    public static final String KEY_Password = "Password";
    public static final String KEY_USER_Salutation = "Salutation";
    public static final String KEY_USER_FirstName = "FirstName";
    public static final String KEY_USER_LastName = "LastName";
    public static final String KEY_USER_Suffix = "Suffix";
    public static final String KEY_USER_MidInit = "MidInit";
    public static final String KEY_USER_Address1 = "Address1";
    public static final String KEY_USER_Address2 = "Address2";
    public static final String KEY_USER_Address3 = "Address3";
    public static final String KEY_USER_City = "City";
    public static final String KEY_USER_State = "State";
    public static final String KEY_USER_StateCode = "StateCode";
    public static final String KEY_USER_Country = "Country";
    public static final String KEY_USER_CountryCode = "CountryCode";
    public static final String KEY_USER_Zip = "Zip";
    public static final String KEY_USER_Phone = "Phone";
    public static final String KEY_USER_PhoneCell = "PhoneCell";
    public static final String KEY_USER_Email = "Email";
    public static final String KEY_USER_UserVerified = "UserVerified";
    public static final String KEY_APIKey = "APIKey";
    public static final String KEY_TPinID = "TPinID";
    public static final String KEY_AccessToken = "AccessToken";
    public static final String KEY_TokenExpirationDate = "TokenExpirationDate";
    public static final String KEY_ConfrimId = "ConfirmationID";
    public static final String KEY_GrossAmount = "GrossAmount";
    public static final String KEY_BuyerCommitment = "BuyerCommitment";
    public static final String KEY_ContractFinanceID = "ContractFinanceID";
    public static final String KEY_GPNStatusID = "GPNStatusID";
    public static final String KEY_GPNStatusDate = "GPNStatusDate";
    public static final String KEY_ConfirmationDate = "ConfirmationDate";
    public static final String KEY_StatusDesc = "StatusDesc";
    public static final String KEY_quan = "Quantity";
    public static final String KEY_BidUserID = "BidUserID";
    public static final String KEY_BidCorpID = "BidCorpID";
    public static final String KEY_TPin = "TPin";
    public static final String KEY_success = "success";

    public static final String KEY_OS = "OSFlagID";
    public static final String Val_OS = "2";
    public static final String Val_APIKEYForList = "9B2BFE83-C113-4B78-AFB1-E363FCC3CFFF";

    public static final String sp_key = "cbdhx";
    public static final String sp_remember_key = "rememberMe";
    public static final String sp_LoginEncryptedResponse = "login_dec_enc";
    public static final String sp_SaveLoginEmail = "login_email_Save";
    public static final String sp_saveLoginFlag = "saveLogin";
    public static final String sp_SaveLoginPwd = "login_pwd_save";
    public static final String sp_ConfrimTpinList = "confirm_tpin_list";

//    public static final String sp_LoginaccessKey = "login_accessKey";
//    public static final String sp_ListccessKey = "list_accessKey";
    public static final String sp_newaccessToken = "token_accessKey";

    public static final String sp_userFname = "user_fname";
    public static final String sp_userLname = "user_lname";
    public static final String sp_userId = "user_id";
    public static final String sp_userEmail = "user_email";


}