package com.hafoos.crmflutternew.data;

public interface ApiHelper {

    void getTrackingapi(String token,String deviceid,String visitid,double lat,double lng, DataManager.Callback<String> callback);
}
