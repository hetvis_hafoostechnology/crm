package com.hafoos.crmflutternew;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "flutter.native/helper";
    String token, visitid, deviceid;
    DataBaseHandler db = new DataBaseHandler(this);
    ArrayList<String> time = new ArrayList<>();


    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {

        GeneratedPluginRegistrant.registerWith(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        if (call.method.equals("helloFromNativeCode")) {//GeofencingService.initialized

//                  locationMethod();
                            token = call.argument("token");
                            visitid = call.argument("visitid");
                            deviceid = call.argument("Deviceid");
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("TrackApiDetails", 0);
                            SharedPreferences.Editor edit = pref.edit();

                            edit.putString("token", token);
                            edit.putString("visitid", visitid);
                            edit.putString("Deviceid", deviceid);
//            edit.putString("timeofLatLong", mLastUpdateTime);
                            edit.apply();
                            Log.e("sp???", pref.getString("token", ""));
                            Log.e("Got all from flutter", token + " " + visitid + " " + deviceid);
                            String greetings = helloFromNativeCode();
                            Log.e("Succes", greetings);

                            result.success(greetings);
                        } else if (call.method.equals("endService")) {
                            Log.e("Successstr end service", "Called");
                            endService();
                        }
                    }
                });

    }


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);


//    AndroidAlarmManagerPlugin.registerWith(registrarFor("io.flutter.plugins.androidalarmmanager.AndroidAlarmManagerPlugin"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {


            boolean permissionAccessCoarseLocationApproved =
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED;

            if (permissionAccessCoarseLocationApproved) {
                boolean backgroundLocationPermissionApproved =
                        ActivityCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                                == PackageManager.PERMISSION_GRANTED;

                if (backgroundLocationPermissionApproved) {
                    doStuff();
                } else {
                    // App can only access location in the foreground. Display a dialog
                    // warning the user that your app must have all-the-time access to
                    // location in order to function properly. Then, request background
                    // location.
                    ActivityCompat.requestPermissions(this, new String[]{
                                    Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                            1);
                }
            } else {
                // App doesn't have access to the device's location at all. Make full request
                // for permission.
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, 1);
            }
        } else {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                }
            } else {
                doStuff();
            }
        }
    }

    private String helloFromNativeCode() {


        return doStuff();
    }

    void endService() {
        Log.e("stop-end", "service");
//        Location_Services mservices = new Location_Services();
//        mservices.stopLocationUpdates();

        stopService(new Intent(this, Location_Services.class));

    }

    String doStuff() {
Log.e("here in ","do stuff");
        Intent serviceIntent = new Intent(this, Location_Services.class);
//        serviceIntent.putExtra("inputExtra", "Ongoing visit is tracking location!");
//        serviceIntent.putExtra("visitid", getvisitid);
//        serviceIntent.putExtra("deviceId", getdeviceid);
//        serviceIntent.putExtra("token", gettoken);
//        Log.e("send", token + " " + visitid + " " + deviceid);
        startService(serviceIntent);
        ContextCompat.startForegroundService(this, serviceIntent);

//have to put code like api calling add n get data in n from model
//        Latlong getlatlongs = db.getAllLATlong();
//        if (getlatlongs.get_lat() != null) {
//            Log.e("got val", getlatlongs.get_lat());
//            loglist = "lat: " + getlatlongs.get_lat() + "time: " + getlatlongs.get_time() + "lng: " + getlatlongs.get_long();
//        } else {
//            Log.e("empty","no val");
//            loglist = "";
//        }
//        List<Latlong> latlongs = db.getAllLATlong();
//        String loglist;
//        for (Latlong cn : latlongs) {
//             loglist = "Id: " + cn.get_id() + " ,lat: " + cn.get_lat() + " ,long: " + cn.get_long() + " ,time: " + cn.get_time();
//            // Writing Contacts to log
//            Log.d("db latlong: loglist ", loglist);
//            time.add(loglist);
//
//        }
//        Log.d("list: ", loglist.toString());
//        callApi();
        //call api here
//    ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this,
//            R.layout.item_lv, R.id.lvname, time);
//    lvLatlong.setAdapter(adapter);
        //get sp
//        String lat, lng;
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("Latlong", 0);
//
////        SharedPreferences.Editor edit = pref.edit();
//
//        lat = pref.getString("lat", "");
//        lng = pref.getString("longitude", "");
//        String jsonStr;
//        Log.e("got any sp??", lat + "" + lng);
//        JSONObject json = new JSONObject();
//        try {
//            json.put("latitude", lat);
//            json.put("longitude", lng);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        jsonStr = json.toString();
        Log.e("native enter", "every time?");
//        String latlngStrng = "lat" + lat + "longitude" + lng;
        return "success";
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        doStuff();
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

    }


}

