package com.hafoos.crmflutternew;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.hafoos.crmflutternew.base.BaseApplication;
import com.hafoos.crmflutternew.data.DataManager;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Location_Services extends Service implements LalongContract.View,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    Latlong saveLocSerLatlng;

    private static final String TAG = "LocationActivity";
//    private static final long INTERVAL = 1000 * 60;//1 min
    private static final long INTERVAL = 1000 * 10;//1 min
    private static final long FASTEST_INTERVAL = 1000 * 10;
//    private static final long FASTEST_INTERVAL = 1000 * 60;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;
    //    Context ctx;
    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 100;
    DataBaseHandler db = new DataBaseHandler(this);
    LalongContract.Presenter<LalongContract.View> presenter;
    Intent i;

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        presenter = new LatlongPresenter<>();
        presenter.onAttach(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

//        ctx = getBaseContext();
        Log.e("Location service","enter");
        buildGoogleApiClient();

    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
//            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        i = intent;

        mGoogleApiClient.connect();


        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("CRM").setOngoing(true)
                .setContentText("Ongoing visit is tracking location!")
                .setSmallIcon(R.drawable.launch_background)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        return START_STICKY;


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("task removed", "sdsfd");
    }

    @Override
    public void onDestroy() {
        Log.e("Called ", "destroy");
        stopLocationUpdates();
        super.onDestroy();
    }

    public void stopLocationUpdates() {
        Log.e("stop-Locationend", "service");

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();

//        mCurrentLocation.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
        mGoogleApiClient.disconnect();

    }

    private void createNotificationChannel() {
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationManager manager = getSystemService(NotificationManager.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            manager.createNotificationChannel(serviceChannel);
        }
//        else{
//        Notification notify = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).build();
//        manager.notify(0,notify);}
    }

    public synchronized void buildGoogleApiClient() {
//        if (!isGooglePlayServicesAvailable()) {
//            finish();
//        }
        createLocationRequest();
        updateUI();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Firing onLocationChanged..............................................");
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
//        restartServiceIntent.setPackage(getPackageName());
//
//        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
//        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//        alarmService.set(
//                AlarmManager.ELAPSED_REALTIME,
//                SystemClock.elapsedRealtime() + 1000,
//                restartServicePendingIntent);
//
//        super.onTaskRemoved(rootIntent);
//    }

    private void updateUI() {
        Log.d(TAG, "UI update initiated .............");
        Latlong latlong = new Latlong();

        if (null != mCurrentLocation) {
            String lat = String.valueOf(mCurrentLocation.getLatitude());
            String lng = String.valueOf(mCurrentLocation.getLongitude());
            Log.e("At Time: ", mLastUpdateTime);
            Log.e("Latitude: ", String.valueOf(mCurrentLocation.getLatitude()));
            Log.e("Longitude: ", String.valueOf(mCurrentLocation.getLongitude()));
            Log.e("Accuracy ", String.valueOf(mCurrentLocation.getAccuracy()));
            Log.e("Provider ", mCurrentLocation.getProvider());

            SharedPreferences pref = getApplicationContext().getSharedPreferences("TrackApiDetails", 0);
//
            SharedPreferences.Editor edit = pref.edit();

            String token1 = pref.getString("token", "");
            String visit1 = pref.getString("visitid", "");
            String device1 = pref.getString("Deviceid", "");
//            Log.e("IN SP", t1 + v1 + d1);

            callApi(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), token1, device1, visit1);

//            Log.e("inserted", "in sp");
//            SharedPreferences pref = getApplicationContext().getSharedPreferences("Latlong", 0);
//            SharedPreferences.Editor edit = pref.edit();

//            edit.putString("lat", lat);
//            edit.putString("longitude", lng);
////            edit.putString("timeofLatLong", mLastUpdateTime);
//            edit.apply();
//            Log.e("sp???", pref.getString("longitude", ""));
//            latlong.set_lat(lat);
//            latlong.set_long(lng);
//            latlong.set_time(mLastUpdateTime);
            //just want one latlong so removing db and storing sp

//            db.addLatlong(new Latlong(lat,lng,mLastUpdateTime));
//call api here

        } else {
            Log.d(TAG, "location is null ...............");
        }
    }

    void callApi(double latitude, double longitute, String token, String deviceid, String visitid) {

        Log.e("Got latlong or not", latitude + " " + longitute + " " + deviceid);
        Log.e("Called", "api");
        Log.e("in api all datat", token+"device "+deviceid+" visi"+visitid);

        presenter.doTrackApi(token, deviceid, visitid, latitude, longitute);
//        BaseApplication.getDataManager().getTrackingapi(token, deviceid, visitid, latitude, longitute, new DataManager.Callback<String>() {
//            @Override
//            public void onSuccess(String baseRes) {
//                Log.e("API", "Success");
//            }
//
//            @Override
//            public void onFailed(int code, String msg) {
//                Log.e("Api", "fail");
//            }
//        });
//        OkHttpClient client = new OkHttpClient();
//        String weburl = "https://dev-api.hafooz.com/";
//
//        RequestBody requestBody = new  .Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("subscriptionUR", weburl)
//                .build();

//        Call<ResponseBody> res = apiInterface.location(token, visitid, deviceid, "android", latitude, longitute);
//        res.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Log.e("here", "url->" + call.request().url());
//                    String str = response.body().string();
//                    Log.e("succes", str);//convert reponse to string
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                t.printStackTrace();
//                Log.e("fail", "url->" + call.request().url());
//                Log.e("fail", t.getMessage());
//            }
//        });

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void addinfoFail() {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showConnectionLostView() {

    }

    @Override
    public void hideConnectionLostView() {

    }

    @Override
    public void showError(int code, String res) {

    }
}
