package com.hafoos.crmflutternew;

import android.util.Log;

import com.hafoos.crmflutternew.base.BaseApplication;
import com.hafoos.crmflutternew.base.BasePresenter;
import com.hafoos.crmflutternew.data.ApiHelper;
import com.hafoos.crmflutternew.data.AppApiHelper;
import com.hafoos.crmflutternew.data.DataManager;
import com.hafoos.crmflutternew.utill.Constants;


public class LatlongPresenter<V extends LalongContract.View> extends BasePresenter<V> implements LalongContract.Presenter<V> {

    private static final String TAG = "Presenter";


    @Override
    public void doTrackApi(String token, String deviceid, String visitid, double lat, double lng) {
        startLoading();
        Log.e(TAG,"api");
        new AppApiHelper().getTrackingapi(token,deviceid,visitid,lat,lng, new DataManager.Callback<String>() {
            @Override
            public void onSuccess(String baseResponse) {
                Log.e(TAG,"Success");
                loadingSuccess();
                if (getView() == null) return;
                getView().onSuccess();
            }

            @Override
            public void onFailed(int code, String message) {
                Log.e(TAG,"fail");
                if (code != Constants.FAIL_INTERNET_CODE) {
                    Log.e(TAG,"fail net");
                }
                loadingFailed(code, message);
            }
        });

    }
}
