package com.hafoos.crmflutternew;


import com.hafoos.crmflutternew.base.BaseContractPresenter;
import com.hafoos.crmflutternew.base.BaseContractView;

public class LalongContract {
    interface View extends BaseContractView

    {

        void onSuccess();
        void addinfoFail();
    }
    interface Presenter<V extends View>extends BaseContractPresenter<V> {
        void doTrackApi(String token,String deviceid,String visitid,double lat,double lng);

    }
}
