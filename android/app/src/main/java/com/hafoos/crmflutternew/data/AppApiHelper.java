package com.hafoos.crmflutternew.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.hafoos.crmflutternew.data.rest.RestConst;
import com.hafoos.crmflutternew.data.rest.RestHelper;
import com.hafoos.crmflutternew.data.rest.RestResponse;

import org.json.JSONObject;

import java.util.HashMap;

public class AppApiHelper implements ApiHelper {


    Context context;
    private Gson gson = new Gson();

    private static final String BASE_URL = "https://dev-api.hafooz.com/";
    private static final String TrackAPiUrl = BASE_URL + "visit/track/save";


    private static final String TAG = "AppApiHelper";
    private static final String KEY_PAGE_NUMBER = "page";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    public AppApiHelper() {
        RestHelper.DEFAULT_BASE_URL = BASE_URL;
    }


    @Override
    public void getTrackingapi(String token, String deviceid, String visitid, double lat, double lng, DataManager.Callback<String> callback) {
        HashMap<String, String> params = new HashMap<>();
        params.put("x-access-token", token);
        try {
            JSONObject jsonToSend = new JSONObject();
            jsonToSend.put("visitId", visitid);
            jsonToSend.put("deviceId", deviceid);
            jsonToSend.put("platform", "android");
            jsonToSend.put("latitude", lat);
            jsonToSend.put("longitude", lng);


            Log.e("Native BODY", jsonToSend.toString());
            new RestHelper.Builder()
                    .setUrl(TrackAPiUrl).setHeaders(params).setJsonObject(jsonToSend)
                    .setRequestMethod(RestConst.RequestMethod.METHOD_POST)
                    .setCallBack(new RestHelper.RestHelperCallback() {
                        @Override
                        public void onRequestCallback(int code, String message, RestResponse restResponse) {

                            Log.e("in res", restResponse.getResString());
                            Log.e("code", String.valueOf(code));
                            Log.e("msg", message);
                            Log.e(" url", restResponse.getRequest().getReqUrl());
                            callback.onSuccess(restResponse.getResString());

                        }
                    })
                    .build()
                    .sendRequest();
        } catch (Exception e) {
            Log.e("Exc", e.toString());
        }
    }


}
