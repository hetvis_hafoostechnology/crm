package com.hafoos.crmflutternew.utill;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.hafoos.crmflutternew.data.rest.RestResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.hafoos.crmflutternew.utill.Constants.RES_CODE_KEY;
import static com.hafoos.crmflutternew.utill.Constants.RES_MSG_KEY;


public class AppUtils {
    public static Context context;
    public static String[] PERMISSIONS = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_PHONE_STATE
    };

//    public static String[] WRITE_STORAGE = new String[]{
//            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//            Manifest.permission.CAMERA
//    };

    public static String LANGUAGE_DEFAULT_VALUE = "english";

    public static void setToast(Context context, String msg) {

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

    }


//    public static void logoutPopup(Context context) {
//        final Dialog dialog = new Dialog(context);
//        dialog.setContentView(R.layout.popup_logout_alert);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setCancelable(true);
//
//        final AppCompatButton btnpopuploginok = (AppCompatButton) dialog.findViewById(R.id.btnpopuploginok);
//
//        btnpopuploginok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//    }

//    public static boolean isConnectingToInternet() {
//        ConnectivityManager cm = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
//
//        return (networkInfo != null && networkInfo.isConnected());
//    }

    public static boolean isPermissionGranted(Activity context, String[] permissions) {
        boolean flag = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String per : permissions) {
                if (context.checkSelfPermission(per) == PackageManager.PERMISSION_GRANTED) {
                    flag = true;
                } else {
                    flag = false;
                    break;
                }
            }
        } else {
            flag = true;
        }

        return flag;

    }

    private static boolean checkFirstTimePermission(String permission[]) {
//        for (String s : permission) {
//            if (!Cbdhx_mockup.getDataManager().getBooleanAppPref(s, false)) {
//                return true;
//            }
//        }
        return false;
    }

//    public static boolean AskAndCheckPermission(Activity context, String permission[], int requestCode) {
//        boolean flag = false;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkFirstTimePermission(permission)) {
//                context.requestPermissions(permission, requestCode);
//                for (String s : permission) {
//                    Cbdhx_mockup.getDataManager().setBooleanAppPref(s, true);
//                }
//            } else {
//                if (isPermissionGranted(context, permission))
//                    return true;
//                for (String per : permission) {
//                    if (context.checkSelfPermission(per) != PackageManager.PERMISSION_GRANTED && !context.shouldShowRequestPermissionRationale(per)) {
//                        flag = true;
//                        break;
//                    } else {
//                        flag = false;
//                    }
//                }
//
//                if (flag) {
//                    flag = false;
//                    CustomAlertDialog.showPermissionAlert(context, context.getResources().getString(R.string.msg_alert_you_have_permission));
//                } else {
//                    context.requestPermissions(permission, requestCode);
//                }
//            }
//        } else {
//            return true;
//        }
//        return flag;
//    }

    public static JSONObject checkResponse(int code, String message, RestResponse restResponse) {

        if (code == Constants.SUCCESS_CODE) {
            int flag;
            try {
                JSONObject res = new JSONObject(restResponse.getResString());
//                Log.d("response ", res.toString());
                flag = res.getInt(RES_CODE_KEY);
                String msg = res.getString(RES_MSG_KEY);
                if (flag == 1) {
                } else {
                    res = null;
                }
                return res;

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (code == Constants.FAIL_CODE) {

        } else {

        }
        return null;

    }

    public static void visibility(View view, int visibilty) {
        if (view == null) return;
        if (view.getVisibility() == visibilty) {
            view.setVisibility(visibilty);
        }
    }

    private static final String key = "0615fdbe850a4467";
    private static final String initVector = "1111111111111111";

    public static String aesEncryptString(String content) {
        try {
            byte[] contentBytes = content.getBytes("UTF-8");

            byte[] encryptedBytes = aesEncryptBytes(contentBytes);
            String hash = Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
            return hash;
        } catch (Exception e) {
            Log.e("execption in enc", e.toString());
        }
        return null;
    }

    public static String aesDecryptString(String content) {
//		Decoder decoder = Base64.getDecoder();
        // encodedString.replace('+', '-').replace('/', '_')
        try {
//            content = content.split("==")[0];
            content = content.split("<br>")[0];
//            Log.e("cutted?", content);
            byte[] encryptedBytes = Base64.decode(content, Base64.DEFAULT);

            byte[] decryptedBytes = aesDecryptBytes(encryptedBytes);
            return new String(decryptedBytes, "UTF-8");
        } catch (Exception e) {
            Log.e("execption in dec", e.toString());
        }
        return null;
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static byte[] aesEncryptBytes(byte[] contentBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        return cipherOperation(contentBytes, Cipher.ENCRYPT_MODE);
    }

    public static byte[] aesDecryptBytes(byte[] contentBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        return cipherOperation(contentBytes, Cipher.DECRYPT_MODE);
    }

    private static byte[] cipherOperation(byte[] contentBytes, int mode) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");//can chg here

        byte[] initParam = initVector.getBytes("UTF-8");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, secretKey, ivParameterSpec);

        return cipher.doFinal(contentBytes);
    }


//    public static String encrypt(String value) {
//        //wasn't working on andori 8 or below
//        try {//goood
//            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
//            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
//
//            byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
//
//            return Base64.encodeBase64String(encrypted);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    public static String decrypt(String encrypted) {
//        //wasn't working on andori 8 or below
//        try {//good
//            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
//            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
//            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
//
//            return new String(original);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        return null;
//    }

    public static String getFormatedString(String s) {
        if (s != null) {
            return String.format("%,d", Long.parseLong(s));
        } else {
            return null;
        }


    }


}


