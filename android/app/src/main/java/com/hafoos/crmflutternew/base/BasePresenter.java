package com.hafoos.crmflutternew.base;


import com.hafoos.crmflutternew.utill.Constants;

public class BasePresenter<V extends BaseContractView> implements BaseContractPresenter<V> {

    private V baseView;

    protected V getView() {
        return baseView;
    }


    @Override
    public void onAttach(V baseView) {
        this.baseView = baseView;
    }

    @Override
    public void onDetach() {
        baseView = null;
    }



    protected void startLoading() {
        if (getView() == null) return;
        getView().showProgressBar();
        getView().hideConnectionLostView();
    }

    protected void loadingSuccess() {
        if (getView() == null) return;
        getView().hideProgressBar();
        getView().hideConnectionLostView();
    }

    protected void loadingFailed(int code, String message) {
        loadingFailed(code, message, true, true);
    }

    protected void loadingFailed(int code, String message, boolean isShowDialog, boolean isConnectionView) {
        if (getView() == null) return;
        getView().hideProgressBar();
        if (code == Constants.FAIL_INTERNET_CODE) {
            if (isConnectionView)
                getView().showConnectionLostView();
            if (isShowDialog)
                getView().showError(code, message);
        }else{
            getView().showError(code, message);
        }
    }

    @Override
    public void retry() {
    }
}
