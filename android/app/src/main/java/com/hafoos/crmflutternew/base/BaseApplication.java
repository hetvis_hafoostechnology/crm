package com.hafoos.crmflutternew.base;

import android.content.Context;
import android.location.Location;

import androidx.multidex.MultiDexApplication;

import com.hafoos.crmflutternew.data.AppApiHelper;
import com.hafoos.crmflutternew.data.AppDataManager;
import com.hafoos.crmflutternew.data.DataManager;
import com.hafoos.crmflutternew.data.pref.AppPreferencesHelper;


public class BaseApplication extends MultiDexApplication {

    public static Context appContext;

    private static DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        dataManager = new AppDataManager(this, new AppApiHelper(), new AppPreferencesHelper(this));


    }

    public static DataManager getDataManager() {
        return dataManager;
    }

}
