package com.hafoos.crmflutternew.data.rest;

public class RestConst {
    public enum RequestMethod {
        METHOD_GET,
        METHOD_POST;
    }

    public enum ContentType {
        CONTENT_JSON,
        CONTENT_FORMDATA,
        CONTENT_MULTIPART;
    }

    public enum ResponseCode {
        SUCCESS,
        ERROR,
        CANCEL,
        FAIL;
    }

    public enum ResponseType {
        RES_TYPE_JSON,
        RES_TYPE_IMAGE;
    }
}
