package com.hafoos.crmflutternew.data;


import com.hafoos.crmflutternew.data.pref.PreferencesHelper;

public interface DataManager extends ApiHelper, PreferencesHelper {
    interface Callback<V> {
        void onSuccess(V baseRes);

        void onFailed(int code, String msg);
    }
}
