package com.hafoos.crmflutternew;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "latlongManager";
    private static final String TABLE_Latlong = "Latlong";
    private static final String KEY_ID = "id";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LONG = "long";
    private static final String KEY_TIME = "time";

    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_Latlong + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_LAT + " TEXT," + KEY_LONG + " TEXT,"
                + KEY_TIME + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Latlong);

        // Create tables again
        onCreate(db);
    }

    // code to add the new contact
    void addLatlong(Latlong latlong) {
//        SQLiteDatabase db = this.getWritableDatabase();
        Log.e("Added", "value");
        Log.e("?", latlong.get_lat());

//        ContentValues values = new ContentValues();
        // Latlong Name
        latlong.set_lat(latlong.get_lat()); // Latlong Name
        latlong.set_long(latlong.get_long()); // Latlong Name
        latlong.set_time(latlong.get_time()); // Latlong Phone

        // Inserting Row
//        db.insert(TABLE_Latlong, null, values);
        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
    }

    // code to get the single contact
    Latlong getLatlong(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_Latlong, new String[]{KEY_ID,
                        KEY_LAT, KEY_LONG, KEY_TIME}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Latlong contact = new Latlong(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        // return contact
        return contact;
    }

    // code to get all contacts in a list view
    public String getAllLATlong() {
//        List<Latlong> contactList = new ArrayList<Latlong>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_Latlong;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//
//        if (cursor != null) {
//
//            while (cursor.moveToNext()) {
//                Latlong latlong = new Latlong();
//                latlong.set_id(Integer.parseInt(cursor.getString(0)));
//                latlong.set_lat(cursor.getString(1));
//                latlong.set_long(cursor.getString(2));
//                latlong.set_time(cursor.getString(3));
//
//                // Adding contact to list
//                contactList.add(latlong);
//            }
//        }
        Latlong latlong = new Latlong();
        String loglist = "lat: " + latlong.get_lat() + "time: " + latlong.get_time() + "lng: " + latlong.get_long();
        Log.e("in get mthod", loglist);
        // return contact list
        return loglist;
    }

//    // code to update the single contact
//    public int updateContact(Latlong Latlong) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, Latlong.getName());
//        values.put(KEY_PH_NO, Latlong.getPhoneNumber());
//
//        // updating row
//        return db.update(TABLE_Latlong, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.getID()) });
//    }

//    // Deleting single contact
//    public void deleteContact(Latlong latlong) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_Latlong, KEY_ID + " = ?",
//                new String[] { String.valueOf(latlong.getID()) });
//        db.close();
//    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_Latlong;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
