import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:crmflutternew/main.dart';

void main() {
  testWidgets('test widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    await tester.pump(Duration(seconds: 3));

    // Verify that our counter starts at 0.
    expect(find.text('hafooz'), findsOneWidget);



  });
}
