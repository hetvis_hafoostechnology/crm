import 'package:flutter/services.dart';

class FlutterPlugin {
  static const MethodChannel _channel = const MethodChannel('flutter_plugin');

  static Future<List> get platformVersion async {
    final List result = await _channel.invokeMethod('plugin');
    return result;
  }
}

class Contact {
  Contact({this.fullName, this.phoneNumber});

  factory Contact.fromMap(Map<dynamic, dynamic> map) =>
      new Contact(fullName: map['fullName'], phoneNumber: map['phoneNumber']);

  /// The full name of the contact, e.g. "Dr. Daniel Higgens Jr.".
  final String fullName;

  /// The phone number of the contact.
  final String phoneNumber;

  @override
  String toString() => '$fullName: $phoneNumber';
}
