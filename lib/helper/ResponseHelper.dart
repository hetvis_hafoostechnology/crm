import 'package:crmflutternew/model/ChangePwd.dart';
import 'package:crmflutternew/model/ForgotPwd.dart';
import 'package:crmflutternew/model/latlongByAddress/GetNearestLatlongModel.dart';
import 'package:crmflutternew/model/User.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class ResponseHelper {
  static User fromjson_user(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if(data==null){
      return null;
    }else {
      print(data["user"][AppUtils.KEY_firstName]);
      String firstname = data["user"][AppUtils.KEY_firstName];
      String id = data["user"][AppUtils.KEY_id];
      String lastname = data["user"][AppUtils.KEY_lastName];
      String email = data["user"][AppUtils.KEY_Email];
      String mobileNo = data["user"][AppUtils.KEY_Mobile];
      String telephoneNo = data["user"][AppUtils.KEY_telephone];
      String updatedBy_user = data["user"][AppUtils.KEY_updatedBy]["user"];
      String updatedBy_time = data["user"][AppUtils.KEY_updatedBy]["time"];
      String aclPolicy = data["user"]["aclPolicy"][0];
      String token = data["token"];

      return new User(
          id,
          firstname,
          lastname,
          email,
          updatedBy_user,
          aclPolicy,
          token);
    }
    }

  static ForgotPwd fromjson_forgot(Map<String, dynamic> json) {
    String message = json["message"];

    return new ForgotPwd(message);
  }
  static ChangePwd fromjson_changepwd(Map<String, dynamic> json) {
    String message = json["message"];

    return new ChangePwd(message);
  }

}
