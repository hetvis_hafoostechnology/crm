import 'package:crmflutternew/dashboard/map/CustomMarker.dart';
import 'package:crmflutternew/model/ChangePwd.dart';
import 'package:crmflutternew/model/CheckIn.dart';
import 'package:crmflutternew/model/ForgotPwd.dart';
import 'package:crmflutternew/model/latlongByAddress/GetNearestLatlongModel.dart';
import 'package:crmflutternew/model/LeadList.dart';
import 'package:crmflutternew/model/User.dart';
import 'package:crmflutternew/helper/ApiHelper.dart';
import 'package:crmflutternew/helper/PrefHelper.dart';
import 'package:fimber/fimber_base.dart';
import 'dart:convert';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/model/account/AccountDetailModel.dart';
import 'package:crmflutternew/model/account/AccountListModel.dart';
import 'package:dio/dio.dart';

class DataManager with ApiHelper, PrefHelper {
  static DataManager _dataManager;

  static DataManager get instance {
    if (_dataManager == null) {
      _dataManager = DataManager._internal();
    }
    return _dataManager;
  }

  var _logs = FimberLog("DataManger");

  DataManager._internal();

  Future initSelf() async {
    await init();
  }

  Future<User> callApiLogin(String email,String mobile, String password, String loginType) async {
print("call api");
    User user = await login(email,mobile,password,loginType);
    return user;
  }
  Future<AccountListModel> listapi(String userid,List sourceApi,String sortingType,String sortingOrder,int pageCount,String token  ) async {
    sourceApi=[];
    var resBody = {};
    resBody[AppUtils.KEY_userId] = userid;
    resBody[AppUtils.KEY_typeIds] = sourceApi;
    resBody[AppUtils.KEY_sort] = {AppUtils.KEY_field: sortingType, AppUtils.KEY_order: sortingOrder};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount, AppUtils.KEY_limit: 15};
    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.AccountListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return AccountListModel.fromjson(response.data);
  }

  Future<ForgotPwd> callApiForgotPassword(String email) async {
    ForgotPwd fp = await forgotPwd(email);
    return fp;
  }
  Future<ChangePwd> callApiChangePassword(String email) async {
    ChangePwd cp = await changePwd(email);
    return cp;
  }

  Future<LeadList> leadListApiCall() async {
    LeadList leadList = await leadListApi();
    return leadList;
  }

//  Future<LeadList> checkinApiCall() async {
//    CheckIn checkIn = await checkinApiCall();
//    return checkIn;
//  }


//  Future<List<Data>> nearestLatlongApiCall(String latitude,String longitude) async {
//    Data getNearestLatlongModel= (await getNearestLatlong(latitude,longitude)) as Data;
//    _logs.e(getNearestLatlongModel.toString());
//
//  }
}
