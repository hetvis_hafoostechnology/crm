import 'package:shared_preferences/shared_preferences.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class PrefHelper {
    SharedPreferences pref;

   Future<void> init() async {
    pref = await SharedPreferences.getInstance();
  }
  static const USER_EMAIL="email";
  static const USER_PASSWORD="password";
  static const USER_ID="userId";
  static const USER_FIRST_NAME="firstName";
  static const USER_LAST_NAME="lastName";


  Future<void> setInt(String key, int value) async {
    await pref.setInt(key, value);
  }

  Future<void> setString(String key, String value) async {
    await pref.setString(key, value);
  }

  Future<void> setBool(String key, bool value) async {
    await pref.setBool(key, value);
  }
  int getInt(String key,{int defaultValue}) {
    var result= pref.getInt(key);
    if(result==null && defaultValue!=null){
      return defaultValue;
    }else
      return result;
  }

  String getString(String key,{String defaultValue}) {
    var result= pref.getString(key);
    if(result==null && defaultValue!=null){
      return defaultValue;
    }else
      return result;
  }

  bool getBool(String key,{bool defaultValue}) {
    var result= pref.getBool(key);
    if(result==null && defaultValue!=null){
      return defaultValue;
    }else
      return result;
  }
 Future<bool> clear() async{
    return await pref.clear();
  }
}
