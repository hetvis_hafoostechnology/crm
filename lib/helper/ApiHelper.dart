import 'dart:convert';
import 'package:crmflutternew/model/ChangePwd.dart';
import 'package:crmflutternew/model/latlongByAddress/Data.dart';
import 'package:crmflutternew/model/ForgotPwd.dart';
import 'package:crmflutternew/model/latlongByAddress/GetNearestLatlongModel.dart';
import 'package:crmflutternew/model/LeadList.dart';
import 'package:crmflutternew/model/User.dart';
import 'package:crmflutternew/helper/ResponseHelper.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:dio/dio.dart';
import 'package:fimber/fimber.dart';

class ApiHelper {
  var _log = FimberLog("ApiHelper");

  Future<User> login(
      String email, String mobile, String password, String loginType) async {
    //  "https://zssskg8wh4.execute-api.us-east-1.amazonaws.com/dev/api/<-base url changed to https://dev-api.hafooz.com
    //  https://zssskg8wh4.execute-api.us-east-1.amazonaws.com/dev/api/login"

    Response response = await Dio().post(
     AppUtils.UserLoginUrl,
      data: {
        AppUtils.KEY_Email: email,
        AppUtils.KEY_Mobile: mobile,
        AppUtils.KEY_Password: password,
        AppUtils.KEY_LoginType: loginType
      },
      options: Options(headers: {AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent}),
    );

    print(response.data);
    return ResponseHelper.fromjson_user(response.data);
  }

//  Future<GetNearestLatlongModel> getNearestLatlong() async {
//    Response response = await Dio().post(
//      "https://zssskg8wh4.execute-api.us-east-1.amazonaws.com/dev/api/account/getNearestAccByCurAddress",
//      data: {"latitude": 22.4106642, "longitude": 73.12020810000001},
//      options: Options(headers: {AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent}),
//    );
//    print("latlongResponse how many times:$response");
//
//    return GetNearestLatlongModel.fromJsonArray(response.data["data"]);
//  }

  Future<ForgotPwd> forgotPwd(String email) async {
    Response response = await Dio().post(
      AppUtils.ForgotChangePwdUrl,
      data: {AppUtils.KEY_Email: '$email'},
      options: Options(headers: {AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent}),
    );
    print(response);

    return ResponseHelper.fromjson_forgot(response.data);
  }

  Future<ChangePwd> changePwd(String email) async {
    Response response = await Dio().post(
      AppUtils.ForgotChangePwdUrl,
      data: {AppUtils.KEY_Email: '$email'},
      options: Options(headers: {AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent}),
    );

    return ResponseHelper.fromjson_changepwd(response.data);
  }

  Future<LeadList> leadListApi() async {
    Response response = await Dio().post(
      AppUtils.LeadListUrl,
      data: {
        "userId": "",
        "sourceIds": "",
        "stageIds": "",
        "entityTypes": "",
        AppUtils.KEY_sort: {AppUtils.KEY_field: "updatedBy.time", AppUtils.KEY_order: "desc"},
        AppUtils.KEY_pagination: {AppUtils.KEY_page: 1, AppUtils.KEY_limit: 25, "total": 0}
      },
      options: Options(headers: {AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent}),
    );
    print("lead$response");

    return LeadList.fromjson_leadlist(response.data);
  }
}
