import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Constants {
  static const Color COLOR_LOGIN_UPPER_BG = Color(0xff1D64A0);
  static const Color COLOR_LOGIN_LOWER_BG = Color(0xffF2F7F8);
  static const Color COLOR_LOGIN_LOWER_BG2 = Color(0xffDCF4FB);
  static const Color COLOR_CARD_WHITE_BG = Color(0xffFFFFFF);
  static const Color COLOR_CARD_BUTTON = Color(0xfF4ABAE5);
  static const Color COLOR_CARD_FP = Color(0xfFAEE3FA);
  static const Color COLOR_CARD_SIGNIN_TITLE = COLOR_CARD_BUTTON;
  static const Color COLOR_TEXT = COLOR_CARD_WHITE_BG;
  static const Color COLOR_APP_SEMI_TRANSPARENT = Color.fromRGBO(0, 0, 0, 0.5);

//  static const Color COLOR_APP_SEMI_TRANSPARENT = Color.fromRGBO(0, 0, 0, 0.5);

  static const SOMETHING_WENT_WRONG = "something went wrong";
  static const FAIL_CODE = 0;
  static const FAIL_INTERNET_CODE = 4;

  static String pref_email = "pref_email";
  static String pref_token = "pref_token";
  static String pref_fname = "pref_fname";
  static String pref_lname = "pref_lname";
  static String pref_image = "pref_image";
  static String pref_role = "pref_role";
  static String pref_licensetype = "pref_licensetype";
  static String pref_id = "pref_id";
  static String pref_mob = "pref_mob";

  //master-list/getList
//to fetch data u have to pass that id to typeId ex
//body
//{"typeId":""
//}
//now pass id of like source find source id from below i.e ="5e47f734a86694d93ec1e8d1"
//
//  export enum MLTGeneral {
//  ContactTitle = '5e47f2f4bbe58ac3696832e9',
//  Salutation = '5e47f339bd6d9666735dcc3f',
//  Department = '5e47f343e41c8428d23654c0',
//}
//
//export enum MLTUser {
//  Role = '5e47f561b77c55184a1089ec',
//License = '5e47f56cc1c7c745dfe594c7',
//}
//
//export enum MLTActivity {
//  Priority = '5e47f661059ef31bf6c80a77',
//Status = '5e47f669e96cdf497e419ad6',
//CallType = '5e47f670bb1db548594bc83c',
//Recurring = '5e47f675fa6d9b57a57ba942',
//TaskType = '5e47f67c781200a95780e3e8', // TODO - check it is use
//EventType = '5e47f3e8d24df75cdb68d003'
//}
//
//export enum MLTLead {
//  Source = '5e47f734a86694d93ec1e8d1',
//Stage = '5e47f73ab7197e5c279285cf',
//}
//
//export enum MLTAccount {
//  Segment = '5e47f6f284da3ad00920e909',
//EntityType = '5e47f6f9b3cdeec35d723499',
//AnnualRevenue = '5e47f7000f34719b1fabb898',
//Industry = '5e47f70658369e649cc15923',
//Type = '5e47f70c5236ab544529fa37',
//}
//
//export enum MLTOpportunity {
//  Stage = '5e47f629429392eafb83d429'
//}
//
//export enum MLTProduct {
//  Category = '5e47f41995cad4f38f5d3697',
//PriceListType = '5e47f421aff8885b2cf9263e',
//}
//
//export enum MLTIssue {
//  Type = '5e47f809c60f2de23598ac45',
//Category = '5e47f81000f4abf751fe0b43',
//Status = '5e47f817286c2aa5d3c14a19',
//ResolutionCategory = '5e47f8202d2f5b5beeb249a5',
//Priority = '5e4803c495a7b6f11467a65a',
//}
//
//export enum MLTDocument {
//  Type = '5e47f3bec7d3303934e0b7a1',
//}
//
//export enum MLTCampaign {
//  Type = '5e47f39bcfffec79610faf5b',
//}
  static String formatDuration(Duration duration) {
    // return duration.toString().split('.').first.padLeft(8, '0');
    String timeAgo;
    if (duration.inSeconds <60) {
      print("here ${duration.inSeconds}");
      timeAgo = duration.inSeconds.toString() + "s";
    } else if (duration.inSeconds >= 60 && duration.inHours < 1) {
      print("here ${duration.inMinutes}");
      timeAgo = duration.inMinutes.toString() + "m";
    } else if (duration.inHours > 24 && duration.inDays < 7) {
      print("here ${duration.inDays}");
      timeAgo = duration.inDays.toString() + "d";
    } else if (duration.inDays >= 7 && duration.inDays < 30) {
      print("here1 ${duration.inDays}");
      int weeks = ((duration.inDays) / 7).toInt();

      timeAgo = weeks.toString() + "w";
    } else if (duration.inDays >= 30) {
      print("here2 ${duration.inDays}");
      int mnt = ((duration.inDays) / 30).toInt();

      timeAgo = mnt.toString() + "mnt";
    } else {
      print("here3 ${duration.inHours}");
      timeAgo = duration.inHours.toString() + "h";
    }
    return timeAgo;
  }

  static changeDateFromate(String input) {
    var strToDateTime = DateTime.parse(input);
    final convertLocal = strToDateTime.toLocal();
    var newFormat = DateFormat("MM/dd/yyyy,hh:mm aaa");
    String updatedDt = newFormat.format(convertLocal);
    return updatedDt;
  }

  static changeDateFromateAndGetDiffrence(String input) {
    //for notification

    // 2021-03-10T04:39:06.000Z
    var strToDateTime = DateTime.parse(input);
    final convertLocal = strToDateTime.toLocal();
    var newFormat = DateFormat("MM/dd/yyyy,hh:mm aaa");

    var now = DateTime.now();

    String updatedDt = newFormat.format(convertLocal);
    final dateTime = newFormat.parse(updatedDt);
    print("dates $updatedDt");
    var difference = now.difference(dateTime);
    return formatDuration(difference);
  }

  static fetchTimeonly(String input) {
    var strToDateTime = DateTime.parse(input);
    final convertLocal = strToDateTime.toLocal();
    var newFormat = DateFormat("hh:mm aaa");
    String updatedDt = newFormat.format(convertLocal);
    return updatedDt;
  }
}
