import 'package:crmflutternew/util/CustomAlertDialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/login/LoginScreen.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:crmflutternew/util/DioException.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:url_launcher/url_launcher.dart';

class AppUtils {
//  String BASE_URL="https://zssskg8wh4.execute-api.us-east-1.amazonaws.com/dev/api/";
  static String BASE_URL = "https://dev-api.hafooz.com/";
//https://dev-api.hafooz.com/notification/markAsRead
  //{"_id":"60484d6ac0148c00084a4bda"}
  //{"flag":1,"message":"Notification marked as read","data":null}
  //https://dev-api.hafooz.com/notification/getUnreadCount
  //{"flag":1,"message":null,"data":0}
  static String AccountListUrl = BASE_URL + "account/list";
  static String AccountSaveUrl = BASE_URL + "account/save";
  static String AccountGetDetailsUrl = BASE_URL + "account/getDetails";
  static String AccountDeleteUrl = BASE_URL + "account/delete";

  static String NotificationCounterUrl = BASE_URL + "notification/getUnreadCount";

  static String VisitListUrl = BASE_URL + "visit/list";
  static String GetVisitByIdtUrl = BASE_URL + "visit/getById";
  static String visitHistoryBYIdtUrl = BASE_URL + "visit/viewHistory";
  static String TrackVisitUrl = BASE_URL + "visit/track/list";
  static String VisitSaveUrl = BASE_URL + "visit/save";
  static String GetNearestAddUrl = BASE_URL + "account/getNearestAccByCurAddress";
  static String UnfinishedVisitUrl = BASE_URL + "visit/getUnfinishedVisitByUserId";
  static String VisitHistoryUrl = BASE_URL + "visit/viewHistory";

  static String SaveUserUrl = BASE_URL + "user/save";
  static String UserLoginProfileUrl = BASE_URL + "user/getLoginUserProfile";
  static String UserDropDownUrl = BASE_URL + "user/dropdown";
  static String UserLoginUrl = BASE_URL + "user/login";
  static String ChangeProfileImg = BASE_URL + "user/changeProfilePicture";
  static String ForgotChangePwdUrl = BASE_URL + "user/sendSetPasswordRequest";

  static String ActContactUrl = BASE_URL + "activity/getContactItem";
  static String ActivityAddUrl = BASE_URL + "activity/add";

  static String LeadGetDetailsUrl = BASE_URL + "lead/getDetails";
  static String LeadSaveUrl = BASE_URL + "lead/save";
  static String LeadDeleteUrl = BASE_URL + "lead/delete";
  static String LeadListUrl = BASE_URL + "lead/list";

  static String MasterGetListsUrl = BASE_URL + "master-list/getList";
  static String TagDropDownUrl = BASE_URL + "tag/dropdown";

  static String NoteListUrl = BASE_URL + "note/list";
  static String NoteAddUrl = BASE_URL + "note/add";

  static String DocumentListUrl = BASE_URL + "document/getList";
  static String DocumentSaveUrl = BASE_URL + "document/save";

  static String GeneralFileUploadUrl = BASE_URL + "general/file/getUploadURL";
  static String DeleteFileUploadUrl = BASE_URL + "general/file/delete";

  static String NotificationListUrl = BASE_URL + "notification/list";

  static String KEY_HeaderContent = "content-type";
  static String KEY_AccessToken = "x-access-token";
  static String Val_HeaderContent = "application/json";
  static String KEY_DATA = "data";
  static String KEY_Email = "email";
  static String KEY_Mobile = "mobile";
  static String KEY_Password = "password";
  static String KEY_LoginType = "loginType";
  static String KEY_id = "_id";
  static String KEY_visitId = "visitId";
  static String KEY_deviceId = "deviceId";
  static String KEY_platform = "platform";
  static String KEY_typeId = "typeId";
  static String KEY_name = "name";
  static String KEY_type = "type";
  static String KEY_segment = "segment";

  static String KEY_userId = "userId";
  static String KEY_industry = "industry";
  static String KEY_entityType = "entityType";
  static String KEY_international = "international";
  static String KEY_accountRep = "accountRep";
  static String KEY_tagIds = "tagIds";
  static String KEY_contact = "contact";
  static String KEY_website = "website";
  static String KEY_annRev = "annRev";
  static String KEY_typeIds = "typeIds";
  static String KEY_accountId = "accountId";
  static String KEY_location = "location";
  static String KEY_address = "address";
  static String KEY_addressLine1 = "addressLine1";
  static String KEY_addressLine2 = "addressLine2";
  static String KEY_addressLine3= "addressLine3";
  static String KEY_city= "city";
  static String KEY_zipCode= "zipCode";
  static String KEY_state= "state";
  static String KEY_country= "country";
  static String KEY_geoLocation= "geoLocation";
  static String KEY_coordinates= "coordinates";
  static String KEY_latitude= "latitude";
  static String KEY_longitude= "longitude";
  static String KEY_time= "time";
  static String KEY_isFinished= "isFinished";
  static String KEY_EventId= "eventId";
  static String KEY_updatedBy= "updatedBy";
  static String KEY_sourceIds= "sourceIds";
  static String KEY_stageIds= "stageIds";
  static String KEY_entityTypes= "entityTypes";
  static String KEY_salutation= "salutation";
  static String KEY_sort= "sort";
  static String KEY_field= "field";
  static String KEY_order= "order";
  static String KEY_pagination= "pagination";
  static String KEY_page= "page";
  static String KEY_limit= "limit";
  static String KEY_firstName= "firstName";
  static String KEY_lastName= "lastName";
  static String KEY_title= "title";
  static String KEY_ownerId= "ownerId";
  static String KEY_telephone= "telephone";
  static String KEY_source= "source";
  static String KEY_stage= "stage";



  static String SP_KEYLoginEmail = "Login_Email";
  static String SP_KEYLoginFname = "Login_FirstName";
  static String SP_KEYLoginLName = "Login_LastName";
  static String SP_KEYLoginID= "Login_Id";
  static String SP_KEYLoginTOKEN= "Login_token";

  static String FontName = 'Montserrat';
  static var FontSize = 14.0;
  static var FontSize16 = 16.0;
  static var FontSize15 = 15.0;
  static FontWeight Bold = FontWeight.w700;
  static FontWeight SemiBold = FontWeight.w600;
  static FontWeight ExtraBold = FontWeight.w800;
  static FontWeight Regular = FontWeight.w400;
  static FontWeight Thin = FontWeight.w500;
  static FontWeight Italic = FontWeight.w200;
  static FontWeight Light = FontWeight.w300;
  static FontWeight Black = FontWeight.w100;

  static bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  static bool isMobile(String mob) {
    String m = r'^(?:[+0]9)?[0-9]{10}$';

    RegExp regExp = new RegExp(m);

    return regExp.hasMatch(mob);
  }

  static showAlert(BuildContext context, String errormessage) {
    CustomAlertDialog.showAlert(
        context: context,
        title: "CRM!!!",
        message: errormessage,
        button1Text: "Ok");
  }

  static showToastWithMsg(String Msg, ToastGravity setGravity) {
    Fluttertoast.showToast(
        msg: Msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: setGravity,
        timeInSecForIos: 1,
        fontSize: 14.0);
  }

  static sessionExpiredLoginCall(BuildContext context, DioError Msg) async {
    final errorMessage = DioException.fromDioError(Msg).toString();
    print("err $errorMessage");
    AppUtils.showToastWithMsg(errorMessage, ToastGravity.BOTTOM);
    if (errorMessage == StringUtils.SessionExpired||errorMessage=="You are unauthorized to make the request.") {
      print("hre logouy");
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.clear();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => LoginScreen(),
          ),
          ModalRoute.withName("/LoginScreen"));
    }else{
      print("hre else");
    }
  }

  static Future<void> launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
