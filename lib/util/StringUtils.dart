class StringUtils {
  static String TitleAppName = "hafooz";
  static String TitleEmail = "Email";
  static String WOrkInProgress = "Kindly note that this Page is in Progress!!!";
  static String SessionExpired = "Session Expired!!!Please Login Again!";
  static String TitleMob = "Mobile";
  static String TitleSubject = "Subject";
  static String TitlePhone = "Phone";
  static String TitleSettings = "Settings";
  static String TitlePwd = "Password";
  static String TitleFname = "First Name";
  static String TitleLName = "Last Name";
  static String TitleName = "Name";
  static String TitleType = "Type";
  static String TitleSegment = "Segment";
  static String TitleOwner = "Owner";
  static String TitleAccRep = "Account Representative";
  static String TitleAnlRev = "Annual Revenue";
  static String TitleInternational = "Intenational";
  static String TitleIndustry = "Industry";
  static String TitleEntityType = "Entity Type";
  static String TitleDepartment= "Department";
  static String TitleTag= "Tag";
  static String TitleEventAdd = "Event Address";
  static String TitleAddress = "Address";
  static String TitleAddress1 = "Address Line 1";
  static String TitleAddress2 = "Address Line 2";
  static String TitleAddress3 = "Address Line 3";
  static String TitleCity = "City";
  static String TitleDetail = "Detail";
  static String Titlerelatedto = "Related To";
  static String TitleTime = "Time";
  static String TitleState = "State";
  static String TitleCountry = "Country";
  static String TitlePincode = "Pincode";
  static String TitleZipcode = "Zip code";
  static String TitleDocument = "Document";
  static String TitleDownload = "Download";
  static String TitlePermissionError = "Permissions error";


  static String spLeadId = "leadid_sp";

  static String TitleContact = "Contact";
  static String TitleLeadName = "Lead Name";
  static String TitleAssignedTo = "Assigned To";
  static String TitleSource = "Source";
  static String TitleLeadStage = "Lead Stage";
  static String TitleSalutaion = "Salutation";
  static String TitleWebsite = "Website";
  static String TitleTitle = "Title";
  static String TitleLocation = "Location";
  static String TitleDescription = "Description";
  static String TitleCreatedBy = "Created By";
  static String TitleUpdatedBy = "Updated By";


  static String BtnStartVisit = "Start Visit";
  static String BtnEndVisit = "End Visit";
  static String BtnSave = "SAVE";
  static String BtnCancel = "CANCEL";
  static String BtnYes = "YES";
  static String BtnNo = "NO";
  static String BtnLogin = "LOGIN";
  static String BtnSignin = "SIGNIN";
  static String BtnLogout = "SIGN OUT";
  static String BtnChangePwd = "CHANGE\nPASSWORD";
  static String BtnForgotPwd = "FORGOT PASSWORD";
  static String BtnAddMore = "Add More";

  static String EmailHint = "Enter Email";
  static String EmailMobHint = "Enter Email or Mobile";
  static String SubjectHint = "Enter Subject";
  static String LocationHint = "Enter Location";
  static String DescHint = "Enter Description";
  static String EventAddHint = "Enter Event Address";
  static String SelectAssignedHint = "Select user to Assigned";
  static String SelectContactHint = "Select Contact";
  static String PwdHint = "Enter Password";
  static String NameHint = "Enter Name";
  static String FirstNameHint = "Enter First Name";
  static String LastNameHint = "Enter Last Name";
  static String MobHint = "Enter Mobile Number";
  static String AddressHint = "Enter Addresss";
  static String EmailValidHint = "Enter Valid Email";
  static String MobValidHint = "Enter Valid MobileNumber";
  static String SelectSalutation = "Please Select Salutation";
  static String SelectTitle = "Please Select Title";
  static String SelectLstage = "Please Select Lead stage";
  static String SelectAnlRev = "Please Select Annual Revenue";
  static String LeadDeleteSuccess = "Lead Deleted Sucessfully";
  static String LeadSaveSuccess = "Lead saved Sucessfully";
  static String AccountDeleteSuccess = "Account Deleted Successfully";
  static String AccountSaveSuccess = "Account saved Sucessfully";
  static String CheckInSuccess = "You have Checkedin successfully";
  static String CheckInFail = "Fail to checkin";
  static String NoteSaveSuccess = "Note saved Successfully";
  static String VisitStartSuccess = "Visit started Successfully";
  static String VisitStopSuccess = "Visit stopped Successfully";

  static String SelectSource = "Please Select Source";
  static String SelectSegment = "Please Select Segment";
  static String SelectInternational = "Select Yes of No in International";
  static String SelectEntityType = "Please Select Enitity Type";
  static String SelectType = "Please Select Type";
  static String SelectIndustry = "Please Select Industry";
  static String SelectDepartment = "Please Select Department";


  static String TitleMyLead = "My Lead";
  static String TitleAllLead = "All Lead";
  static String TitleMyAcc = "My Account";
  static String TitleAllAcc = "All Account";


  static String TxtUserProfile = "User Profile";

  static String TxtHafooz = "hafooz";
  static String TxtUser = "User";
  static String TxtDashBoard = "DASHBOARD";
  static String TxtVisit = "VISIT";
  static String TxtLead = "LEAD";
  static String TxtAccount = "ACCOUNT";
  static String TxtNotification = "NOTIFICATION";
  static String TxtOption = "Select Options";
  static String TxtCamera = "Camera";
  static String TxtGallery = "Gallery";
  static String TxtEdit = "Edit";
  static String TxtDelete = "Delete";
  static String TxtCheckIn = "CheckIn";
  static String TxtStartVisit = "Please Start visit first";

  static String TxtSurityAccountDlt = "Delete";
  static String TxtError = "Error";
  static String TxtWARM = "Warm";
  static String TxtHOT = "Hot";
  static String TxtCold = "Error";
  static String TxtOk = "Ok";
  static String TxtNoData = "No Data Found";
  static String TxtOpenSettings = "Open Settings";
  static String TxtSurityLeadDlt = "Are you sure want to delete this Lead!";
  static String platfromAndroid = "android";
  static String platfromIos = "ios";


  static String TxtLOcationenable = "Please enable Location access ALL THE TIME permission in system settings";

  static String Txtresetpwd ="Password link sent on your register email.";
  static String change_forgot_pwd_emailhint =
      "Email will be sent to your registerd email id to reset your password";

}
