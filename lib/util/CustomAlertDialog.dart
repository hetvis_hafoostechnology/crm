import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';


typedef VoidCallback = void Function();

class CustomAlertDialog {
  static showAlert({
    @required BuildContext context,
    String title,
    String message = "",
    Widget view,
    String button1Text,
    VoidCallback button1Click,
    String button2Text,
    VoidCallback button2Click,
    String button3Text,
    VoidCallback button3Click,
    bool isCancelable = true,
    bool isAutoDismiss = true,
  }) async {
    debugPrint("show dialog called");
    await showDialog(
        context: context,
        barrierDismissible: isCancelable,
        builder: (BuildContext context) {
          return view != null
              ? _getCustomAlertDialog(
                  context: context, view: view, isCancelable: isCancelable)
              : _getSimpleDialog(
                  context: context,
                  title: title,
                  message: message,
                  button1Text: button1Text,
                  button2Text: button2Text,
                  button3Text: button3Text,
                  button1Click: button1Click,
                  button2Click: button2Click,
                  button3Click: button3Click,
                  isAutoDismiss: isAutoDismiss,
                  isCancelable: isCancelable);
        });
  }

  static _getSimpleDialog(
      {@required BuildContext context,
      String title,
      String message,
      Widget view,
      String button1Text,
      VoidCallback button1Click,
      String button2Text,
      VoidCallback button2Click,
      String button3Text,
      VoidCallback button3Click,
      bool isAutoDismiss = true,
      bool isCancelable = true}) {
    List<Widget> buttons = List();
    if (button1Text != null && button1Text.isNotEmpty) {
      buttons.add(Expanded(
        child: CupertinoButton(
          padding: EdgeInsets.all(0),
          child: Text(
            button1Text,
//            style: Themes.getDialogButtonTextStyle(context),
          ),
          onPressed: () {
            if (isAutoDismiss) {
              Navigator.of(context).pop();
            }
            if (button1Click != null) button1Click();
          },
        ),
      ));
    }
    if (button2Text != null && button2Text.isNotEmpty) {
      buttons.add(Expanded(
        child: CupertinoButton(
          padding: EdgeInsets.all(0),
          child: Text(
            button2Text,
//            style: Themes.getDialogButtonTextStyle(context),
          ),
          onPressed: () {
            if (isAutoDismiss) {
              Navigator.of(context).pop();
            }
            if (button2Click != null) button2Click();
          },
        ),
      ));
    }
    List<Widget> actions;
    if (button3Text != null && button3Text.isNotEmpty) {
      actions = [
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: CupertinoButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    if (isAutoDismiss) {
                      Navigator.of(context).pop();
                    }
                    if (button3Click != null) button3Click();
                  },
                  child: Text(
                    button3Text,
//                    style: Themes.getDialogButtonTextStyle(context),
                  )),
            ),
            Row(
              children: buttons,
            )
          ],
        )
      ];
    } else {
      actions = [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: buttons,
          ),
        )
      ];
    }

    return WillPopScope(
      child: CupertinoAlertDialog(
        title: Text(title),
        actions: actions,
        content: Container(
//          constraints:              BoxConstraints(minHeight: 80, minWidth: double.maxFinite),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(message,
                  style: Theme.of(context)
                      .copyWith(brightness: Brightness.light)
                      .dialogTheme
                      .contentTextStyle),
            ),
          ),
        ),


      ),
      onWillPop: () async {
        return isCancelable;
      },
    );
  }

  static _getCustomAlertDialog(
      {@required BuildContext context, Widget view, bool isCancelable}) {
    return WillPopScope(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Center(
          child: CupertinoPopupSurface(
            child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: view),
          ),
        ),
      ),
      onWillPop: () async {
        return isCancelable;
      },
    );
  }

  static showPickerDialog({
    @required BuildContext context,
    Widget view,
  }) async {
    await showDialog(
        context: context,
        builder: (context) {
          return view;
        });
  }
}
