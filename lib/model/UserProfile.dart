import 'package:crmflutternew/util/AppUtils.dart';

class UserProfile {
  String id;
  String companyId;
  String firstName;
  String lastName;
  String email;
  String mobileNo;
  String telephoneNo;
  String roleId;

  // String licenseId;
  String aclPolicy;
  String updatedBy_user;
  String file;
  String role;
  String company;

  UserProfile(
      this.id,
      this.companyId,
      this.firstName,
      this.lastName,
      this.email,
      this.mobileNo,
      this.telephoneNo,
      this.roleId,
      this.aclPolicy,
      this.file,
      this.role);

  static UserProfile fromjson_profile(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    String id = data[AppUtils.KEY_id];
    String companyId = data["companyId"];
    String firstname = data[AppUtils.KEY_firstName];
    String lastname = data[AppUtils.KEY_lastName];
    String email = data[AppUtils.KEY_Email];
    String mobileNo = data[AppUtils.KEY_Mobile];
    String telephoneNo = data[AppUtils.KEY_telephone];
    String roleId = data["roleId"];
    // String licenseId = data["licenseId"];
    String aclPolicy = data["aclPolicy"][0];
    String file;
    if (data["file"]["url"] == null) {
      file = "";
    } else {
      file = data["file"]["url"];
    }

    String role = data["role"]["text"];
    // String license = data["license"]["text"];

    return new UserProfile(id, companyId, firstname, lastname, email, mobileNo,
        telephoneNo, roleId, aclPolicy, file, role);
  }
}
