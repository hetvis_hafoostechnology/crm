import 'package:crmflutternew/util/AppUtils.dart';
class Visit {
  String flag;
  String message;
  String data;

  Visit(this.message);

  static Visit fromJsonResponse(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if (data == null) {
      return null;
    } else {
      String id = data[AppUtils.KEY_id];
      return new Visit(id);
    }
  }
}
