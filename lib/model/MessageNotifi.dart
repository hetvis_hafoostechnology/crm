
import 'dart:async';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MessageNotifi{
  String title;
  String body;
  String itemid;
  String leadorAccount;

  MessageNotifi({this.title,this.body,this.itemid,this.leadorAccount});
}