import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class GetLeadDetails {
  String id;
  String entityType;
  String name; //
  String firstName;
  String lastName;
  String website;
  String email;
  String mobile;
  String title;
  String titleId;
  String telephone;
  String source;
  String sourceId;
  String stage;
  String stageId;
  String salutation;
  String salutationId;
  String updatedByName;
  String updatedByTime;
  bool isConvertedToOpportunity;
  bool isDeleted;
  String createdByName;
  String createdByTime;
  String owner;
  String ownerId;
  List tagids;
  Address address;

  GetLeadDetails(
      {this.id,
      this.entityType,
      this.name,
      this.firstName,
      this.lastName,
      this.website,
      this.email,
      this.mobile,
      this.title,
      this.telephone,
      this.source,
      this.stage,
      this.salutation,
      this.updatedByName,
      this.updatedByTime,
      this.isConvertedToOpportunity,
      this.isDeleted,
      this.createdByName,
      this.createdByTime,
      this.owner,
      this.address,
      this.ownerId,
      this.tagids,
      this.salutationId,
      this.stageId,
      this.titleId,
      this.sourceId});

  static GetLeadDetails fromJsonResponse(Map<String, dynamic> json) {
    Map<String, dynamic> item = json[AppUtils.KEY_DATA];
    if (json == null) {
      return null;
    } else {
      var result = new GetLeadDetails(
        id: item[AppUtils.KEY_id],
        entityType: item[AppUtils.KEY_entityType],
        firstName: item[AppUtils.KEY_firstName],
        lastName: item[AppUtils.KEY_lastName],
        email: item[AppUtils.KEY_Email],
        mobile: item[AppUtils.KEY_Mobile],
        title: item[AppUtils.KEY_title]["text"],
        source: item[AppUtils.KEY_source]["text"],
        stage: item[AppUtils.KEY_stage]["text"],
        salutation: item[AppUtils.KEY_salutation]["text"],
        updatedByName: item[AppUtils.KEY_updatedBy]["user"][AppUtils.KEY_firstName],
        updatedByTime: item[AppUtils.KEY_updatedBy]["time"],
        isConvertedToOpportunity: item["isConvertedToOpportunity"],
        isDeleted: item["isDeleted"],
        createdByName: item["createdBy"]["user"][AppUtils.KEY_firstName],
        createdByTime: item["createdBy"]["time"],
        salutationId: item[AppUtils.KEY_salutation][AppUtils.KEY_id],
        stageId: item[AppUtils.KEY_stage][AppUtils.KEY_id],
        titleId: item[AppUtils.KEY_title][AppUtils.KEY_id],
        sourceId: item[AppUtils.KEY_source][AppUtils.KEY_id],
      );
      if (item.containsKey(AppUtils.KEY_name)) {
        result.name = item[AppUtils.KEY_name];
      }
//additem["updatedBy"]["user"]["firstName"]+item["updatedBy"]["user"][AppUtils.KEY_lastName]
      print(result.name);
      if (item.containsKey("owner")) {
        result.owner = item["owner"][AppUtils.KEY_firstName] + item["owner"][AppUtils.KEY_lastName];
        result.ownerId = item["owner"][AppUtils.KEY_id];
      }
      if (item.containsKey(AppUtils.KEY_address)) {
        result.address = Address.fromMap(item[AppUtils.KEY_address]);
      }
      if (item.containsKey(AppUtils.KEY_website)) {
        result.website = item[AppUtils.KEY_website];
      }
      if (item.containsKey(AppUtils.KEY_telephone)) {
        result.telephone = item[AppUtils.KEY_telephone];
      }
      if (item.containsKey(AppUtils.KEY_tagIds)) {
        result.tagids = item[AppUtils.KEY_tagIds];
      }
      return result;
    }
  }
}
