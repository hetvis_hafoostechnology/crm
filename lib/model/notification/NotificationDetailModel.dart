import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class NotificationDetailModel {
  String id;
  String deviceId;
  String senderId;
  String receiverId;
  String messageTitle;
  String messageBody;
  String dataId;
  int dataAction;
  int status;
  String createdAt;

//export const enum NotificationStatus {
//     Failed = 0,
//     Pending = 1,
//     Delivered = 2,
//     Read = 3,
// }
// export const enum NotificationEvent {
//     LeadAssigned = 1,
//     AccountAssigned = 2,
//     OpportunityAssigned = 3
// }
//
// NotificationEvent use for action
  NotificationDetailModel({
    this.id,
    this.deviceId,
    this.senderId,
    this.receiverId,
    this.messageTitle,
    this.messageBody,
    this.dataId,
    this.dataAction,
    this.status,
    this.createdAt,
  });

  static NotificationDetailModel fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new NotificationDetailModel(
        id: item[AppUtils.KEY_id],
        deviceId: item["deviceId"],
        senderId: item["senderId"],
        receiverId: item["receiverId"],
        messageTitle: item["message"][AppUtils.KEY_title],
        messageBody: item["message"]["body"],
        dataId: item[AppUtils.KEY_DATA]["id"],
        dataAction: item[AppUtils.KEY_DATA]["action"],
        status: item["status"],
        createdAt: item["createdAt"],
      );

      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }


}
