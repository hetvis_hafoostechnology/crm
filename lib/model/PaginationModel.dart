import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class PaginationModel {
  String total;

  PaginationModel({
    this.total,
  });

  static PaginationModel fromJson(Map<String, dynamic> page) {
    Map<String, dynamic> data = page[AppUtils.KEY_DATA];
    try {
      var result = new PaginationModel(
        total: data["total"],
      );

      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
