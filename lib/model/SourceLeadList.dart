class SourceLeadList{

  String _id,type,text,createdAt,updatedAt,order,isDeleted;

  SourceLeadList(this._id, this.type, this.text, this.createdAt, this.updatedAt,
      this.order, this.isDeleted);
  @override
  String toString() {

    return 'SourceLeadList{type: $type, text: $text, createdAt: $createdAt, updatedAt: $updatedAt, order: $order, isDeleted: $isDeleted}';
  }


//"": "5bd697d026e543121d7bd7ba",
  //                    "": "lead_source",
  //                    "": "Web",
  //                    "": "2018-10-29T05:17:01.907Z",
  //                    "": "2018-11-24T23:53:46.322Z",
  //                    "": 3,
  //                    "createdBy": {
  //                        "user": "5ba0dc1abdae44ccaa27574a",
  //                        "time": "2018-11-29T10:31:25.160Z"
  //                    },
  //                    "": false,
  //                    AppUtils.KEY_updatedBy: {
  //                        "user": "5ba0dc1abdae44ccaa27574a",
  //                        "time": "2019-11-13T05:01:34.000Z"
  //                    }
}