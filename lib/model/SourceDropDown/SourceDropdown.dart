import 'package:crmflutternew/model/SourceDropDown/DataLeadSource.dart';

import 'package:crmflutternew/util/AppUtils.dart';
class SourceDropdown{
  List<DataLeadSource> datas;

  SourceDropdown(this.datas);

  static SourceDropdown fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DataLeadSource> result = List();
    try {
      List<dynamic> list = List.from(data["masterList"]);

      list.forEach((item) {
        result.add(DataLeadSource.fromJson(item));
      });
    } catch (e) {
      print(e);
    }
    return new SourceDropdown(result);
  }

}