class User {
  String id;
  String firstName;
  String lastName;
  String email;
  String mobileNo;
  String telephoneNo;
  String updatedBy_user;
  String aclPolicy;
  String token;

  User(this.id,this.firstName, this.lastName, this.email,this.updatedBy_user,this.aclPolicy,this.token);

  @override
  String toString() {
    return 'User{firstName: $firstName, lastName: $lastName, email: $email, updatedBy_user: $updatedBy_user,aclPolicy: $aclPolicy,token:$token}';
  }
}
