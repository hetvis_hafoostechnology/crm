import 'package:crmflutternew/util/AppUtils.dart';
class CheckIn {
  String flag;
  String message;
  String data;//id
//myZoomageView/myZoomageViewmyZoomageView "flag": 1,
//    "message": "Event saved successfully",
//    "data": null
  CheckIn(this.message,this.data);

  static CheckIn fromJson(Map<String, dynamic> json) {
    String message = json["message"];
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    String id = data[AppUtils.KEY_id];

    return new CheckIn(message,id);
  }
}
