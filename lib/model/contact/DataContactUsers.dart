import 'package:crmflutternew/util/AppUtils.dart';
class DataContactUsers {
  String id;
  String email;
  String mobile;
  String accountId;
  String name;


  DataContactUsers({this.id, this.email, this.mobile, this.accountId, this.name});

  static DataContactUsers fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataContactUsers(
        id: item[AppUtils.KEY_id],
        email: item[AppUtils.KEY_Email],
        mobile: item[AppUtils.KEY_Mobile],
        accountId: item[AppUtils.KEY_accountId],
        name: item[AppUtils.KEY_name],
      );
      return result;
    } catch (e) {
      print(e);
    }
  }

}
