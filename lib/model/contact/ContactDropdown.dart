
import 'package:crmflutternew/model/contact/DataContactUsers.dart';

class ContactDropdown{
  List<DataContactUsers> datas;

  ContactDropdown(this.datas);

  static ContactDropdown fromJsonArray(dynamic json) {

    List<DataContactUsers> result = List();
    try {
      List<dynamic> list = List.from(json);

      list.forEach((item) {
        result.add(DataContactUsers.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return ContactDropdown(result);

  }

}