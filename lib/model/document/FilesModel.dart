import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/model/document/FilesDetails.dart';
class FilesModel {
  List<FilesDetails> datas;

  FilesModel(this.datas);

  static FilesModel fromJsonArray(dynamic json) {
    List<FilesDetails> result = List();
    try {
      List<dynamic> list = List.from(json);
      list.forEach((item) {
        result.add(FilesDetails.fromjson(item));
      });
    } catch (e) {
      print(e);
    }

    return FilesModel(result);
  }
}
