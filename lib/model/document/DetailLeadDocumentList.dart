import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/model/document/FilesModel.dart';
class DetailLeadDocumentList {
  String id;
  String name;
  String typeId; //shows it from which lead item
  String updatedByUser;
  String createdByUser;
  String createdByTime;
  bool isDeleted;
  FilesModel files;

  DetailLeadDocumentList(
      {this.id,
      this.name,
      this.typeId,
      this.updatedByUser,
      this.createdByUser,
      this.createdByTime,
      this.files});

  static DetailLeadDocumentList fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DetailLeadDocumentList(
        id: item[AppUtils.KEY_id],
        name: item[AppUtils.KEY_name],
        typeId: item[AppUtils.KEY_typeId],
        updatedByUser: item[AppUtils.KEY_updatedBy]["user"],
        createdByUser: item["createdBy"]["user"],
        createdByTime: item["createdBy"]["time"],

      );
      //List<dynamic> list = List.from(json);
      //
      //       list.forEach((item) {
      //        result.add(Data.fromJson(item));
      //       });
      if (item.containsKey("files")) {
        result.files = FilesModel.fromJsonArray(item["files"]);
      }
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
