import 'package:crmflutternew/util/AppUtils.dart';

class FilesDetails {
  String fileName;
  String filePAth;
  String fileUrl;
  int fileSize;

  FilesDetails({this.fileName, this.filePAth, this.fileUrl,this.fileSize});

  static FilesDetails fromjson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new FilesDetails(
        fileName: item[AppUtils.KEY_name],
        filePAth: item["path"],
        fileSize: item["size"],
        fileUrl: item["url"],
      );
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
