import 'package:crmflutternew/util/AppUtils.dart';
class DataListLead {
  String name;
  String firstName;
  String lastName;
  String mobile;
  String email;
  String id;
  String entityType;
  String sourceText;
  String stageText;

  DataListLead(
      {this.name,
      this.firstName,
      this.lastName,
      this.mobile,
      this.email,
      this.id,
      this.entityType,
      this.sourceText,
      this.stageText});

  static DataListLead fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataListLead(
        id: item[AppUtils.KEY_id],
        mobile: item[AppUtils.KEY_Mobile],
        email: item[AppUtils.KEY_Email],
        entityType: item[AppUtils.KEY_entityType],
      );
      if (item.containsKey(AppUtils.KEY_name)) {
        result.name = item[AppUtils.KEY_name];
      } else {
        result.name = item[AppUtils.KEY_firstName] + " " + item[AppUtils.KEY_lastName];
      }
      if(item.containsKey(AppUtils.KEY_source)){
        result.sourceText=item[AppUtils.KEY_source]["text"];
      }else{
        result.sourceText="";
      }
      if(item.containsKey(AppUtils.KEY_stage)){
        result.stageText=item[AppUtils.KEY_stage]["text"];
      }else{
        result.stageText="";
      }

      print(result.stageText);
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
//  static DataSalutationUsers fromMap(item) {
//    if (item == null) {
//      return null;
//    }
//    var result =new DataSalutationUsers(
//      id:item[AppUtils.KEY_id],
//      type:item[AppUtils.KEY_type],
//      text:item["text"],
//    );
//
//    return result;
//  }
//
//  static List<DataSalutationUsers> fromJsonArray(dynamic json) {
//
//    List<DataSalutationUsers> result = List();
//    try {
//      List<dynamic> list = List.from(json);
//
//
//      list.forEach((item) {
//        result.add(DataSalutationUsers.fromMap(item));
//      });
//    } catch (e) {
//      print(e);
//    }
//
//    return result;
////
//
//  }
}
