import 'package:fimber/fimber_base.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/model/document/DetailLeadDocumentList.dart';

class LeadDocumentList{
  List<DetailLeadDocumentList> datas;
  int paginationModel;
  LeadDocumentList(this.datas,this.paginationModel);

 static LeadDocumentList fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DetailLeadDocumentList> result = List();
    int page = data[AppUtils.KEY_pagination]["total"];
    try {
      List<dynamic> list = List.from(data["list"]);

      list.forEach((item) {
        result.add(DetailLeadDocumentList.fromJson(item));
      });
      var res = new LeadDocumentList(result, page);
      print("in model res $res");
      return res;
    } catch (e) {
      print(e);
    }
    return null;
  }

}