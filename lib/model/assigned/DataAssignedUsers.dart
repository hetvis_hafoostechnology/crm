import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class DataAssignedUsers {
  String id;
  String firstname;
  String lastname;
  String name;


  DataAssignedUsers({this.id, this.firstname, this.lastname, this.name});

  static DataAssignedUsers fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataAssignedUsers(
        id: item[AppUtils.KEY_id],
        firstname: item[AppUtils.KEY_firstName],
        lastname: item[AppUtils.KEY_lastName],
        name: item[AppUtils.KEY_name],
      );
      // print("rs in dp$result");
      return result;
    } catch (e) {
      print("drop$e");
    }
  }

}
