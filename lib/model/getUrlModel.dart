import 'package:crmflutternew/util/AppUtils.dart';
class getUrlModel{

  String dataUrl;
  String filePath;


  getUrlModel(this.dataUrl,this.filePath);

  static getUrlModel fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if (data == null) {
      return null;
    } else {
      String dataUrl = data["uploadURL"];
      String filePath = data["fileInfo"]["path"];
      return new getUrlModel(dataUrl, filePath);
    }
  }


}