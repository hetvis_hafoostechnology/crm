import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class VisitLocationDetails {
  Address location;
  String time;
  String eventId;
  String accountName;

  VisitLocationDetails(
      {this.time, this.eventId, this.location, this.accountName});

  static VisitLocationDetails fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new VisitLocationDetails(
        eventId: item["eventId"],
        time: item["time"],
      );
      if (item.containsKey("location")) {
        result.location = Address.fromMap(item["location"]);
      }
      if (item.containsKey("accountName")) {
        result.accountName = item["accountName"];
      }
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
