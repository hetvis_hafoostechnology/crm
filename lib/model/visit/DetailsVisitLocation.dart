import 'package:crmflutternew/model/visit/VisitLocationDetails.dart';

class DetailsVisitLocation {
  List<VisitLocationDetails> datas;

//  String list;
//   int paginationModel;

  // NotificationListModel(this.datas, this.paginationModel);
  DetailsVisitLocation(this.datas);

//
  static DetailsVisitLocation fromJsonArray(dynamic json) {
    List<VisitLocationDetails> result = List();
    try {
      List<dynamic> list = List.from(json);

      list.forEach((item) {
        result.add(VisitLocationDetails.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return DetailsVisitLocation(result);
  }

}