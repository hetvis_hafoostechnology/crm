import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/model/visit/DetailsVisitLocation.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class GetVisitByIdModel {
  String id;
  bool isFinished;
  String userId;

  DetailsVisitLocation details;

  GetVisitByIdModel({
    this.id,
    this.isFinished,
    this.userId,
    this.details,
  });

  static GetVisitByIdModel fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> item = json[AppUtils.KEY_DATA];
    if (json == null) {
      return null;
    }
    try {
      var result = new GetVisitByIdModel(
        id: item[AppUtils.KEY_id],
        isFinished: item["isFinished"],
        userId: item["userId"],

      );
      if (item.containsKey("details")) {
        result.details = DetailsVisitLocation.fromJsonArray(item["details"]);
      }

      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
