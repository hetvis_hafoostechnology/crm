import 'package:crmflutternew/model/visit/visitHistory/HistoryVisitLocationDetails.dart';

class DetailsHistoryVisit{
  List<HistoryVisitLocationDetails> datas;

//  String list;
//   int paginationModel;

  // NotificationListModel(this.datas, this.paginationModel);
  DetailsHistoryVisit(this.datas);

//
  static DetailsHistoryVisit fromJsonArray(dynamic json) {
    List<HistoryVisitLocationDetails> result = List();
    try {
      List<dynamic> list = List.from(json);

      list.forEach((item) {
        result.add(HistoryVisitLocationDetails.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return DetailsHistoryVisit(result);
  }

}