import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/model/visit/DetailsVisitLocation.dart';
import 'package:crmflutternew/model/visit/visitHistory/DetailsHistoryVisit.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class HistoryVisitByIdModel {
  String id;
  bool isFinished;
  String userId;

  DetailsHistoryVisit details;

  HistoryVisitByIdModel({
    this.id,
    this.isFinished,
    this.userId,
    this.details,
  });

  static HistoryVisitByIdModel fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> item = json[AppUtils.KEY_DATA];
    if (json == null) {
      return null;
    }
    try {
      var result = new HistoryVisitByIdModel(
        id: item[AppUtils.KEY_id],
        isFinished: item["isFinished"],
        userId: item["userId"],

      );
      if (item.containsKey("details")) {
        result.details = DetailsHistoryVisit.fromJsonArray(item["details"]);
      }

      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
