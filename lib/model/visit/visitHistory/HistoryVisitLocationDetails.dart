import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class HistoryVisitLocationDetails {
  Address location;
  String time;
  String eventId;
  String accountName;



  HistoryVisitLocationDetails({
    this.time,
    this.eventId,
    this.location,
    this.accountName
  });

  static HistoryVisitLocationDetails fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new HistoryVisitLocationDetails(
        eventId: item["eventId"],
        time: item["time"],

      );
      if (item.containsKey("location")) {
        result.location = Address.fromMap(item["location"]);
      }if (item.containsKey("accountName")) {
        result.accountName = item["accountName"];
      }
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }


}