import 'package:crmflutternew/model/PaginationModel.dart';
import 'package:crmflutternew/model/visit/VisitDetailModel.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class VisitListModel {
  List<VisitDetailModel> datas;

  int paginationModel;

  VisitListModel(this.datas, this.paginationModel);

  static VisitListModel fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if (data == null) {
      return null;
    } else {
      int page = data[AppUtils.KEY_pagination]["total"];
      List<VisitDetailModel> result = List();
      try {
        List<dynamic> list = List.from(data["list"]);

        list.forEach((item) {
          result.add(VisitDetailModel.fromJson(item));
        });
        return new VisitListModel(result, page);
      } catch (e) {
        print(e);
      }
      return null;
    }
  }
}
