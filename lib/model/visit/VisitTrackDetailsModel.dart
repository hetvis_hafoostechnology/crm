import 'package:crmflutternew/util/AppUtils.dart';
class VisitTrackDetailsModel{
  String id;
  String visitId;
  String userId;
  String deviceId;
  String platform;
  String time;
  double locationGeoCoordinatesLat;
  double locationGeoCoordinatesLng;


  VisitTrackDetailsModel({this.id, this.visitId, this.userId, this.deviceId, this.platform, this.time, this.locationGeoCoordinatesLat, this.locationGeoCoordinatesLng});

  static VisitTrackDetailsModel fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new VisitTrackDetailsModel(
        id: item[AppUtils.KEY_id],
        visitId: item[AppUtils.KEY_visitId],
        userId: item[AppUtils.KEY_userId],
        deviceId: item[AppUtils.KEY_deviceId],
        platform: item[AppUtils.KEY_platform],
        time: item[AppUtils.KEY_time],
        locationGeoCoordinatesLat: item[AppUtils.KEY_location][AppUtils.KEY_geoLocation][AppUtils.KEY_coordinates][1],
        locationGeoCoordinatesLng: item[AppUtils.KEY_location][AppUtils.KEY_geoLocation][AppUtils.KEY_coordinates][0],

      );
      return result;
    } catch (e) {
      print("latlng$e");
    }
  }

}