import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/model/visit/DetailsVisitLocation.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class VisitDetailModel {
  String id;
  bool isFinished;
  String userId;
  String userName;
  DetailsVisitLocation details;

//export const enum NotificationStatus {
//     Failed = 0,
//     Pending = 1,
//     Delivered = 2,
//     Read = 3,
// }
// export const enum NotificationEvent {
//     LeadAssigned = 1,
//     AccountAssigned = 2,
//     OpportunityAssigned = 3
// }
//
// NotificationEvent use for action
  VisitDetailModel({
    this.id,
    this.isFinished,
    this.userId,
    this.userName,
    this.details,
  });

  static VisitDetailModel fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new VisitDetailModel(
        id: item[AppUtils.KEY_id],
        isFinished: item["isFinished"],
        userId: item["user"]["_id"],
        userName: item["user"]["name"],
      );
      if (item.containsKey("details")) {
        result.details = DetailsVisitLocation.fromJsonArray(item["details"]);
      }

      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
