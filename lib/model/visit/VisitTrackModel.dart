import 'package:crmflutternew/model/visit/VisitTrackDetailsModel.dart';
class VisitTrackModel{
  List<VisitTrackDetailsModel> datas;
  VisitTrackModel(this.datas);

  static VisitTrackModel fromJsonArray(dynamic json) {
    List<VisitTrackDetailsModel> result = List();
    try {
      List<dynamic> list = List.from(json);

      list.forEach((item) {
        result.add(VisitTrackDetailsModel.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return VisitTrackModel(result);
  }
}