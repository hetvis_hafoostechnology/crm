import 'package:crmflutternew/util/AppUtils.dart';
class GeoLatLong {
  double latitude;
  double longitude;

  GeoLatLong({this.latitude, this.longitude});

  static GeoLatLong fromMap(item) {
    if (item == null) {
      return null;
    }
    var result = new GeoLatLong(
      latitude: item[AppUtils.KEY_latitude],
      longitude: item[AppUtils.KEY_longitude],
    );
    return result;
  }
}
