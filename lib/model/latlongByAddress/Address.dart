import 'package:crmflutternew/model/latlongByAddress/GeoLatLong.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class Address {
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String city;
  String zipCode;
  String state;
  String country;
  GeoLatLong geoLatlong;
  String geoJSON_type;
  String geoJSON_coordinatesarray;

  Address(
      {this.addressLine1,
      this.addressLine2,
      this.addressLine3,
      this.city,
      this.zipCode,
      this.state,
      this.country,
      this.geoLatlong,
      this.geoJSON_type,
      this.geoJSON_coordinatesarray}); //  @override
//  String toString() {
//    return 'Address{addressLine1: $addressLine1, addressLine2: $addressLine2, addressLine3: $addressLine3, city: $city, zipCode: $zipCode, state: $state, country: $country}';
//  }

  static Address fromMap(item) {
    if (item == null) {
      return null;
    }
    var result = new Address(
      city: item[AppUtils.KEY_city],
      zipCode: item[AppUtils.KEY_zipCode],
      state: item[AppUtils.KEY_state],
      country: item[AppUtils.KEY_country],

//      city:item[AppUtils.KEY_city],
//      zipCode:item[AppUtils.KEY_zipCode],
//      state:item[AppUtils.KEY_state],
//      country:item[AppUtils.KEY_country],
    );

    if (item.containsKey(AppUtils.KEY_geoLocation)) {
      result.geoLatlong = GeoLatLong.fromMap(item[AppUtils.KEY_geoLocation]);
    }
    if (item.containsKey(AppUtils.KEY_addressLine1)) {
      result.addressLine1 = item[AppUtils.KEY_addressLine1];
    } else if (result.addressLine1 == null) {
      result.addressLine1 = "";
    } else {
      result.addressLine1 = "";
    }
    if (item.containsKey(AppUtils.KEY_addressLine2)) {
      result.addressLine2 = item[AppUtils.KEY_addressLine2];
    } else {
      result.addressLine2 = "";
    }
    if (item.containsKey(AppUtils.KEY_addressLine3)) {
      result.addressLine3 = item[AppUtils.KEY_addressLine3];
    } else {
      result.addressLine3 = "";
    }
    if (item.containsKey('_geoJSON')) {
      result.geoJSON_type = item["_geoJSON"][AppUtils.KEY_type];
    }
    return result;
  }

//                "geoLocation": {
//                    "latitude": 22.4036275,
//                    "longitude": 73.10840519999999
//                },
//                "_geoJSON": {
//                    AppUtils.KEY_type: "Point",
//                    "coordinates": [
//                        73.10840519999999,
//                        22.4036275
//                    ]
//                }
//            },

}
