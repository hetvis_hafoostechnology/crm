import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class Data {
  String id;
  String name;
  String website;
  String phone;
  String entityType;
  String type;
  String accountRep;
  Address address;
  String leadId;
  String updatedBy_user;
  String updatedBy_time;
  bool isDeleted;
  String createdBy_user;
  String createdBy_time;
  
  Data({this.id, this.name, this.website, this.phone, this.entityType, this.type,
      this.accountRep, this.address, this.leadId, this.updatedBy_user,
      this.updatedBy_time, this.isDeleted, this.createdBy_user,
      this.createdBy_time});

  static Data fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new Data(
        id: item[AppUtils.KEY_id],
        name: item[AppUtils.KEY_name],
        website: item[AppUtils.KEY_website],
        phone: item["phone"],
        entityType: item[AppUtils.KEY_entityType],
        type: item[AppUtils.KEY_type],
        accountRep: item[AppUtils.KEY_accountRep],
        leadId: item["leadId"],
        updatedBy_user: item[AppUtils.KEY_updatedBy]["user"],
        updatedBy_time: item[AppUtils.KEY_updatedBy]["time"],
        isDeleted: item["isDeleted"],
        createdBy_user: item["createdBy"]["user"],
        createdBy_time: item["createdBy"]["time"],
      );

      if (item.containsKey(AppUtils.KEY_address)) {
        result.address = Address.fromMap(item[AppUtils.KEY_address]);
      }

      return result;
    } catch (e) {
      print(e);
    }
  }
//  static Data fromjson_data(Map<String, dynamic> json) {
//    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
//
//    String data_id = data["data"][AppUtils.KEY_id];
//    return new Data(data_id);
//  }
}
