
import 'package:crmflutternew/model/contact/DataContactUsers.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'DataSalutationUsers.dart';

class SalutationDropdown{
  List<DataSalutationUsers> datas;

  SalutationDropdown(this.datas);

  static SalutationDropdown fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DataSalutationUsers> result = List();
    try {
      List<dynamic> list = List.from(data["masterList"]);

      list.forEach((item) {
        result.add(DataSalutationUsers.fromJson(item));
      });
    } catch (e) {
      print(e);
    }
    return new SalutationDropdown(result);
  }

}