import 'package:crmflutternew/util/AppUtils.dart';
class EndVisit{
  String flag;
  String message;
  String visit;


  EndVisit(this.message);


  static EndVisit fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    String visit = data["visit"];
    return new EndVisit(visit);
  }

}