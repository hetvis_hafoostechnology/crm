import 'package:crmflutternew/model/SourceLeadList.dart';

class LeadListData {
  String _id,
      entityType,
      firstName,
      lastName,
      email,
      mobile,
      title,
      telephone,
      isDeleted,
      salutation,
      isConvertedToOpportunity;

  LeadListData(this._id, this.entityType, this.firstName, this.lastName,
      this.email, this.mobile, this.title, this.telephone, this.isDeleted,
      this.salutation, this.isConvertedToOpportunity);

//
//
//  static LeadListData fromJson(Map<String, dynamic > json) {
//    Map<String,dynamic >data=json["data"];
//    String firstName=data["firstName"];
//    String lastName=data[AppUtils.KEY_lastName];
//    return new LeadListData(firstName,lastName );
//
//  }
//  static List<Model> fromJsonArray(List<Map<String,dynamic>> array){
//
//  }

  @override
  String toString() {
    return 'LeadListData{entityType: $entityType,firstName: $firstName, lastName: $lastName, email: $email, mobile: $mobile, title: $title, telephone: $telephone, isDeleted: $isDeleted, salutation: $salutation, isConvertedToOpportunity: $isConvertedToOpportunity}';
  }

}
