import 'package:crmflutternew/util/AppUtils.dart';
class StartendVisit{
  String flag;
  String message;
  String visit;


  StartendVisit(this.message,this.visit);


  static StartendVisit fromJson(Map<String, dynamic> json) {
    String message = json["message"];
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

      String visit = data[AppUtils.KEY_id];

    return new StartendVisit(message,visit);
  }

}