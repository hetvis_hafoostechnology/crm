import 'package:crmflutternew/model/LeadListData.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class LeadList {
  List<LeadListData> listData;
  String pagination_total;

  LeadList(
    this.listData,
    this.pagination_total,
  );

  static LeadList fromjson_leadlist(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    var list = data["list"] as List;
    String pagination_total = data[AppUtils.KEY_pagination]["total"];
    return new LeadList(list, pagination_total);
  }
}