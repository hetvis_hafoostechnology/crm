import 'package:crmflutternew/model/Table/DataContactDept.dart';
class ContactDepartment {
  List<DataContactDept> datas;

  ContactDepartment(this.datas);

  static ContactDepartment fromJsonArray(dynamic json) {
    List<DataContactDept> result = List();
    try {
      List<dynamic> list = List.from(json);
      list.forEach((item) {
        result.add(DataContactDept.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return ContactDepartment(result);
  }
}
