import 'package:crmflutternew/util/AppUtils.dart';
class DataContactDept {
  String department;
  String phone;
  String email;



  DataContactDept({this.department, this.phone,this.email});

  static DataContactDept fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataContactDept(
        department: item["department"],
        phone: item["phone"],
        email: item[AppUtils.KEY_Email],
      );

      return result;
    } catch (e) {
      print("err$e");
    }
  }

}

