import 'package:crmflutternew/model/document/DataListLead.dart';
import 'package:crmflutternew/model/leadStorage/DataLeadStage.dart';
import 'package:fimber/fimber_base.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class ListForLeadInfo {
  List<DataListLead> datas;
  var log = FimberLog("listforleadinfo");
  int paginationModel;

  ListForLeadInfo(this.datas, this.paginationModel);

  static ListForLeadInfo fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if (data == null) {
      return null;
    } else {
      int page = data[AppUtils.KEY_pagination]["total"];
      List<DataListLead> result = List();
      try {
        List<dynamic> list = List.from(data["list"]);

        list.forEach((item) {
          result.add(DataListLead.fromJson(item));
        });
        return new ListForLeadInfo(result, page);
      } catch (e) {
        print(e);
      }
      return null;
    }
  }
}
