import 'package:crmflutternew/model/account/NoteDetail.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class NoteList {
  List<NoteDetail> datas;

  NoteList(this.datas);

  static NoteList fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<NoteDetail> result = List();
    try {
      List<dynamic> list = List.from(data["list"]);

      list.forEach((item) {
        result.add(NoteDetail.fromJson(item));
      });
      return new NoteList(result);
    } catch (e) {
      print(e);
    }
    return null;
  }

}