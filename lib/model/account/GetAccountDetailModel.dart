import 'package:crmflutternew/model/Table/ContactDepartment.dart';
import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';

class GetAccountDetailModel {
  String id;
  String name;
  String type;
  String typeId;
  String segment;
  String segmentId;
  String accRepresentative;
  String accRepresentativeId;
  String annualRevenue;
  String annualRevenueId;
  String website;
  String intenational;
  String industry;
  String industryId;
  String entityType;
  String entityTypeId;
  String createdByUser;
  String createdByTime;
  String updatedByUser;
  String updatedByTime;
  String department;
  String phone;
  String email;
  Address address;
  List tagids;
  ContactDepartment contacts;

  GetAccountDetailModel(
      {this.id,
      this.name,
      this.type,
      this.typeId,
      this.segment,
      this.segmentId,
      this.accRepresentative,
      this.accRepresentativeId,
      this.annualRevenue,
      this.annualRevenueId,
      this.website,
      this.intenational,
      this.industry,
      this.industryId,
      this.entityType,
      this.entityTypeId,
      this.createdByUser,
      this.createdByTime,
      this.updatedByUser,
      this.updatedByTime,
      this.address,
      this.email,
      this.phone,
      this.department,
      this.tagids,
      this.contacts});

  static GetAccountDetailModel fromJsonResponse(Map<String, dynamic> json) {
    Map<String, dynamic> item = json[AppUtils.KEY_DATA];
    if (json == null) {
      return null;
    } else {
      var result = new GetAccountDetailModel(
        id: item[AppUtils.KEY_id],
        name: item[AppUtils.KEY_name],
        type: item[AppUtils.KEY_type]["text"],
        typeId: item[AppUtils.KEY_type][AppUtils.KEY_id],
        entityType: item[AppUtils.KEY_entityType]["text"],
        entityTypeId: item[AppUtils.KEY_entityType][AppUtils.KEY_id],
        createdByUser: item["createdBy"]["user"][AppUtils.KEY_firstName],
        createdByTime: item["createdBy"]["time"],
        updatedByUser: item[AppUtils.KEY_updatedBy]["user"][AppUtils.KEY_firstName],
        updatedByTime: item[AppUtils.KEY_updatedBy]["time"],
      );
      if (item.containsKey(AppUtils.KEY_segment)) {
        result.segment = item[AppUtils.KEY_segment]["text"];
        result.segmentId = item[AppUtils.KEY_segment][AppUtils.KEY_id];
      }
      if (item.containsKey(AppUtils.KEY_accountRep)) {
        result.accRepresentative =
            item[AppUtils.KEY_accountRep][AppUtils.KEY_firstName] + item[AppUtils.KEY_accountRep][AppUtils.KEY_lastName];
        result.accRepresentativeId = item[AppUtils.KEY_accountRep][AppUtils.KEY_id];
      }

      if (item.containsKey(AppUtils.KEY_website)) {
        result.website = item[AppUtils.KEY_website];
      }
      if (item.containsKey(AppUtils.KEY_annRev)) {
        result.annualRevenue = item[AppUtils.KEY_annRev]["text"];
        result.annualRevenueId = item[AppUtils.KEY_annRev][AppUtils.KEY_id];
      }
      if (item.containsKey(AppUtils.KEY_industry)) {
        result.industry = item[AppUtils.KEY_industry]["text"];
        result.industryId = item[AppUtils.KEY_industry][AppUtils.KEY_id];
      }
      if (item.containsKey(AppUtils.KEY_international)) {
        result.intenational = item[AppUtils.KEY_international];
        print("model internation${result.intenational}");
      }
      if (item.containsKey(AppUtils.KEY_contact)) {

        result.contacts = ContactDepartment.fromJsonArray(item[AppUtils.KEY_contact]);
      }
      if (item.containsKey(AppUtils.KEY_address)) {
        result.address = Address.fromMap(item[AppUtils.KEY_address]);
      }
      if (item.containsKey(AppUtils.KEY_tagIds)) {
        result.tagids = item[AppUtils.KEY_tagIds];
      }
      return result;
    }
  }
}
