import 'package:crmflutternew/model/PaginationModel.dart';
import 'package:crmflutternew/model/account/AccountDetailModel.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class AccountListModel {
  List<AccountDetailModel> datas;

//  String list;
  int paginationModel;

  AccountListModel(this.datas, this.paginationModel);

  static AccountListModel fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];
    if (data == null) {
      return null;
    } else {
      List<AccountDetailModel> result = List();
      int page = data[AppUtils.KEY_pagination]["total"];
      try {
        List<dynamic> list = List.from(data["list"]);

        list.forEach((item) {
          result.add(AccountDetailModel.fromJson(item));
        });
//      result. = Address.fromMap(item[AppUtils.KEY_address]);
        var res = new AccountListModel(result, page);
//      print(res);

        return res;
      } catch (e) {
        print(e);
      }
      return null;
    }
  }
}
