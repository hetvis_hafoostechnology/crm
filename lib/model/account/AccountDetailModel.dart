import 'package:crmflutternew/model/latlongByAddress/Address.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class AccountDetailModel {
  String accName;
  String firstName;
  String lastName;

  String typeInsRet;
  String segment;
  String website;
  String id;
  String ownerid;
  String entityType;
  String city;
  String state;
  String country;
  String code;
  Address address;

  AccountDetailModel({
    this.id,
    this.ownerid,
    this.accName,
    this.firstName,
    this.lastName,
    this.website,
    this.typeInsRet,
    this.entityType,
    this.segment,
    this.address,
  });

// this.addLine1,
//      this.addLine2,
//      this.addLine3,
//      this.city,
//      this.state,
//      this.country,
//      this.code
  static AccountDetailModel fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new AccountDetailModel(
        id: item[AppUtils.KEY_id],
        ownerid: item[AppUtils.KEY_accountRep][AppUtils.KEY_id],
        accName: item[AppUtils.KEY_name],
        firstName: item[AppUtils.KEY_accountRep][AppUtils.KEY_firstName],
        lastName: item[AppUtils.KEY_accountRep][AppUtils.KEY_lastName],
        website: item[AppUtils.KEY_website],
        typeInsRet: item[AppUtils.KEY_type]["text"],
        entityType: item[AppUtils.KEY_entityType],

//        addLine1: item["address"]["addressLine1"],
//        addLine2: item["address"]["addressLine2"],
//        addLine3: item["address"][AppUtils.KEY_addressLine3],
//        city: item["address"]["city"],
//        state: item["address"]["state"],
//        country: item["address"]["country"],
//        code: item["address"]["zipCode"],
      );

      if (item.containsKey(AppUtils.KEY_address)) {
        result.address = Address.fromMap(item[AppUtils.KEY_address]);
      }  if (item.containsKey(AppUtils.KEY_segment)) {
        result.segment = item[AppUtils.KEY_segment]["text"];
      }
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
