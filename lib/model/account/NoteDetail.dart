import 'package:crmflutternew/util/AppUtils.dart';
class NoteDetail {
  String id;
  String detail;
  String retaledTo;
  String time;
  String accountId;
  String opportunityId;


  NoteDetail(
      {this.id,
      this.detail,
      this.retaledTo,
      this.time,
      this.accountId,
      this.opportunityId,
      });

  static NoteDetail fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new NoteDetail(
          id: item[AppUtils.KEY_id],
          detail: item["detail"],
          accountId: item[AppUtils.KEY_accountId],
          opportunityId: item["opportunityId"],
          time: item[AppUtils.KEY_updatedBy]["time"]);

      if (item.containsKey("opportunity")) {
        print("related ${item["opportunity"][AppUtils.KEY_name]}");
        result.retaledTo = item["opportunity"][AppUtils.KEY_name];
      }
      return result;
    } catch (e) {
      print(e);
    }
    return null;
  }
}
