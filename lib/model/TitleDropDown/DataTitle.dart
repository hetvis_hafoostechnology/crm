import 'package:crmflutternew/util/AppUtils.dart';
class DataTitle {
  String id;
  String type;
  String text;

  DataTitle({this.id, this.type, this.text});


  static DataTitle fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataTitle(
        id:item[AppUtils.KEY_id],
      type:item[AppUtils.KEY_type],
      text:item["text"],
      );
      return result;
    } catch (e) {
      print(e);
    }
  }
//  static DataSalutationUsers fromMap(item) {
//    if (item == null) {
//      return null;
//    }
//    var result =new DataSalutationUsers(
//      id:item[AppUtils.KEY_id],
//      type:item[AppUtils.KEY_type],
//      text:item["text"],
//    );
//
//    return result;
//  }
//
//  static List<DataSalutationUsers> fromJsonArray(dynamic json) {
//
//    List<DataSalutationUsers> result = List();
//    try {
//      List<dynamic> list = List.from(json);
//
//
//      list.forEach((item) {
//        result.add(DataSalutationUsers.fromMap(item));
//      });
//    } catch (e) {
//      print(e);
//    }
//
//    return result;
////
//
//  }
}
