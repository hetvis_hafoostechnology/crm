import 'package:crmflutternew/model/TitleDropDown/DataTitle.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class TitleDropdown {
  List<DataTitle> datas;

  TitleDropdown(this.datas);

  static TitleDropdown fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DataTitle> result = List();
    try {
      List<dynamic> list = List.from(data["masterList"]);

      list.forEach((item) {
        result.add(DataTitle.fromJson(item));
      });
    } catch (e) {
      print(e);
    }
    return new TitleDropdown(result);
  }
//  static TitleDropdown fromJsonArray(dynamic json) {
//
//    List<DataSalutationUsers> result = List();
//    try {
//      List<dynamic> list = List.from(json);
//
//      list.forEach((item) {
//        result.add(DataSalutationUsers.fromJson(item));
//      });
//    } catch (e) {
//      print(e);
//    }
//
//    return TitleDropdown(result);
//
//  }
}