import 'package:crmflutternew/model/leadStorage/DataLeadStage.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class LeadStorageDropdown{
  List<DataLeadStage> datas;

  LeadStorageDropdown(this.datas);

  static LeadStorageDropdown fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DataLeadStage> result = List();
    try {
      List<dynamic> list = List.from(data["masterList"]);

      list.forEach((item) {
        result.add(DataLeadStage.fromJson(item));
      });
    } catch (e) {
      print(e);
    }
    return new LeadStorageDropdown(result);
  }

}