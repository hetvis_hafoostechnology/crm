import 'package:crmflutternew/util/AppUtils.dart';
class DataTaggedUsers {
  String id;
  String name;



  DataTaggedUsers({this.id, this.name});

  static DataTaggedUsers fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataTaggedUsers(
        id: item[AppUtils.KEY_id],
        name: item[AppUtils.KEY_name],
      );

      return result;
    } catch (e) {
      print("tag drop$e");
    }
  }

}

