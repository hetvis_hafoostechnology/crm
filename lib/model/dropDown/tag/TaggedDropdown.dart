import 'package:crmflutternew/model/dropDown/tag/DataTaggedUsers.dart';
class TaggedDropdown {
  List<DataTaggedUsers> datas;

  TaggedDropdown(this.datas);

  static TaggedDropdown fromJsonArray(dynamic json) {
    List<DataTaggedUsers> result = List();
    try {
      List<dynamic> list = List.from(json);
      list.forEach((item) {
        result.add(DataTaggedUsers.fromJson(item));
      });
    } catch (e) {
      print(e);
    }

    return TaggedDropdown(result);
  }
}
