import 'package:crmflutternew/util/AppUtils.dart';
class DataDropDown {
  String id;
  String type;
  String text;

  DataDropDown({this.id, this.type, this.text});


  static DataDropDown fromJson(item) {
    if (item == null) {
      return null;
    }
    try {
      var result = new DataDropDown(
        id:item[AppUtils.KEY_id],
      type:item[AppUtils.KEY_type],
      text:item["text"],
      );
      return result;
    } catch (e) {
      print(e);
    }
  }

}
