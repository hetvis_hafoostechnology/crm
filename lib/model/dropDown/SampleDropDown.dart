
import 'package:crmflutternew/model/dropDown/DataDropDown.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class SampleDropDown{
  List<DataDropDown> datas;

  SampleDropDown(this.datas);

  static SampleDropDown fromjson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json[AppUtils.KEY_DATA];

    List<DataDropDown> result = List();
    try {
      List<dynamic> list = List.from(data["masterList"]);

      list.forEach((item) {
        result.add(DataDropDown.fromJson(item));
      });
    } catch (e) {
      print(e);
    }
    return new SampleDropDown(result);
  }

}