import 'package:country_code_picker/country_code_picker.dart';
import 'package:crmflutternew/dashboard/DashboardScreen.dart';
import 'package:crmflutternew/dashboard/forgotPassword/ForgotPassword.dart';
import 'package:crmflutternew/login/LoginBloc.dart';
import 'package:crmflutternew/login/LoginEvent.dart';
import 'package:crmflutternew/login/LoginState.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginPageState createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginScreen> {
  LoginBloc _bloc;
  String data = "";
  ProgressDialog pr;
  String selectedcc = "+1";
  String loginType;
  TextEditingController emailController, passwordController, mobController;
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _mobFocus = FocusNode();
  final FocusNode _pwdFocus = FocusNode();

  int _radioValue = 0;

  @override
  void initState() {
    super.initState();
    _bloc = LoginBloc();
    _bloc.state.listen(_loginStateListener);
    emailController = TextEditingController();
    mobController = TextEditingController();
    passwordController = TextEditingController();
    _bloc.dispatch(LoginInitStateCalled());
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
    });
    print(_radioValue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<LoginEvent, LoginState>(
        bloc: _bloc,
        builder: _builder,
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  Widget _builder(BuildContext context, LoginState ls) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return SingleChildScrollView(
        child: Stack(
      children: <Widget>[
        new Column(
          children: <Widget>[
            new Container(
              height: MediaQuery.of(context).size.height * .50,
              color: Constants.COLOR_LOGIN_UPPER_BG,
            ),
            new Container(
              height: MediaQuery.of(context).size.height * .50,
              color: Constants.COLOR_LOGIN_LOWER_BG,
            )
          ],
        ),

        new Column(
          children: <Widget>[
            new Container(
              alignment: Alignment.topCenter,
              child: Text("hafooz",
                  style: TextStyle(
                      color: Constants.COLOR_TEXT,
                      decoration: TextDecoration.none,
                      fontSize: 40.0)),
              padding: new EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .15),
            ),
          ],
        ),

        new Container(
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .30,
              right: 10.0,
              left: 10.0),
          child: new Container(
            padding: EdgeInsets.only(left: 5.0, right: 5.0),
            height: 400.0,
            width: MediaQuery.of(context).size.width,
            child: new Card(
              color: Constants.COLOR_CARD_WHITE_BG,
              elevation: 4.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0),
                      child: Text("SIGN IN",
                          key: Key('signinBtn'),
                          style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                              color: Constants.COLOR_CARD_SIGNIN_TITLE)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: new Row(
                        children: <Widget>[
                          new Text(
                            'Loging Using:',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          new Radio(
                            value: 0,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange,
                          ),
                          new Text('Email'),
                          new Radio(
                            value: 1,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange,
                          ),
                          new Text('Mobile'),
                        ],
                      ),
                    ),
                    _radioValue == 0
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 10.0, left: 15.0, right: 15.0),
                                child: Text("Email",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 5.0, left: 15.0, right: 15.0),
                                child: TextFormField(
                                  textInputAction: TextInputAction.next,
                                  focusNode: _emailFocus,
                                  controller: emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  autofocus: false,
                                  onFieldSubmitted: (term) {
                                    _fieldFocusChange(
                                        context, _emailFocus, _pwdFocus);
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Email',
                                    contentPadding: EdgeInsets.all(10.0),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0)),
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 10.0, left: 15.0, right: 15.0),
                                child: Text("Mobile",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 5.0, left: 15.0, right: 15.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Flexible(
                                      child: CountryCodePicker(
                                        onChanged: _onCountryChangeMob,
                                        initialSelection: 'US',
                                        showCountryOnly: false,
                                        showOnlyCountryWhenClosed: false,
                                        alignLeft: false,
                                      ),
                                      flex: 1,
                                    ),
                                    new Flexible(
                                      child: TextFormField(
                                        controller: mobController,
                                        textInputAction: TextInputAction.next,
                                        focusNode: _mobFocus,
                                        onFieldSubmitted: (term) {
                                          _fieldFocusChange(
                                              context, _mobFocus, _pwdFocus);
                                        },
                                        keyboardType: TextInputType.phone,
                                        autofocus: false,
                                        decoration: InputDecoration(
                                          hintText: "Mobile",
                                          contentPadding: EdgeInsets.all(10.0),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(0.0)),
                                        ),
                                      ),
                                      flex: 3,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
                      child: Text("Password",
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.bold)),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        focusNode: _pwdFocus,
                        controller: passwordController,
                        autofocus: false,
                        obscureText: true,
                        onFieldSubmitted: (value) {
                          _emailFocus.unfocus();
                          _mobFocus.unfocus();
                          _loginPressed();
                        },
                        decoration: InputDecoration(
                          hintText: 'Password',
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0)),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: FlatButton(
                        child: Text(
                          "Forgot password?",
                          style: TextStyle(
                              color: Constants.COLOR_CARD_FP,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ForgotPassword()));
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, right: 15.0, left: 15.0, bottom: 10.0),
                      child: SizedBox(
                        width: double.maxFinite,
                        height: 50.0,
                        child: RaisedButton(
                          child:
                              Text('Sign In', style: TextStyle(fontSize: 20.0)),
                          color: Constants.COLOR_CARD_BUTTON,
                          textColor: Constants.COLOR_TEXT,
                          onPressed: _loginPressed,

//                                onPressed: () {
//                                  Navigator.of(context).push(MaterialPageRoute(
//                                      builder: (context) => DashboardScreen()));
//                                }
                        ),
                      ),
                    ),
                  ]),
            ),
          ),
        ),
//            Visibility(
//              child: Loading(),
//              visible: ls is LoginLoading,
//            )
      ],
    ));
  }

  void _loginPressed() {
    pr.show();
    print("show");
    String email = emailController.text;
    String mob = mobController.text;
    if (mob != null) {
      mob = selectedcc + mobController.text;
    }
    if (_radioValue == 0) {
      loginType = "email";
//      mob = "";
    } else {
      loginType = "mobile";
      email = "";
    }

    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (_isValidData()) {
      pr.hide();
      _bloc.dispatch(
          LoginButtonPressed(email, mob, passwordController.text, loginType));
    } else {
      pr.hide();
    }
  }

  void _onCountryChangeMob(CountryCode countryCode) {
    selectedcc = countryCode.toString();
  }

  void _loginStateListener(LoginState ls) {

    if (ls is SetLoginData) {
      setState(() {
        emailController.text = ls.email;
        mobController.text = ls.mob;
        passwordController.text = ls.password;
      });
    } else if (ls is LoginFailed) {
      AppUtils.showAlert(context, ls.reason);
    } else if (ls is LoginSuccess) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => DashboardScreen(),
          ),
          ModalRoute.withName("/DashboardScreen"));
//      Navigator.of(context)
//          .push(MaterialPageRoute(builder: (context) => DashboardScreen()));

    }
  }

  bool _isValidData() {
    var emailV;
    if (_radioValue == 0) {
      emailV = emailController.text;
    } else {
      emailV = mobController.text;
    }

    var password = passwordController.text;
    if (emailV == null || emailV.length == 0) {
      AppUtils.showAlert(context, StringUtils.EmailHint);
      return false;
    } else if (password == null || password.length == 0) {
      AppUtils.showAlert(context, StringUtils.PwdHint);
      return false;
    }
    return true;
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
