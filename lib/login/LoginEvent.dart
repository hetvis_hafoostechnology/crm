abstract class LoginEvent{}
class LoginButtonPressed extends LoginEvent{
  String email,mobile,password,type;
  bool isRememberPassword=false;
  LoginButtonPressed(this.email,this.mobile, this.password,this.type);
  @override
  String toString() => 'LoginButtonPressed { email: $email, password: $password }';
}
class LoginInitStateCalled extends LoginEvent{}
class ResendPassPressed extends LoginEvent{
  String email;

  ResendPassPressed(this.email);

}