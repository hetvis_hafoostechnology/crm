abstract class LoginState {}

class LoginUninitialized extends LoginState {}

class LoginLoading extends LoginState {
  @override
  String toString() => "{LoginStateLoading}";
}

class LoginFailed extends LoginState {
  int resCode;
  String reason;

  LoginFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{LoginFailed reason: $reason}";
  }
}

class LoginSuccess extends LoginState {
  @override
  String toString() {
    return "{Login Sucess}";
  }
}

class SetLoginData extends LoginState{
  String email,mob,password,loginType;

  SetLoginData(this.email,this.mob, this.password,this.loginType);

}

