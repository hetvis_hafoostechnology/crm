import 'package:bloc/bloc.dart';
import 'package:crmflutternew/model/User.dart';
import 'package:crmflutternew/login/LoginEvent.dart';
import 'package:crmflutternew/login/LoginState.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:fimber/fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import '../helper/DataManager.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  var log = FimberLog("LoginBloc");


  @override
  LoginState get initialState => LoginUninitialized();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
//      yield LoginLoading();
      print(event.email);
      print(event.mobile);
      print(event.password);
      print(event.type);
      try {
        User user = await DataManager.instance.callApiLogin(
            event.email, event.mobile, event.password, event.type);
        saveData(
            user.email, user.firstName, user.lastName, user.id, user.token);

        print("in bloc $user");
        yield LoginSuccess();
      } catch (e, stacktrace) {
        print("else $e");
        if (e is NoSuchMethodError) {
          yield LoginFailed(
              resCode: Constants.FAIL_INTERNET_CODE,
              reason: "Something went wrong!Please check all details!");
        }
      }
    } else if (event is LoginInitStateCalled) {
      var email = DataManager.instance.getString(AppUtils.KEY_Email);
      var password = DataManager.instance.getString(AppUtils.KEY_Password);
      var mobile = DataManager.instance.getString(AppUtils.KEY_Mobile);
      var loginType = DataManager.instance.getString(AppUtils.KEY_LoginType);
      yield SetLoginData(email, mobile, password, loginType);
    }
  }

  Future<bool> saveData(
      String email, String fname, String lname, String id, String token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(AppUtils.SP_KEYLoginEmail, email);
    preferences.setString(AppUtils.SP_KEYLoginFname, fname);
    preferences.setString(AppUtils.SP_KEYLoginLName, lname);
    preferences.setString(AppUtils.SP_KEYLoginID, id);
    preferences.setString(AppUtils.SP_KEYLoginTOKEN, token);
  }
}
