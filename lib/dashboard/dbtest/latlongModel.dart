import 'package:sqflite/sqflite.dart';

class latlongModel {
  final int id;
  final String lat;
  final String lng;

  latlongModel({this.id, this.lat, this.lng});

  factory latlongModel.fromMap(Map<String, dynamic> json) => new latlongModel(
        id: json["id"],
        lat: json["lat"],
        lng: json["lng"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "lat": lat,
        "lng": lng,
      };

  @override
  String toString() {
    return 'latlongModel{id: $id, lat: $lat, lng: $lng}';
  }
}
