import 'dart:io';

import 'package:crmflutternew/dashboard/dbtest/latlongModel.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class llDbHelper {
  llDbHelper._();

  static final llDbHelper db = llDbHelper._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    print("path to db$documentsDirectory");
    String path = join(documentsDirectory.path, "Test2DB.db");
    print("path $path");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE latlongs(id INTEGER PRIMARY KEY, lat TEXT, lng TEXT)");
    });
  }

  newLatlng(latlongModel llm) async {
    print("inserted");
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM latlongs");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into latlongs (id,lat,lng)"
        " VALUES (?,?,?)",
        [id, llm.lat, llm.lng]);
    return raw;
  }

  getAllLatlongs() async {
    final db = await database;
    var res = await db.query("latlongs");
    List<latlongModel> list =
        res.isNotEmpty ? res.map((c) => latlongModel.fromMap(c)).toList() : [];
    return list;
  }
}
