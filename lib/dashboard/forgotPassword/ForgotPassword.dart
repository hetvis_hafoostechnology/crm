import 'package:crmflutternew/dashboard/forgotPassword/ForgotPasswordBloc.dart';
import 'package:crmflutternew/dashboard/forgotPassword/ForgotPwdEvent.dart';
import 'package:crmflutternew/dashboard/forgotPassword/ForgotPwdState.dart';
import 'package:crmflutternew/login/LoginScreen.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/CustomAlertDialog.dart';
import 'package:crmflutternew/util/StringUtils.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ForgotPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _forgotpasswordState();
  }
}

class _forgotpasswordState extends State<ForgotPassword> {
  ForgotPasswordBloc _bloc;
  ProgressDialog pr;
  TextEditingController emailController;

  @override
  void initState() {
    super.initState();
    _bloc = ForgotPasswordBloc();
    _bloc.state.listen(_forgotPwdStateListener);
    emailController = TextEditingController();
//    _bloc.dispatch(ForgotPwdInitStateCalled());
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Scaffold(
      appBar: AppBar(
          title: Text("Forgot password"),
          backgroundColor: Constants.COLOR_LOGIN_UPPER_BG),
      body: BlocBuilder<ForgotPwdEvent, ForgotPwdState>(
        bloc: _bloc,
        builder: _builder,
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  Widget _builder(BuildContext context, ForgotPwdState ls) {
    return SingleChildScrollView(
        child: Stack(
      children: <Widget>[
        // The containers in the background
        new Column(
          children: <Widget>[
            new Container(
              height: MediaQuery.of(context).size.height * .40,
              color: Constants.COLOR_LOGIN_UPPER_BG,
            ),
            new Container(
              height: MediaQuery.of(context).size.height * .40,
              color: Constants.COLOR_LOGIN_LOWER_BG,
            )
          ],
        ),

        new Container(
          alignment: Alignment.topCenter,
          child: Text("hafooz",
              style: TextStyle(
                  color: Constants.COLOR_TEXT,
                  decoration: TextDecoration.none,
                  fontSize: 40.0)),
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .15),
        ),

        new Container(
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .25,
              right: 10.0,
              left: 10.0),
          child: new Container(
            padding: EdgeInsets.only(left: 5.0, right: 5.0),
            height: 250.0,
            width: MediaQuery.of(context).size.width,
            child: new Card(
              color: Constants.COLOR_CARD_WHITE_BG,
              elevation: 4.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
//                        Padding(
//                          padding: EdgeInsets.only(
//                              top: 10.0, left: 15.0, right: 15.0),
//                          child: Text("Forgot Password",
//                              style: TextStyle(
//                                  fontSize: 24.0,
//                                  fontWeight: FontWeight.bold,
//                                  color: Constants.COLOR_CARD_SIGNIN_TITLE)),
//                        ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
                      child: Text("Email",
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.bold)),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0),
                      child: TextFormField(
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 20.0, right: 15.0, left: 15.0, bottom: 10.0),
                      child: SizedBox(
                        width: double.maxFinite,
                        height: 45.0,
                        child: RaisedButton(
                          child: Text('Send Email',
                              style: TextStyle(fontSize: 20.0)),
                          color: Constants.COLOR_CARD_BUTTON,
                          textColor: Constants.COLOR_TEXT,
                          onPressed: () {
                            pr.show();
                            _btnPressed();
                          },
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: FlatButton(
                        child: Text(
                          StringUtils.change_forgot_pwd_emailhint,
                          style: TextStyle(
                              color: Constants.COLOR_CARD_FP,
                              fontWeight: FontWeight.bold),
                        ),
                              onPressed: () {
//                                Navigator.of(context).push(
//
//                                    MaterialPageRoute(
//                                        builder: (context) => ChangePassword()));
                              }
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ],
    ));
  }

  void _btnPressed() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (_isValidData()) pr.hide();
    _bloc.dispatch(ForgotPwdButtonPressed(emailController.text));
  }

  void _forgotPwdStateListener(ForgotPwdState ls) {
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {},
    );
    Widget doneButton = FlatButton(
      child: Text("Done"),
      onPressed: () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            ModalRoute.withName("/LoginScreen"));
      },
    );

    if (ls is ForgotPwdFailed) {
      AlertDialog alert = AlertDialog(
        title: Text("Failed"),
        content: Text("sending Email failed"),
        actions: [
          okButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    } else if (ls is ForgotPwdSuccess) {
      AlertDialog alert = AlertDialog(
        title: Text("Success"),
        content: Text("Password link sent on your registered email"),
        actions: [
          doneButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  bool _isValidData() {
    var email = emailController.text;
    if (email == null || email.length == 0) {
      CustomAlertDialog.showAlert(
          context: context,
          title: "CRM",
          message: "Enter Email",
          button1Text: "Ok");
      return false;
    } else if (!AppUtils.isEmail(email)) {
      CustomAlertDialog.showAlert(
          context: context,
          title: "CRM",
          message: "Enter Valid email",
          button1Text: "Ok");
      return false;
    }
    return true;
  }
}
