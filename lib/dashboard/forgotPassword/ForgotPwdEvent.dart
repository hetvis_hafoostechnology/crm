abstract class ForgotPwdEvent{}
class ForgotPwdButtonPressed extends ForgotPwdEvent{
  String email;
  ForgotPwdButtonPressed(this.email);

  @override
  String toString() => 'ForgotPwdButtonPressed { email: $email}';

}
class ForgotPwdInitStateCalled extends ForgotPwdEvent{}

