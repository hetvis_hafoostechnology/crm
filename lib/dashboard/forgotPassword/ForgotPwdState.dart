abstract class ForgotPwdState {}

class ForgotPwdUninitialized extends ForgotPwdState {}


class ForgotPwdLoading extends ForgotPwdState {
  @override
  String toString() => "{ForgotPwdLoading}";
}

class ForgotPwdFailed extends ForgotPwdState {
  int resCode;
  String reason;

  ForgotPwdFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{ForgotPwdFailed reason: $reason}";
  }
}

class ForgotPwdSuccess extends ForgotPwdState {
  @override
  String toString() {
    return "{ForgotPwd Sucess}";
  }
}



