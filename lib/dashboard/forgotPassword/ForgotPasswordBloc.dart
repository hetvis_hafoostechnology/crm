import 'package:bloc/bloc.dart';
import 'package:crmflutternew/model/ForgotPwd.dart';
import 'package:crmflutternew/model/User.dart';
import 'package:crmflutternew/dashboard/forgotPassword/ForgotPwdEvent.dart';
import 'package:crmflutternew/dashboard/forgotPassword/ForgotPwdState.dart';
import 'package:crmflutternew/helper/DataManager.dart';
import 'package:crmflutternew/login/LoginEvent.dart';
import 'package:crmflutternew/login/LoginState.dart';
import 'package:fimber/fimber.dart';


class ForgotPasswordBloc extends Bloc<ForgotPwdEvent, ForgotPwdState> {
  var log = FimberLog("ForgotPwdBloc");

//  @override
//  LoginState get initialState => ForgotPwdUninitialized();

  @override
  Stream<ForgotPwdState> mapEventToState(ForgotPwdEvent event) async* {
    if (event is ForgotPwdButtonPressed) {
      yield ForgotPwdLoading();
      try {
        ForgotPwd fp = await DataManager.instance.callApiForgotPassword(
            event.email);
        print("in bloc $fp");

        yield ForgotPwdSuccess();
      } catch (e, stacktrace) {
        log.e("something went wrong", ex: e, stacktrace: stacktrace);

      }
    }
//    else if (event is LoginInitStateCalled) {
//      var email = DataManager.instance.getString("email");
//      var password = DataManager.instance.getString("password");
//      yield SetLoginData(email, password);
//    }
  }

  @override
  ForgotPwdState get initialState => ForgotPwdUninitialized();
}
