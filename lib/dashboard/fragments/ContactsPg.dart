import 'package:contacts_service/contacts_service.dart';
import 'package:crmflutternew/dashboard/fragments/phoneBtn.dart';
import 'package:flutter/material.dart';

//took from https://github.com/Xenon-Labs/Flutter-Development/tree/master/contacts_app/lib
class ContactsPg extends StatefulWidget {
  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPg> {
  Iterable<Contact> _contacts;

  @override
  void initState() {
    getContacts();
    super.initState();
  }

  Future<void> getContacts() async {

    final Iterable<Contact> contacts = await ContactsService.getContacts();

    //map((job) => new Job.fromJson(job)).toList()
    setState(() {
      _contacts = contacts;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: (Text('Contacts')),
      ),
      body: _contacts != null
          //Build a list view of all contacts, displaying their avatar and
          // display name
          ? ListView.builder(
              itemCount: _contacts?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                Contact contact = _contacts?.elementAt(index);
                return ListTile(
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 2, horizontal: 18),
                  leading: (contact.avatar != null && contact.avatar.isNotEmpty)
                      ? CircleAvatar(
                          backgroundImage: MemoryImage(contact.avatar),
                        )
                      : CircleAvatar(
                          child: Text(contact.initials()),
                          backgroundColor: Theme.of(context).accentColor,
                        ),
                  title: Text(contact.toMap().toString()?? ''),
                  //This can be further expanded to showing contacts detail
                  // onPressed().
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      phoneBtn(phoneNumbers: contact.phones),
//                SmsButton(phoneNumbers: contact.phones)
                    ],
                  ),
                );
              },
            )
          : Center(child: const CircularProgressIndicator()),
    );
  }
}
