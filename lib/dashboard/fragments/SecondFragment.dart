import 'package:crmflutternew/dashboard/fragments/ContactsPg.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:crmflutternew/dashboard/callRecordDemo.dart';

class SecondFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // return Text("Welcome");
    return RaisedButton(
      child: Text("Welcome"),
      onPressed: () {
        // Navigator.push(context, MaterialPageRoute(builder: (context) => callRecordDemo()));
      },
    );
  }

//RaisedButton(
//      onPressed: () async {
//        final PermissionStatus permissionStatus = await _getPermission();
//        if (permissionStatus == PermissionStatus.granted) {
//          Navigator.push(
//              context, MaterialPageRoute(builder: (context) => ContactsPg()));
//        } else {
//          //If permissions have been denied show standard cupertino alert dialog
//          showDialog(
//              context: context,
//              builder: (BuildContext context) =>
//                  CupertinoAlertDialog(
//                    title: Text('Permissions error'),
//                    content: Text('Please enable contacts access '
//                        'permission in system settings'),
//                    actions: <Widget>[
//                      CupertinoDialogAction(
//                        child: Text('OK'),
//                        onPressed: () => Navigator.of(context).pop(),
//                      )
//                    ],
//                  ));
//        }
//      },
//      child: Container(child: Text('See Contacts')),
//    )
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }
}
//RaisedButton(
//      onPressed: () async {
//        final PermissionStatus permissionStatus = await _getPermission();
//        if (permissionStatus == PermissionStatus.granted) {
//          Navigator.push(
//              context, MaterialPageRoute(builder: (context) => ContactsPage()));
//        } else {
//          //If permissions have been denied show standard cupertino alert dialog
//          showDialog(
//              context: context,
//              builder: (BuildContext context) => CupertinoAlertDialog(
//                    title: Text('Permissions error'),
//                    content: Text('Please enable contacts access '
//                        'permission in system settings'),
//                    actions: <Widget>[
//                      CupertinoDialogAction(
//                        child: Text('OK'),
//                        onPressed: () => Navigator.of(context).pop(),
//                      )
//                    ],
//                  ));
//        }
//      },
//      child: Container(child: Text('See Contacts')),
//    );
//  }
//class _SecondFragState extends State<SecondFragment> {
//  @override
//  Widget build(BuildContext context) {
//    return RaisedButton(
//      onPressed: () {}
//  }
//}
