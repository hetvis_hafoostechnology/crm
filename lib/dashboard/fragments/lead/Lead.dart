import 'dart:convert';
import 'package:crmflutternew/dashboard/fragments/lead/LeadDetail.dart';
import 'package:crmflutternew/dashboard/fragments/lead/LeadFilterPg.dart';
import 'package:crmflutternew/dashboard/fragments/lead/LeadSortPg.dart';
import 'package:crmflutternew/model/ListForLeadInfo.dart';
import 'package:crmflutternew/model/document/DataListLead.dart';
import 'package:crmflutternew/dashboard/fragments/lead/LeadDetail.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/dashboard/InstitutionalLeadScreen.dart';
import 'package:crmflutternew/dashboard/RetailLeadScreen.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grouped_list/grouped_list.dart';
import 'dart:math' as math;

import 'package:shared_preferences/shared_preferences.dart';


List sourceApi,stageApi,entityApi;

String sortingType;
String sortingOrder;
BuildContext _context;

class Lead extends StatelessWidget {
  String sortingType3,sortingOrder3;
  int currentTabIndex3,sortingIndex3;
  List source3,stage3, entity3,selfiltersource3,selfilterstage3,selfilterentity3;

  Lead(
      {Key key,
      @required this.sortingType3,
      @required this.sortingOrder3,
      @required this.currentTabIndex3,
      @required this.sortingIndex3,
      @required this.source3,
      @required this.stage3,
      @required this.entity3,
      @required this.selfiltersource3,
      @required this.selfilterstage3,
      @required this.selfilterentity3})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new LeadPage(
        sortingType2: sortingType3,
        sortingOrder2: sortingOrder3,
        currentTabIndex2: currentTabIndex3,
        sortingIndex2: sortingIndex3,
        source2: source3,
        stage2: stage3,
        entity2: entity3,
        selfiltersource2: selfiltersource3,
        selfilterstage2: selfilterstage3,
        selfilterentity2: selfilterentity3,

      ),
    );
  }
}

class LeadPage extends StatefulWidget {
  String sortingType2,sortingOrder2;
  int currentTabIndex2,sortingIndex2;
  List source2,stage2,entity2,selfiltersource2,selfilterstage2,selfilterentity2;

  LeadPage({
    Key key,
    @required this.sortingType2,
    @required this.sortingOrder2,
    @required this.currentTabIndex2,
    @required this.sortingIndex2,
    @required this.source2,
    @required this.stage2,
    @required this.entity2,
    @required this.selfiltersource2,
    @required this.selfilterstage2,
    @required this.selfilterentity2,
  }) : super(key: key);

  @override
  State createState() => new LeadPageState(
      sortingTypef: sortingType2,
      sortingOrderf: sortingOrder2,
      currentTabIndexf: currentTabIndex2,
      sortingIndexf: sortingIndex2,
      sourcef: source2,
      stagef: stage2,
      entityf: entity2,
      selfilterstagef: selfilterstage2,
      selfiltersourcef: selfiltersource2,
      selfilterentityf: selfilterentity2);
}

class LeadPageState extends State<LeadPage> {
  List<Widget> _fragments = [Fragment1(), Fragment2()];
  int _currentIndex = 0;
  final List<int> _backstack = [0];

  bool myLeadSelected,allLeadSelected;
  Color mybtnClr,mybtnTxtClr,allbtnClr,allbtnTxtClr;

  String sortingTypef,sortingOrderf;
  int currentTabIndexf,sortingIndexf;
  List sourcef,stagef,entityf,selfilterstagef,selfiltersourcef,selfilterentityf;

  LeadPageState(
      {Key key2,
      @required this.sortingTypef,
      @required this.sortingOrderf,
      @required this.currentTabIndexf,
      @required this.sortingIndexf,
      @required this.sourcef,
      @required this.stagef,
      @required this.entityf,
      @required this.selfiltersourcef,
      @required this.selfilterstagef,
      @required this.selfilterentityf});

  @override
  void initState() {
    if (sortingTypef == null) {
      sortingType = "updatedBy.time";
      sortingOrder = "desc";
    } else {
      sortingType = sortingTypef;
      sortingOrder = sortingOrderf;
    }
    _currentIndex = currentTabIndexf;
    if (_currentIndex == 0) {
      myLeadSelected = true;
      allLeadSelected = false;
      selectMyAccBtn();
    } else {
      myLeadSelected = false;
      allLeadSelected = true;
      selectAllAccBtn();
    }
    if (sourcef.isNotEmpty) {

      sourceApi = sourcef[0];
      entityApi = entityf[2];
      stageApi = stagef[1];
    } else {

      sourceApi = null;
      stageApi = null;
      entityApi = null;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext contextm) {
    _context = contextm;

    return WillPopScope(
      onWillPop: () {
        return customPop(contextm);
      },
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                RaisedButton(
                  color: mybtnClr,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(0.0),
                      side: BorderSide(color: Colors.blue)),
                  child: Text(
                    StringUtils.TitleMyLead,
                    style: TextStyle(
                      color: mybtnTxtClr,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Bold,
                    ),
                  ),
                  onPressed: () {
                    myLeadSelected = true;
                    allLeadSelected = false;
                    _currentIndex = 0;

                    selectMyAccBtn();
                    navigateTo(0);
                    setState(() {});
                  },
                ),
                RaisedButton(
                  color: allbtnClr,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(0.0),
                      side: BorderSide(color: Colors.blue)),
                  child: Text(
                    StringUtils.TitleAllLead,
                    style: TextStyle(
                      color: allbtnTxtClr,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Bold,
                    ),
                  ),
                  onPressed: () {
                    myLeadSelected = false;
                    allLeadSelected = true;
                    _currentIndex = 1;
                    selectAllAccBtn();
                    navigateTo(1);
                    setState(() {});
                  },
                ),

                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      print("stage $selfilterstagef spurce $selfiltersourcef");
                      Navigator.of(_context).push(
                        MaterialPageRoute(
                            builder: (_context) => LeadFilterPg(
                              sortOrder: sortingOrderf,
                              sortType: sortingTypef,
                              selectedSortingIndex: sortingIndexf,
                              selectedtabIndex: _currentIndex,
                              selectedFiltersource:selfiltersourcef ,
                              selectedFilterstage:selfilterstagef ,
                              selectedFilterentity: selfilterentityf,
                            )),
                      );
                    },
                    child: SvgPicture.asset(
                      "images/filter.svg",
                      height: 25.0,
                      width: 25.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(_context).push(
                        MaterialPageRoute(
                            builder: (_context) => LeadSortPg(
                                  selectedtabIndex: _currentIndex,
                                  selectedSortingIndex: sortingIndexf,
                                  selectedEntityFilter: selfilterentityf,
                                  selectedStageFilter: selfilterstagef,
                                  selectedSourceFilter: selfiltersourcef,
                                  types: sourcef,
                                  typeentity: stagef,
                                  typestage: entityf,
                                )),
                      );
                    },
                    child: SvgPicture.asset(
                      "images/sort.svg",
                      height: 25.0,
                      width: 25.0,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: _fragments[_currentIndex],
            ),
          ],
        ),
      ),
    );
  }

  selectMyAccBtn() {
    mybtnClr = Colors.blue;
    mybtnTxtClr = Colors.white;
    allbtnClr = Colors.white;
    allbtnTxtClr = Colors.blue;
  }

  selectAllAccBtn() {
    allbtnTxtClr = Colors.white;
    allbtnClr = Colors.blue;
    mybtnClr = Colors.white;
    mybtnTxtClr = Colors.blue;
  }

  void navigateTo(int index) {
    _backstack.add(index);
    setState(() {
      _currentIndex = index;
    });
  }

  void navigateBack(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Future<bool> customPop(BuildContext context) {
    if (_currentIndex == 1) {
      navigateBack(0);
      myLeadSelected = true;
      allLeadSelected = false;
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }


}

class Fragment1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Fragment1Pg(),
    );
  }
}

class Fragment1Pg extends StatefulWidget {
  @override
  State createState() => new Fragment1PgState();
}

//mylead
class Fragment1PgState extends State<Fragment1Pg>
    with TickerProviderStateMixin {
  AnimationController _controller;
  List<DataListLead> leadlist = List();
  String userid, token;
  bool isLoading = false;
  bool isFirstApiCall = false;
  bool hasMore = true;
  int pageCount = 1;
  static const List<IconData> icons = const [Icons.attach_money, Icons.info];

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    super.initState();

    setState(() {
      setData();
    });
  }

  Widget buildlist(String id, String name, String email, String entityType,
      String mob, String source) {
    return InkWell(
      onTap: () {
        callActivity(id);

      },
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.SemiBold,
                      color: Colors.black,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    email.length > 20 ? '${email.substring(0, 20)}...' : email,
                    style: TextStyle(
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    mob,
                    style: TextStyle(
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  entityType == "retail"
                      ? Text(
                          entityType,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Thin,
                            color: Colors.deepPurpleAccent,
                          ),
                        )
                      : Text(
                          entityType,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Thin,
                            color: Colors.green,
                          ),
                        ),
                  new Spacer(),
                  Text(
                    source,
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<DataListLead> leadLiST) {
    if (!isFirstApiCall) {
      return Center(child: CupertinoActivityIndicator());
    }
    if (leadLiST == null || leadLiST.length == 0) {
      return Center(
        child: Text("No Data Found"),
      );
    } else {
      return GroupedListView<dynamic, String>(
        elements: leadlist,
        groupBy: (element) => element.stageText,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: value == StringUtils.TxtWARM
                  ? Container(
                      width: double.maxFinite,
                      height: 40.0,
                      color: Colors.pinkAccent,
                      child: Center(
                        child: Text(
                          value,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              fontFamily: AppUtils.FontName,
                              fontSize: 18,
                              fontWeight: AppUtils.Bold,
                              color: Colors.white),
                        ),
                      ))
                  : value == StringUtils.TxtHOT
                      ? Container(
                          width: double.maxFinite,
                          height: 40.0,
                          color: Colors.blue,
                          child: Center(
                            child: Text(
                              value,
                              style: TextStyle(
                                  fontFamily: AppUtils.FontName,
                                  decoration: TextDecoration.none,
                                  fontSize: 18,
                                  fontWeight: AppUtils.Bold,
                                  color: Colors.white),
                            ),
                          ))
                      : Container(
                          width: double.maxFinite,
                          height: 40.0,
                          color: Colors.amber,
                          child: Center(
                            child: Text(
                              value,
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontSize: 18,
                                  fontFamily: AppUtils.FontName,
                                  fontWeight: AppUtils.Bold,
                                  color: Colors.white),
                            ),
                          ))),
        ),
        indexedItemBuilder: (c, element, index) {
//          index = index + 1;
          if (index == leadlist.length - 1) {
            getListApicall();
          }
          if (index == leadlist.length) {
            if (hasMore) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }
          return buildlist(element.id, element.name, element.email,
              element.entityType, element.sourceText, element.mobile);

        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Theme.of(context).cardColor;
    Color foregroundColor = Theme.of(context).accentColor;
    return Scaffold(
      body: _ListView(leadlist),
      floatingActionButton: Container(
        alignment: Alignment.bottomRight,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: new List.generate(icons.length, (int index) {
              Widget child = new Container(
                height: 70.0,
                width: 70.0,
                alignment: FractionalOffset.topCenter,
                child: new ScaleTransition(
                  scale: new CurvedAnimation(
                    parent: _controller,
                    curve: new Interval(0.0, 1.0 - index / icons.length / 2.0,
                        curve: Curves.easeOut),
                  ),
                  child: new FloatingActionButton(
                    heroTag: null,
                    backgroundColor: backgroundColor,
                    mini: true,
                    child: new Icon(icons[index], color: foregroundColor),
                    onPressed: () {
                      if (index == 0) {
                        callRetailFAB();
                      } else {
                        callInstFAB();
                      }
                    },
                  ),
                ),
              );
              return child;
            }).toList()
              ..add(
                new FloatingActionButton(
                  heroTag: null,
                  child: new AnimatedBuilder(
                    animation: _controller,
                    builder: (BuildContext context, Widget child) {
                      return new Transform(
                        transform: new Matrix4.rotationZ(
                            _controller.value * 0.5 * math.pi),
                        alignment: FractionalOffset.center,
                        child: new Icon(
                            _controller.isDismissed ? Icons.add : Icons.close),
                      );
                    },
                  ),
                  onPressed: () {
                    if (_controller.isDismissed) {
                      _controller.forward();
                    } else {
                      _controller.reverse();
                    }
                  },
                ),
              )),
      ),
    );
  }

  Future<ListForLeadInfo> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<DataListLead> retaillist = List();
    try {
      if (!isLoading && hasMore) {
        isLoading = true;
        isFirstApiCall = true;

        ListForLeadInfo d1 = await listapi();
        if (d1 == null) {
          leadlist = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });

          leadlist.addAll(retaillist);

          if (d1.paginationModel <= leadlist.length) {
            hasMore = false;
          } else {
            hasMore = true;
          }
          pageCount++;
          isLoading = false;
        }
        setState(() {});

        return d1;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<ListForLeadInfo> listapi() async {

    var resBody = {};
    resBody[AppUtils.KEY_ownerId] = userid;
    resBody[AppUtils.KEY_sourceIds] = sourceApi;
    resBody[AppUtils.KEY_stageIds] = stageApi;
    resBody[AppUtils.KEY_entityTypes] = entityApi;
    resBody[AppUtils.KEY_sort] = {AppUtils.KEY_field: sortingType, AppUtils.KEY_order: sortingOrder};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount, AppUtils.KEY_limit: 15};
    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.LeadListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return ListForLeadInfo.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);

    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        getListApicall();
      });
    });
  }
}

class Fragment2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Fragment2Pg(),
    );
  }
}

class Fragment2Pg extends StatefulWidget {
  @override
  State createState() => new Fragment2PgState();
}

//alllead
class Fragment2PgState extends State<Fragment2Pg>
    with TickerProviderStateMixin {
  AnimationController _controller;
  List<DataListLead> leadlist = List();
  String token;
  bool isLoading = false;
  bool isFirstApiCall = false;
  bool hasMore = true;
  int pageCount = 1;
  static const List<IconData> icons = const [Icons.attach_money, Icons.info];

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    super.initState();


    setState(() {
      setData();
    });
  }

  Widget buildlist(String id, String name, String email, String entityType,
      String mob, String source) {
    return InkWell(
      onTap: () {
        callActivity(id);

      },
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.SemiBold,
                      color: Colors.black,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    email.length > 20 ? '${email.substring(0, 20)}...' : email,
                    style: TextStyle(
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    mob,
                    style: TextStyle(
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  entityType == "retail"
                      ? Text(
                          entityType,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Thin,
                            color: Colors.deepPurpleAccent,
                          ),
                        )
                      : Text(
                          entityType,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Thin,
                            color: Colors.green,
                          ),
                        ),
                  new Spacer(),
                  Text(
                    source,
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Thin,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<DataListLead> leadLiST) {
    if (!isFirstApiCall) {
      return Center(child: CupertinoActivityIndicator());
    }
    if (leadLiST == null || leadLiST.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return GroupedListView<dynamic, String>(
        elements: leadlist,
        groupBy: (element) =>
            element.stageText == null ? "" : element.stageText,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: value == StringUtils.TxtWARM
                  ? Container(
                      width: double.maxFinite,
                      height: 40.0,
                      color: Colors.pinkAccent,
                      child: Center(
                        child: Text(
                          value,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              fontFamily: AppUtils.FontName,
                              fontSize: 18,
                              fontWeight: AppUtils.Bold,
                              color: Colors.white),
                        ),
                      ))
                  : value == StringUtils.TxtHOT
                      ? Container(
                          width: double.maxFinite,
                          height: 40.0,
                          color: Colors.blue,
                          child: Center(
                            child: Text(
                              value,
                              style: TextStyle(
                                  fontFamily: AppUtils.FontName,
                                  decoration: TextDecoration.none,
                                  fontSize: 18,
                                  fontWeight: AppUtils.Bold,
                                  color: Colors.white),
                            ),
                          ))
                      : Container(
                          width: double.maxFinite,
                          height: 40.0,
                          color: Colors.amber,
                          child: Center(
                            child: Text(
                              value,
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontSize: 18,
                                  fontFamily: AppUtils.FontName,
                                  fontWeight: AppUtils.Bold,
                                  color: Colors.white),
                            ),
                          ))),
        ),
        indexedItemBuilder: (c, element, index) {
          index = index + 1;
          if (index == leadlist.length - 1) {
            getListApicall();
          }
          if (index == leadlist.length) {
            if (hasMore) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }
          return buildlist(element.id, element.name, element.email,
              element.entityType, element.sourceText, element.mobile);

        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Theme.of(context).cardColor;
    Color foregroundColor = Theme.of(context).accentColor;
    return Scaffold(
      body: _ListView(leadlist),
      floatingActionButton: Container(
        alignment: Alignment.bottomRight,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: new List.generate(icons.length, (int index) {
              Widget child = new Container(
                height: 70.0,
                width: 70.0,
                alignment: FractionalOffset.topCenter,
                child: new ScaleTransition(
                  scale: new CurvedAnimation(
                    parent: _controller,
                    curve: new Interval(0.0, 1.0 - index / icons.length / 2.0,
                        curve: Curves.easeOut),
                  ),
                  child: new FloatingActionButton(
                    heroTag: null,
                    backgroundColor: backgroundColor,
                    mini: true,
                    child: new Icon(icons[index], color: foregroundColor),
                    onPressed: () {
                      if (index == 0) {
                        callRetailFAB();
                      } else {
                        callInstFAB();
                      }
                    },
                  ),
                ),
              );
              return child;
            }).toList()
              ..add(
                new FloatingActionButton(
                  heroTag: null,
                  child: new AnimatedBuilder(
                    animation: _controller,
                    builder: (BuildContext context, Widget child) {
                      return new Transform(
                        transform: new Matrix4.rotationZ(
                            _controller.value * 0.5 * math.pi),
                        alignment: FractionalOffset.center,
                        child: new Icon(
                            _controller.isDismissed ? Icons.add : Icons.close),
                      );
                    },
                  ),
                  onPressed: () {
                    if (_controller.isDismissed) {
                      _controller.forward();
                    } else {
                      _controller.reverse();
                    }
                  },
                ),
              )),
      ),
    );
  }

  Future<ListForLeadInfo> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<DataListLead> retaillist = List();
    try {
      if (!isLoading && hasMore) {
        isLoading = true;
        isFirstApiCall = true;

        ListForLeadInfo d1 = await listapi();

        if (d1 == null) {
          leadlist = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });
          leadlist.addAll(retaillist);

          if (d1.paginationModel <= leadlist.length) {
            hasMore = false;
          } else {
            hasMore = true;
          }
          pageCount++;
          isLoading = false;
        }
        setState(() {});

        return d1;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<ListForLeadInfo> listapi() async {
    var resBody = {};

    resBody[AppUtils.KEY_ownerId] = null;
    resBody[AppUtils.KEY_sourceIds] = sourceApi;
    resBody[AppUtils.KEY_stageIds] = stageApi;
    resBody[AppUtils.KEY_entityTypes] = entityApi;
    resBody[AppUtils.KEY_sort]= {AppUtils.KEY_field: sortingType,  AppUtils.KEY_order: sortingOrder};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount, AppUtils.KEY_limit: 15};
    String str = json.encode(resBody);

    Response response = await Dio().post(
      AppUtils.LeadListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return ListForLeadInfo.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);

    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        getListApicall();
      });
    });
  }
}
//just adding cooment tochck
callActivity(String id) {
  // AppUtils.showToastWithMsg(StringUtils.WOrkInProgress,ToastGravity.CENTER);
  Navigator.push(_context,
      MaterialPageRoute(builder: (context) => LeadDetail(leadId: id)));
}

callRetailFAB() {
  // AppUtils.showToastWithMsg(StringUtils.WOrkInProgress,ToastGravity.CENTER);
  Navigator.of(_context).push(
      MaterialPageRoute(builder: (context) => RetailLeadScreen(isfrom: null)));
}

callInstFAB() {
  // AppUtils.showToastWithMsg(StringUtils.WOrkInProgress,ToastGravity.CENTER);
  Navigator.of(_context).push(MaterialPageRoute(
      builder: (context) => InstitutionalLeadScreen(isfrom: null)));
}
