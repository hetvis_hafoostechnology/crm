import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:crmflutternew/model/getUrlModel.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:mime/mime.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crmflutternew/model/document/FilesDetails.dart';

class DocumentDialogue extends StatefulWidget {
  String leadId;
  String docId;
  String docname;
  List<FilesDetails> list;

  DocumentDialogue(
      {Key key,
      @required this.leadId,
      @required this.docname,
      @required this.list,@required this.docId})
      : super(key: key);

  @override
  State createState() => new DocumentDialogueState(
      getLeadId: leadId, getdocname: docname, getlist: list,getDocId: docId);
}

class DocumentDialogueState extends State<DocumentDialogue> {
  File _pickedImage;
  String filename;
  double fileSizeKb;
  String nameOfFile;
  String pathFromApi;
  int fileSizeB;
  String userid;
  String token;
  String loginId = "Login_Id";
  String loginToken = "Login_token";
  String getLeadId;
  String getDocId;
  String getdocname;
  List<FilesDetails> getlist = List();
  List<File> selectedFiles = List();
  ProgressDialog pr;
  TextEditingController nameController;
  List<String> storeUrls = List();
  List<int> storeFilesSize= List();
  List<String> storeFilesTypes= List();
  List<String> docnames = List();
  List<String> storeFilePathOfAws = List();

  DocumentDialogueState(
      {Key key2,
      @required this.getLeadId,
      @required this.getdocname,
      @required this.getlist,
      @required this.getDocId});

  @override
  void initState() {
    super.initState();
    setData();
    nameController = TextEditingController();
    if (getdocname != null) {
      nameController.text = getdocname;
    }
    if(getlist!=null){
      for(int i=0;i<getlist.length;i++){
        docnames.add(getlist[i].fileName);
        storeUrls.add(getlist[i].fileUrl);
        storeFilePathOfAws.add(getlist[i].filePAth);
        storeFilesSize.add(getlist[i].fileSize);
        storeFilesTypes.add(null);
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: AppBar(
            backgroundColor: Colors.white,
            leading: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 10.0),
              child: Text(
                'Add',
                style: TextStyle(color: Colors.blue, fontSize: 20.0),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  Navigator.pop(context, "");
                },
              ),
            ],
          )),
      body: Column(children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 20.0, top: 10.0),
            child: Text("Name",
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.grey,
                )),
          ),
        ),
        Container(
          height: 60.0,
          child: Padding(
            padding: EdgeInsets.only(
                left: 20.0, top: 10.0, bottom: 10.0, right: 20.0),
            child: TextFormField(
              controller: nameController,
              keyboardType: TextInputType.text,
//              textInputAction: TextInputAction.next,
//              focusNode: _emailFocus,
//              onFieldSubmitted: (term) {
//                _fieldFocusChange(context, _emailFocus, _mobFocus);
//              },
              autofocus: false,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(left: 20.0, top: 30.0, bottom: 30.0, right: 20.0),
          child: DottedBorder(
            color: Colors.grey,
            strokeWidth: 1,
            child: Container(
              height: 40,
              width: MediaQuery.of(context).size.width,
              child: FlatButton(
                  child: Text("Click here to upload",
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.blue,
                      )),
                  onPressed: () {
                    // _choose(context);
                    multiplefiles();
                  }),
            ),
          ),
        ),
        docnames.isEmpty
            ? Container() : SizedBox(
                height: 200,
                child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: docnames.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: const EdgeInsets.only(
                          bottom: 10.0, left: 10.0, right: 10.0),
                      padding: const EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blueAccent)),
                      child: Row(
                        children: [
                          new Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              docnames[index],
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                          new Align(
                            alignment: Alignment.centerRight,
                            child: IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                setState(() {
                                  deleteUrlApicall(storeFilePathOfAws[index]);
                                  docnames.removeAt(index);
                                  storeUrls.removeAt(index);
                                  storeFilesSize.removeAt(index);
                                  storeFilePathOfAws.removeAt(index);
                                });

                                // deletefile();{"path":["document/1exy88kn4nlsgx.png"]}
                                //{"flag":1,"message":"File deleted successfully","data":null}
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: RaisedButton(
                  color: Constants.COLOR_CARD_SIGNIN_TITLE,
                  elevation: 4,
                  child: Text(
                    StringUtils.BtnSave,
                    style: TextStyle(color: Constants.COLOR_TEXT),
                  ),
                  splashColor: Constants.COLOR_CARD_FP,
                  onPressed: () {
                    if (nameController.text.isEmpty) {
                      AppUtils.showToastWithMsg(
                          "Please enter name", ToastGravity.CENTER);
                    } else {
                      // print("selected files list ${selectedFiles}");
                      print("selected files list ${docnames}");
                      print("size list ${storeFilesSize}");
                      print("urls list ${storeUrls}");
                      print("path list ${storeFilePathOfAws}");
                      print("type list ${storeFilesTypes}");

                      //   "status": "uploaded",
                      // AppUtils.KEY_name: filename,
                      // "size": ,
                      // AppUtils.KEY_type: ,
                      // "directory": "document",
                      // "path":
                      getSaveDocumentApicall();
                      Navigator.of(context).pop(true);
                    }
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: OutlineButton(
                  child: Text(
                    StringUtils.BtnCancel,
                    style: TextStyle(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  borderSide:
                      BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  highlightElevation: 4.0,
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(30.0)
//                      )
                ),
              ),
            ],
          ),
        ),
      ]),
    );

//      body: Column(
//        children: <Widget>[
//          new Align(
//            alignment: Alignment.topLeft,
//            child: Padding(
//              padding: EdgeInsets.only(left: 10.0, top: 10.0),
//              child: Text("Name",
//                  style: TextStyle(
//                    fontSize: 16.0,
//                    color: Colors.grey,
//                  )),
//            ),
//          ),
//          Align(
//            alignment: Alignment.bottomCenter,
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                Padding(
//                  padding:
//                  EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
//                  child: RaisedButton(
//                    color: Constants.COLOR_CARD_SIGNIN_TITLE,
//                    elevation: 4,
//                    child: Text(
//                      StringUtils.BtnSave,
//                      style: TextStyle(color: Constants.COLOR_TEXT),
//                    ),
//                    splashColor: Constants.COLOR_CARD_FP,
//                    onPressed: () {},
//                  ),
//                ),
//                Padding(
//                  padding:
//                  EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
//                  child: OutlineButton(
//                    child: Text(
//                      StringUtils.BtnCancel,
//                      style: TextStyle(
//                          color: Constants.COLOR_CARD_SIGNIN_TITLE),
//                    ),
//                    onPressed: () {
//                      Navigator.of(context).pop(true);
//                    },
//                    borderSide: BorderSide(
//                        color: Constants.COLOR_CARD_SIGNIN_TITLE),
//                    highlightElevation: 4.0,
////                      shape: new RoundedRectangleBorder(
////                          borderRadius: new BorderRadius.circular(30.0)
////                      )
//                  ),
//                ),
//              ],
//            ),
//          ),
//        ],
//      ),
//    );
  }

  void multiplefiles() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: [
        'pdf',
        'doc',
        'odt',
        'txt',
        'jpg',
        'jpeg',
        'png',
        'xls',
        'xlsx'
      ],
    );
    if (result != null) {
      List<File> mulfiles = result.paths.map((path) => File(path)).toList();

      print(mulfiles);
      setState(() {
        selectedFiles = mulfiles;

        for (int i = 0; i <= selectedFiles.length; i++) {

          uploadUrl(selectedFiles[i]);
        }
      });
    } else {
      // User canceled the picker
    }
  }

  Future<getUrlModel> uploadUrl(File file) async {
//
    try {
      getUrlModel d1 = await getUploadUrl(file);
      print("url to call ${d1.dataUrl}");
      print("file path ${d1.filePath}");

      // String profpath = d1.filePath;
      getUrlandCallAws(d1.dataUrl, file);
      // getUrlandCallAws(d1.dataUrl, enc.length,file);
      setState(() {
        storeUrls.add(d1.dataUrl);
        storeFilePathOfAws.add(d1.filePath);
      });

      return d1;
    } catch (e) {
      print(e);
    }
    // data: Stream.fromIterable(image.map((e) => [e])),
    return null;
  }

  Future<getUrlModel> getUploadUrl(File file) async {
    var bytes = new File(file.path);
    // String filename = basename(file.path);
    var enc = await bytes.readAsBytes();
    // String mimeStr = lookupMimeType(file.path);
    // var filetype = mimeStr.split('/');
    //
    // String filetyp1 = filetype[0];
    // String filetyp2 = filetype[1];
    // String type = filetyp1 + "/" + filetyp2;
    docnames.add(basename(file.path));
    String mimeStr = lookupMimeType(file.path);
    var filetype = mimeStr.split('/');
    //
    String filetyp1 = filetype[0];
    String filetyp2 = filetype[1];
    String type = filetyp1 + "/" + filetyp2;
    storeFilesTypes.add(type);
    storeFilesSize.add(enc.length);



    Response response = await Dio().post(
      AppUtils.GeneralFileUploadUrl,
      data: {
        "status": "inprogress",
        AppUtils.KEY_name: basename(file.path),
        "size": enc.length,
        AppUtils.KEY_type: type,
        "directory": "document"
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return getUrlModel.fromjson(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<int> getUrlandCallAws(String url, File file) async {
    try {
      int d1 = await uploadOnaws(url, file);
      print("status ${d1}");

      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<int> uploadOnaws(String url, File file) async {
    var bytes = new File(file.path);
    String filename = basename(file.path);
    var enc = await bytes.readAsBytes();
    String mimeStr = lookupMimeType(file.path);
    var filetype = mimeStr.split('/');

    String filetyp1 = filetype[0];
    String filetyp2 = filetype[1];
    String type = filetyp1 + "/" + filetyp2;
    print(" call aws url size ${enc.length}, file type $type");

    Uint8List image = File(file.path).readAsBytesSync();

    Options options = Options(contentType: type, headers: {
      'Accept': "*/*",
      'Content-Length': enc.length,
      'Connection': 'keep-alive',
      'User-Agent': 'ClinicPlush' //try to remove these things
    });

    Response response = await Dio().put(url,
        data: Stream.fromIterable(image.map((e) => [e])), options: options);

    return response.statusCode;
  }

  Future<SampleModel> deleteUrlApicall(String pathOfaws) async {
    try {
      SampleModel d1 = await dltUrl(pathOfaws);

      print("deleted ${d1.message}");
      AppUtils.showToastWithMsg(d1.message, ToastGravity.CENTER);
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<SampleModel> dltUrl(String filepath) async {
    Response response = await Dio().post(
      AppUtils.DeleteFileUploadUrl,
      data: {
        "path": [filepath]
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return SampleModel.fromjson(response.data);
  }

  Future<SampleModel> getSaveDocumentApicall() async {
    List testE = List();
    List testN = List();
    List testP = List();
    List testT = List();

    for (int i = 0; i < storeFilePathOfAws.length; i++) {
      testP.add(storeFilePathOfAws[i]);
    }
    // for (int i = 0; i < selectedFiles.length; i++) {
    //   var bytes = new File(selectedFiles[i].path);
    //   var enc = await bytes.readAsBytes();
    //   testE.add(enc.length);
    // }
    for (int i = 0; i < storeFilesSize.length; i++) {
      testE.add(storeFilesSize[i]);
    }
    for (int i = 0; i < docnames.length; i++) {
      // String name = basename(selectedFiles[i].path);
      testN.add(docnames[i]);
    }
    for (int i = 0; i < storeFilesTypes.length; i++) {
      // String mimeStr = lookupMimeType(selectedFiles[i].path);
      // var filetype = mimeStr.split('/');
      // //
      // String filetyp1 = filetype[0];
      // String filetyp2 = filetype[1];
      // String type = filetyp1 + "/" + filetyp2;
      testT.add(storeFilesTypes[i]);
    }
    print("list in save ${testE} and ${testP},${testN},typees ${testT}");

    List<makeJsonArray> jarray = List();
    makeJsonArray addd;
    for (var i = 0; i < docnames.length; i++) {
      addd = new makeJsonArray("uploaded", testN[i], testE[i], testT[i], "document", testP[i]);
      // addd = new makeJsonArray("uploaded", testN[i], testE[i], testT[i], "document", testP[i]);
      jarray.add(addd);
    }
    print("Arraayyy ${jarray}");
    String jsonTags = jsonEncode(jarray);
    try {
      SampleModel d1 = await getSaveDocument(jarray);

      print(d1.message);
      AppUtils.showToastWithMsg(d1.message, ToastGravity.CENTER);
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    return null;
  }

//{"_id":null,"name":"twst","type":"lead","typeId":"604893e55e9e580008df369c","
// files":[{"status":"uploaded",
// "name":"Screenshot_(12).png",
// "size":28761,"type":"image/png","directory":"document","path":"document/dxwl58kn5ouffi.png"}]}
  Future<SampleModel> getSaveDocument(List<makeJsonArray> array) async {
    print(array);
    var resBody = {};
    resBody[AppUtils.KEY_id] = getDocId;
    resBody[AppUtils.KEY_name] = nameController.text;
    resBody[AppUtils.KEY_type] = "lead";
    resBody[AppUtils.KEY_typeId] = getLeadId;
    resBody["files"] = array;

    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.DocumentSaveUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return SampleModel.fromjson(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(loginId);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
      });
    });
  }
}

class makeJsonArray {
  String name;
  String status;
  var size;
  String type;
  String doc;
  String path;

  makeJsonArray(
      this.status, this.name, this.size, this.type, this.doc, this.path);

  Map<String, dynamic> toJson() => {
        "status": status,
        "name": name,
        "size": size,
        "type": type,
        "directory": doc,
        "path": path
      };
}
//web:https://hafooz-uploads-dev.s3.amazonaws.com/document/
// 1a8kac7dgtc.jpeg?
// AWSAccessKeyId=ASIA3BDHAU7OJQ3RVGSP&
// Content-Type=image%2Fjpeg&
// Expires=1589789985&
// Signature=RlH00Wm5w0Hs9UNMIeV2sfSvnS4%3D&
// x-amz-acl=public-read&
// x-amz-security-token=IQoJb3JpZ2luX2VjEIj
// %2F%2F%2F%2F%2F%2F%2F%2F%2F%2F
// wEaCXVzLWVhc3QtMSJGMEQCIGikue0g%2F0T2WQ0M0p1owhakqUIvZCah9SdswvbihDWwAiA3I4GRWwO%2B2La5QNIaNwVLh%2Ft1Ue9%2B4mvzD56CgjWzwyraAQjR%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAEaDDc1ODI3Nzc3NzM3MiIMqKlZM3c1JIlQnIW0Kq4B415UDhUjyM2xg%2Bjactx%2B4DTVio1yKVGFIt8F8%2BkTHgcDZPZyrP75C8yIOuFh%2BrLPmW1lmUec2N2d%2Bcjr8hIn%2BKgq4XFtRwQcCAAvgjZkx4vHRMAPptZBEM%2F9cXaazPO5%2FLnkFYZRqVfFkHzEda2WGCXUk9a5lvoid1BDtnT8oir9B%2B8SbPtdHRg3AUrkpRVF7aLz7%2BmFy4FzhO%2BOj01sb3Edyk9s8UjDWTkBTeeeMIT%2BiPYFOuEBiwjSFGXsY5lTWhnfpL9YRClxXAEZ0E9FxRQDqGds1IyDLA7NyXnAZl9FvjYLy1p%2BmOKcDVLkJsPCPrnbebllAMLkFlQFWKdrhS1G6y7soWR7X3cp%2F07t6EwQOQujND2lCGDeO9jcxK6FGJe6Ow9JE7bgFGRxlCJFij2agencEBs%2BXtx%2BVc43Icw1gz6lPCtNxoXJRUq7O4qzGaQpP88IMNie%2BxonlLFc5l3U6RnrS7WB2Tf6V8XZTCaKjth0GrhGXeBUqMV0ZVFGSE7jj8TFfm9ivRL0GzHTN8fSD65Jjzfj
//web:
// https://hafooz-uploads-dev.s3.amazonaws.com/document/
// h1f8kac7hg0h.jpeg?
// AWSAccessKeyId=ASIA3BDHAU7ONROVWAFG&
// Content-Type=image%2Fjpeg&
// Expires=1589790170&
// Signature=Ouvk5LCQe7vtRiulMMhGDmbvBGk%3D&
// x-amz-acl=public-read&
// x-amz-security-token=IQoJb3JpZ2luX2VjEIn%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJHMEUCIFb2zedz1KBIZtQL7vW0I3D7K0bhiZ%2F%2FGzedrQuN9XWwAiEA8oBwhMf4zpQSV82cDohGKIsOEKa7UIrP7ANM0eFTNpUq2gEI0f%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARABGgw3NTgyNzc3NzczNzIiDGoD2vw6yEf6hdi2cyquAT2j%2FKQm6TLfkR1uo0qWX%2FBMZvOzOZouJJtShfj1KiXYRXbNWL2iOizUwLOk0t2cGpSF2HPkuIek%2BWbq9Negl6rTrdfXf3D3JAWG6tLgqaMXAokDNXGyB%2BEXINWlxQ8t02C0mnAvsYr1ASdiC94d2v2U0pAtJu1ge5Y3Y8Z3kUuCXFj2g5gac%2BAax2XqUcIdtwOmd2Q30lBX2l1gs7h1BoAiEV4vqldygeXkN%2FVsKTCrhIn2BTrgAaOA0rhs2xrFdmpEgXsivO0ZF%2F%2Bv3zG4Oej0j5ixSevhZnvbiBmfZUudR1M4S4vaIiC0c%2FFs3PKGvc6%2Bc9G1us%2B%2FB40guGswpYrFCPEGVS7QWFY44yUxOoYcgUglz2hAse8JMm%2B8JV8VRcVpbNeTnh5msV8IjlV55yFr5bvMUsEaUlGUHzAdc4pEpjovAJLdWH9YPFQWU%2BaVWl0GToHyVib9PrTsDS3%2FRRSdheWNLYEwkjdCyOztrpPYwV17gFx9yCNv7xM1yLXeIbPPSlgBpz8c3vfyYiJoN0%2FmIgdKxHcm
//my :https://hafooz-uploads-dev.s3.amazonaws.com/document/
// 8pn9kac6grxm?
// AWSAccessKeyId=ASIA3BDHAU7OGLUEEH65&
// Content-Type=image%2Fjpeg&
// Expires=1589788459&
// Signature=Le5vD1HKRJpZypjnnPvmR8tQtpA%3D&
// x-amz-acl=public-read&
// x-amz-security-token=IQoJb3JpZ2luX2VjEIf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJHMEUCIGrI2500QnAw1RYrLuYC9mJdmifYgggAgReVXCdGgxOAAiEAqjVi2B1grbGlqjUDSkhZwmSIJxfIvC0ahIvprnBR9kEq2gEI0P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARABGgw3NTgyNzc3NzczNzIiDCbwJP98l%2FgZ%2FxpUbyquATF1qwCrEvEGiv%2FraR4M%2FOBD2BhD8S6slj5GZlSK376b85xmHgqlz%2FzIskHsBe8%2BpJ4FGh7MBGeV4rQPQQnUD29QWiRfsL2ZL3HzW96gdQFWT8J4GYeY616pLdDhdQhEnwkLc3BczRncHQh2dtdGohdY0WoXHJimq5%2BH0NjVNMftSdiKgJ83b7VCJlyLKDNLDPad%2Fdw0Xdvyhs%2Fpr%2FJr%2Bo5v2r02pmQUAPKVTuesBDDW3Yj2BTrgAf4IeYERO%2Fsjgkz796QHpbualwpB4aT3iRab3a54gdzosHk4AWriTI199nBU00wQTrk2XOYuiWkaEJMs6IpmNEHxcM6KTUfaQ%2B0iOeAYyl%2BIaTnuWWnE28tRJ%2BjdD7gNUVGex5OWhL1g%2BbR5RhCgzD6SAs0BnAmAruhUzraEULg%2BA6laX9snYPgnDncdOWjyctAfOZhkM8iCSzbJvWc22cuNQDPriacyIi4kTJcFg1HJY80U3kDTES7oaFyuzjEJUzs1Wv863MOVLj
