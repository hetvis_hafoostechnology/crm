import 'package:crmflutternew/dashboard/fragments/lead/Lead.dart';
import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class LeadFilterPg extends StatefulWidget {
  int selectedtabIndex;
  int selectedSortingIndex;
  String sortType;
  String sortOrder;
  List selectedFiltersource;
  List selectedFilterstage;
  List selectedFilterentity;

  LeadFilterPg(
      {Key key,
      @required this.selectedtabIndex,
      @required this.selectedSortingIndex,
      @required this.sortType,
      @required this.sortOrder,
      @required this.selectedFiltersource,
      @required this.selectedFilterstage,
      @required this.selectedFilterentity})
      : super(key: key);

  @override
  State createState() => new LeadFilterPgState(
      selectedSortingIndex2: selectedSortingIndex,
      selectedtabIndex2: selectedtabIndex,
      sortOrder2: sortOrder,
      sortType2: sortType,
      selectedFiltersource2: selectedFiltersource,
      selectedFilterstage2: selectedFilterstage,
      selectedFilterentity2: selectedFilterentity);
}

class LeadFilterPgState extends State<LeadFilterPg> {
  List sources = [];
  String _sourcesResult;
  List stages = [];
  String _stagesResult;
  List entityType = [];

  // List entityTypeDisplay;
  String _entityTypeResult;
  List sendResult;

//  List myAllLead;

//  final sourceKey = new GlobalKey<FormState>();
//  final stageKey = new GlobalKey<FormState>();
//  final entityTypeKey = new GlobalKey<FormState>();

  int _currentIndex = 0;
  bool myLeadSelected = true;
  bool allLeadSelected = false;
  Color mybtnClr = Colors.blue;
  Color mybtnTxtClr = Colors.white;
  Color allbtnClr = Colors.white;
  Color allbtnTxtClr = Colors.blue;

  List selectedFiltersource2;
  List selectedFilterstage2;
  List selectedFilterentity2;
  int selectedtabIndex2;
  int selectedSortingIndex2;
  String sortType2;
  String sortOrder2;

  LeadFilterPgState(
      {Key key2,
      @required this.selectedSortingIndex2,
      this.selectedtabIndex2,
      this.sortType2,
      this.sortOrder2,
      this.selectedFiltersource2,
      this.selectedFilterstage2,
      this.selectedFilterentity2});

  @override
  void initState() {
    super.initState();

    sendResult = [];
    print(
        "Filterorder${sortOrder2}&type${sortType2}&selectedSOrt${selectedSortingIndex2}&seltab${selectedtabIndex2}"
        "&selected${selectedFiltersource2}&${selectedFilterstage2}&${selectedFilterentity2}");

    _sourcesResult = '';
    _stagesResult = '';
    _entityTypeResult = '';

    sources = selectedFiltersource2;
    print(sources);
    stages = selectedFilterstage2;
    print(stages);
    entityType = selectedFilterentity2;
    print(entityType);
    setState(() {});
  }

//  _saveSources() {
//    var form = sourceKey.currentState;
//    if (form.validate()) {
//      form.save();
//      setState(() {
//        _sourcesResult = sources.toString();
//      });
//    }
//    print("vals$_sourcesResult");
//  }
//
//  _saveStages() {
//    var form = stageKey.currentState;
//    if (form.validate()) {
//      form.save();
//      setState(() {
//        _stagesResult = stages.toString();
//      });
//    }
//    print("vals$_stagesResult");
//  }
//
//  _saveEntityTypes() {
//    var form = entityTypeKey.currentState;
//    if (form.validate()) {
//      form.save();
//      setState(() {
//        _entityTypeResult = entityType.toString();
//      });
//    }
//    print("vals$_entityTypeResult");
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            title: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 10.0),
              child: Text(
                'Filter',
                style: TextStyle(color: Colors.blue, fontSize: 20.0),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_forward,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )),
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                new Align(
                  alignment: Alignment.topLeft,
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: MultiSelectFormField(
                            autovalidate: false,
                            titleText: 'Source',
                            validator: (value) {
                              if (value == null || value.length == 0) {
                                return 'Select options';
                              }
                            },

                            dataSource: [
                              {
                                "display": "Chatbot",
                                "value": "5d75ea5cd6b5c134a6225c0c",
                              },
                              {
                                "display": "Email",
                                "value": "5bd6982226e543121d7bd7bb",
                              },
                              {
                                "display": "Vebal",
                                "value": "5c2757d0df4722d2024a0cc4",
                              },
                              {
                                "display": "Web",
                                "value": "5bd697d026e543121d7bd7ba",
                              },
                              {
                                "display": "Inquiry",
                                "value": "5bd6988126e543121d7bd7bc",
//                                "value": "\"5bd6988126e543121d7bd7bc\"",
                              },
                            ],
                            textField: 'display',
                            valueField: 'value',
                            okButtonLabel: 'OK',
                            cancelButtonLabel: 'CANCEL',
                            // required: true,
                            hintText: 'Select options',
                            initialValue: sources,
                            onSaved: (value) {
                              if (value == null) return;
                              setState(() {
                                sources = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Text(_sourcesResult),
                ),
                new Align(
                  alignment: Alignment.topLeft,
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: MultiSelectFormField(
                            autovalidate: false,
                            titleText: 'Stages',
                            validator: (value) {
                              if (value == null || value.length == 0) {
                                return 'Select options';
                              }
                            },

                            dataSource: [
                              {
                                "display": "Warm",
                                "value": "5bea5ab3ef740214024631d6",
                              },
                              {
                                "display": "Hot",
                                "value": "5bea5abcef740214024631d7",
                              },
                              {
                                "display": "Cold",
                                "value": "5bea5aa3ef740214024631d5",
                              },
                            ],
                            textField: 'display',
                            valueField: 'value',
                            okButtonLabel: 'OK',
                            cancelButtonLabel: 'CANCEL',
                            // required: true,
                            hintText: 'Select options',
                            initialValue: stages,
                            onSaved: (value) {
                              if (value == null) return;
                              setState(() {
                                stages = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Text(_stagesResult),
                ),
                new Align(
                  alignment: Alignment.topLeft,
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: MultiSelectFormField(
                            autovalidate: false,
                            titleText: 'Entity Type',
                            validator: (value) {
                              if (value == null || value.length == 0) {
                                return 'Select options';
                              }
                            },

                            dataSource: [
                              {
                                "display": "Retail Lead",
                                "value": "retail",
                              },
                              {
                                "display": "Institutional Lead",
                                "value": "institutional",
                              },
                            ],
                            textField: 'display',
                            valueField: 'value',
                            okButtonLabel: 'OK',
                            cancelButtonLabel: 'CANCEL',
                            // required: true,
                            hintText: 'Select options',
                            initialValue: entityType,
                            onSaved: (value) {
                              if (value == null) return;
                              setState(() {
                                entityType = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Text(_entityTypeResult),
                ),
                new Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        height: 40.0,
                        child: RaisedButton(
                          color: Colors.white,
                          onPressed: () {
                            stages = [];
                            entityType = [];
                            sources = [];
                            popPg(context, []);
                          },
                          child: Text(
                            "RESET",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        height: 40.0,
                        child: RaisedButton(
                          color: Colors.white,
                          onPressed: () {
//                            _saveStages();
//                            _saveSources();
//                            _saveEntityTypes();
//                            myAllLead = [myLeadSelected, allLeadSelected];
                            sendResult.add(sources); //[0]
                            sendResult.add(stages); //[1]
                            sendResult.add(entityType); //[2]
//                            sendResult.add(myAllLead);

//                            sendResult = sources+stages + entityType+myAllLead;
                            print("result f$sendResult");
                            popPg(context, sendResult);
                            setState(() {});
                          },
                          child: Text(
                            "APPLY",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  popPg(BuildContext ctx, List res) {
//    print(
//        "Filter sentdetails-sorttype${sortType2}&sortorder${sortOrder2}&curseltab${selectedtabIndex2}&selSorting${selectedSortingIndex2}&filtertypeids${res}&selctedfilter${_myActivities}");
    Navigator.pushAndRemoveUntil(
        ctx,
        MaterialPageRoute(
          builder: (context) => Lead(
            sortingIndex3: selectedSortingIndex2,
            sortingOrder3: sortOrder2,
            sortingType3: sortType2,
            currentTabIndex3: selectedtabIndex2,
            source3: res,
            stage3: res,
            entity3: res,
            selfiltersource3: sources,
            selfilterstage3: stages,
            selfilterentity3: entityType,
          ),
        ),
        ModalRoute.withName("/Lead"));
  }
}
