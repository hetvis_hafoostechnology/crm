import 'package:crmflutternew/dashboard/fragments/lead/Lead.dart';
import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class LeadSortPg extends StatefulWidget {
  int selectedtabIndex;
  int selectedSortingIndex;
  List selectedSourceFilter,selectedStageFilter,selectedEntityFilter,types,typestage,typeentity;

  LeadSortPg(
      {Key key,
      @required this.selectedtabIndex,
      @required this.selectedSortingIndex,
      @required this.selectedStageFilter,
      @required this.selectedSourceFilter,
      @required this.selectedEntityFilter,
      @required this.types,
      @required this.typestage,
      @required this.typeentity})
      : super(key: key);

  @override
  State createState() => new LeadSortPgState(
      selTabIndex: selectedtabIndex,
      selSortIndex: selectedSortingIndex,
      selStageFilter: selectedStageFilter,
      selSourceFilter: selectedSourceFilter,
      selEntityFilter: selectedEntityFilter,
      typeids: types,
      typeidstage: typestage,
      typeidentity: typeentity);
}

class LeadSortPgState extends State<LeadSortPg> {
  String result;
  int selTabIndex;
  int prevSelectedSorting = 0;
  int selSortIndex;
  List selStageFilter,selSourceFilter,selEntityFilter,typeids,typeidstage,typeidentity;

  LeadSortPgState(
      {Key key2,
      @required this.selTabIndex,
      this.selSortIndex,
      this.selStageFilter,
      this.selSourceFilter,
      this.selEntityFilter,
      this.typeids,
      this.typeidstage,
      this.typeidentity});

  @override
  void initState() {
    super.initState();
    prevSelectedSorting = selSortIndex;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 10.0),
              child: Text(
                'Sort By',
                style: TextStyle(color: Colors.blue, fontSize: 20.0),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  child: (prevSelectedSorting == 0
                      ? Text("Latest", style: TextStyle(color: Colors.blue))
                      : Text("Latest")),
                  onPressed: () {
                    prevSelectedSorting = 0;
                    poppage(context, "updatedBy.time", "desc");
                  },
                ),
                FlatButton(
                  child: (prevSelectedSorting == 1
                      ? Text("Oldest", style: TextStyle(color: Colors.blue))
                      : Text("Oldest")),
                  onPressed: () {
                    prevSelectedSorting = 1;
                    poppage(context, "updatedBy.time", "asc");
                  },
                ),
                FlatButton(
                  child: (prevSelectedSorting == 2
                      ? Text("Name (A-Z)", style: TextStyle(color: Colors.blue))
                      : Text("Name (A-Z)")),
                  onPressed: () {
                    prevSelectedSorting = 2;
                    poppage(context, "name", "asc");
                  },
                ),
                FlatButton(
                  child: (prevSelectedSorting == 3
                      ? Text("Name (Z-A)", style: TextStyle(color: Colors.blue))
                      : Text("Name (Z-A)")),
                  onPressed: () {
                    prevSelectedSorting = 3;
                    poppage(context, "name", "desc");
                  },
                ),
                FlatButton(
                  child: (prevSelectedSorting == 4
                      ? Text("Email (A-Z)",
                          style: TextStyle(color: Colors.blue))
                      : Text("Email (A-Z)")),
                  onPressed: () {
                    prevSelectedSorting = 4;
                    poppage(context, "email", "asc");
                  },
                ),
                FlatButton(
                  child: (prevSelectedSorting == 5
                      ? Text("Email (Z-A)",
                          style: TextStyle(color: Colors.blue))
                      : Text("Email (Z-A)")),
                  onPressed: () {
                    prevSelectedSorting = 5;
                    poppage(context, "email", "desc");
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  poppage(BuildContext ctx, String type, String order) {
    print(
        "Lead Sort sentdetails-sorttype${type}&sortorder${order}&curseltab${selTabIndex}&selSorting${prevSelectedSorting}&filtertypeids${typeids}&selctedfilter${selEntityFilter}");

    Navigator.pushAndRemoveUntil(
        ctx,
        MaterialPageRoute(
          builder: (context) => Lead(
            sortingType3: type,
            sortingOrder3: order,
            currentTabIndex3: selTabIndex,
            sortingIndex3: prevSelectedSorting,
            source3: typeids,
            stage3: typeidstage,
            entity3: typeidentity,
            selfiltersource3: selSourceFilter,
            selfilterstage3: selStageFilter,
            selfilterentity3: selEntityFilter,
          ),
        ),
        ModalRoute.withName("/Lead"));
  }
}
