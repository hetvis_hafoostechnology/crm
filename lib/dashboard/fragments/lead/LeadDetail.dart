import 'dart:convert';
import 'dart:io';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/dashboard/InstitutionalLeadScreen.dart';
import 'package:crmflutternew/dashboard/RetailLeadScreen.dart';
import 'package:crmflutternew/dashboard/fragments/lead/DocumentDialogue.dart';
import 'package:crmflutternew/dashboard/fragments/lead/Lead.dart';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:crmflutternew/model/document/DetailLeadDocumentList.dart';
import 'package:crmflutternew/model/document/FilesDetails.dart';
import 'package:crmflutternew/model/document/LeadDocumentList.dart';
import 'package:crmflutternew/model/lead/GetLeadDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/model/dropdown/tag/TaggedDropdown.dart';
import 'package:crmflutternew/util/ColorUtil.dart';
import 'package:intl/intl.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

//
// 5de503e6949ebb846a106dac
class LeadDetail extends StatefulWidget {
  String leadId;

  LeadDetail({Key key, @required this.leadId}) : super(key: key);

  static const String routeName = '/LeadDetail';

  @override
  State<StatefulWidget> createState() {
    return LeadDetailstate(getLeadId: leadId);
  }
}

class LeadDetailstate extends State<LeadDetail> {
  String leadName = "",
      name = "",
      website = "",
      mobile = "",
      email = "",
      title = "",
      salutation = "",
      stage = "",
      phone = "",
      owner = "",
      source = "",
      entity = "",
      updatedby = "",
      createdby = "",
      add1 = "",
      add2 = "",
      add3 = "",
      city = "",
      pincode = "",
      state = "",
      country = "";
  String userid;
  String token;
  bool add2Visible,
      add3Visible,
      phoneVisible,
      webVisible,
      leadVisible,
      wholeAddVisible,
      ownerVisible,
      mobVisible,
      emailVisible;
  String getLeadId;
  List savedTagsname;
  ProgressDialog pr;
  List<DetailLeadDocumentList> leadlist = List();
  List<FilesDetails> fileList = List();

  bool isLoadingDoc = false;
  bool isFirstApiCalledDoc = false;
  bool hasMoreDoc = true;
  int pageCountDoc = 0;

  LeadDetailstate({Key key2, @required this.getLeadId});

  @override
  void initState() {
    add2Visible = false;
    add3Visible = false;
    phoneVisible = false;
    mobVisible = false;
    emailVisible = false;
    ownerVisible = false;
    webVisible = false;
    leadVisible = false;
    wholeAddVisible = false;
    setState(() {});
//    getStringValuesSF();
    print("received$getLeadId");
    setData();
    super.initState();

  }

  Widget _ListView(List<DetailLeadDocumentList> listt) {
    if (!isFirstApiCalledDoc) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (listt == null || listt.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
          itemCount: leadlist.length + 1,
          itemBuilder: (context, index) {
            if (index == leadlist.length - 1) {
              getListApicall();
            }
            if (index == leadlist.length) {
              if (hasMoreDoc) {
                return CupertinoActivityIndicator();
              } else {
                return Container();
              }
            }
            return buildlist(listt[index].name, listt[index].files.datas);
          });
    }
  }

  Widget _ListViewFiles(List<FilesDetails> files) {
    if (!isFirstApiCalledDoc) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (files == null || files.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
          itemCount: files.length + 1,
          itemBuilder: (context, index) {
            if (index == files.length - 1) {
              getListApicall();
            }
            if (index == files.length) {
              if (hasMoreDoc) {
                return CupertinoActivityIndicator();
              } else {
                return Container();
              }
            }
            return buildlistFiles(files[index].fileName, files[index].fileSize,
                files[index].filePAth);
          });
    }
  }

  Widget buildlist(String name, List<FilesDetails> fileList) {
    return Card(
      child: Column(
        children: [
          Row(
            children: <Widget>[
              Padding(
                padding:
                    new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  name,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 300,
            child: _ListViewFiles(fileList),
          ),
          // Padding(
          //   padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
          //   child: Text(
          //     filename,
          //     style: TextStyle(
          //       fontSize: 14.0,
          //       fontWeight: FontWeight.bold,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

  Widget buildlistFiles(String filename, int filesize, String path) {
    return Card(
      child: Row(
        children: <Widget>[
          Padding(
            padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: Text(
              filename + " | " + filesize.toString(),
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.black,
              ),
            ),
          ),
          Padding(
              padding: new EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
              child: RaisedButton(
                padding: EdgeInsets.all(0),
                child: Text(StringUtils.TitleDownload, style: TextStyle(fontSize: 11.0)),
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    side: BorderSide(color: Constants.COLOR_CARD_BUTTON)),
                color: Colors.white,
                textColor: Constants.COLOR_CARD_BUTTON,
                onPressed: () {
                  AppUtils.launchInBrowser(
                      "https://s3.amazonaws.com/hafooz-uploads-dev/" + path);
                },
              )),
        ],
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Material(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Align(
            alignment: Alignment.topRight,
            child: PopupMenuButton(
              onSelected: (result) {
                if (result == "0") {
                  if (entity == "retail") {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                RetailLeadScreen(isfrom: getLeadId)));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                InstitutionalLeadScreen(isfrom: getLeadId)));
                  }
                }
                if (result == "2") {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(StringUtils.TxtDelete),
                        content: Text(StringUtils.TxtSurityLeadDlt),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(StringUtils.BtnYes),
                            onPressed: () {
                              getDeleteApiRes();
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(StringUtils.BtnCancel),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                }
              },
              icon: Icon(Icons.settings),
              itemBuilder: (context) => [
                PopupMenuItem(
                  value: "0",
                  child: Text(
                    StringUtils.TxtEdit,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular),
                  ),
                ),
//                PopupMenuItem(
//                  value: "1",
//                  child: Text("Change to Opportunity"),
//                ),
                PopupMenuItem(
                  value: "2",
                  child: Text(
                    StringUtils.TxtDelete,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular),
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(StringUtils.TitleLeadName,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text((leadName == null ? "" : leadName),
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
            visible: leadVisible,
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleSalutaion,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text((salutation == null ? "" : salutation),
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleName,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(name,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleEmail,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(email,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
            visible: emailVisible,
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleMob,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(mobile,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
            visible: mobVisible,
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleWebsite,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(website,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
            visible: webVisible,
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleTitle,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(title,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitlePhone,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(phone,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
            visible: phoneVisible,
          ),
          Visibility(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleOwner,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
              ],
            ),
            visible: ownerVisible,
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(owner,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Thin,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleSource,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(source,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleLeadStage,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(stage,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleEntityType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(entity,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleCreatedBy,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(createdby,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleUpdatedBy,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(updatedby,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          savedTagsname == null
              ? Text("")
              : Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleTag,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
          savedTagsname == null
              ? Text("")
              : Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Wrap(
                    children: List<Widget>.generate(
                      savedTagsname.length,
                      (int index) {
                        return ChoiceChip(
                          backgroundColor: Constants.COLOR_CARD_FP,
                          label: Text(
                            savedTagsname[index],
                            style:
                                TextStyle(color: Constants.COLOR_CARD_BUTTON),
                          ),
                          selected: false,
                          onSelected: (bool selected) {},
                        );
                      },
                    ).toList(),
                  )),
          Visibility(
            visible: wholeAddVisible,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize16,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress1,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(add1 == null ? "" : add1,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Visibility(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 0.0, top: 15.0),
                        child: Text(StringUtils.TitleAddress2,
                            style: TextStyle(
                                fontSize: AppUtils.FontSize,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Bold,
                                color: ColorUtil.Grey)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text(add2,
                            style: TextStyle(
                                fontSize: AppUtils.FontSize,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Regular,
                                color: ColorUtil.Black)),
                      ),
                    ],
                  ),
                  // visible: add2Visible,
                ),
                Visibility(
                  // visible: add3Visible,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 0.0, top: 15.0),
                        child: Text(StringUtils.TitleAddress3,
                            style: TextStyle(
                                fontSize: AppUtils.FontSize,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Bold,
                                color: ColorUtil.Grey)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text(add3 != null ? add3 : "",
                            style: TextStyle(
                                fontSize: AppUtils.FontSize,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Regular,
                                color: ColorUtil.Black)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleCity,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(city,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleZipcode,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(pincode,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleState,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(state,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleCountry,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(country,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
              ],
            ),
          ),
          Padding(
            padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      StringUtils.TitleDocument,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize16,
                          color: ColorUtil.Black,
                          fontWeight: AppUtils.SemiBold,
                          fontFamily: AppUtils.FontName),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    new Spacer(),
                    IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () {
                        _navigateToDoc(context);
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 300,
            child: _ListView(leadlist),
          ),
        ],
      ),
    );
  }

  _navigateToDoc(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => DocumentDialogue(leadId: getLeadId,docname: null,list: null,docId: null,)),
    );
  }

//  getStringValuesSF() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    //Return String
//    String stringValue = prefs.getString(StringUtils.spLeadId);
//    print("select lead is $stringValue");
//    return stringValue;
//  }

  Future<GetLeadDetails> getAPicall() async {
    pr.show();
    try {
      GetLeadDetails d1 = await leadDetailsApi();

      if (d1.name != null) {
        leadVisible = true;
        leadName = d1.name;
        print("lead$leadName");
      }
      if (d1.firstName == null) {
        d1.firstName = "";
      } else if (d1.lastName == null) {
        d1.lastName = "";
      } else {
        name = d1.firstName + " " + d1.lastName;
        print(name);
      }
      if (d1.website != null) {
        webVisible = true;
        website = d1.website;
        print(website);
      }
      if (d1.mobile != null && d1.mobile != "") {
        mobVisible = true;
        mobile = d1.mobile;
        print(mobile);
      }
      if (d1.email != null && d1.email != "") {
        emailVisible = true;
        email = d1.email;
        print(email);
      }

      title = d1.title;
      print(title);
      salutation = d1.salutation;
      print(salutation);
      stage = d1.stage;
      print(stage);
      if (d1.telephone != null && d1.telephone != "") {
        phoneVisible = true;
        phone = d1.telephone;
        print(phone);
      }
      if (d1.owner != null) {
        ownerVisible = true;
        owner = d1.owner;
        print(owner);
      }

      source = d1.source;
      print(source);
      entity = d1.entityType;
      print(entity);
//
//      String date=d1.updatedByTime.substring(0,9);
//      String time=d1.updatedByTime.substring(11,19);
//     var  dd = DateFormat('yyyy/MM/dd').format(date);
//      print("$date and $time and $dd");
      String getCreatedDate = Constants.changeDateFromate(d1.createdByTime);
      String getUpdatedDate = Constants.changeDateFromate(d1.updatedByTime);
      if (d1.updatedByName == null) {
        updatedby = "" + "-" + getUpdatedDate;
      } else if (d1.updatedByTime == null) {
        updatedby = d1.updatedByName + "-" + "";
      } else {
        updatedby = d1.updatedByName + "-" + getUpdatedDate;
        print(updatedby);
      }
      if (d1.createdByName == null) {
        createdby = "" + "-" + getCreatedDate;
      } else if (d1.createdByTime == null) {
        createdby = d1.createdByName + "-" + "";
      } else {
        createdby = d1.createdByName + "-" + getCreatedDate;
        print(createdby);
      }
      print("tags${d1.tagids}");
      List saveid = List();

      if (d1.tagids != null) {
        d1.tagids.forEach((data) {
          String tagid = data;
          saveid.add(tagid);
        });
        getDropDownApicallTag(saveid);
      } else {
        saveid = null;
      }
      if (d1.address == null) {
        wholeAddVisible = false;
      } else {
        wholeAddVisible = true;
        add1 = d1.address.addressLine1;
        print(add1);
        if (add2 != null) {
          // add2Visible = true;
          add2 = d1.address.addressLine2;
          print(add2);
        }
        if (add3 != null) {
          // add3Visible = true;
          add3 = d1.address.addressLine3;
          print(add3);
        } else {
          // add3Visible = false;
        }
        city = d1.address.city;
        print(city);
        pincode = d1.address.zipCode;
        print(pincode);
        state = d1.address.state;
        print(state);
        country = d1.address.country;
        print(country);
      }
      pr.hide();
      setState(() {});
      return d1;
    } catch (e) {
      print("here$e");
    }
  }

  Future<GetLeadDetails> leadDetailsApi() async {
    Response response = await Dio().post(
      AppUtils.LeadGetDetailsUrl,
      data: {AppUtils.KEY_id: getLeadId},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response);
    return GetLeadDetails.fromJsonResponse(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<SampleModel> getDeleteApiRes() async {
    try {
      SampleModel d1 = await deleteLeadApi();
      print(d1.message);
      AppUtils.showToastWithMsg(d1.message, ToastGravity.CENTER);
      Navigator.of(context).pop(true);
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<SampleModel> deleteLeadApi() async {
    Response response = await Dio().post(
      AppUtils.LeadDeleteUrl,
      data: {AppUtils.KEY_id: getLeadId, AppUtils.KEY_updatedBy: userid},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return SampleModel.fromjson(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<TaggedDropdown> TagTypedropdown() async {
    Response response = await Dio().post(
      AppUtils.TagDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("tag dp ${response.data[AppUtils.KEY_DATA]}");
    return TaggedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  Future<TaggedDropdown> getDropDownApicallTag(List gettagid) async {
    try {
      TaggedDropdown d1 = await TagTypedropdown();
      List save1 = List();
      List saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.name;
        String id = data.id;
        saveid.add(id);
        gettagid.forEach((element) {
          if (element == id) {
            save1.add(titlename);
          }
        });
      });

      savedTagsname = save1;
      print("tag names${savedTagsname}");
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);

    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        getAPicall();
        getListApicall();
      });
    });
  }

  Future<LeadDocumentList> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<DetailLeadDocumentList> retaillist = List();
    List<FilesDetails> filess = List();
    try {
      if (!isLoadingDoc && hasMoreDoc) {
        isLoadingDoc = true;
        isFirstApiCalledDoc = true;

        LeadDocumentList d1 = await listapi();

        if (d1 == null) {
          leadlist = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });
          leadlist.addAll(retaillist);

          if (d1.paginationModel <= leadlist.length) {
            hasMoreDoc = false;
          } else {
            hasMoreDoc = true;
          }
          pageCountDoc++;
          isLoadingDoc = false;
        }
        setState(() {});
        return d1;
      }
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }
    return null;
  }
  Future<LeadDocumentList> listapi() async {
    Response response = await Dio().post(
      AppUtils.DocumentListUrl,
      data: {
        AppUtils.KEY_pagination: {
          AppUtils.KEY_page: pageCountDoc,
          AppUtils.KEY_limit: 5
        },
        AppUtils.KEY_typeId: getLeadId,
        AppUtils.KEY_type: "lead"
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return LeadDocumentList.fromjson(response.data);
  }
}
