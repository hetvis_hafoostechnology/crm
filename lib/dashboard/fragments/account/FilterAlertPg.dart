import 'package:flutter/material.dart';
import 'package:crmflutternew/dashboard/fragments/account/AccountList.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class FilterAlertPg extends StatefulWidget {
  int selectedtabIndex;
  int selectedSortingIndex;
  String sortType;
  String sortOrder;
  List selectedFilter;

  FilterAlertPg(
      {Key key,
      @required this.selectedtabIndex,
      @required this.selectedSortingIndex,
      @required this.sortType,
      @required this.sortOrder,
      @required this.selectedFilter})
      : super(key: key);

  @override
  State createState() => new FilterAlertPgState(
      selectedSortingIndex2: selectedSortingIndex,
      selectedtabIndex2: selectedtabIndex,
      sortOrder2: sortOrder,
      sortType2: sortType,
      selectedFilter2: selectedFilter);
}

class FilterAlertPgState extends State<FilterAlertPg> {
  List _myActivities;
  List selectedFilter2;
  List sendResult;
  String _myActivitiesResult;

//  final formKey = new GlobalKey<FormState>();
  List<dropItems> users = <dropItems>[
    const dropItems('SelectAll'),
    const dropItems('Institutional'),
    const dropItems('Partner'),
    const dropItems('Individual'),
  ];

  int selectedtabIndex2;
  int selectedSortingIndex2;
  String sortType2;
  String sortOrder2;

  FilterAlertPgState(
      {Key key2,
      @required this.selectedSortingIndex2,
      this.selectedtabIndex2,
      this.sortType2,
      this.sortOrder2,
      this.selectedFilter2});

  @override
  void initState() {
    super.initState();

    sendResult = [];
    _myActivitiesResult = '';
    print(
        "order${sortOrder2}&type${sortType2}&selectedSOrt${selectedSortingIndex2}&seltab${selectedtabIndex2}");
    _myActivities = selectedFilter2;
    setState(() {});
  }

//  _saveForm() {
//    var form = formKey.currentState;
//    if (form.validate()) {
//      form.save();
//      setState(() {
//        _myActivitiesResult = _myActivities.toString();
//      });
//    }
//    print("vals$_myActivitiesResult");
//  }

  @override
  Widget build(BuildContext context) {
    var selectedUser = null;
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 10.0),
              child: Text(
                'Filter',
                style: TextStyle(color: Colors.blue, fontSize: 20.0),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_forward,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )),
      body: Column(
        children: <Widget>[
//          new Align(
//            alignment: Alignment.topLeft,
//            child: Padding(
//              padding: EdgeInsets.only(left: 10.0, bottom: 10.0, top: 20.0),
//              child: Text("Type",
//                  style: TextStyle(
//                    fontSize: 16.0,
//                  )),
//            ),
//          ),
          new Align(
            alignment: Alignment.topLeft,
            child: Form(
//              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: MultiSelectFormField(
                      autovalidate: false,
                      titleText: 'Type',
                      validator: (value) {
                        if (value == null || value.length == 0) {
                          return 'Please select one or more options';
                        }
                      },
                      dataSource: [
//                        {
//                          "display": "Select all",
//                          "value": "all",
//                        },
                        {
                          "display": "Institutional",
                          "value": "5bf6399ca9d55e3f6662d3c6",
                        },
                        {
                          "display": "Individual",
                          "value": "5bf6397aa9d55e3f6662d3c5",
                        },
                        {
                          "display": "Partner",
                          "value": "5bf639a9a9d55e3f6662d3c7",
                        },
                      ],
                      textField: 'display',
                      valueField: 'value',
                      okButtonLabel: 'OK',
                      cancelButtonLabel: 'CANCEL',
                      // required: true,
                      hintText: 'Select options',
                      initialValue: _myActivities,
                      onSaved: (value) {
                        if (value == null) return;
                        setState(() {
                          _myActivities = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),

          Container(
            padding: EdgeInsets.all(16),
            child: Text(_myActivitiesResult),
          ),
          new Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      onPressed: () {
                        // Close the screen and return "Yep!" as the result.
//                        _saveForm();
                        sendResult.add(_myActivities);
//                        Navigator.pop(context, sendResult);
                        popPg(context, sendResult);
                      },
                      child: Text('Apply'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      onPressed: () {
                        // Close the screen and return "Nope!" as the result.
                        _myActivities = [];
                        popPg(context, []);
                      },
                      child: Text('Reset'),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  popPg(BuildContext ctx, List res) {
    print(
        "Filter sentdetails-sorttype${sortType2}&sortorder${sortOrder2}&curseltab${selectedtabIndex2}&selSorting${selectedSortingIndex2}&filtertypeids${res}&selctedfilter${_myActivities}");
    Navigator.pushAndRemoveUntil(
        ctx,
        MaterialPageRoute(
          builder: (context) => Account(
            sortingIndex3: selectedSortingIndex2,
            sortingOrder3: sortOrder2,
            sortingType3: sortType2,
            currentTabIndex3: selectedtabIndex2,
            source3: res,
            selfilter3: _myActivities,
          ),
        ),
        ModalRoute.withName("/Account"));
  }
}

class dropItems {
  const dropItems(this.name);

  final String name;
}
