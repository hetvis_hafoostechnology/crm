import 'package:crmflutternew/dashboard/fragments/account/AccountCreation.dart';
import 'package:crmflutternew/model/account/GetAccountDetailModel.dart';
import 'package:crmflutternew/model/account/NoteDetail.dart';
import 'package:crmflutternew/model/account/NoteList.dart';
import 'package:crmflutternew/dashboard/fragments/note/AddNote.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/ColorUtil.dart';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:crmflutternew/model/dropDown/SampleDropDown.dart';
import 'package:crmflutternew/model/dropdown/tag/TaggedDropdown.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountDetail extends StatefulWidget {
  String accId;

  AccountDetail({Key key, @required this.accId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AccountDetailState(getAccId: accId);
  }
}

class AccountDetailState extends State<AccountDetail> {
  String accName = "",
      type = "",
      segement = "",
      annualReve = "",
      accRep = "",
      website = "",
      international = "",
      industry = "",
      entityType = "",
      createdby = "",
      updatedby = "",
      add1 = "",
      add2 = "",
      add3 = "",
      city = "",
      pincode = "",
      state = "",
      country = "";

  String userid;
  List savedTagsname;
  List emailDept;
  List phoneDept;
  List departmentDept;
  List tableDetail;
  List tableRelate;
  List tableTime;
  List tableNoteId;
  List tableoppid;

  String selectedChoice = "";
  String token;
  String getAccId;
  bool wholeAddVisible;
  bool noteVisibility;
  List photos;
  ProgressDialog pr;

  AccountDetailState({Key key2, @required this.getAccId});

  List<NoteDetail> listt = List();

  @override
  void initState() {
    noteVisibility = false;
    wholeAddVisible = false;

    getStringValuesSF();
    setData();
    setState(() {});
    print("in acc detail pg");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Material(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Text("Account",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Bold,
                        color: ColorUtil.Black)),
              ),
              PopupMenuButton(
                onSelected: (result) {
                  if (result == "0") {
                    //edit
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                AccountCreation(isfrom: getAccId)));
                  }

                },
                icon: Icon(Icons.more_vert),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: "0",
                    child: Text(
                      StringUtils.TxtEdit,
                      style: TextStyle(
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular),
                    ),
                  ),

                ],
              ),
            ],
          ),

          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 5.0),
            child: Text(StringUtils.TitleName,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(accName == null ? "" : accName,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(type == null ? "" : type,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleSegment,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(segement == null ? "" : segement,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleAccRep,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(accRep == null ? "" : accRep,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleAnlRev,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(annualReve == null ? "" : annualReve,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleWebsite,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(website == null ? "" : website,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleInternational,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(international == null ? "" : international,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleIndustry,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(industry == null ? "" : industry,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleEntityType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(entityType == null ? "" : entityType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleCreatedBy,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(createdby == null ? "" : createdby,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, top: 15.0),
            child: Text(StringUtils.TitleUpdatedBy,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0),
            child: Text(updatedby == null ? "" : updatedby,
                style: TextStyle(
                    fontSize: AppUtils.FontSize,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Regular,
                    color: ColorUtil.Black)),
          ),
          savedTagsname == null
              ? Text("")
              : Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleTag,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
          savedTagsname == null
              ? Text("")
              : Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Wrap(
                    children: List<Widget>.generate(
                      savedTagsname.length,
                      (int index) {
                        return ChoiceChip(
                          backgroundColor: Constants.COLOR_CARD_FP,
                          label: Text(
                            savedTagsname[index],
                            style:
                                TextStyle(color: Constants.COLOR_CARD_BUTTON),
                          ),
                          selected: false,
                          onSelected: (bool selected) {},
                        );
                      },
                    ).toList(),
                  )),
          departmentDept == null
              ? Text("")
              : FittedBox(
                  child: DataTable(
                    showCheckboxColumn: false,
                    columns: [
                      DataColumn(
                          label: Text(
                        'Department',
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                      DataColumn(
                          label: Text(
                        'Phone',
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                      DataColumn(
                          label: Text(
                        'Email',
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                    ],
                    rows: List<DataRow>.generate(
                      emailDept.length,
                      (index) => DataRow(
                        cells: [
                          DataCell(Text(departmentDept[index] == null
                              ? '-'
                              : departmentDept[index])),
                          DataCell(Text(phoneDept[index] == null
                              ? '-'
                              : phoneDept[index])),
                          DataCell(Text(emailDept[index] == null
                              ? '-'
                              : emailDept[index]))
                        ],
                        selected: false,
                        onSelectChanged: (bool value) {},
                      ),
                    ),
                  ),
                ),

          Visibility(
            visible: wholeAddVisible,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize16,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress1,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(add1 == null ? "" : add1,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress2,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(add2 == null ? "" : add2,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleAddress3,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(add3 != null ? add3 : "-",
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleCity,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(city == null ? "" : city,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleZipcode,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(pincode == null ? "" : pincode,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleState,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(state == null ? "" : state,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0, top: 15.0),
                  child: Text(StringUtils.TitleCountry,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: ColorUtil.Grey)),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(country == null ? "" : country,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: ColorUtil.Black)),
                ),
//                Padding(
//                  padding: new EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
//                  child: Row(
//                      children: [
//                        Expanded(child: Text('Detail')),
//                        Expanded(child: Text('Related To')),
//                        Expanded(child: Text('Time')),
//                      ]
//                  ),
//                ),
//
//                Padding(
//                  padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
//                    child:FutureBuilder<NoteList>(
//                      future: getListApicall(),
//                      builder: (context, snapshot) {
//                        if (snapshot.hasData) {
//                          List<NoteDetail> data = snapshot.data.datas;
//                          return _ListView(data);
//                        } else if (snapshot.hasError) {
//                          return Text("${snapshot.error}");
//                        }
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      },
//                    ),
//                ),
              ],
            ),
          ),
          Padding(
            padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Notes",
                      style: TextStyle(
                          fontSize: AppUtils.FontSize16,
                          color: ColorUtil.Black,
                          fontWeight: AppUtils.SemiBold,
                          fontFamily: AppUtils.FontName),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    new Spacer(),
                    IconButton(
                      icon: Icon(
                        Icons.add_circle_sharp,
                        color: Constants.COLOR_CARD_BUTTON,
                      ),
                      onPressed: () {
//                              if(noteVisibility==false) {
//                                noteVisibility = true;
//                              }
//                              else{
//                                noteVisibility=false;
//                              }

                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (_context) => AddNote(
                                    id: null,
                                    accountId: getAccId,
                                    notedetailtext: null,
                                    opportunityId: null,
                                    token: token,
                                  )),
                          // call getlist
                        );
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
//      Visibility(
//        child: TextField(
//          keyboardType:TextInputType.multiline,
//          maxLines: 5,
//          style: TextStyle(fontSize:AppUtils.FontSize15,fontFamily: AppUtils.FontName,fontWeight: AppUtils.Regular,color: ColorUtil.Black),
//          autofocus: false,
//          decoration: InputDecoration(
//            contentPadding: EdgeInsets.all(10.0),
//            border:
//            OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
//          ),
//        ),
//        visible: noteVisibility,
//      ),
//          noteVisibility==true?
          tableDetail == null
              ? Text("There is no data to display")
              : FittedBox(
                  child: DataTable(
                    showCheckboxColumn: false,
                    columns: [
                      DataColumn(
                          label: Text(
                        StringUtils.TitleDetail,
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                      DataColumn(
                          label: Text(
                        StringUtils.Titlerelatedto,
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                      DataColumn(
                          label: Text(
                        StringUtils.TitleTime,
                        style: TextStyle(color: Constants.COLOR_CARD_BUTTON),
                      )),
                    ],
                    rows: List<DataRow>.generate(
                      tableDetail.length,
                      (index) => DataRow(
                        cells: [
                          DataCell(
                            Container(
                              width: 140,
                              child: Text(
                                tableDetail[index] == null
                                    ? '-'
                                    : tableDetail[index],
                                maxLines: 5,
                              ),
                            ),
                          ),
                          DataCell(Text(tableRelate[index] == null
                              ? '-'
                              : tableRelate[index])),
                          DataCell(
                            Text(tableTime[index] == null
                                ? '-'
                                : Constants.changeDateFromate(
                                    tableTime[index])),
                            showEditIcon: true,
                            onTap: () {
                              print("details text${tableDetail[index]}");
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_context) => AddNote(
                                        id: tableNoteId[index],
                                        notedetailtext: tableDetail[index],
                                        accountId: getAccId,
                                        opportunityId: tableoppid[index],
                                        token: token,
                                      )));
                            },
                          )
                        ],
                        selected: false,
                        onSelectChanged: (bool value) {},
                      ),
                    ),
                  ),
                ),

//              :Container(height:0),
        ],
      ),
    );
  }

//CALL DEPARTMNET API
  Future<SampleDropDown> depts() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f343e41c8428d23654c0"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getdepts(List dept) async {
    try {
      SampleDropDown d1 = await depts();
      print("met${dept}");
      List save1 = List();
      d1.datas.forEach((data) {
        dept.forEach((element) {
          if (element == data.id) {
            print("matched${data.text}");
            save1.add(data.text);
          }
        });
      });
      print("deptlist$departmentDept");
      departmentDept = save1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String stringValue = prefs.getString(StringUtils.spLeadId);
    print("select lead is $stringValue");
    return stringValue;
  }

  Future<GetAccountDetailModel> getAPicall() async {
    pr.show();
    print("acc${getAccId}&token${token}");
    try {
      GetAccountDetailModel d1 = await accDetailsApi();

      accName = d1.name;
      print("name$accName");
      type = d1.type;
      print("type$type");
      if (d1.segment != null) {
        segement = d1.segment;
      }
      print("seg$segement");
      if (d1.accRepresentative != null) {
        accRep = d1.accRepresentative;
        print("acc$accRep");
      }

      if (d1.annualRevenue != null) {
        annualReve = d1.annualRevenue;
      }
      print("ar$annualReve");
      if (d1.website != null) {
        website = d1.website;
      }
      print("web$website");
      if (d1.intenational != null) {
        international = d1.intenational;
      }
      print("in$international");
      if (d1.industry != null) {
        industry = d1.industry;
      }
      print("ind$industry");
      entityType = d1.entityType;
      print("Et$entityType");

      String getCreatedDate = Constants.changeDateFromate(d1.createdByTime);
      String getUpdatedDate = Constants.changeDateFromate(d1.updatedByTime);

      createdby = d1.createdByUser + "-" + getCreatedDate;
      updatedby = d1.updatedByUser + "-" + getUpdatedDate;
      if (d1.address == null) {
        wholeAddVisible = false;
      } else {
        wholeAddVisible = true;
        add1 = d1.address.addressLine1;
        add2 = d1.address.addressLine2;
        add3 = d1.address.addressLine3;
        city = d1.address.city;
        pincode = d1.address.zipCode;
        state = d1.address.state;
        country = d1.address.country;
      }
      print("add${d1.address}");
      print("tags${d1.tagids}");
      List saveid = List();

      if (d1.tagids != null) {
        d1.tagids.forEach((data) {
          String tagid = data;
          saveid.add(tagid);
        });
        getDropDownApicallTag(saveid);
      } else {
        saveid = null;
      }
      print("dept${d1.department}");
      if (d1.contacts != null) {
        List email = List();
        List dept = List();
        List phone = List();
        d1.contacts.datas.forEach((element) {
          email.add(element.email);
          phone.add(element.phone);
          dept.add(element.department);
        });
        emailDept = email;
        phoneDept = phone;
        getdepts(dept);
      } else {
        emailDept = null;
        phoneDept = null;
        departmentDept = null;
      }
      pr.hide();
      setState(() {});
      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<TaggedDropdown> TagTypedropdown() async {
    Response response = await Dio().post(
      AppUtils.TagDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("tag dp ${response.data[AppUtils.KEY_DATA]}");
    return TaggedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  Future<TaggedDropdown> getDropDownApicallTag(List gettagid) async {
    try {
      TaggedDropdown d1 = await TagTypedropdown();
      List save1 = List();
      List saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.name;
        String id = data.id;
        saveid.add(id);
        gettagid.forEach((element) {
          if (element == id) {
            save1.add(titlename);
          }
        });
      });

      savedTagsname = save1;
      print("tag names${savedTagsname}");
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<NoteList> getListApicall() async {
    print("note api called");
    List<NoteDetail> retaillist = List();
    try {
      NoteList d1 = await listapi();
      if (d1 == null) {
        tableDetail = null;
      } else {
        List detail = List();
        List relate = List();
        List time = List();
        List noteid = List();
        List opppid = List();

        d1.datas.forEach((data) {
          detail.add(data.detail);

          time.add(data.time);
          noteid.add(data.id);
          opppid.add(data.opportunityId);
          if (data.retaledTo != null) {
            relate.add(data.retaledTo);
          } else {
            relate.add("-");
          }
        });
        print("get related${relate}");
        tableDetail = detail;
        tableRelate = relate;
        tableTime = time;
        tableNoteId = noteid;
        tableoppid = opppid;

        setState(() {});

        //setState(() {});
        // print(d1.toString());
        return d1;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<NoteList> listapi() async {
//    var resBody = {};
//    resBody["userId"] = userid;
//    resBody[AppUtils.KEY_typeIds] = sourceApi;
//    resBody["AppUtils.KEY_sort] = {"field": sortingType, "order": sortingOrder};
//    resBody[KEY_pagination] = {"page": 1, KEY_limit: 25, "total": 0};
//    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.NoteListUrl,
      data: {
        AppUtils.KEY_pagination: {AppUtils.KEY_page: 0, AppUtils.KEY_limit: 5},
        AppUtils.KEY_accountId: getAccId
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
//    print(response.data);
    return NoteList.fromjson(response.data);
  }

  Future<GetAccountDetailModel> accDetailsApi() async {
    Response response = await Dio().post(
      AppUtils.AccountGetDetailsUrl,
      data: {AppUtils.KEY_id: getAccId},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return GetAccountDetailModel.fromJsonResponse(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        getListApicall();
        getAPicall();
      });
    });
  }
}
