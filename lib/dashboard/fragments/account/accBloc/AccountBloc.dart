import 'package:bloc/bloc.dart';
import 'package:fimber/fimber.dart';
import 'package:crmflutternew/dashboard/fragments/account/accBloc/AccountEvent.dart';
import 'package:crmflutternew/dashboard/fragments/account/accBloc/AccountState.dart';
import 'package:crmflutternew/model/account/AccountListModel.dart';
import 'package:crmflutternew/helper/DataManager.dart';
import 'package:crmflutternew/util/Constants.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  var log = FimberLog("AccountBloc");

  // String loginEmail = "Login_Email";
  // String loginFirstName = "Login_FirstName";
  // String loginLastName = "Login_LastName";
  // String loginId = "Login_Id";
  // String loginToken = "Login_token";

  @override
  AccountState get initialState => AccountUninitialized();

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is AccountMyListcalled) {
      try {
        AccountListModel accmodel = await DataManager.instance.listapi(
            event.userid,
            event.sourceApi,
            event.sortingType,
            event.sortingOrder,
            event.pageCount,
            event.token);
        // saveData(
        //     user.email, user.firstName, user.lastName, user.id, user.token);
        calllist(accmodel);
        print("in bloc $accmodel");
        yield AccountSuccess();
      } catch (e, stacktrace) {
        print("else $e");
        if (e is NoSuchMethodError) {
          yield AccountFailed(
              resCode: Constants.FAIL_INTERNET_CODE,
              reason: "Something went wrong!Please check all details!");
        }
      }
    } else if (event is AccountInitStateCalled) {
//
    }
  }

  calllist(AccountListModel d1) {
    // if (d1 == null) {
    //   retail = null;
    // } else {
    //   d1.datas.forEach((data) {
    //     retaillist.add(data);
    //   });
    //
    //   retail.addAll(retaillist);
    //
    //   if (d1.paginationModel <= retail.length) {
    //     hasMore_All = false;
    //   } else {
    //     hasMore_All = true;
    //   }
    //   pageCount_All++;
    //   isLoading_All = false;
    // }
    // setState(() {});
    return d1;
  }
}
