abstract class AccountEvent{}
class AccountMyListcalled extends AccountEvent{
  String userid,sortingType,sortingOrder,type,token;
  int pageCount;
  List sourceApi;
  AccountMyListcalled(this.userid,this.sourceApi, this.sortingType,this.sortingOrder,this.pageCount,this.token);
  @override
  String toString() => 'AccountMyListcalled { userid: $userid, token: $token,list$sourceApi }';
}
class AccountInitStateCalled extends AccountEvent{}
