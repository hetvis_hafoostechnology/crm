abstract class AccountState {}

class AccountUninitialized extends AccountState {}

class AccountLoading extends AccountState {
  @override
  String toString() => "{AccountStateLoading}";
}

class AccountFailed extends AccountState {
  int resCode;
  String reason;

  AccountFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{AccountFailed reason: $reason}";
  }
}

class AccountSuccess extends AccountState {
  @override
  String toString() {
    return "{Account Sucess}";
  }
}



