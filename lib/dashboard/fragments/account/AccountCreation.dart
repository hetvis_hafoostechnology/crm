import 'dart:async';
import 'dart:convert';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:crmflutternew/model/Table/ContactDepartment.dart';
import 'package:crmflutternew/model/Table/DataContactDept.dart';
import 'package:crmflutternew/model/account/GetAccountDetailModel.dart';
import 'package:crmflutternew/model/assigned/AssignedDropdown.dart';
import 'package:crmflutternew/model/dropdown/tag/TaggedDropdown.dart';
import 'package:crmflutternew/model/dropDown/SampleDropDown.dart';
import 'package:editable/editable.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:crmflutternew/model/lead/GetLeadDetails.dart';
import 'package:crmflutternew/model/lead/Retailead.dart';
import 'package:crmflutternew/model/account/NoteDetail.dart';
import 'package:crmflutternew/model/account/NoteList.dart';

import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/ColorUtil.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_webservice/places.dart';

class AccountCreation extends StatefulWidget {
  String isfrom;

  AccountCreation({Key key, @required this.isfrom}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AccountCreationState(getisfrom: isfrom);
  }
}

const kGoogleApiKey = "AIzaSyCXjOzOeaTIvxptSctHI3XctUT98Y75h-s";
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class AccountCreationState extends State<AccountCreation> {
  TextEditingController nameController,
      pincodeController,
      adress1controller,
      adress2controller,
      emailController,
      webController,
      mobController,
      telephoneController;
  List<TextEditingController> _controllersdept = new List();
  List<TextEditingController> _controllersemail = new List();
  List<TextEditingController> _controllersphone = new List();
  var lat = 0.0;
  var lng = 0.0;
  List<String> resultEntityType,
      resultType,
      resultSegment,
      resultInd,
      resultAnlRev,
      resultAccRep,
      resultDepartment,
      idsAssign,
      typeid,
      anlrevid,
      segmentid,
      entityTypeid,
      indid,
      departmentid,
      tagid,
      resultid;
  List resultTag;
  int tableIntialCounter = 1;
  String assiid, userid, selectedName = null, dropdownSegmentValue = null;
  var dropdownSegmentId;

  String dropdownEntityTypeValue = null;
  var dropdownEntityTypeValueID;

  String dropdownIndVale = null;
  var dropdownIndValeID;

  String dropdownAnlRevValue = null;
  var dropdownAnlRevValueID;

  String dropdownType = null;
  var dropdownTypeId;

  List dropdownDepartment = null;
  var dropdownDepartmentId;

  String dropdownTag = null;
  var dropdownTagId;

  var dropdownInterNationalValue;

  String internationaval;

  String name;
  String loginId = "Login_Id";
  String token;
  String loginToken = "Login_token";
  var selectedAddlat, selectedAddlng;
  var addressFromsearch = "";
  bool address_editable = false;
  var addressline3 = "", city = "", state = "", country = "";
  var selectedid;
  var clat = 0.0;
  var clong = 0.0;
  ProgressDialog pr;
  ProgressDialog pr_wait;
  var assignedName;
  String selectedcc = "+1";
  String selectedcc2 = "+1";
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  String fromLeadAssignedid;

  // List departmentDept;
  List emailDept;
  List phoneDept;
  final FocusNode _nameFocus = FocusNode();
  final FocusNode _webFocus = FocusNode();
  final FocusNode _fnameFocus = FocusNode();
  List _myActivities_dp;
  bool col1rowreadonly = true;

  // List _myActivities_dp1;
  String _myActivitiesResult;
  String getisfrom;
  final formKey = new GlobalKey<FormState>();

  AccountCreationState({Key key2, @required this.getisfrom});

  void _onMapCreated(GoogleMapController controller) {
    Marker cmarker = Marker(
      markerId: MarkerId("00"),
      position: LatLng(clat, clong),
      icon: BitmapDescriptor.defaultMarker,
      infoWindow: InfoWindow(
        title: "Current Location",
      ),
    );
    _markers.add(cmarker);

    _controller.complete(controller);
  }

  List dropdowncItemsTag;
  List sendContact;
  List<DataContactDept> listt = List();

  @override
  void initState() {
    super.initState();

//    _getCurrentLocation();

    print("getkeyyy $getisfrom");
    setData();
    setState(() {});
    // _myActivities_dp = [];
    _myActivitiesResult = '';
    resultAccRep = List();

    resultInd = List();
    resultAnlRev = List();
    resultSegment = List();
    resultType = List();
    resultEntityType = List();
    resultDepartment = List();
    resultTag = List();
    dropdowncItemsTag = List();
    sendContact = List();
    nameController = TextEditingController();
    pincodeController = TextEditingController();
    adress1controller = TextEditingController();
    adress2controller = TextEditingController();

    webController = TextEditingController();

    telephoneController = TextEditingController();

    emailController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );

    return Material(
        child: Center(
      child: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0),
            child: Text(StringUtils.TitleName,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          TextFormField(
            controller: nameController,
            textInputAction: TextInputAction.next,
            focusNode: _nameFocus,
            autofocus: false,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _nameFocus, _webFocus);
            },
            style: TextStyle(
                fontSize: AppUtils.FontSize15,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Regular,
                color: ColorUtil.Black),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callTypeDropDown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleWebsite,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          TextFormField(
            controller: webController,
//                  textInputAction: TextInputAction.next,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            focusNode: _webFocus,
            autofocus: false,
            style: TextStyle(
                fontSize: AppUtils.FontSize15,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Regular,
                color: ColorUtil.Black),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleSegment,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callSegmentDropDown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleAccRep,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callAssigneddropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleAnlRev,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callAnlRevDropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleInternational,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callInternationalDropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleIndustry,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callIndustryDropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleEntityType,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callEntityTypelDropdown(),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            height: 150,
            child: ListView.builder(
//      controller: _scrollController,
//        itemCount: retail.length+1,
                itemCount: dropdownDepartment.length + 1,
                itemBuilder: (context, index) {
                  if (index == 0) {
                    return ListTile(
                        //return new ListTile(
                        onTap: null,
                        title: Row(children: <Widget>[
                          Expanded(
                            child: Text(
                              StringUtils.TitleDepartment,
                              style: TextStyle(
                                  fontFamily: AppUtils.FontName,
                                  fontWeight: AppUtils.Bold,
                                  fontSize: 12.0),
                            ),
                            flex: 33,
                          ),
                          Expanded(
                            child: Text(
                              StringUtils.TitlePhone,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: AppUtils.FontName,
                                  fontWeight: AppUtils.Bold,
                                  fontSize: 12.0),
                            ),
                            flex: 33,
                          ),
                          Expanded(
                            child: Text(
                              StringUtils.TitleEmail,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: AppUtils.FontName,
                                  fontWeight: AppUtils.Bold,
                                  fontSize: 12.0),
                            ),
                            flex: 33,
                          ),
                        ]));
                  }
                  index = index - 1;

                  return InkWell(
                    onTap: () {},
                    child: ListTile(
                        //return new ListTile(
                        onTap: null,
                        title: Row(children: <Widget>[
                          Expanded(
                            child: callDepartmentDropdown(index),
                            flex: 32,
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: changeCol2data(index),
                              style: TextStyle(fontSize: 12.0),
                            ),
                            flex: 32,
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: changeCol3data(index),
                              style: TextStyle(fontSize: 12.0),
                            ),
                            flex: 31,
                          ),
                          Expanded(
                            child: IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                print("delete");
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text(StringUtils.TxtDelete),
                                      content:
                                          Text("Want to delete this item?"),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text(StringUtils.TxtDelete),
                                          onPressed: () {
                                            print("index to dlt $index");
                                            setState(() {
                                              emailDept.removeAt(index);
                                              phoneDept.removeAt(index);
                                              dropdownDepartment
                                                  .removeAt(index);
                                            });
                                            print(
                                                "lists after dlt $dropdownDepartment");
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        FlatButton(
                                          child: Text(StringUtils.BtnCancel),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                            flex: 5,
                          ),
                        ])),
                  );
                }),
          ),
          dropdownDepartment == null
              ? Container()
              : Container(
                  padding: EdgeInsets.only(top: 10.0),
                  width: 50.0,
                  child: OutlineButton(
                    child: Text(
                      StringUtils.BtnAddMore,
                      style: TextStyle(
                        fontSize: 12.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                      ),
                    ),
                    onPressed: () async {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(StringUtils.TxtDelete),
                            content: Text("Add new Item?"),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("Add"),
                                onPressed: () {
                                  setState(() {
                                    //5c62c4dc79d80e4264b1d794
                                    dropdownDepartment.insert(0, "Maintenance");
                                    emailDept.insert(0, "add val");
                                    phoneDept.insert(0, "add val");
                                    _controllersemail = [
                                      for (int i = 0; i < emailDept.length; i++)
                                        TextEditingController()
                                    ];
                                    _controllersphone = [
                                      for (int i = 0; i < phoneDept.length; i++)
                                        TextEditingController()
                                    ];
                                  });
                                  print(
                                      "list len ${dropdownDepartment.length} and email ${emailDept.length} and other len ${phoneDept.length}");
                                  print(
                                      "list len ${dropdownDepartment} and email ${emailDept} and other len ${phoneDept}");
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: Text(StringUtils.BtnCancel),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    borderSide:
                        BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                    highlightElevation: 4.0,
                  ),
                ),
//           Padding(
//             padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
//             child: Text(StringUtils.TitleDepartment,
//                 style: TextStyle(
//                     fontSize: AppUtils.FontSize16,
//                     fontFamily: AppUtils.FontName,
//                     fontWeight: AppUtils.SemiBold,
//                     color: ColorUtil.Grey)),
//           ),
//           callDepartmentDropdown(),
//           Padding(
//             padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
//             child: Text(StringUtils.TitlePhone,
//                 style: TextStyle(
//                     fontSize: AppUtils.FontSize16,
//                     fontFamily: AppUtils.FontName,
//                     fontWeight: AppUtils.SemiBold,
//                     color: ColorUtil.Grey)),
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               new Flexible(
//                 child: CountryCodePicker(
//                   onChanged: _onCountryChangePhone,
//                   initialSelection: 'US',
//                   showCountryOnly: false,
//                   showOnlyCountryWhenClosed: false,
//                   alignLeft: false,
//                 ),
//                 flex: 3,
//               ),
//               new Flexible(
//                 child: TextFormField(
//                   maxLength: 10,
//                   controller: telephoneController,
//                   keyboardType: TextInputType.phone,
//                   style: TextStyle(
//                       fontSize: AppUtils.FontSize15,
//                       fontFamily: AppUtils.FontName,
//                       fontWeight: AppUtils.Regular,
//                       color: ColorUtil.Black),
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.all(10.0),
//                     border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(0.0)),
//                   ),
//                 ),
//                 flex: 7,
//               )
//             ],
//           ),
//           Padding(
//             padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
//             child: Text(StringUtils.TitleEmail,
//                 style: TextStyle(
//                     fontSize: AppUtils.FontSize16,
//                     fontFamily: AppUtils.FontName,
//                     fontWeight: AppUtils.SemiBold,
//                     color: ColorUtil.Grey)),
//           ),
//           TextFormField(
//             controller: emailController,
// //                  textInputAction: TextInputAction.next,
//             keyboardType: TextInputType.text,
//             textInputAction: TextInputAction.next,
//             autofocus: false,
//             style: TextStyle(
//                 fontSize: AppUtils.FontSize15,
//                 fontFamily: AppUtils.FontName,
//                 fontWeight: AppUtils.Regular,
//                 color: ColorUtil.Black),
//             decoration: InputDecoration(
//               contentPadding: EdgeInsets.all(10.0),
//               border:
//               OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
//             ),
//           ),
          callTagDropdown(),

          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleAddress,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Flexible(
                  child: InkWell(
                    onTap: _handlePressButton,
                    child: IgnorePointer(
                      child: TextFormField(
                        readOnly: address_editable,
                        controller:
                            TextEditingController(text: addressFromsearch),
//                              textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                            fontSize: AppUtils.FontSize15,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Regular,
                            color: ColorUtil.Black),
//                              autofocus: false,
                        decoration: InputDecoration(
                          hintText: "Search Address ",
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0)),
                        ),
                      ),
                    ),
                  ),
//
                  flex: 10,
                ),
                new Flexible(
                  child: Container(
                    color: Constants.COLOR_LOGIN_UPPER_BG,
                    child: IconButton(
                      icon: Icon(Icons.search),
                      color: Constants.COLOR_CARD_WHITE_BG,
                      onPressed: _handlePressButton,
                    ),
                  ),
                  flex: 2,
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 20.0),
            child: SizedBox(
                width: double.maxFinite,
                height: 300.0,
                child: GoogleMap(
                  onMapCreated: _onMapCreated,
                  myLocationEnabled: true,
                  initialCameraPosition:
                      CameraPosition(target: LatLng(clat, clong)),
                  markers: _markers,
                )),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleAddress1,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            //cahnge
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              controller: adress1controller,
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
//                    autofocus: false,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleAddress2,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              controller: adress2controller,
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
//                    autofocus: false,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleAddress3,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              controller: TextEditingController(text: addressline3),
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              autofocus: false,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleCity,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              readOnly: true,
              controller: TextEditingController(text: city),
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              autofocus: false,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleState,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              readOnly: true,
              controller: TextEditingController(text: state),
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              autofocus: false,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleCountry,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              readOnly: true,
              controller: TextEditingController(text: country),
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              autofocus: false,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitlePincode,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: TextFormField(
              readOnly: true,
              controller: pincodeController,
//                    textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              autofocus: false,
              style: TextStyle(
                  fontSize: AppUtils.FontSize15,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: ColorUtil.Black),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: OutlineButton(
                  child: Text(
                    StringUtils.BtnCancel,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.SemiBold,
                        color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  borderSide:
                      BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  highlightElevation: 4.0,
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(30.0)
//                      )
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: RaisedButton(
                  color: Constants.COLOR_CARD_SIGNIN_TITLE,
                  elevation: 4,
                  child: Text(
                    StringUtils.BtnSave,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.SemiBold,
                        color: Constants.COLOR_TEXT),
                  ),
                  splashColor: Constants.COLOR_CARD_FP,
                  onPressed: () {
                    List testE = List();
                    List testP = List();

                    for (int i = 0; i < _controllersphone.length; i++) {
                      testP.add(_controllersphone[i].text);
                    }
                    for (int i = 0; i < _controllersemail.length; i++) {
                      testE.add(_controllersemail[i].text);
                    } //
                    print("list in save ${testE} and ${testP}");
                    List<makecontact> tags = List();
                    makecontact addd;
                    for (var i = 0; i < dropdownDepartment.length; i++) {
                      print(
                          "in loop here ${dropdownDepartment[i]} & ${testE[i]} & ${testP[i]}");
                      addd = new makecontact(
                          dropdownDepartment[i], testE[i], testP[i]);
                      tags.add(addd);
                    }
                    //IF ERROR occurs that means this array is in string check documnetdioalog
                    print("tags $tags");
                    String jsonTags = jsonEncode(tags);
                    print("array$jsonTags");
                    //make contact api

                    if (_isValidData()) {
//                          getInstitutionalApicall();
                      getSaveAccApicall(jsonTags);
                      // _saveForm();
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }

  bool _isValidData() {
    var name = nameController.text;
//    var email = emailController.text;
//    var mob = mobController.text;

    if (name == null || name.length == 0) {
      AppUtils.showAlert(context, StringUtils.NameHint);
      return false;
    }else if(dropdownType==null){
      AppUtils.showAlert(context, StringUtils.SelectType);
      return false;
    }
    else if (dropdownSegmentValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectSegment);
      return false;
    } else if (dropdownAnlRevValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectAnlRev);
      return false;
    }  else if (dropdownInterNationalValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectSegment);
      return false;
    }else if (dropdownIndVale == null) {
      AppUtils.showAlert(context, StringUtils.SelectIndustry);
      return false;
    } else if (dropdownEntityTypeValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectEntityType);
      return false;
    } else if (dropdownDepartment == null) {
      AppUtils.showAlert(context, StringUtils.SelectDepartment);
      return false;
    } else if (addressFromsearch == null) {
      AppUtils.showAlert(context, StringUtils.AddressHint);
      return false;
    }

    return true;
  }

  TextEditingController changeCol2data(int index) {
    print(phoneDept.length);
    print("index $index");

    if (phoneDept != null) {
      print("what added ${phoneDept[index]}");
      _controllersphone[index].text = phoneDept[index];
    }
    print("addded ${_controllersphone[index]}");
    return _controllersphone[index];
  }

  TextEditingController changeCol3data(int index) {
    if (emailDept != null) {
      _controllersemail[index].text = emailDept[index];
    }
    return _controllersemail[index];
  }

  callInternationalDropdown() {
    print("hre inter $dropdownInterNationalValue");
    return DropdownButtonFormField<String>(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(3.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
      ),
      value: dropdownInterNationalValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 24,
      style: TextStyle(
          fontSize: AppUtils.FontSize15,
          fontFamily: AppUtils.FontName,
          fontWeight: AppUtils.Regular,
          color: ColorUtil.Black),
      onChanged: (String newValue) {
        setState(() {
          print("hjre");
          dropdownInterNationalValue = newValue;
        });
      },
      items:
          <String>['Yes', 'No'].map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  Future<SampleDropDown> getDropDownApicallSegment() async {
    print("22");
    try {
      SampleDropDown d1 = await segmentDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultSegment = save1;
      segmentid = saveid;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<SampleDropDown> segmentDropdown() async {
    print("tokrn in segment${token}");
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f6f284da3ad00920e909"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return SampleDropDown.fromjson(response.data);
  }

  callSegmentDropDown() {
    if (resultSegment != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownSegmentValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownSegmentValue = newValue;
            int indexid = resultSegment.indexOf(newValue);
            dropdownSegmentId = segmentid.asMap()[indexid];
          });
        },
        items: getSegmentItems(),
      );
    } else {
      return Container();
    }
  }

  getSegmentItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultSegment.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  Future<SampleDropDown> typeDropDown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f70c5236ab544529fa37"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getDropDownApicallType() async {
    print("66");
    try {
      SampleDropDown d1 = await typeDropDown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultType = save1;
      typeid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  callTypeDropDown() {
    if (resultType != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownType,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownType = newValue;
            int indexid = resultType.indexOf(newValue);
            dropdownTypeId = typeid.asMap()[indexid];
          });
        },
        items: getTypeItems(),
      );
    } else {
      return Container();
    }
  }

  getTypeItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultType.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  Future<SampleDropDown> AnlRevDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f7000f34719b1fabb898"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getDropDownApicallAnlRev() async {
    print("55");
    try {
      SampleDropDown d1 = await AnlRevDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultAnlRev = save1;
      anlrevid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  getAnlRevItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultAnlRev.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  callAnlRevDropdown() {
    if (resultAnlRev != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownAnlRevValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownAnlRevValue = newValue;
            int indexid = resultAnlRev.indexOf(newValue);
            dropdownAnlRevValueID = anlrevid.asMap()[indexid];
          });
        },
        items: getAnlRevItems(),
      );
    } else {
      return Container();
    }
  }

  Future<SampleDropDown> IndustryDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f70658369e649cc15923"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getDropDownApicallIndustry() async {
    print("33");
    try {
      SampleDropDown d1 = await IndustryDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultInd = save1;
      indid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  getIndustryItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultInd.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  callIndustryDropdown() {
    if (resultInd != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownIndVale,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownIndVale = newValue;
            int indexid = resultInd.indexOf(newValue);
            dropdownIndValeID = indid.asMap()[indexid];
            // print("which is id $dropdownIndVale or $dropdownIndValeID");
          });
        },
        items: getIndustryItems(),
      );
    } else {
      return Container();
    }
  }

  Future<SampleDropDown> EntityTyperopdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f6f9b3cdeec35d723499"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getDropDownApicallEntityType() async {
    print("44");
    // pr.show();
    try {
      SampleDropDown d1 = await EntityTyperopdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultEntityType = save1;
      entityTypeid = saveid;
//      idContact = saveid1;
      setState(() {});
// pr.hide();
      return d1;
    } catch (e) {
      print(e);
    }
  }

  getEntityTypeItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultEntityType.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  callEntityTypelDropdown() {
    if (resultEntityType != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownEntityTypeValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownEntityTypeValue = newValue;
            int indexid = resultEntityType.indexOf(newValue);
            dropdownEntityTypeValueID = entityTypeid.asMap()[indexid];
          });
        },
        items: getEntityTypeItems(),
      );
    } else {
      return Container();
    }
  }

  Future<SampleDropDown> DepartmentTypedropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f343e41c8428d23654c0"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getDropDownApicallDepartment() async {
    print("77");
    try {
      SampleDropDown d1 = await DepartmentTypedropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultDepartment = save1;
      departmentid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  getDepartmentItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultDepartment.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  callDepartmentDropdown(int index) {
    print("index in call method $index");

    print("list ${dropdownDepartment[index]}");
    if (resultDepartment != null) {
      return DropdownButtonFormField<String>(
        value: dropdownDepartment[index],
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 9,
        style: TextStyle(
            fontSize: 12.0,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownDepartment[index] = newValue;
            int indexid = resultDepartment.indexOf(newValue);
            dropdownDepartmentId = departmentid.asMap()[indexid];
          });
        },
        items: getDepartmentItems(),
      );
    } else {
      return Container();
    }
  }

  Future<TaggedDropdown> TagTypedropdown() async {
    Response response = await Dio().post(
      AppUtils.TagDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("tag dp ${response.data[AppUtils.KEY_DATA]}");
    return TaggedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  Future<TaggedDropdown> getDropDownApicallTag() async {
    print("88");
    try {
      TaggedDropdown d1 = await TagTypedropdown();

      d1.datas.forEach((cvalue) {
        dropdowncItemsTag.add({"display": cvalue.name, "value": cvalue.id});
      });
      print("tag items $dropdowncItemsTag");
      // tagid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  // getTagItems() {
  //   var dropdowncItems = List<DropdownMenuItem<String>>();
  //   resultTag.forEach((cvalue,value) {
  //     dropdowncItems.add(DropdownMenuItem<String,String>(
  //       value: value,
  //       child: Text(cvalue),
  //     ));
  //   });
  //   print("getitem$resultTag");
  //
  //   return dropdowncItems;
  // }

  callTagDropdown() {
    if (resultTag != null) {
      print("tag items $dropdowncItemsTag");
      return new Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: MultiSelectFormField(
                      autovalidate: false,
                      titleText: StringUtils.TitleTag,
                      validator: (value) {
                        if (value == null || value.length == 0) {
                          return 'Please select one or more options';
                        }
                      },
                      dataSource: dropdowncItemsTag,
                      textField: 'display',
                      valueField: 'value',
                      okButtonLabel: 'OK',
                      cancelButtonLabel: 'CANCEL',

                      // required: true,
                      hintText: 'Select options',
                      initialValue: _myActivities_dp,
                      onSaved: (value) {
                        print("tap val $value");
                        if (value == null) return;
                        setState(() {
                          _myActivities_dp = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            child: Text(_myActivitiesResult),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  // _saveForm() {
  //   var form = formKey.currentState;
  //   if (form.validate()) {
  //     form.save();
  //     setState(() {
  //       _myActivitiesResult = _myActivities_dp.toString();
  //     });
  //   }
  //   print("vals$_myActivitiesResult");
  // }
  Future<AssignedDropdown> getAssignedtoApicall() async {
    print("11");
    try {
      AssignedDropdown d1 = await assignedDropdown();
//      result = List();
      List<String> save = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        name = data.name;
        assiid = data.id;
        save.add(name);
        saveid.add(assiid);
        print("outside $fromLeadAssignedid");
        if (getisfrom != null) {
          print("if $fromLeadAssignedid");
          if (assiid == fromLeadAssignedid) {
            selectedid = assiid;
            selectedName = data.name;
            assignedName = assiid;
          }
        } else {
          print("else1");
          if (assiid == userid) {
            selectedid = assiid;
            selectedName = data.name;
            assignedName = assiid;
          }
        }

//        String fname = data.firstname;
//        String lname = data.lastname;
      });
      resultAccRep = save; //nmae
      idsAssign = saveid; //id

      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<AssignedDropdown> assignedDropdown() async {
    Response response = await Dio().post(
      AppUtils.UserDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return AssignedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  callAssigneddropdown() {
    if (resultAccRep != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: selectedName,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            selectedName = newValue;
            int indexid = resultAccRep.indexOf(newValue);
            assignedName = idsAssign.asMap()[indexid];
            //idsAssign.indexOf(indexid.toString());
          });
        },
        items: getAssignedItems(),
      );
    } else {
      return Container();
    }
  }

  getAssignedItems() {
    var dropdownItems = List<DropdownMenuItem<String>>();
    resultAccRep.forEach((value) {
      dropdownItems.add(DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      ));
    });
    return dropdownItems;
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(loginId);
    token = preferences.getString(loginToken);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {

        userid = value;
        if (getisfrom != null) {
          getAPicall();
        } else {
          emailDept = [];
          phoneDept = [];
          dropdownDepartment = [];
        }
        print("1");
        getAssignedtoApicall();
        print("2");
        getDropDownApicallSegment();
        print("3");
        getDropDownApicallIndustry();
        print("4");
        getDropDownApicallEntityType();
        print("5");
        getDropDownApicallAnlRev();
        print("6");
        getDropDownApicallType();
        print("7");
        getDropDownApicallDepartment();
        print("8");
        getDropDownApicallTag();
        print("9");

      });
    });
  }

  Future<void> _handlePressButton() async {
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      components: [Component(Component.country, "ind")],
    );

    displayPrediction(p, Scaffold.of(context));
  }

  void onError(PlacesAutocompleteResponse response) {
    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<String> addmarkerAftersearch(var alat, var alng) async {
    address_editable = true;
    _markers.clear();
    Marker marker = Marker(
      markerId: MarkerId("02"),
      position: LatLng(alat, alng),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      infoWindow: InfoWindow(
        title: "here",
      ),
      onTap: () {},
    );
    _markers.add(marker);
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(alat, alng), zoom: 12),
      ),
    );
    setState(() {});
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      selectedAddlat = detail.result.geometry.location.lat;
      selectedAddlng = detail.result.geometry.location.lng;

      addressFromsearch = p.description;
      addmarkerAftersearch(selectedAddlat, selectedAddlng);

      final coordinates = new Coordinates(selectedAddlat, selectedAddlng);
      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      var first = addresses.first;

      adress1controller.text = first.subThoroughfare;
      adress2controller.text = first.thoroughfare;
      addressline3 = first.subLocality;
      city = first.subAdminArea;
      state = first.adminArea;
      pincodeController.text = first.postalCode;
      country = first.countryName;
    }
    setState(() {});
  }

  Future<SampleModel> getSaveAccApicall(String contactarray) async {
    try {
      pr.show();
      SampleModel d1 = await getSaveAccApi(contactarray);
      AppUtils.showToastWithMsg(
          StringUtils.AccountSaveSuccess, ToastGravity.BOTTOM);

      Navigator.of(context).pop();
      setState(() {});
      pr.hide();
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(context, e);
    }
  }

  Future<SampleModel> getSaveAccApi(String contactArray) async {
    //10 digit mob
    print(
        "call api country code ${selectedcc2} industry${dropdownIndValeID}& international ${dropdownInterNationalValue} &entity${dropdownEntityTypeValueID}& rev${dropdownAnlRevValueID} &seg${dropdownSegmentId} &type${typeid} &");
    //addressline

    Response response = await Dio().post(
      AppUtils.AccountSaveUrl,
      data: {
        AppUtils.KEY_id: getisfrom,
        AppUtils.KEY_name: nameController.text,
        AppUtils.KEY_type: dropdownTypeId,
        AppUtils.KEY_segment: dropdownSegmentId,
        AppUtils.KEY_annRev: dropdownAnlRevValueID,
        AppUtils.KEY_industry: dropdownIndValeID,
        AppUtils.KEY_website: webController.text,
        AppUtils.KEY_accountRep: assignedName,
        AppUtils.KEY_entityType: dropdownEntityTypeValueID,
        AppUtils.KEY_international: dropdownInterNationalValue,
        AppUtils.KEY_tagIds: _myActivities_dp,
        AppUtils.KEY_contact: contactArray,
        AppUtils.KEY_address: {
          AppUtils.KEY_addressLine1: adress1controller.text,
          AppUtils.KEY_addressLine2: adress2controller.text,
          AppUtils.KEY_addressLine3: addressline3,
          AppUtils.KEY_city: city,
          AppUtils.KEY_zipCode: pincodeController.text,
          AppUtils.KEY_state: state,
          AppUtils.KEY_country: country,
          AppUtils.KEY_geoLocation: {
            AppUtils.KEY_latitude: selectedAddlat,
            AppUtils.KEY_longitude: selectedAddlng
          }
        },
        AppUtils.KEY_updatedBy: userid
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return SampleModel.fromjson(response.data);
  }

  void _onCountryChangeMob(CountryCode countryCode) {
    selectedcc = countryCode.toString();
  }

  void _onCountryChangePhone(CountryCode countryCode) {
    selectedcc2 = countryCode.toString();
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Future<GetAccountDetailModel> getAPicall() async {
    pr.show();
    try {
      GetAccountDetailModel d1 = await accDetailsApi();
      nameController.text = d1.name;

      webController.text = d1.website;
//      print("phone${d1.phone}");
//      if (d1.phone != null && d1.phone != "") {
//        //split string
//        if (d1.phone.contains(" ")) {
//          var arr = d1.phone.split(" ");
//
//          selectedcc = arr[0];
//          mobController.text = arr[1];
//          print(mobController.text);
//        } else {
//          mobController.text = d1.phone;
//        }
//      }
//      if (d1.email != null && d1.email != "") {
//        emailController.text = d1.email;
//      }

      dropdownSegmentValue = d1.segment;
      dropdownSegmentId = d1.segmentId;

      dropdownType = d1.type;
      dropdownTypeId = d1.typeId;

      dropdownAnlRevValue = d1.annualRevenue;
      dropdownAnlRevValueID = d1.annualRevenueId;

      dropdownIndVale = d1.industry;
      dropdownIndValeID = d1.industryId;

      dropdownEntityTypeValue = d1.entityType;
      dropdownEntityTypeValueID = d1.entityTypeId;

      print("tags${d1.tagids}");
      List saveid = List();
      if (d1.tagids != null) {
        d1.tagids.forEach((data) {
          String tagid = data;
          saveid.add(tagid);
        });
        print("tag ids${saveid}");
        _myActivities_dp = d1.tagids;
      }
//      dropdownDepartmentId=d1.department;

      if (d1.intenational != null) {
        internationaval = d1.intenational;
        print("SET Internat${internationaval}");
      }
      if (d1.accRepresentative != null) {
        print("acc rep${d1.accRepresentative}");
        fromLeadAssignedid = d1.accRepresentativeId;
        assignedName = d1.accRepresentative;
      }
      print("dept${d1.department}");

      if (d1.contacts != null) {
        List email = List();
        List dept = List();
        List phone = List();
        d1.contacts.datas.forEach((element) {
          email.add(element.email);
          phone.add(element.phone);
          dept.add(element.department);
        });
        emailDept = email;
        phoneDept = phone;
        getdepts(dept);
      } else {
        emailDept = null;
        phoneDept = null;

        dropdownDepartment = null;
      }
      adress1controller.text = d1.address.addressLine1;
      adress2controller.text = d1.address.addressLine2;
      addressline3 = d1.address.addressLine3;
      city = d1.address.city;
      state = d1.address.state;
      country = d1.address.country;
      pincodeController.text = d1.address.zipCode;

      pr.hide();
      setState(() {});
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(context, e);
    }
  }

  Future<GetAccountDetailModel> accDetailsApi() async {
    Response response = await Dio().post(
      AppUtils.AccountGetDetailsUrl,
      data: {AppUtils.KEY_id: getisfrom},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return GetAccountDetailModel.fromJsonResponse(response.data);
  }

  Future<SampleDropDown> depts() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f343e41c8428d23654c0"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SampleDropDown.fromjson(response.data);
  }

  Future<SampleDropDown> getdepts(List dept) async {
    try {
      SampleDropDown d1 = await depts();
      print("met${dept}");

      List save1 = List();
      d1.datas.forEach((data) {
        dept.forEach((element) {
          if (element == data.id) {
            print("matched${data.text}");
            save1.add(data.text);
          }
        });
      });

      dropdownDepartment = save1;
      _controllersemail = [
        for (int i = 0; i < emailDept.length; i++) TextEditingController()
      ];
      _controllersphone = [
        for (int i = 0; i < phoneDept.length; i++) TextEditingController()
      ];
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

// Future<ContactDepartment> depts() async {
//   Response response = await Dio().post(
//     AppUtils.MasterGetListsUrl,
//     data: {AppUtils.KEY_typeId: "5e47f343e41c8428d23654c0"},
//     options: Options(headers: {
//       AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
//       AppUtils.KEY_AccessToken: token
//     }),
//   );
//   return ContactDepartment.fromJsonArray(response.data);
// }
//
// Future<ContactDepartment> getdepts() async {
//   List<DataContactDept> clist = List();
//   try {
//     ContactDepartment d1 = await depts();
//
//     d1.datas.forEach((data) {
//       clist.add(data);
//     });
//
//     listt = clist;
//     setState(() {});
//
//     //setState(() {});
//     // print(d1.toString());
//     return d1;
//   } catch (e) {
//     print(e);
//   }
//   return null;
// }

}

class makecontact {
  String dept;
  String phone;
  String email;

  makecontact(this.dept, this.email, this.phone);

  // tojjjson(){
  //  List<dynamic> list = List.from(dept);
  //  list.forEach((item) {
  //    result.add(DataContactDept.fromJson(item));
  //  });
  Map<String, dynamic> toJson() =>
      {"departnment": dept, "email": email, "phone": phone};
// }

}
