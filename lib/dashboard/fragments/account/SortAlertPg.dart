import 'package:crmflutternew/dashboard/fragments/account/AccountList.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/util/AppUtils.dart';
class SortAlertPg extends StatefulWidget {
  int selectedtabIndex;
  int selectedSortingIndex;
  List selectedFilter;
  List types;

  SortAlertPg(
      {Key key,
      @required this.selectedtabIndex,
      @required this.selectedSortingIndex,
      @required this.selectedFilter,
      @required this.types})
      : super(key: key);

  @override
  State createState() => new SortAlertPgState(
      selTabIndex: selectedtabIndex,
      selSortIndex: selectedSortingIndex,
      selFilter: selectedFilter,
      typeids: types);
}

class SortAlertPgState extends State<SortAlertPg> {
  String result;
  int selTabIndex;
  int prevSelectedSorting = 0;
  int selSortIndex;
  List selFilter;
  List typeids;

  SortAlertPgState(
      {Key key2,
      @required this.selTabIndex,
      this.selSortIndex,
      this.selFilter,
      this.typeids});

  @override
  void initState() {
    super.initState();
    prevSelectedSorting = selSortIndex;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            title: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 10.0),
              child: Text(
                'Sort By',
                style: TextStyle(color: Colors.blue, fontSize: 20.0),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  poppage(context, null, null);
                },
              ),
            ],
          )),
      body: Column(
        children: <Widget>[
          FlatButton(
            child: (prevSelectedSorting == 0
                ? Text("Latest", style: TextStyle(color: Colors.blue))
                : Text("Latest")),
            onPressed: () {
              prevSelectedSorting = 0;
              //updatedBy.time
              //desc
              poppage(context, "updatedBy.time", "desc");
            },
          ),
          FlatButton(
            child: (prevSelectedSorting == 1
                ? Text("Oldest", style: TextStyle(color: Colors.blue))
                : Text("Oldest")),
            onPressed: () {
              prevSelectedSorting = 1;
              //updatedBy.time
              //asc
              poppage(context, "updatedBy.time", "asc");
            },
          ),
          FlatButton(
            child: (prevSelectedSorting == 2
                ? Text("Name (A-Z)", style: TextStyle(color: Colors.blue))
                : Text("Name (A-Z)")),
            onPressed: () {
              prevSelectedSorting = 2;
              //sortingType = AppUtils.KEY_name;
              //      sortingOrder = "asc";
              poppage(context, AppUtils.KEY_name, "asc");
            },
          ),
          FlatButton(
            child: (prevSelectedSorting == 3
                ? Text("Name (Z-A)", style: TextStyle(color: Colors.blue))
                : Text("Name (Z-A)")),
            onPressed: () {
              prevSelectedSorting = 3;
              poppage(context, "name", "desc");
            },
          ),
        ],
      ),
    );
  }

  poppage(BuildContext ctx, String type, String order) {
    print(
        "Sort sentdetails-sorttype${type}&sortorder${order}&curseltab${selTabIndex}&selSorting${prevSelectedSorting}&filtertypeids${typeids}&selctedfilter${selFilter}");

    Navigator.pushAndRemoveUntil(
        ctx,
        MaterialPageRoute(
          builder: (context) => Account(
            sortingType3: type,
            sortingOrder3: order,
            currentTabIndex3: selTabIndex,
            sortingIndex3: prevSelectedSorting,
            source3: typeids,
            selfilter3: selFilter,
          ),
        ),
        ModalRoute.withName("/Account"));
  }
}
