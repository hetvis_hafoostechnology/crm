import 'dart:convert';

import 'package:crmflutternew/dashboard/fragments/account/AccountCreation.dart';
import 'package:crmflutternew/dashboard/fragments/account/AccountDetail.dart';
import 'package:crmflutternew/model/account/AccountDetailModel.dart';
import 'package:crmflutternew/model/account/AccountListModel.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyAccountF extends StatelessWidget {
  List sourceApi;
  String sortingType;
  String sortingOrder;

  MyAccountF({Key key, @required this.sortingType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyAccountPg(
        sortingType2: sortingType,
      ),
    );
  }
}

class MyAccountPg extends StatefulWidget {
  String sortingType2;

  MyAccountPg({Key key2, @required this.sortingType2});

  @override
  State createState() => new MyAccountPgState(sortingType3: sortingType2);
}

class MyAccountPgState extends State<MyAccountPg> {
  List<AccountDetailModel> retail = List();
  String userid;
  String loginId = "Login_Id";
  String token;
  String loginToken = "Login_token";

  bool isLoading = false;
  bool isFirstApiCall = false;
  bool hasMore = true;
  int pageCount = 1;
  String sortingType3;

  MyAccountPgState({Key key2, @required this.sortingType3});

  @override
  void initState() {
    super.initState();
    print("here acc frag");
    print(sortingType3);
//    sortingOrder = "desc";
//    sourceApi = [];
    setState(() {});
    setData();
  }

  Widget buildlist(BuildContext context, String id, String accName, String type,
      String segment, String ownerName, String address) {
    return InkWell(
      onTap: () {
        callActivity(context, id);
      },
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Text(
                accName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Bold,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
                padding:
                    new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      type,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.redAccent,
                      ),
                    ),
                    new Spacer(),
                    Text(
                      segment,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.deepPurple,
                      ),
                    ),
                  ],
                )),
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Text(
                ownerName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                address,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<AccountDetailModel> fnameList) {
    if (!isFirstApiCall) {
      return Center(child: CupertinoActivityIndicator());
    }
    if (fnameList == null || fnameList.length == 0) {
      return Center(
        child: Text("No Data Found"),
      );
    } else {
      return ListView.builder(
//        controller: _scrollController,
        itemCount: retail.length + 1,
        itemBuilder: (context, index) {
          print("item builder$index");
          if (index == retail.length - 1) {
            getListApicall();
          }
          if (index == retail.length) {
            if (hasMore) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }

          return buildlist(
              context,
              fnameList[index].id,
              fnameList[index].accName,
              fnameList[index].typeInsRet,
              fnameList[index].segment == null ? "" : fnameList[index].segment,
              fnameList[index].firstName + " " + fnameList[index].lastName,
              fnameList[index].address == null
                  ? " "
                  : fnameList[index].address.addressLine1 +
                      "," +
                      fnameList[index].address.addressLine2);
//          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ListView(retail),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: IconButton(
          iconSize: 50.0,
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          icon: Icon(
            Icons.add_circle,
            color: Colors.blue,
          ),
          onPressed: () {
            callFAB(context);
          },
        ),
      ),
    );
  }

  Future<AccountListModel> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<AccountDetailModel> retaillist = List();
    print("here pg$pageCount");
    try {
      if (!isLoading && hasMore) {
        isLoading = true;
        isFirstApiCall = true;
        AccountListModel d1 = await listapi();
        print(d1.paginationModel);

        d1.datas.forEach((data) {
          retaillist.add(data);
        });
        retail.addAll(retaillist);

        if (d1.paginationModel <= retail.length) {
          hasMore = false;
        } else {
          hasMore = true;
        }
        pageCount++;
        isLoading = false;
        setState(() {});
        return d1;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<AccountListModel> listapi() async {
    print("sorting${sortingType3}");
    var resBody = {};
    resBody[AppUtils.KEY_userId] = userid;
    resBody[AppUtils.KEY_typeIds] = null;
    resBody[AppUtils.KEY_sort] = {AppUtils.KEY_field: sortingType3, AppUtils.KEY_order: "desc"};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount, AppUtils.KEY_limit: 15};
    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.AccountListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("response$response");
    return AccountListModel.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(loginId);
    token = preferences.getString(loginToken);

    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        getListApicall();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}

callActivity(BuildContext _context, String id) {
  Navigator.push(_context,
      MaterialPageRoute(builder: (context) => AccountDetail(accId: id)));
}

callFAB(BuildContext _context) {
  Navigator.push(_context,
      MaterialPageRoute(builder: (context) => AccountCreation(isfrom: null)));
}
