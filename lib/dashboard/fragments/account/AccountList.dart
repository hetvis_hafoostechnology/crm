import 'dart:convert';
import 'package:crmflutternew/dashboard/fragments/account/AccountCreation.dart';
import 'package:crmflutternew/dashboard/fragments/account/AccountDetail.dart';
import 'package:crmflutternew/dashboard/fragments/account/FilterAlertPg.dart';
import 'package:crmflutternew/dashboard/fragments/account/SortAlertPg.dart';
import 'package:crmflutternew/model/account/AccountDetailModel.dart';
import 'package:crmflutternew/model/account/AccountListModel.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/login/LoginScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

List sourceApi = [];
String sortingType;
String sortingOrder;
BuildContext _context;

class Account extends StatelessWidget {
  String sortingType3,sortingOrder3;
  int currentTabIndex3,sortingIndex3;
  List source3,selfilter3;

  Account(
      {Key key,
      @required this.sortingType3,
      @required this.sortingOrder3,
      @required this.currentTabIndex3,
      @required this.sortingIndex3,
      @required this.source3,
      @required this.selfilter3})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new AccountPage(
        sortingType2: sortingType3,
        sortingOrder2: sortingOrder3,
        currentTabIndex2: currentTabIndex3,
        sortingIndex2: sortingIndex3,
        source2: source3,
        selfilter2: selfilter3,
      ),
    );
  }
}

class AccountPage extends StatefulWidget {
  String sortingType2,sortingOrder2;
  int currentTabIndex2,sortingIndex2;
  List source2,selfilter2;

  AccountPage(
      {Key key,
      @required this.sortingType2,
      @required this.sortingOrder2,
      @required this.currentTabIndex2,
      @required this.sortingIndex2,
      @required this.source2,
      @required this.selfilter2})
      : super(key: key);

  @override
  State createState() => new AccountPagePageState(
      sortingTypef: sortingType2,
      sortingOrderf: sortingOrder2,
      currentTabIndexf: currentTabIndex2,
      sortingIndexf: sortingIndex2,
      sourcef: source2,
      selfilterf: selfilter2);
}

class AccountPagePageState extends State<AccountPage> {
  int _currentIndex;
  final List<int> _backstack = [0];

  bool myLeadSelected,allLeadSelected;
  Color mybtnClr,mybtnTxtClr,allbtnClr,allbtnTxtClr;

  String sortingTypef,sortingOrderf;
  int currentTabIndexf,sortingIndexf;
  List sourcef,selfilterf;

  AccountPagePageState(
      {Key key2,
      @required this.sortingTypef,
      @required this.sortingOrderf,
      @required this.currentTabIndexf,
      @required this.sortingIndexf,
      @required this.sourcef,
      @required this.selfilterf});

  List<Widget> _fragments = [new MyAccount(), AllAccount()];

  @override
  void initState() {
    super.initState();
    if (sortingTypef == null) {
      sortingType = "updatedBy.time";
      sortingOrder = "desc";
    } else {
      sortingType = sortingTypef;
      sortingOrder = sortingOrderf;
    }
    _currentIndex = currentTabIndexf;
    if (_currentIndex == 0) {
      myLeadSelected = true;
      allLeadSelected = false;
      selectMyAccBtn();
    } else {
      myLeadSelected = false;
      allLeadSelected = true;
      selectAllAccBtn();
    }
    if (sourcef.isNotEmpty) {
      sourceApi = sourcef[0];
    } else {
      sourceApi = [];
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return WillPopScope(
      onWillPop: () {
        return customPop(context);
      },
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                RaisedButton(
                  color: mybtnClr,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(0.0),
                      side: BorderSide(color: Colors.blue)),
                  child: Text(
                    StringUtils.TitleMyAcc,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Bold,
                        color: mybtnTxtClr),
                  ),
                  onPressed: () {
                    myLeadSelected = true;
                    allLeadSelected = false;
                    _currentIndex = 0;

                    selectMyAccBtn();
                    navigateTo(0);
                    setState(() {});
                  },
                ),
                RaisedButton(
                  color: allbtnClr,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(0.0),
                      side: BorderSide(color: Colors.blue)),
                  child: Text(
                    StringUtils.TitleAllAcc,
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Bold,
                        color: allbtnTxtClr),
                  ),
                  onPressed: () {
                    myLeadSelected = false;
                    allLeadSelected = true;
                    _currentIndex = 1;
                    selectAllAccBtn();
                    navigateTo(1);
                    setState(() {});
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(_context).push(
                        MaterialPageRoute(
                            builder: (_context) => FilterAlertPg(
                                  sortOrder: sortingOrderf,
                                  sortType: sortingTypef,
                                  selectedSortingIndex: sortingIndexf,
                                  selectedtabIndex: _currentIndex,
                                  selectedFilter: selfilterf,
                                )),
                      );
                    },
                    child: SvgPicture.asset(
                      "images/filter.svg",
                      height: 25.0,
                      width: 25.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(_context).push(
                        MaterialPageRoute(
                            builder: (_context) => SortAlertPg(
                                  selectedtabIndex: _currentIndex,
                                  selectedSortingIndex: sortingIndexf,
                                  selectedFilter: selfilterf,
                                  types: sourcef,
                                )),
                      );
                    },
                    child: SvgPicture.asset(
                      "images/sort.svg",
                      height: 25.0,
                      width: 25.0,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: _fragments[_currentIndex],
            ),
          ],
        ),
      ),
    );
  }

  selectMyAccBtn() {
    mybtnClr = Colors.blue;
    mybtnTxtClr = Colors.white;
    allbtnClr = Colors.white;
    allbtnTxtClr = Colors.blue;
  }

  selectAllAccBtn() {
    allbtnTxtClr = Colors.white;
    allbtnClr = Colors.blue;
    mybtnClr = Colors.white;
    mybtnTxtClr = Colors.blue;
  }

  void navigateTo(int index) {
    _backstack.add(index);
    setState(() {
      _currentIndex = index;
    });
  }

  void navigateBack(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Future<bool> customPop(BuildContext context) {
    if (_currentIndex == 1) {
      navigateBack(0);
      myLeadSelected = true;
      allLeadSelected = false;
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
}

class AllAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new AllAccountPg(),
    );
  }
}

class AllAccountPg extends StatefulWidget {
  @override
  State createState() => new AllAccountPgState();
}

class AllAccountPgState extends State<AllAccountPg> {
  List<AccountDetailModel> retail = List();
  String token;

  bool isLoading_All = false;
  bool isFirstApiCalled_All = false;
  bool hasMore_All = true;
  int pageCount_All = 1;

  @override
  void initState() {
    super.initState();

    setState(() {
      setData();
    });
  }

  Widget buildlist(String id, String accName, String type, String segment,
      String ownerName, String address) {
    return InkWell(
      onTap: () {
        callActivity(id);
      },
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Text(
                accName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Bold,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
                padding: new EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      type,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.redAccent,
                      ),
                    ),
                    new Spacer(),
                    Text(
                      segment,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.deepPurple,
                      ),
                    ),
                  ],
                )),
            Padding(
              padding: new EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
              child: Text(
                ownerName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(
                  top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Text(
                address == null ? address = "" : address,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<AccountDetailModel> fnameList) {
    if (!isFirstApiCalled_All) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (fnameList == null || fnameList.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
          itemCount: retail.length + 1,
          itemBuilder: (context, index) {
            if (index == retail.length - 1) {
              getListApicall();
            }
            if (index == retail.length) {
              if (hasMore_All) {
                return CupertinoActivityIndicator();
              } else {
                return Container();
              }
            }
            return buildlist(
                fnameList[index].id,
                fnameList[index].accName,
                fnameList[index].typeInsRet,
                fnameList[index].segment == null
                    ? ""
                    : fnameList[index].segment,
                (fnameList[index].firstName == null
                        ? ""
                        : fnameList[index].firstName) +
                    " " +
                    (fnameList[index].lastName == null
                        ? ""
                        : fnameList[index].lastName),
                fnameList[index].address == null
                    ? " "
                    : fnameList[index].address.addressLine1 +
                        "," +
                        fnameList[index].address.addressLine2);
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ListView(retail),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: IconButton(
          iconSize: 50.0,
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          icon: Icon(
            Icons.add_circle,
            color: Colors.blue,
          ),
          onPressed: () {
            callFAB();
          },
        ),
      ),
    );
  }

  Future<AccountListModel> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<AccountDetailModel> retaillist = List();
    try {
      if (!isLoading_All && hasMore_All) {
        isLoading_All = true;
        isFirstApiCalled_All = true;

        AccountListModel d1 = await listapi();

        if (d1 == null) {
          retail = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });

          retail.addAll(retaillist);

          if (d1.paginationModel <= retail.length) {
            hasMore_All = false;
          } else {
            hasMore_All = true;
          }
          pageCount_All++;
          isLoading_All = false;
        }
        setState(() {});
        return d1;
      }
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }
    return null;
  }

  Future<AccountListModel> listapi() async {
    var resBody = {};
    resBody[AppUtils.KEY_userId] = null;
    resBody[AppUtils.KEY_typeIds] = sourceApi;
    resBody[AppUtils.KEY_sort] = {AppUtils.KEY_field: sortingType, AppUtils.KEY_order: sortingOrder};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount_All, AppUtils.KEY_limit: 15};
    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.AccountListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return AccountListModel.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);

    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        token = value;
        getListApicall();
      });
    });
  }
}

class MyAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyAccountPg(),
    );
  }
}

class MyAccountPg extends StatefulWidget {
  @override
  State createState() => new MyAccountPgState();
}

class MyAccountPgState extends State<MyAccountPg> {
  List<AccountDetailModel> retail = List();
  String token,userid;
  bool isLoading = false;
  bool isFirstApiCall = false;
  bool hasMore = true;
  int pageCount = 1;

  @override
  void initState() {
    super.initState();

    setState(() {
      setData();
    });
  }

  Widget buildlist(String id, String accName, String type, String segment,
      String ownerName, String address) {
    return InkWell(
      onTap: () {
        callActivity(id);
      },
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Text(
                accName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Bold,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
                padding:
                    new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      type,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.redAccent,
                      ),
                    ),
                    new Spacer(),
                    Text(
                      segment,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.deepPurple,
                      ),
                    ),
                  ],
                )),
            Padding(
              padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              child: Text(
                ownerName,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                address,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Regular,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<AccountDetailModel> fnameList) {
    if (!isFirstApiCall) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (fnameList == null || fnameList.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
        itemCount: retail.length + 1,
        itemBuilder: (context, index) {
          if (index == retail.length - 1) {
            getListApicall();
          }
          if (index == retail.length) {
            if (hasMore) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }

          return buildlist(
              fnameList[index].id,
              fnameList[index].accName,
              fnameList[index].typeInsRet,
              fnameList[index].segment == null ? "" : fnameList[index].segment,
              fnameList[index].firstName + " " + fnameList[index].lastName,
              fnameList[index].address == null
                  ? " "
                  : fnameList[index].address.addressLine1 +
                      "," +
                      fnameList[index].address.addressLine2);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ListView(retail),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: IconButton(
          iconSize: 50.0,
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          icon: Icon(
            Icons.add_circle,
            color: Colors.blue,
          ),
          onPressed: () {
            callFAB();
          },
        ),
      ),
    );
  }

  Future<AccountListModel> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<AccountDetailModel> retaillist = List();
    try {
      if (!isLoading && hasMore) {
        isLoading = true;
        isFirstApiCall = true;
        AccountListModel d1 = await listapi();
        if (d1 == null) {
          retail = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });
          retail.addAll(retaillist);
          if (d1.paginationModel <= retail.length) {
            hasMore = false;
          } else {
            hasMore = true;
          }
          pageCount++;
          isLoading = false;
        }
        setState(() {});

        return d1;
      }
    } catch (e) {

      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
      // Navigator.pushAndRemoveUntil(
      //     _context,
      //     MaterialPageRoute(
      //       builder: (ctx) => LoginScreen(),
      //     ),
      //     ModalRoute.withName("/LoginScreen"));

    }
    return null;
  }

  Future<AccountListModel> listapi() async {
    var resBody = {};
    resBody[AppUtils.KEY_userId] = userid;
    resBody[AppUtils.KEY_typeIds] = sourceApi;
    resBody[AppUtils.KEY_sort] = {AppUtils.KEY_field: sortingType, AppUtils.KEY_order: sortingOrder};
    resBody[AppUtils.KEY_pagination] = {AppUtils.KEY_page: pageCount, AppUtils.KEY_limit: 15};
    //{"owner":"ALL","userId":"60484b289aa6f6000824b2a3","typeIds":null,"tagIds":null,"accountRepIds":null,"entityTypeIds":null,"segmentIds":null,"annRevIds":null,"sort":{"field":"updatedBy.time","order":"desc"},"pagination":{"page":1,"limit":15,"total":0}}
    String str = json.encode(resBody);
    Response response = await Dio().post(
      AppUtils.AccountListUrl,
      data: str,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return AccountListModel.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        getListApicall();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}

callActivity(String id) {
  // AppUtils.showToastWithMsg(StringUtils.WOrkInProgress,ToastGravity.CENTER);

  Navigator.push(_context,
      MaterialPageRoute(builder: (context) => AccountDetail(accId: id)));
}

callFAB() {
  // AppUtils.showToastWithMsg(StringUtils.WOrkInProgress,ToastGravity.CENTER);
  Navigator.push(_context,
      MaterialPageRoute(builder: (context) => AccountCreation(isfrom: null)));
}
