import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:crmflutternew/dashboard/fragments/account/AccountDetail.dart';
import 'package:crmflutternew/dashboard/fragments/lead/LeadDetail.dart';
import 'package:crmflutternew/model/notification/NotificationDetailModel.dart';
import 'package:crmflutternew/model/notification/NotificationListModel.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new NotificationListPg(),
    );
  }
}

class NotificationListPg extends StatefulWidget {
  @override
  State createState() => new NotificationListPgState();
}

class NotificationListPgState extends State<NotificationListPg> {
  List<NotificationDetailModel> retail = List();
  String token;
  String loginToken = "Login_token";
  bool isLoading_All = false;
  bool isFirstApiCalled_All = false;
  bool hasMore_All = true;
  int pageCount_All = 1;

  @override
  void initState() {
    super.initState();
    setData();

    setState(() {});
  }

  Widget buildlist(String title, String body, String time, int statusreadUnread,
      String idToCall, int action) {
    print("time $time");
    // String fetchTimeonly = Constants.fetchTimeonly(time);
    String formqateddate = Constants.changeDateFromateAndGetDiffrence(time);

    return InkWell(
      onTap: () {
        // callActivity(id);
        if (action == 1) {
          //lead
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LeadDetail(leadId: idToCall)));
        } else if (action == 2) {
          //acc

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AccountDetail(accId: idToCall)));
        } else {
          //oppo
          print("tapped");
        }
      },
      child: Card(
        color: statusreadUnread == 3 ? null : Constants.COLOR_LOGIN_LOWER_BG2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Row(
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: AppUtils.FontName,
                      color: Colors.black,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    formqateddate,
                    style: TextStyle(
                      fontSize: 10.0,
                      fontFamily: AppUtils.FontName,
                      color: Colors.grey,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
                padding:
                    new EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      body,
                      style: TextStyle(
                        fontSize: 12.0,
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Regular,
                        color: Colors.black45,
                      ),
                    ),
                    // new Spacer(),
                    // Text(
                    //   convetredtime,
                    //   style: TextStyle(
                    //     fontSize: 14.0,
                    //     fontFamily: AppUtils.FontName,
                    //     fontWeight: AppUtils.Regular,
                    //     color: Colors.deepPurple,
                    //   ),
                    // ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<NotificationDetailModel> notificationlist) {
    if (!isFirstApiCalled_All) {
      return Center(child: CupertinoActivityIndicator());
    }
    if (notificationlist == null || notificationlist.length == 0) {
      return Center(
        child: Text("No Data Found"),
      );
    } else {
      return ListView.builder(
        itemCount: retail.length + 1,
        itemBuilder: (context, index) {
          if (index == retail.length - 1) {
            getListApicall();
          }
          if (index == retail.length) {
            if (hasMore_All) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }
          return buildlist(
              notificationlist[index].messageTitle,
              notificationlist[index].messageBody,
              notificationlist[index].createdAt,
              notificationlist[index].status,
              notificationlist[index].dataId,
              notificationlist[index].dataAction);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ListView(retail),
    );
  }

  Future<NotificationListModel> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<NotificationDetailModel> retaillist = List();
    try {
      if (!isLoading_All && hasMore_All) {
        isLoading_All = true;
        isFirstApiCalled_All = true;

        NotificationListModel d1 = await listapi();

        if (d1 == null) {
          retail = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });

          retail.addAll(retaillist);

          // if (d1.paginationModel <= retail.length) {
          hasMore_All = false;
          // } else {
          //   hasMore_All = true;
          // }
          pageCount_All++;
          isLoading_All = false;
        }
        setState(() {});
        return d1;
      }
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }

    return null;
  }

  Future<NotificationListModel> listapi() async {
    var resBody = {};

    Response response = await Dio().post(
      AppUtils.NotificationListUrl,
      data: {},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("here in api call");
    return NotificationListModel.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(loginToken);
    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        token = value;
        getListApicall();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
