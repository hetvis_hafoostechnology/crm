import 'package:flutter/material.dart';
import 'package:crmflutternew/dashboard/fragments/account/AccountDetail.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/ColorUtil.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/model/SampleModel.dart';

class AddNote extends StatefulWidget {
  String id; //note id
  String accountId;
  String token;
  String notedetailtext;
  String opportunityId;

  AddNote(
      {Key key,
      @required this.id,
      @required this.accountId,
      @required this.token,
      @required this.notedetailtext,
      @required this.opportunityId})
      : super(key: key);

  @override
  State createState() => new AddNotestate(
      id2: id,
      accountId2: accountId,
      notedetailtext2: notedetailtext,
      opportunityId2: opportunityId,
      token2: token);
}

class AddNotestate extends State<AddNote> {
  String id2;
  String accountId2;
  String opportunityId2;
  String token2;
  String notedetailtext2;
  TextEditingController detailController;

  AddNotestate(
      {Key key2,
      this.id2,
      this.accountId2,
      this.opportunityId2,
      this.token2,
      this.notedetailtext2});

  @override
  void initState() {
    super.initState();
    print("in addnote ${id2}&${accountId2}&${token2}&${notedetailtext2}");

    detailController = TextEditingController();
    if (notedetailtext2 != null) {
      detailController.text = notedetailtext2;
    }
    setState(() {});
  }

//SingleChildScrollView(
//         child: Stack(
//       children: <Widget>[
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            title: Padding(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 10.0),
              child: Text(
                'Add Note',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.only(right: 10.0, top: 0.0, bottom: 10.0),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )),
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        reverse: true,
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 20.0, right: 10.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Detail',
                      style: TextStyle(color: Colors.black45, fontSize: 18.0),
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 10.0),
                    child: Container(
                      height: 200,
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Colors.black12,
                          width: 2.0,
                        ),
                      ),
                      child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: TextFormField(
                            controller: detailController,
//                  textInputAction: TextInputAction.next,
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            keyboardType: TextInputType.multiline,
                            minLines: 1,
                            maxLines: 10,
                            style: TextStyle(
                                fontSize: AppUtils.FontSize15,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Regular,
                                color: ColorUtil.Black),
                          )),
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                      child: OutlineButton(
                        child: Text(
                          StringUtils.BtnCancel,
                          style: TextStyle(
                              fontFamily: AppUtils.FontName,
                              fontWeight: AppUtils.SemiBold,
                              color: Constants.COLOR_CARD_SIGNIN_TITLE),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        borderSide: BorderSide(
                            color: Constants.COLOR_CARD_SIGNIN_TITLE),
                        highlightElevation: 4.0,
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(30.0)
//                      )
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                      child: RaisedButton(
                        color: Constants.COLOR_CARD_SIGNIN_TITLE,
                        elevation: 4,
                        child: Text(
                          StringUtils.BtnSave,
                          style: TextStyle(
                              fontFamily: AppUtils.FontName,
                              fontWeight: AppUtils.SemiBold,
                              color: Constants.COLOR_TEXT),
                        ),
                        splashColor: Constants.COLOR_CARD_FP,
                        onPressed: () {
                          getListApicall();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<SampleModel> getListApicall() async {
    try {
      SampleModel d1 = await listapi();
      AppUtils.showToastWithMsg(StringUtils.NoteSaveSuccess, ToastGravity.BOTTOM);

      popPg(context);
      setState(() {});
      //setState(() {});
      print(d1.message);
      return d1;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<SampleModel> listapi() async {
    print("details${id2}&${accountId2}&${detailController.text}");
    Response response = await Dio().post(
      AppUtils.NoteAddUrl,
      data: {
        AppUtils.KEY_id: id2,
        AppUtils.KEY_accountId: accountId2,
        "opportunityId": opportunityId2,
        "detail": detailController.text
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token2
      }),
    );
//    print(response.data);
    return SampleModel.fromjson(response.data);
  }

  popPg(BuildContext ctx) {
    Navigator.pushAndRemoveUntil(
        ctx,
        MaterialPageRoute(
          builder: (context) => AccountDetail(accId: accountId2),
        ),
        ModalRoute.withName("/Account"));
  }
}
