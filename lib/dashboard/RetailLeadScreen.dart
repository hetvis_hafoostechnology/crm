import 'package:country_code_picker/country_code_picker.dart';
import 'package:crmflutternew/dashboard/fragments/lead/DocumentDialogue.dart';
import 'package:crmflutternew/model/SourceDropDown/SourceDropdown.dart';
import 'package:crmflutternew/model/TitleDropDown/TitleDropdown.dart';
import 'package:crmflutternew/model/document/DetailLeadDocumentList.dart';
import 'package:crmflutternew/model/document/FilesDetails.dart';
import 'package:crmflutternew/model/document/LeadDocumentList.dart';
import 'package:crmflutternew/model/lead/GetLeadDetails.dart';
import 'package:crmflutternew/model/lead/Retailead.dart';
import 'package:crmflutternew/model/leadStorage/LeadStorageDropdown.dart';
import 'package:crmflutternew/model/dropdown/tag/TaggedDropdown.dart';
import 'package:crmflutternew/model/salutation/SalutationDropdown.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/ColorUtil.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:fimber/fimber_base.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crmflutternew/model/assigned/AssignedDropdown.dart';
import 'package:url_launcher/url_launcher.dart';

class RetailLeadScreen extends StatefulWidget {
  String isfrom;

  RetailLeadScreen({Key key, @required this.isfrom}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _RetailLeadScreenState(getisfrom: isfrom);
  }
}

class _RetailLeadScreenState extends State<RetailLeadScreen> {
  List<String> result;
  List<String> resultTitle;
  List<String> resultLsrc;
  List<String> resultLstage;
  List<String> resultSalutation;
  List<String> idsAssign;
  List<String> titleid;
  List<String> salutationid;
  List<String> lsrcid;
  List<String> lStageid;
  String id;
  String fromLeadAssignedid;
  String selectedcc = "+1";
  String selectedcc2 = "+1";
  String getisfrom;

  _RetailLeadScreenState({Key key2, @required this.getisfrom});

  TextEditingController fnameController,
      lnameController,
      emailController,
      mobController,
      phoneController;

  String userid;
  String loginId = "Login_Id";
  String token;
  String loginToken = "Login_token";
  List _myActivities_dp;
  String name;
  String selectedName = null;
  String dropdownTitleValue = null;
  var dropdownTitleID;
  String dropdownLsrcValue = null;
  var dropdownLsrcID;
  String dropdownLstageValue = null;
  var dropdownLstageID;
  String dropdownSalutation = null;
  var dropdownSalutationId;
  var selectedid;
  var assignedName;
  String _myActivitiesResult;
  List resultTag;
  List<String> tagid;
  List dropdowncItemsTag;
  String dropdownTag = null;
  var dropdownTagId;
  ProgressDialog pr;
  final FocusNode _fnameFocus = FocusNode();
  final FocusNode _lnameFocus = FocusNode();
  final FocusNode _titleFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _mobFocus = FocusNode();
  final FocusNode _teleFocus = FocusNode();
  final FocusNode _ownerFocus = FocusNode();
  final FocusNode _srcFocus = FocusNode();
  final FocusNode _lsFocus = FocusNode();
  final formKey = new GlobalKey<FormState>();
  bool isLoadingDoc = false;
  bool isFirstApiCalledDoc = false;
  bool hasMoreDoc = true;
  int pageCountDoc = 0;
  List<DetailLeadDocumentList> leadlist = List();
  List<FilesDetails> fileList = List();

  @override
  void initState() {
    result = List();
    resultTitle = List();
    resultLsrc = List();
    resultLstage = List();
    resultSalutation = List();
    resultTag = List();
    dropdowncItemsTag = List();
    _myActivitiesResult = '';

    print("get keyyy $getisfrom");

    setData();
    setState(() {});
    fnameController = TextEditingController();
    lnameController = TextEditingController();
    emailController = TextEditingController();
    mobController = TextEditingController();
    phoneController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Material(
        child: Center(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
            child: Text(StringUtils.TitleSalutaion,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callSalutationDropdown(),
//          DropdownButtonFormField<String>(
//            decoration: InputDecoration(
//              contentPadding: EdgeInsets.all(3.0),
//              border:
//                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
//            ),
//            value: dropdownValue,
//            icon: Icon(Icons.arrow_drop_down),
//            iconSize: 24,
//            style: TextStyle(color: Colors.black),
//            onChanged: (String newValue) {
//              setState(() {
//                dropdownValue = newValue;
//              });
//            },
//            items: <String>['Select option', 'One', 'Two', 'Free', 'Four']
//                .map<DropdownMenuItem<String>>((String value) {
//              return DropdownMenuItem<String>(
//                value: value,
//                child: Text(value),
//              );
//            }).toList(),
//          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleFname,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          TextFormField(
            textInputAction: TextInputAction.next,
            focusNode: _fnameFocus,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _fnameFocus, _lnameFocus);
            },
            style: TextStyle(
                fontSize: AppUtils.FontSize15,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Regular,
                color: ColorUtil.Black),
            controller: fnameController,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleLName,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          TextFormField(
            textInputAction: TextInputAction.next,
            focusNode: _lnameFocus,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _lnameFocus, _emailFocus);
            },
            style: TextStyle(
                fontSize: AppUtils.FontSize15,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Regular,
                color: ColorUtil.Black),
            controller: lnameController,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleTitle,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callTitledropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleEmail,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          TextFormField(
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            focusNode: _emailFocus,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _emailFocus, _mobFocus);
            },
            style: TextStyle(
                fontSize: AppUtils.FontSize15,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Regular,
                color: ColorUtil.Black),
            autofocus: false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleMob,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Flexible(
                child: CountryCodePicker(
                  onChanged: _onCountryChangeMob,
                  initialSelection: 'US',
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: false,
                  alignLeft: false,
                ),
                flex: 1,
              ),
              new Flexible(
                child: TextFormField(
                  controller: mobController,
                  maxLength: 11,
                  textInputAction: TextInputAction.next,
                  focusNode: _mobFocus,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, _mobFocus, _teleFocus);
                  },
                  style: TextStyle(
                      fontSize: AppUtils.FontSize15,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Regular,
                      color: ColorUtil.Black),
                  keyboardType: TextInputType.phone,
                  autofocus: false,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                  ),
                ),
                flex: 3,
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitlePhone,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Flexible(
                child: CountryCodePicker(
                  onChanged: _onCountryChangePhone,
                  initialSelection: 'US',
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: false,
                  alignLeft: false,
                ),
                flex: 1,
              ),
              new Flexible(
                child: TextFormField(
                  controller: phoneController,
                  maxLength: 11,
                  textInputAction: TextInputAction.done,
                  focusNode: _teleFocus,
                  keyboardType: TextInputType.phone,
                  autofocus: false,
                  style: TextStyle(
                      fontSize: AppUtils.FontSize15,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Regular,
                      color: ColorUtil.Black),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                  ),
                ),
                flex: 3,
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleOwner,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callAssigneddropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleSource,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callLsrcropdown(),
          Padding(
            padding: EdgeInsets.only(left: 0.0, bottom: 10.0, top: 10.0),
            child: Text(StringUtils.TitleLeadStage,
                style: TextStyle(
                    fontSize: AppUtils.FontSize16,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.SemiBold,
                    color: ColorUtil.Grey)),
          ),
          callLstageDropdown(),
          callTagDropdown(),
          getisfrom!=null?Padding(
            padding: new EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      StringUtils.TitleDocument,
                      style: TextStyle(
                          fontSize: AppUtils.FontSize16,
                          color: ColorUtil.Black,
                          fontWeight: AppUtils.SemiBold,
                          fontFamily: AppUtils.FontName),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    new Spacer(),
                    IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () {
                        _navigateToDoc(context,null,null,null);
                      },
                    )
                  ],
                ),
              ],
            ),
          ):Container(),
          getisfrom!=null?SizedBox(
            height: 300,
            child: _ListView(leadlist),
          ):Container(),

          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: OutlineButton(
                  child: Text(
                    StringUtils.BtnCancel.toUpperCase(),
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.SemiBold,
                        color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  borderSide:
                      BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                  highlightElevation: 4.0,
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(30.0)
//                      )
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: RaisedButton(
                  color: Constants.COLOR_CARD_SIGNIN_TITLE,
                  elevation: 4,
                  child: Text(
                    StringUtils.BtnSave.toUpperCase(),
                    style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.SemiBold,
                        color: Constants.COLOR_TEXT),
                  ),
                  splashColor: Constants.COLOR_CARD_FP,
                  onPressed: () {
                    if (_isValidData()) {
                      getRetailApicall();
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }

  bool _isValidData() {
    var fname = fnameController.text;
    var lname = lnameController.text;
    var email = emailController.text;
    var mob = mobController.text;

    if (dropdownSalutation == null) {
      AppUtils.showAlert(context, StringUtils.SelectSalutation);
      return false;
    } else if (fname == null || fname.length == 0) {
      AppUtils.showAlert(context, StringUtils.FirstNameHint);
      return false;
    } else if (lname == null || lname.length == 0) {
      AppUtils.showAlert(context, StringUtils.LastNameHint);
      return false;
    } else if (dropdownTitleValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectTitle);
      return false;
    } else if (email == null || email.length == 0) {
      AppUtils.showAlert(context, StringUtils.EmailHint);
      return false;
    } else if (!AppUtils.isEmail(email)) {
      AppUtils.showAlert(context, StringUtils.EmailValidHint);
      return false;
    } else if (mob == null || mob.length == 0) {
      AppUtils.showAlert(context, StringUtils.MobHint);
      return false;
    }
    // else if (!AppUtils.isMobile(mob)) {
    //   AppUtils.showAlert(context, StringUtils.MobValidHint);
    //   return false;
    // }
    else if (dropdownLsrcValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectSource);
      return false;
    } else if (dropdownLstageValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectLstage);
      return false;
    }

    return true;
  }

  Widget _ListView(List<DetailLeadDocumentList> listt) {
    if (!isFirstApiCalledDoc) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (listt == null || listt.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
          itemCount: leadlist.length + 1,
          itemBuilder: (context, index) {
            if (index == leadlist.length - 1) {
              getListApicall();
            }
            if (index == leadlist.length) {
              if (hasMoreDoc) {
                return CupertinoActivityIndicator();
              } else {
                return Container();
              }
            }
            return buildlist(listt[index].name, listt[index].files.datas,listt[index].id);
          });
    }
  }

  Widget _ListViewFiles(List<FilesDetails> files) {
    if (!isFirstApiCalledDoc) {
      return Center(child: CupertinoActivityIndicator());
    }

    if (files == null || files.length == 0) {
      return Center(
        child: Text(StringUtils.TxtNoData),
      );
    } else {
      return ListView.builder(
          itemCount: files.length + 1,
          itemBuilder: (context, index) {
            if (index == files.length - 1) {
              getListApicall();
            }
            if (index == files.length) {
              if (hasMoreDoc) {
                return CupertinoActivityIndicator();
              } else {
                return Container();
              }
            }
            return buildlistFiles(files[index].fileName, files[index].fileSize,
                files[index].filePAth);
          });
    }
  }

  Widget buildlist(String name, List<FilesDetails> fileList,String docid) {
    return Card(
      child: Column(
        children: [
          Row(
            children: <Widget>[
              Padding(
                padding: new EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
                child: Row(
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Constants.COLOR_CARD_SIGNIN_TITLE,
                        ),
                        onPressed: () {
                          _navigateToDoc(context,name,fileList,docid);
                        })
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 300,
            child: _ListViewFiles(fileList),
          ),
          // Padding(
          //   padding: new EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
          //   child: Text(
          //     filename,
          //     style: TextStyle(
          //       fontSize: 14.0,
          //       fontWeight: FontWeight.bold,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

  Widget buildlistFiles(String filename, int filesize, String path) {
    return Card(
      child: Row(
        children: <Widget>[
          Padding(
            padding: new EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
            child: Text(
              filename + " | " + filesize.toString(),
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.black,
              ),
            ),
          ),
          Padding(
              padding: new EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
              child: RaisedButton(
                padding: EdgeInsets.all(0),
                child: Text(StringUtils.TitleDownload,
                    style: TextStyle(fontSize: 10.0)),
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    side: BorderSide(color: Constants.COLOR_CARD_BUTTON)),
                color: Colors.white,
                textColor: Constants.COLOR_CARD_BUTTON,
                onPressed: () {
                  AppUtils.launchInBrowser(
                      "https://s3.amazonaws.com/hafooz-uploads-dev/" + path);
                },
              )),
        ],
      ),
    );
  }

  callTitledropdown() {
    if (resultTitle != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownTitleValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownTitleValue = newValue;
            int indexid = resultTitle.indexOf(newValue);
            dropdownTitleID = titleid.asMap()[indexid];
          });
        },
        items: getTitleItems(),
      );
    } else {
      return Container();
    }
  }

  getTitleItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultTitle.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  Future<TitleDropdown> getDropDownApicallTitle() async {
    try {
      TitleDropdown d1 = await titleDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultTitle = save1;
      titleid = saveid;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<TitleDropdown> titleDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f2f4bbe58ac3696832e9"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return TitleDropdown.fromjson(response.data);
  }

  callSalutationDropdown() {
    if (resultSalutation != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownSalutation,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            FocusScope.of(context).requestFocus(_fnameFocus);

            dropdownSalutation = newValue;
            int indexid = resultSalutation.indexOf(newValue);
            dropdownSalutationId = salutationid.asMap()[indexid];
          });
        },
        items: getSalutationItems(),
      );
    } else {
      return Container();
    }
  }

  getSalutationItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultSalutation.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  Future<SalutationDropdown> getDropDownApicallSalutation() async {
    try {
      SalutationDropdown d1 = await salutationDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultSalutation = save1;
      salutationid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<SalutationDropdown> salutationDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f339bd6d9666735dcc3f"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SalutationDropdown.fromjson(response.data);
  }

  getLsrcItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultLsrc.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  callLsrcropdown() {
    if (resultLsrc != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownLsrcValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(color: Colors.black),
        onChanged: (String newValue) {
          setState(() {
            dropdownLsrcValue = newValue;
            int indexid = resultLsrc.indexOf(newValue);
            dropdownLsrcID = lsrcid.asMap()[indexid];
          });
        },
        items: getLsrcItems(),
      );
    } else {
      return Container();
    }
  }

  Future<SourceDropdown> getDropDownApicallLeadSrc() async {
    try {
      SourceDropdown d1 = await lSrcDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultLsrc = save1;
      lsrcid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<SourceDropdown> lSrcDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f734a86694d93ec1e8d1"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return SourceDropdown.fromjson(response.data);
  }

  Future<TaggedDropdown> TagTypedropdown() async {
    Response response = await Dio().post(
      AppUtils.TagDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("tag dp ${response.data[AppUtils.KEY_DATA]}");
    return TaggedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  Future<TaggedDropdown> getDropDownApicallTag() async {
    print("88");
    try {
      TaggedDropdown d1 = await TagTypedropdown();

      d1.datas.forEach((cvalue) {
        dropdowncItemsTag.add({"display": cvalue.name, "value": cvalue.id});
      });
      print("tag items $dropdowncItemsTag");
      // tagid = saveid;
//      idContact = saveid1;
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  callTagDropdown() {
    if (resultTag != null) {
      print("tag items $dropdowncItemsTag");
      return new Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: MultiSelectFormField(
                      autovalidate: false,
                      titleText: StringUtils.TitleTag,
                      validator: (value) {
                        if (value == null || value.length == 0) {
                          return 'Please select one or more options';
                        }
                      },
                      dataSource: dropdowncItemsTag,
                      textField: 'display',
                      valueField: 'value',
                      okButtonLabel: 'OK',
                      cancelButtonLabel: 'CANCEL',

                      // required: true,
                      hintText: 'Select options',
                      initialValue: _myActivities_dp,
                      onSaved: (value) {
                        print("tap val $value");
                        if (value == null) return;
                        setState(() {
                          _myActivities_dp = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            child: Text(_myActivitiesResult),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  callLstageDropdown() {
    if (resultLstage != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownLstageValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(fontFamily: AppUtils.FontName, color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            dropdownLstageValue = newValue;
            int indexid = resultLstage.indexOf(newValue);
            dropdownLstageID = lStageid.asMap()[indexid];
          });
        },
        items: getLStageItems(),
      );
    } else {
      return Container();
    }
  }

  getLStageItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultLstage.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  Future<LeadStorageDropdown> getDropDownApicallLeadStage() async {
    pr.show();
    try {
      LeadStorageDropdown d1 = await lStageDropdown();
      List<String> save1 = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        String titlename = data.text;
        String id = data.id;
        saveid.add(id);
        save1.add(titlename);
      });
      resultLstage = save1;
      lStageid = saveid;
//      idContact = saveid1;
      setState(() {});
      pr.hide();
      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<LeadStorageDropdown> lStageDropdown() async {
    Response response = await Dio().post(
      AppUtils.MasterGetListsUrl,
      data: {AppUtils.KEY_typeId: "5e47f73ab7197e5c279285cf"},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return LeadStorageDropdown.fromjson(response.data);
  }

  Future<AssignedDropdown> getAssignedtoApicall() async {
    try {
      AssignedDropdown d1 = await assignedDropdown();
//      result = List();
      List<String> save = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        name = data.name;
        id = data.id;
        save.add(name);
        saveid.add(id);
        if (getisfrom != null) {
          print("hhehree$fromLeadAssignedid");
          if (id == fromLeadAssignedid) {
            selectedid = id;
            selectedName = data.name;
            assignedName = id;
          }
        } else {
          if (id == userid) {
            selectedid = id;
            selectedName = data.name;
            assignedName = id;
          }
        }

//        String fname = data.firstname;
//        String lname = data.lastname;
      });
      result = save;
      idsAssign = saveid;

      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<AssignedDropdown> assignedDropdown() async {
    Response response = await Dio().post(
      AppUtils.UserDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    return AssignedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  callAssigneddropdown() {
    print(selectedid);
    if (result != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: selectedName,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(
            fontSize: AppUtils.FontSize15,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.Regular,
            color: ColorUtil.Black),
        onChanged: (String newValue) {
          setState(() {
            selectedName = newValue;
            int indexid = result.indexOf(newValue);

            assignedName = idsAssign.asMap()[indexid];
            //idsAssign.indexOf(indexid.toString());
          });
        },
        items: getAssignedItems(),
      );
    } else {
      return Container();
    }
  }

  getAssignedItems() {
    var dropdownItems = List<DropdownMenuItem<String>>();
    result.forEach((value) {
      dropdownItems.add(DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      ));
    });
    return dropdownItems;
  }

  Future<Retaillead> getRetailApicall() async {
    try {
      Retaillead d1 = await getRetailList();
      AppUtils.showToastWithMsg(
          StringUtils.LeadSaveSuccess, ToastGravity.BOTTOM);
      Navigator.of(context).pop();
      setState(() {});
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(context, e);
    }
  }

  Future<Retaillead> getRetailList() async {
    Response response = await Dio().post(
      AppUtils.LeadSaveUrl,
      data: {
        AppUtils.KEY_id: getisfrom,
        AppUtils.KEY_entityType: "retail",
        AppUtils.KEY_Email: emailController.text,
        AppUtils.KEY_firstName: fnameController.text,
        AppUtils.KEY_lastName: lnameController.text,
        AppUtils.KEY_Mobile: selectedcc + " " + mobController.text,
        AppUtils.KEY_ownerId: assignedName,
        AppUtils.KEY_salutation: dropdownSalutationId,
        AppUtils.KEY_source: dropdownLsrcID,
        AppUtils.KEY_stage: dropdownLstageID,
        AppUtils.KEY_tagIds: _myActivities_dp,
        AppUtils.KEY_telephone: selectedcc2 + " " + phoneController.text,
        AppUtils.KEY_title: dropdownTitleID,
        AppUtils.KEY_updatedBy: userid
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return Retaillead.fromjson_lead(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(loginId);
    token = preferences.getString(loginToken);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        if (getisfrom != null) {
          getAPicall();
          getListApicall();
        }
        getAssignedtoApicall();
        getDropDownApicallTitle();
        getDropDownApicallLeadSrc();
        getDropDownApicallLeadStage();
        getDropDownApicallSalutation();
        getDropDownApicallTag();
      });
    });
  }

  Future<LeadDocumentList> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<DetailLeadDocumentList> retaillist = List();
    List<FilesDetails> filess = List();
    try {
      if (!isLoadingDoc && hasMoreDoc) {
        isLoadingDoc = true;
        isFirstApiCalledDoc = true;

        LeadDocumentList d1 = await listapi();

        if (d1 == null) {
          leadlist = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });
          leadlist.addAll(retaillist);

          if (d1.paginationModel <= leadlist.length) {
            hasMoreDoc = false;
          } else {
            hasMoreDoc = true;
          }
          pageCountDoc++;
          isLoadingDoc = false;
        }
        setState(() {});
        return d1;
      }
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }
    return null;
  }

  Future<LeadDocumentList> listapi() async {
    Response response = await Dio().post(
      AppUtils.DocumentListUrl,
      data: {
        AppUtils.KEY_pagination: {
          AppUtils.KEY_page: pageCountDoc,
          AppUtils.KEY_limit: 5
        },
        AppUtils.KEY_typeId: getisfrom,
        AppUtils.KEY_type: "lead"
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return LeadDocumentList.fromjson(response.data);
  }

  void _onCountryChangeMob(CountryCode countryCode) {
    selectedcc = countryCode.toString();
  }

  void _onCountryChangePhone(CountryCode countryCode) {
    selectedcc2 = countryCode.toString();
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Future<GetLeadDetails> getAPicall() async {
    try {
      GetLeadDetails d1 = await leadDetailsApi();

      fnameController.text = d1.firstName;
      lnameController.text = d1.lastName;

      if (d1.mobile != null && d1.mobile != "") {
        //split string
        if (d1.mobile.contains(" ")) {
          var arr = d1.mobile.split(" ");

          selectedcc = arr[0];
          mobController.text = arr[1];
        } else {
          mobController.text = d1.mobile;
        }
      }
      if (d1.email != null && d1.email != "") {
        emailController.text = d1.email;
      }

      dropdownTitleValue = d1.title;
      dropdownTitleID = d1.titleId;

      dropdownSalutation = d1.salutation;
      dropdownSalutationId = d1.salutationId;

      dropdownLstageValue = d1.stage;
      dropdownLstageID = d1.stageId;

      if (d1.telephone != null && d1.telephone != "") {
        if (d1.telephone.contains(" ")) {
          var arr = d1.mobile.split(" ");

          selectedcc2 = arr[0];
          phoneController.text = arr[1];
        } else {
          phoneController.text = d1.telephone;
        }
      }
      if (d1.owner != null) {
        print("in api owner");
        print(d1.ownerId);
        fromLeadAssignedid = d1.ownerId;
        selectedName = d1.owner;
      }
      print("tags${d1.tagids}");
      List saveid = List();
      if (d1.tagids != null) {
        d1.tagids.forEach((data) {
          String tagid = data;
          saveid.add(tagid);
        });
        print("tag ids${saveid}");
        _myActivities_dp = d1.tagids;
      }
      dropdownLsrcValue = d1.source;
      dropdownLsrcID = d1.sourceId;
      setState(() {});
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(context, e);
    }
  }

  Future<GetLeadDetails> leadDetailsApi() async {
    Response response = await Dio().post(
      AppUtils.LeadGetDetailsUrl,
      data: {AppUtils.KEY_id: getisfrom},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return GetLeadDetails.fromJsonResponse(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  _navigateToDoc(BuildContext context,String name,List<FilesDetails> file,String docid) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => DocumentDialogue(leadId: getisfrom,docname: name,list: file,docId: docid,)),
    );
  }
}
