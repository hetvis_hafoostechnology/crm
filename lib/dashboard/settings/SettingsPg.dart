import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/StringUtils.dart';
//PUT BUTTONS FOR SAVE CHANGES OR RESET CHANGES HERE
class SettingsPg extends StatelessWidget {
  // SettingsPg(this.itemid);
  //
  // final String itemid;

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new SettingsPg1(),
    );
  }
}

class SettingsPg1 extends StatefulWidget {
  @override
  State createState() => new SettingsPgState();
}

class SettingsPgState extends State<SettingsPg1> {
  final List<String> titles = <String>[
    'Allow Sound',
    'Allow Vibration',
    // 'Allow Badge',
    "Allow Content"
  ];
  bool allowSoundSp = true,
      allowVibrationSp = true,
      // allowBadgeSp = true,
      allowNotificationFeatures = true,
      allowContentSp = true;

  List<bool> values = [];

  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
// print("ietem $itemid");
    setState(() {
      // getSpValues();
    });
  }

  getSpValues() async {
    prefs = await SharedPreferences.getInstance();
    allowSoundSp = prefs.getBool('allowNotification_sound') == null
        ? true
        : prefs.getBool('allowNotification_sound');
    allowVibrationSp = prefs.getBool('allowNotification_vibration') == null
        ? true
        : prefs.getBool('allowNotification_vibration');
    // allowBadgeSp = prefs.getBool('allowNotification_badge') == null
    //     ? true
    //     : prefs.getBool('allowNotification_badge');
    allowContentSp = prefs.getBool('allowNotification_content') == null
        ? true
        : prefs.getBool('allowNotification_content');
    allowNotificationFeatures = prefs.getBool('AllNotificationFeaturesAllow') == null
        ? true
        : prefs.getBool('AllNotificationFeaturesAllow');

    setState(() {
      values = <bool>[
        allowSoundSp,
        allowVibrationSp,
        // allowBadgeSp,
        allowContentSp
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: BackButton(
        //   onPressed: (){
        //     Navigator.of(context).pop(true);
        //   },
        // ),
        title: new Text(
          StringUtils.TitleSettings,
          style: TextStyle(
            fontSize: 16.0,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.SemiBold,
          ),
        ),

      ),
      body: Column(
        children: [
          Container(
            padding:
                EdgeInsets.only(top: 20.0, right: 10.0, left: 5.0, bottom: 0.0),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 5.0, right: 10.0, left: 20.0),
                    child: Text(
                      "Show Notifications",
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                  flex: 8,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
                    child: Switch(
                      value: (allowSoundSp==false&&allowVibrationSp==false&&allowContentSp==false)?false:allowNotificationFeatures,
                      onChanged: (bool value) {
                        setState(() {
                          allowNotificationFeatures = value;
                          setAllowAll();
                        });
                        print("allow not $allowNotificationFeatures");
                        if (allowNotificationFeatures == false) {
                          offAllNotificationFeatures();
                        } else {
                          onAllNotificationFeatures();

                        }
                      },
                    ),
                  ),
                  flex: 2,
                ),
              ],
            ),
          ),
          allowNotificationFeatures == true
              ? ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: titles.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 5.0, right: 5.0, left: 5.0),
                              child: Text(titles[index]),
                            ),
                            flex: 8,
                          ),
                          index == 0
                              ? setSwitch(index, allowSoundSp)
                              : index == 1
                                  ? setSwitch(index, allowVibrationSp)
                                  : index == 2
                                          ? setSwitch(index, allowContentSp)
                                          : Container(),
                          //index == 0
                          //                               ? setSwitch(index, allowSoundSp)
                          //                               : index == 1
                          //                                   ? setSwitch(index, allowVibrationSp)
                          //                                   : index == 2
                          //                                       ? setSwitch(index, allowBadgeSp)
                          //                                       : index == 3
                          //                                           ? setSwitch(index, allowContentSp)
                          //                                           : Container(),
                        ],
                      ),
                    );
                  },
                )
              : Container()
        ],
      ),
    );
  }

  Widget setSwitch(index, switchval) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
        child: Switch(
          value: switchval,
          onChanged: (bool value) {
            setState(() {
              switchval = value;
              setSpvals(index, value);
            });
          },
        ),
      ),
      flex: 2,
    );
  }

  setAllowAll() async{
    await prefs.setBool("AllNotificationFeaturesAllow", allowNotificationFeatures);
  }
  //call this method on apply and call getsp on reset or cancel btn click
  setSpvals(index, value) async {
    if (index == 0) {
      print('changed $index');
      allowSoundSp = value;
    } else if (index == 1) {
      print('changed $index');
      allowVibrationSp = value;
    }
    // else if (index == 2) {
    //   allowBadgeSp = value;
    // }
    else {
      print('changed $index');
      allowContentSp = value;
    }
    await prefs.setBool("allowNotification_sound", allowSoundSp);
    await prefs.setBool("allowNotification_vibration", allowVibrationSp);
    // await prefs.setBool("allowNotification_badge", allowBadgeSp);
    await prefs.setBool("allowNotification_content", allowContentSp);
    setState(() {});
  }

  onAllNotificationFeatures() async {
    allowSoundSp = true;
    allowVibrationSp = true;
    // allowBadgeSp = true;
    allowNotificationFeatures = true;
    allowContentSp = true;
    await prefs.setBool("allowNotification_sound", true);
    await prefs.setBool("allowNotification_vibration", true);
    // await prefs.setBool("allowNotification_badge", true);
    await prefs.setBool("allowNotification_content", true);
  }

  offAllNotificationFeatures() async {
    allowNotificationFeatures = false;
    await prefs.setBool("allowNotification_sound", false);
    await prefs.setBool("allowNotification_vibration", false);
    // await prefs.setBool("allowNotification_badge", false);
    await prefs.setBool("allowNotification_content", false);
  }

  @override
  void dispose() {
    print("dispossal");
    super.dispose();
    // if(allowContentSp==false&&allowVibrationSp==false&&allowSoundSp==false){
    //   allowNotificationFeatures=false;
    // }
    //  prefs.setBool("AllNotificationFeaturesAllow", allowNotificationFeatures);
  }
}
