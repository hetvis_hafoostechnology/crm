import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

//void main() {
//  runApp(MapExample());
//}

class MapExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: " Google Map",
      home: MyMap(),
    );
  }
}

class MyMap extends StatefulWidget {
  @override
  _MyMapState createState() => _MyMapState();
}

class _MyMapState extends State<MyMap> {
  GoogleMapController _mapController;
  MapType _type = MapType.normal;
  Map<CircleId, Circle> circles = <CircleId, Circle>{};
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<PolylineId, Polyline> polyLines = <PolylineId, Polyline>{};

  bool _nightMode = false;

  @override
  void dispose() {
    super.dispose();
  }

  static final _options = CameraPosition(
    target: LatLng(37.526330, -77.460830),
    zoom: 11,
    //37.526330, -77.460830
  );

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _mapController = controller;
      addMarker();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: const Text('Google Maps demo'),

      ),
      body: GoogleMap(
        initialCameraPosition: _options,
        onMapCreated: onMapCreated,
        mapType: _type,
        circles: Set<Circle>.of(circles.values),
        myLocationEnabled: true,
        polylines: Set<Polyline>.of(polyLines.values),
        markers: Set<Marker>.of(markers.values),
      ),
    );
  }
///_createDrawerListTile("Add Marker", addMarker),



  ///Drop marker at the Ahmadabad
  void addMarker() {
    if (markers.isNotEmpty) {
      Navigator.of(context).pop();
      return;
    }

    final Marker marker = Marker(
      markerId: MarkerId("1"),
      position: LatLng(37.526330, -77.460830),
      infoWindow: InfoWindow(title: "Marker", snippet: 'Ahmedabad'),
      onTap: () {
        _onMarkerTapped(MarkerId("1"));
      },
    );
    final Marker marker2 = Marker(
      markerId: MarkerId("2"),
      position: LatLng(21.0225, 71.5714),
      infoWindow: InfoWindow(title: "Marker2", snippet: '2'),
      onTap: () {
        _onMarkerTapped(MarkerId("2"));
      },
    );
    setState(() {
      markers[MarkerId("2")] = marker;
    });
    Navigator.of(context).pop();
  }

//  ///Draw circle at the Ahmadabad
//  void addCircle() {
//    if (circles.isNotEmpty) {
//      Navigator.of(context).pop();
//      return;
//    }
//    final Circle circle = Circle(
//      circleId: CircleId("1"),
//      consumeTapEvents: true,
//      strokeColor: Colors.blueAccent.withOpacity(0.5),
//      fillColor: Colors.lightBlue.withOpacity(0.5),
//      center: LatLng(23.0225, 72.5714),
//      strokeWidth: 5,
//      radius: 5000,
//      onTap: () {},
//    );
//
//    setState(() {
//      circles[CircleId("1")] = circle;
//    });
//
//    Navigator.of(context).pop();
//  }

//  void addPolyline() {
//    if (polyLines.isNotEmpty) {
//      Navigator.of(context).pop();
//      return;
//    }
//
//    final Polyline polyline = Polyline(
//      polylineId: PolylineId("1"),
//      consumeTapEvents: true,
//      color: Colors.blueAccent,
//      width: 5,
//      points: [
//        LatLng(23.0225, 72.5714),
//        LatLng(22.3072, 73.1812),
//        LatLng(21.1702, 72.8311),
//      ],
//      onTap: () {
//        _onPolylineTapped(PolylineId("1"));
//      },
//    );
//
//    setState(() {
//      polyLines[PolylineId("1")] = polyline;
//    });
//
//    Navigator.of(context).pop();
//  }

  void _onMarkerTapped(MarkerId markerId) {
    print("Marker $markerId Tapped!");
  }


}