import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/model/visit/VisitDetailModel.dart';
import 'package:crmflutternew/model/visit/VisitListModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitMapDemo.dart';
import 'package:crmflutternew/dashboard/visit/VisitLocationDetails.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:crmflutternew/model/visit/VisitTrackDetailsModel.dart';
import 'package:crmflutternew/model/visit/VisitTrackModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitLocationDetails.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:crmflutternew/model/visit/GetVisitByIdModel.dart';

class VisitTimelineChart extends StatefulWidget {
  String getVisistid;

  VisitTimelineChart({
    Key key,
    @required this.getVisistid,
  }) : super(key: key);

  @override
  VisitTimelineChartState createState() =>
      VisitTimelineChartState(visitid: getVisistid);
}

class VisitTimelineChartState extends State<VisitTimelineChart> {
  String token;
  String visitid;
  String loginToken = "Login_token";

  VisitTimelineChartState({Key key2, @required this.visitid});

  List<TimelineModel> items = List();

  @override
  void initState() {
    super.initState();
    setData();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0),
      child: Timeline(
        children: items,
        position: TimelinePosition.Left,
        lineColor: Constants.COLOR_LOGIN_UPPER_BG,
      ),
    );
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(loginToken);
    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        token = value;
        getListApicall();
        // getTrackPoints();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<GetVisitByIdModel> getListApicall() async {
    //HistoryVisitByIdModel class
    int i = 0;
    try {
      GetVisitByIdModel d1 = await listapi(); //HistoryVisitByIdModel
      print("visit details ${d1.details.datas.length}");
      //timeline charte
      d1.details.datas.forEach((element) {
        i++;
        items.add(TimelineModel(
            Container(
              height: 110.0,
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: new EdgeInsets.only(
                          top: 10.0, left: 10.0, right: 10.0),
                      child: Text(
                        element.eventId == null
                            ? (i == 1 ? "Start Visit" : "End Visit")
                            : "Check In " + element.accountName,
                        style: TextStyle(
                          fontSize: 14.0,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Bold,
                          color: Constants.COLOR_LOGIN_UPPER_BG,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Padding(
                      padding: new EdgeInsets.only(
                          top: 5.0, left: 10.0, right: 10.0),
                      child: Text(
                        Constants.changeDateFromate(element.time),
                        style: TextStyle(
                          fontSize: 14.0,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                        padding: new EdgeInsets.only(
                            top: 5.0, left: 10.0, right: 10.0),
                        child: Row(
                          children: <Widget>[
                            element.eventId == null
                                ? Icon(Icons.location_on,
                                    size: 12.0,
                                    color: Constants.COLOR_LOGIN_UPPER_BG)
                                : Icon(Icons.location_on,
                                    size: 12.0, color: Colors.red),
                            Text(
                              (element.location.addressLine1 == null
                                      ? ""
                                      : element.location.addressLine1) +
                                  "," +
                                  (element.location.addressLine2 == null
                                      ? ""
                                      : element.location.addressLine2),
                              style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: AppUtils.FontName,
                                fontWeight: AppUtils.Regular,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        )),
                    Padding(
                      padding: new EdgeInsets.only(
                          top: 5.0, left: 10.0, right: 10.0),
                      child: Text(
                        element.location.state +
                            "," +
                            element.location.city +
                            " " +
                            element.location.zipCode,
                        style: TextStyle(
                          fontSize: 14.0,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: Colors.black,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            position: TimelineItemPosition.random,
            iconBackground: Constants.COLOR_LOGIN_UPPER_BG,
            icon: element.eventId == null
                ? Icon(
                    Icons.location_on,
                    size: 14.0,
                    color: Constants.COLOR_CARD_WHITE_BG,
                  )
                : Icon(
                    Icons.circle,
                    size: 12.0,
                    color: Constants.COLOR_CARD_WHITE_BG,
                  )));
      });

      // getTrackPoints();
      setState(() {});
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }

    return null;
  }

  Future<GetVisitByIdModel> listapi() async {
    //HistoryVisitByIdModel
    Response response = await Dio().post(
      AppUtils.visitHistoryBYIdtUrl, //AppUtils.VisitHistoryUrl
      data: {"_id": visitid},
      //605b49bf56e90b000947b8f0,605d6e579268e30008bee65e
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("here in api call ${response.data}");
    return GetVisitByIdModel.fromJson(response.data); //HistoryVisitByIdModel
  }
}
