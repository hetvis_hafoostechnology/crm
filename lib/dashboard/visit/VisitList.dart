import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/model/visit/VisitDetailModel.dart';
import 'package:crmflutternew/model/visit/VisitListModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitMapDemo.dart';
import 'package:crmflutternew/dashboard/visit/VisitLocationDetails.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:crmflutternew/model/visit/VisitTrackDetailsModel.dart';
import 'package:crmflutternew/model/visit/VisitTrackModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitLocationDetails.dart';

class VisitList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new VisitListPg(),
    );
  }
}

class VisitListPg extends StatefulWidget {
  @override
  State createState() => new VisitListState();
}

class VisitListState extends State<VisitListPg> {
  List<VisitDetailModel> retail = List();
  String token;
  String loginToken = "Login_token";
  bool isLoading_All = false;
  bool isFirstApiCalled_All = false;
  bool hasMore_All = true;
  int pageCount_All = 1;
  int lastIndexOfLocationEndVisit = 0;

  @override
  void initState() {
    super.initState();
    setData();

    setState(() {});
  }

  Widget buildlist(String username, bool isfinished, String startTime,
      String endTime, String startLoc, String endLoc,String visitid,double sendlat,double sendlng) {
    return InkWell(
      onTap: () {
        if (isfinished) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => VisitMapDemo(getVisistid: visitid,getInitiatelat: sendlat,getInitiatelng: sendlng,)));
        } else {
          AppUtils.showToastWithMsg("Visit haven't finished yet!", ToastGravity.CENTER);
        }
      },
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: new EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Text(
                username==null?"":username,
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
              child: Row(
                children: [
                  Text(
                    "Start Location",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: AppUtils.FontName,
                      color: Constants.COLOR_CARD_BUTTON,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    "Time",
                    style: TextStyle(
                      fontSize: 10.0,
                      fontFamily: AppUtils.FontName,
                      color: Constants.COLOR_CARD_BUTTON,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(
                  top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Row(
                children: [
                  Text(
                    startLoc,
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: AppUtils.FontName,
                      color: Colors.black,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    startTime,
                    style: TextStyle(
                      fontSize: 10.0,
                      fontFamily: AppUtils.FontName,
                      color: Colors.grey,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Padding(
              padding: new EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
              child: Row(
                children: [
                  Text(
                    "End Location",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: AppUtils.FontName,
                      color: Constants.COLOR_CARD_BUTTON,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  new Spacer(),
                  Text(
                    "Time",
                    style: TextStyle(
                      fontSize: 10.0,
                      fontFamily: AppUtils.FontName,
                      color: Constants.COLOR_CARD_BUTTON,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            isfinished == false
                ? Container()
                : Padding(
                    padding: new EdgeInsets.only(
                        top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
                    child: Row(
                      children: [
                        Text(
                          endLoc,
                          style: TextStyle(
                            fontSize: 12.0,
                            fontFamily: AppUtils.FontName,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        new Spacer(),
                        Text(
                          endTime,
                          style: TextStyle(
                            fontSize: 10.0,
                            fontFamily: AppUtils.FontName,
                            color: Colors.grey,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget _ListView(List<VisitDetailModel> visitlist) {
    if (!isFirstApiCalled_All) {
      return Center(child: CupertinoActivityIndicator());
    }
    if (visitlist == null || visitlist.length == 0) {
      return Center(
        child: Text("No Data Found"),
      );
    } else {
      return ListView.builder(
        itemCount: retail.length + 1,
        itemBuilder: (context, index) {
          if (index == retail.length - 1) {
            getListApicall();
          }
          if (index == retail.length) {
            if (hasMore_All) {
              return CupertinoActivityIndicator();
            } else {
              return Container();
            }
          }
          return buildlist(
              visitlist[index].userName,
              visitlist[index].isFinished,
              getStarttime(visitlist, index),
              getendtime(visitlist, index),
              getstartLoc(visitlist, index),
              getendloc(visitlist, index),
              visitlist[index].id,visitlist[index].details.datas[0].location.geoLatlong.latitude,visitlist[index].details.datas[0].location.geoLatlong.longitude);
        },
      );
    }
  }

  getStarttime(List<VisitDetailModel> visitlist, int index) {
    String getstartdatetime =
        Constants.changeDateFromate(visitlist[index].details.datas[0].time);
    return getstartdatetime;
  }

  getendtime(List<VisitDetailModel> visitlist, int index) {
    lastIndexOfLocationEndVisit = visitlist[index].details.datas.length;
    String getenddatetime = Constants.changeDateFromate(
        visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].time);
    return getenddatetime;
  }

  getstartLoc(List<VisitDetailModel> visitlist, int index) {
    String loc = visitlist[index].details.datas[0].location.addressLine1 == null
        ? ""
        : (visitlist[index].details.datas[0].location.addressLine1.length > 18)
            ? (visitlist[index]
                    .details
                    .datas[0]
                    .location
                    .addressLine1
                    .substring(0, 18) +
                "...")
            : visitlist[index].details.datas[0].location.addressLine1;
    String loc2 = visitlist[index].details.datas[0].location.addressLine2 ==
            null
        ? " "
        : (visitlist[index].details.datas[0].location.addressLine2.length > 16)
            ? (visitlist[index]
                    .details
                    .datas[0]
                    .location
                    .addressLine2
                    .substring(0, 16) +
                "...")
            : visitlist[index].details.datas[0].location.addressLine2;
    String loc3 = visitlist[index].details.datas[0].location.state == null
        ? " "
        : (visitlist[index].details.datas[0].location.state.length > 10)
            ? (visitlist[index]
                    .details
                    .datas[0]
                    .location
                    .state
                    .substring(0, 10) +
                "...")
            : visitlist[index].details.datas[0].location.state;
    String loc4 = visitlist[index].details.datas[0].location.city == null
        ? " "
        : (visitlist[index].details.datas[0].location.city.length > 10)
            ? (visitlist[index]
                    .details
                    .datas[0]
                    .location
                    .city
                    .substring(0, 10) +
                "...")
            : visitlist[index].details.datas[0].location.city;
    String loc5 = visitlist[index].details.datas[0].location.zipCode == null
        ? " "
        : visitlist[index].details.datas[0].location.zipCode;
    var add = loc + ", " + loc2 + "\n" + loc3 + "," + loc4 + " " + loc5;
    print(add);

    return add;
  }

  getendloc(List<VisitDetailModel> visitlist, int index) {
    lastIndexOfLocationEndVisit = visitlist[index].details.datas.length;
    print("index $lastIndexOfLocationEndVisit");
    String loc = visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine1 == null
        ? ""
        : (visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine1.length > 18) ? (visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine1.substring(0, 18) + "...") : visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine1;
    String loc2 = visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine2 == null
        ? " "
        : (visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine2.length>16)?(visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine2.substring(0,16)+"..."):visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.addressLine2;
    String loc3 = visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.state == null
        ? " "
        : (visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.state.length>10)?(visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.state.substring(0,10)+"..."):visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.state;
    String loc4 = visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.city == null
        ? " "
        : (visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.city.length>10)?(visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.city.substring(0,10)+"..."):visitlist[index].details.datas[lastIndexOfLocationEndVisit - 1].location.city;
    String loc5 = visitlist[index]
                .details
                .datas[lastIndexOfLocationEndVisit - 1]
                .location
                .zipCode ==
            null
        ? " "
        : visitlist[index]
            .details
            .datas[lastIndexOfLocationEndVisit - 1]
            .location
            .zipCode;
    var add = loc + ", " + loc2 + "\n" + loc3 + "," + loc4 + " " + loc5;

    return add;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ListView(retail),
    );
  }

  Future<VisitListModel> getListApicall() async {
    await Future.delayed(Duration(seconds: 1));
    List<VisitDetailModel> retaillist = List();
    try {
      if (!isLoading_All && hasMore_All) {
        isLoading_All = true;
        isFirstApiCalled_All = true;

        VisitListModel d1 = await listapi();

        if (d1 == null) {
          retail = null;
        } else {
          d1.datas.forEach((data) {
            retaillist.add(data);
          });

          retail.addAll(retaillist);

          // if (d1.paginationModel <= retail.length) {
          hasMore_All = false;
          // } else {
          //   hasMore_All = true;
          // }
          pageCount_All++;
          isLoading_All = false;
        }
        setState(() {});
        return d1;
      }
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }

    return null;
  }

  Future<VisitListModel> listapi() async {
    var resBody = {};

    Response response = await Dio().post(
      AppUtils.VisitListUrl,
      data: {
        "pagination": {"page": 1, "limit": 15}
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("here in api call ${response.data}");
    return VisitListModel.fromjson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(loginToken);
    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        token = value;
        getListApicall();
        // getTrackPoints();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
