import 'dart:async';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/cupertino.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:crmflutternew/model/visit/VisitTrackDetailsModel.dart';
import 'package:crmflutternew/model/visit/VisitTrackModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitLocationDetails.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/model/visit/VisitDetailModel.dart';
import 'package:crmflutternew/model/visit/GetVisitByIdModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitTimelineChart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;

//call get by id api first -take start end locarion from that api check if visit finished is true and event id =null,display time on locator
class VisitMapDemo extends StatefulWidget {
  // List<LatLng> getPointsArray2=<LatLng>[];
  String getVisistid;
  double getInitiatelat, getInitiatelng;

  VisitMapDemo(
      {Key key,
      @required this.getVisistid,
      this.getInitiatelat,
      this.getInitiatelng})
      : super(key: key);

  @override
  VisitModelDemostate createState() => VisitModelDemostate(
      visitid: getVisistid,
      initialLat: getInitiatelat,
      initiateLng: getInitiatelng);
}

class VisitModelDemostate extends State<VisitMapDemo> {
  Set<Marker> _markers = {};
  String googleAPIKey = "AIzaSyCXjOzOeaTIvxptSctHI3XctUT98Y75h-s";
  BitmapDescriptor sourceIcon;
  BitmapDescriptor checkinIcon;
  String token;
  String loginToken = "Login_token";
  Map<PolylineId, Polyline> _mapPolylines = {};
  int _polylineIdCounter = 1;
  List<LatLng> getPointsArray = <LatLng>[];
  String visitid;
  double initialLat, initiateLng;

  // var sourcelat,sourcelng;

  VisitModelDemostate(
      {Key key2,
      @required this.visitid,
      @required this.initialLat,
      @required this.initiateLng});

  @override
  void initState() {
    super.initState();
    print(
        "got visitid? $visitid and initiate lat lng ${initialLat} ${initiateLng}");
    setState(() {});
    // _add();
    setData();

    setSourceAndDestinationIcons();
  }

  void setSourceAndDestinationIcons() async {
    final Uint8List startIcon =
        await getBytesFromAsset('images/startloc.png', 100);
    final Uint8List endIcon =
        await getBytesFromAsset('images/checkinpoint.png', 100);
    sourceIcon = await BitmapDescriptor.fromBytes(startIcon);
    checkinIcon = await BitmapDescriptor.fromBytes(endIcon);
  }

  @override
  Widget build(BuildContext context) {
    // CameraPosition initialLocation = CameraPosition(
    //     zoom: CAMERA_ZOOM,
    //     bearing: CAMERA_BEARING,
    //     tilt: CAMERA_TILT,
    //     target: SOURCE_LOCATION);
    return Scaffold(body:Stack(
      children: [
        GoogleMap(
          //put here start visit location in target
          initialCameraPosition:
          CameraPosition(target: LatLng(initialLat, initiateLng), zoom: 15.0),
          markers: _markers,
          polylines: Set<Polyline>.of(_mapPolylines.values),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: 20.0, right: 15.0, left: 15.0, bottom: 10.0),
              child: SizedBox(
                height: 40.0,
                child: RaisedButton(
                  child: Text("View Timeline", style: TextStyle(fontSize: 20.0)),
                  color: Constants.COLOR_CARD_BUTTON,
                  textColor: Constants.COLOR_TEXT,
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => VisitTimelineChart(getVisistid: visitid,)));
                  },
                ),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  // void onMapCreated(GoogleMapController controller) {
  //   _controller.complete(controller);
  //   // setMapPins();
  //   // getListApicall();
  //   // setPolylines();
  //   getTrackPoints();
  // }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<GetVisitByIdModel> getListApicall() async {//HistoryVisitByIdModel class
    try {
      GetVisitByIdModel d1 = await listapi();//HistoryVisitByIdModel
      print("visit details ${d1.details.datas.length}");
      _markers.clear();
      Marker marker;
      int i = 1;
      int k=d1.details.datas.length;
      //605d6e579268e30008bee65e
      // sourcelat=d1.details.datas[0].location.geoLatlong.latitude;
      // sourcelng=d1.details.datas[0].location.geoLatlong.longitude;
      d1.details.datas.forEach((element) {
        i++;
        // if (element.eventId == null) {
        var timeformate = Constants.changeDateFromate(element.time);
        marker = Marker(
            markerId: MarkerId("marker${i}"),
            position: LatLng(element.location.geoLatlong.latitude,
                element.location.geoLatlong.longitude),
            infoWindow: InfoWindow(
                title: element.accountName == null//add account name
                    ? (i==2?"Start Visit":"End Visit")
                    : element.accountName,
                snippet: timeformate),
            icon: element.eventId==null?sourceIcon:checkinIcon);
        // } else {
        //   marker = Marker(
        //     markerId: MarkerId("marker${i}"),
        //     position: LatLng(element.location.geoLatlong.latitude,
        //         element.location.geoLatlong.longitude),
        //   );
        // }
        // setState(() {
        _markers.add(marker);
        // });
      });

      print(" marker size ${_markers.length}");
      // getTrackPoints();
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }

    return null;
  }

  Future<GetVisitByIdModel> listapi() async {//HistoryVisitByIdModel
    Response response = await Dio().post(
      AppUtils.visitHistoryBYIdtUrl,//AppUtils.VisitHistoryUrl
      data: {"_id": visitid}, //605b49bf56e90b000947b8f0,605d6e579268e30008bee65e
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("here in api call ${response.data}");
    return GetVisitByIdModel.fromJson(response.data);//HistoryVisitByIdModel
  }


  Future<VisitTrackDetailsModel> getTrackPoints() async {
    try {
      VisitTrackModel d1 = await trackapi();

      final List<LatLng> points = <LatLng>[];
      for (int i = 0; i < d1.datas.length; i++) {
        // print(LatLng(d1.datas[i].locationGeoCoordinatesLat,
        //     d1.datas[i].locationGeoCoordinatesLng));
        points.add(LatLng(d1.datas[i].locationGeoCoordinatesLat,
            d1.datas[i].locationGeoCoordinatesLng));
      }
      final String polylineIdVal = 'polyline_id_$_polylineIdCounter';
      _polylineIdCounter++;
      final PolylineId polylineId = PolylineId(polylineIdVal);

      // PolylinePoints polylinePoints = PolylinePoints();
      // List<PointLatLng> result = polylinePoints.decodePolyline("_p~iF~ps|U_ulLnnqC_mqNvxq`@");
      // print("whaaasts $result");
      // print("points array ${points.toString()}");
      setState(() {
        final Polyline polyline = Polyline(
          polylineId: polylineId,
          consumeTapEvents: true,
          color: Color.fromARGB(255, 40, 122, 198),
          width: 3,
          points: points,
        );

        _mapPolylines[polylineId] = polyline;
      });
    } catch (e) {
      print("dioerror catch${e.toString()}");
      // AppUtils.seesionLogin(_context);
    }

    return null;
  }

  Future<VisitTrackModel> trackapi() async {
    Response response = await Dio().post(
      AppUtils.TrackVisitUrl,
      data: {"visitId": visitid}, //605b49bf56e90b000947b8f0
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return VisitTrackModel.fromJsonArray(response.data["data"]);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    token = preferences.getString(loginToken);
    return token;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        token = value;
        getTrackPoints();
        getListApicall();
      });
    });
  }
}
//take per minute latlong and make ployline
