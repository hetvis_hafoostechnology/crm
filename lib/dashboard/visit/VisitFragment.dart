import 'dart:async';
import 'package:crmflutternew/dashboard/map/CustomMarker.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

//void main() => runApp(new VisitFragment());

class VisitFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '',
      theme: new ThemeData(primarySwatch: Colors.red),
      home: new CustomMarker(),
    );
  }

}