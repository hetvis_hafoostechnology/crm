import 'package:bloc/bloc.dart';
import 'package:crmflutternew/dashboard/changePwd/ChangePwdEvent.dart';
import 'package:crmflutternew/dashboard/changePwd/ChangePwdState.dart';
import 'package:crmflutternew/model/ChangePwd.dart';

import 'package:crmflutternew/helper/DataManager.dart';
import 'package:crmflutternew/login/LoginEvent.dart';
import 'package:crmflutternew/login/LoginState.dart';
import 'package:fimber/fimber.dart';


class ChangePasswordBloc extends Bloc<ChangePwdEvent, ChangePwdState> {
  var log = FimberLog("ChangePwdBloc");


  @override
  Stream<ChangePwdState> mapEventToState(ChangePwdEvent event) async* {
    if (event is ChangePwdButtonPressed) {
      yield ChangePwdLoading();
      try {
        ChangePwd fp = await DataManager.instance.callApiChangePassword(
            event.email);
        print("in bloc $fp");

        yield ChangePwdSuccess();
      } catch (e, stacktrace) {
        log.e("something went wrong", ex: e, stacktrace: stacktrace);
      }
    }
//    else if (event is LoginInitStateCalled) {
//      var email = DataManager.instance.getString(AppUtils.KEY_Email);
//      var password = DataManager.instance.getString("password");
//      yield SetLoginData(email, password);
//    }
  }

  @override
  ChangePwdState get initialState => ChangePwdUninitialized();
}
