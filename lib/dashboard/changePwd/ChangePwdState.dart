abstract class ChangePwdState {}

class ChangePwdUninitialized extends ChangePwdState {}


class ChangePwdLoading extends ChangePwdState {
  @override
  String toString() => "{ChangePwdLoading}";
}

class ChangePwdFailed extends ChangePwdState {
  int resCode;
  String reason;

  ChangePwdFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{ChangetPwdFailed reason: $reason}";
  }
}

class ChangePwdSuccess extends ChangePwdState {
  @override
  String toString() {
    return "{ChangePwd Sucess}";
  }
}



