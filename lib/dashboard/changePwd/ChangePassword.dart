import 'package:crmflutternew/dashboard/DashboardScreen.dart';
import 'package:crmflutternew/dashboard/changePwd/ChangePasswordBloc.dart';
import 'package:crmflutternew/dashboard/changePwd/ChangePwdEvent.dart';
import 'package:crmflutternew/dashboard/changePwd/ChangePwdState.dart';
import 'package:crmflutternew/login/LoginScreen.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/CustomAlertDialog.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/model/ChangePwd.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _changePasswordState();
  }
}

class _changePasswordState extends State<ChangePassword> {
  ChangePasswordBloc _bloc;
  ProgressDialog pr;
  String email;

//  TextEditingController emailController;

  @override
  void initState() {
    super.initState();
    setData();
    _bloc = ChangePasswordBloc();
    _bloc.state.listen(_ChangePwdStateListener);
//    emailController = TextEditingController();
//    _bloc.dispatch(ForgotPwdInitStateCalled());
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );

    return Scaffold(
        appBar: AppBar(
            title: Text("Change password"),
            backgroundColor: Constants.COLOR_LOGIN_UPPER_BG),
        body: SingleChildScrollView(
            child: Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new Container(
                  height: MediaQuery.of(context).size.height * .40,
                  color: Constants.COLOR_LOGIN_UPPER_BG,
                ),
                new Container(
                  height: MediaQuery.of(context).size.height * .45,
                  color: Constants.COLOR_LOGIN_LOWER_BG,
                )
              ],
            ),
            new Container(
              alignment: Alignment.topCenter,
              child: Text("hafooz",
                  style: TextStyle(
                      color: Constants.COLOR_TEXT,
                      decoration: TextDecoration.none,
                      fontSize: 40.0)),
              padding: new EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .15),
            ),
            new Container(
              padding: new EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .25,
                  right: 10.0,
                  left: 10.0),
              child: new Container(
                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                height: 250.0,
                width: MediaQuery.of(context).size.width,
                child: new Card(
                  color: Constants.COLOR_CARD_WHITE_BG,
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              top: 10.0, left: 15.0, right: 15.0),
                          child: Text(StringUtils.TitleEmail,
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: 10.0, left: 15.0, right: 15.0),
                          child: TextFormField(
//                            controller: emailController,
//                            keyboardType: TextInputType.emailAddress,
                            autofocus: false,
                            readOnly: true,
                            decoration: InputDecoration(
                              hintText: email,
                              contentPadding: EdgeInsets.all(10.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0.0)),
                            ),
                          ),
                        ),
//                        Padding(
//                          padding: EdgeInsets.only(
//                              top: 10.0, left: 15.0, right: 15.0),
//                          child: Text("Old Password",
//                              style: TextStyle(
//                                  fontSize: 16.0, fontWeight: FontWeight.bold)),
//                        ),
//                        Padding(
//                          padding: EdgeInsets.only(
//                              top: 10.0, left: 15.0, right: 15.0),
//                          child: TextFormField(
//                            keyboardType: TextInputType.emailAddress,
//                            autofocus: false,
//                            decoration: InputDecoration(
//                              hintText: 'Old Password',
//                              contentPadding: EdgeInsets.all(10.0),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(0.0)),
//                            ),
//                          ),
//                        ),
//                        Padding(
//                          padding: EdgeInsets.only(
//                              top: 10.0, left: 15.0, right: 15.0),
//                          child: Text("New Password",
//                              style: TextStyle(
//                                  fontSize: 16.0, fontWeight: FontWeight.bold)),
//                        ),
//                        Padding(
//                          padding: EdgeInsets.only(
//                              top: 10.0, left: 15.0, right: 15.0),
//                          child: TextFormField(
//                            keyboardType: TextInputType.emailAddress,
//                            autofocus: false,
//                            decoration: InputDecoration(
//                              hintText: 'New Password',
//                              contentPadding: EdgeInsets.all(10.0),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(0.0)),
//                            ),
//                          ),
//                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: 10.0, right: 15.0, left: 15.0, bottom: 10.0),
                          child: SizedBox(
                            width: double.maxFinite,
                            height: 50.0,
                            child: RaisedButton(
                              child: Text('Change Password',
                                  style: TextStyle(fontSize: 20.0)),
                              color: Constants.COLOR_CARD_BUTTON,
                              textColor: Constants.COLOR_TEXT,
                              onPressed: () {
                                pr.show();
                                _btnPressed();
//                                Navigator.of(context).push(MaterialPageRoute(
//                                    builder: (context) => DashboardScreen()));
                              },
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: FlatButton(
                            child: Text(StringUtils.change_forgot_pwd_emailhint,
                              style: TextStyle(
                                  color: Constants.COLOR_CARD_FP,
                                  fontWeight: FontWeight.bold),
                            ),
                              onPressed: () {
//                                Navigator.of(context).push(
//
//                                    MaterialPageRoute(
//                                        builder: (context) => ChangePassword()));
                              }
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ],
        )));
  }

  void _btnPressed() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
//    if (_isValidData())
    pr.hide();
//      _bloc.dispatch(
//          ChangePwdButtonPressed(emailController.text));
    _bloc.dispatch(ChangePwdButtonPressed(email));
  }

  void _ChangePwdStateListener(ChangePwdState ls) {
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () {},
    );
    Widget doneButton = FlatButton(
      child: Text("Done"),

      onPressed: () async {
        SharedPreferences preferences =
        await SharedPreferences.getInstance();
        preferences.clear();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            ModalRoute.withName("/LoginScreen"));
      },
    );

    if (ls is ChangePwdFailed) {
      AlertDialog alert = AlertDialog(
        title: Text("Failed"),
        content: Text("sending Email failed"),
        actions: [
          okButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    } else if (ls is ChangePwdSuccess) {
      AlertDialog alert = AlertDialog(
        title: Text("Success"),
        content: Text("Password link sent on your registered email"),
        actions: [
          doneButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

//  bool _isValidData() {
//    var email = emailController.text;
//    if (email == null || email.length == 0) {
//      CustomAlertDialog.showAlert(
//          context: context,
//          title: "CRM",
//          message: "Enter Email",
//          button1Text: "Ok");
//      return false;
//    }
//    else if (!AppUtils.isEmail(email)) {
//      CustomAlertDialog.showAlert(
//          context: context,
//          title: "CRM",
//          message: "Enter Valid email",
//          button1Text: "Ok");
//      return false;
//    }
//    return true;
//  }

  Future<String> getdata() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    email = preferences.getString(Constants.pref_email);

    return email;
  }

  setData() {
    getdata().then((value) {
      setState(() {
        email = value;
      });
    });
  }
}
