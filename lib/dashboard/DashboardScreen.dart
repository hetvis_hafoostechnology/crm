import 'dart:io';
import 'package:badges/badges.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:crmflutternew/dashboard/fragments/account/AccountList.dart';
import 'package:crmflutternew/dashboard/fragments/SecondFragment.dart';
import 'package:crmflutternew/dashboard/fragments/lead/Lead.dart';
import 'package:crmflutternew/dashboard/fragments/notification/NotificationtList.dart';
import 'package:crmflutternew/model/UserProfile.dart';
import 'package:crmflutternew/dashboard/settings/SettingsPg.dart';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitFragment.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:crmflutternew/login/LoginScreen.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:crmflutternew/model/getUrlModel.dart';
import 'package:geolocation/geolocation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:progress_dialog/progress_dialog.dart';
class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title);
}

class DashboardScreen extends StatefulWidget {
  final drawerItems = [
    new DrawerItem(StringUtils.TxtDashBoard),
    new DrawerItem(StringUtils.TxtVisit),
    new DrawerItem(StringUtils.TxtLead),
    new DrawerItem(StringUtils.TxtAccount),
    new DrawerItem(StringUtils.TxtNotification),
    new DrawerItem("User"),
    new DrawerItem("Settings")
  ];

  DashboardScreen() {
    Geolocation.loggingEnabled = true;
  }

  @override
  State<StatefulWidget> createState() {
    return dashboardScreenState();
  }
}

class dashboardScreenState extends State<DashboardScreen> {
  int _selectedDrawerIndex = 0, navindex = 0, notificationCounter = 0;
  String lname = "", email = "", imagelink;

  String token, userid, aemail = "", alname = "", aimage = "", arole = "";

  // File _pickedImage;
  // File slectedfile;
  // String filename;
  // double fileSizeKb;
  // String nameOfFile;
  // String pathFromApi;
  // int fileSizeB;
  BuildContext _context;
  DateTime currentBackPressTime;
  File _image;
  ProgressDialog pr;

  @override
  void initState() {
    super.initState();
    setLoginData();
  }

  Widget _getDrawerItemWidget(int pos, int indexx) {
    switch (pos) {
      case 0:
        return new SecondFragment();
      case 1:
        return new VisitFragment();
      case 2:
        return new Lead(
          sortingOrder3: null,
          sortingType3: null,
          currentTabIndex3: 0,
          sortingIndex3: 0,
          source3: [],
          stage3: [],
          entity3: [],
          selfilterentity3: [],
          selfiltersource3: [],
          selfilterstage3: [],
        );
      case 3:
        return new Account(
          sortingOrder3: null,
          sortingType3: null,
          currentTabIndex3: 0,
          sortingIndex3: 0,
          source3: [],
          selfilter3: [],
        );
      case 4:
        return new NotificationList();
    }
    return new Text(StringUtils.TxtError);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return new Scaffold(
      endDrawer: Drawer(
          child: SafeArea(
              child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.zero,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 10.0),
                    child: Text(
                      StringUtils.TxtUserProfile,
                      style: TextStyle(
                        fontFamily: AppUtils.FontName,
                        fontWeight: AppUtils.Bold,
                        color: Constants.COLOR_CARD_SIGNIN_TITLE,
                        fontSize: 18.0,
                      ),
                    ),
                  )),
            ),
            Container(
              child: Divider(
                color: Colors.grey,
                height: 26,
              ),
            ),
            new GestureDetector(
              onTap: () {
                _getImage();
                // choose2();
              },
              child: Container(
                width: 100.0,
                height: 100.0,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new NetworkImage(aimage),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
                  border: new Border.all(
                    color: Constants.COLOR_CARD_SIGNIN_TITLE,
                    width: 2.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Text(alname,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Bold,
                  ),
                  textAlign: TextAlign.center),
            ),
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Text(arole,
                  style: TextStyle(
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.SemiBold,
                      color: Constants.COLOR_CARD_SIGNIN_TITLE,
                      fontSize: 16.0),
                  textAlign: TextAlign.center),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: RaisedButton(
                    color: Constants.COLOR_CARD_SIGNIN_TITLE,
                    child: Text(
                      StringUtils.BtnChangePwd,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12.0,
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: Constants.COLOR_TEXT),
                    ),
                    splashColor: Constants.COLOR_CARD_FP,
                    onPressed: () {
                      Fluttertoast.showToast(
                          msg: StringUtils.Txtresetpwd,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIos: 1,
                          fontSize: 14.0);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0, left: 5.0, right: 5.0),
                  child: Padding(
                    padding: EdgeInsets.only(left: 5.0, right: 5.0),
                    child: OutlineButton(
                      child: Text(
                        StringUtils.BtnLogout,
                        style: TextStyle(
                            fontSize: 12.0,
                            fontFamily: AppUtils.FontName,
                            fontWeight: AppUtils.Regular,
                            color: Constants.COLOR_CARD_SIGNIN_TITLE),
                      ),
                      onPressed: () async {
                        SharedPreferences preferences =
                            await SharedPreferences.getInstance();
                        preferences.clear();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginScreen(),
                            ),
                            ModalRoute.withName("/LoginScreen"));
                      },
                      borderSide:
                          BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                      highlightElevation: 4.0,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ))),
      appBar: AppBar(
        backgroundColor: Constants.COLOR_LOGIN_UPPER_BG,//remo
        title: new Text(
          widget.drawerItems[_selectedDrawerIndex].title,
          style: TextStyle(
            fontSize: 16.0,
            fontFamily: AppUtils.FontName,
            fontWeight: AppUtils.SemiBold,
          ),
        ),
        actions: [
          Builder(
            builder: (context) => IconButton(
              icon: Icon(Icons.person),
              onPressed: () => Scaffold.of(context).openEndDrawer(),
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: Container(
          color: Constants.COLOR_LOGIN_UPPER_BG,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 90.0,
                child: DrawerHeader(
                  child: Text(
                    StringUtils.TxtHafooz,
                    style: TextStyle(
                      color: Constants.COLOR_TEXT,
                      fontSize: 26.0,
                      fontFamily: AppUtils.FontName,
                      fontWeight: AppUtils.Bold,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  decoration: BoxDecoration(
                    color: Constants.COLOR_CARD_SIGNIN_TITLE,
                  ),
                ),
              ),
              Container(
                  child: Column(children: <Widget>[
                ListTile(
                    leading: Icon(Icons.account_circle,
                        color: Constants.COLOR_CARD_WHITE_BG),
                    title: Text(
                      StringUtils.TxtUser,
                      style: TextStyle(
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: Constants.COLOR_TEXT,
                          fontSize: 16.0),
                    ),
                    onTap: () {
//                      _onSelectItem(4);
                    }),
                ListTile(
                    leading: Icon(Icons.settings,
                        color: Constants.COLOR_CARD_WHITE_BG),
                    title: Text(
                      "Settings",
                      style: TextStyle(
                          fontFamily: AppUtils.FontName,
                          fontWeight: AppUtils.Regular,
                          color: Constants.COLOR_TEXT,
                          fontSize: 16.0),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SettingsPg()));
//                      _onSelectItem(4);
                    }),
              ])),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Constants.COLOR_LOGIN_UPPER_BG,
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedDrawerIndex,
        onTap: (value) => setState(() => _selectedDrawerIndex = value),
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.dashboard),
            title: new Text(
              StringUtils.TxtDashBoard,
              style: TextStyle(
                fontSize: 12.0,
                fontFamily: AppUtils.FontName,
                fontWeight: AppUtils.Thin,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.person),
            title: new Text(StringUtils.TxtVisit,
                style: TextStyle(
                  fontSize: 12.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Thin,
                )),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.view_list),
            title: Text(StringUtils.TxtLead,
                style: TextStyle(
                  fontSize: 12.0,
                  fontFamily: AppUtils.FontName,
                  fontWeight: AppUtils.Thin,
                )),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_balance),
              title: Text(StringUtils.TxtAccount,
                  style: TextStyle(
                    fontSize: 12.0,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Thin,
                  ))),
          BottomNavigationBarItem(
              icon: notificationCounter == 0
                  ? Icon(Icons.notifications)
                  : new Stack(
                      children: <Widget>[
                        new Icon(Icons.notifications),
                        new Positioned(
                          top: 0.0,
                          right: 0.0,
                          child: new Stack(
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: 14.0, color: Colors.redAccent),
                              new Positioned(
                                top: 1.0,
                                right: 4.0,
                                child: new Text(notificationCounter.toString(),
                                    style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 10.0,
                                    )),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
              title: Text(StringUtils.TxtNotification,
                  style: TextStyle(
                    fontSize: 12.0,
                    fontFamily: AppUtils.FontName,
                    fontWeight: AppUtils.Thin,
                  ))),
        ],
      ),
      body: WillPopScope(
        child: _getDrawerItemWidget(_selectedDrawerIndex, navindex),
        onWillPop: onWillPop,
      ),
//      body:_children[_currentindex],
    );
  }

  Future<String> loadLoginData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);

    return userid;
  }

  setLoginData() {
    print("call api for profile");
    loadLoginData().then((value) {
      setState(() {
        userid = value;
      });
      getProfile(userid, token);
      getconuter(token);
    });
  }

  Future<String> loadProfileData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    aemail = preferences.getString(Constants.pref_email);
    alname = preferences.getString(Constants.pref_fname) +
        " " +
        preferences.getString(Constants.pref_lname);
    aimage = preferences.getString(Constants.pref_image);
    arole = preferences.getString(Constants.pref_role);

    return alname;
  }

  setProfileData() {
    loadProfileData().then((value) {
      setState(() {
        alname = value;
      });
    });
  }

  Future<UserProfile> getProfile(String usrid, String token) async {
    try {
      UserProfile d1 = await getProfileApi(usrid, token);
      saveData(
          d1.id,
          d1.companyId,
          d1.firstName,
          d1.lastName,
          d1.email,
          d1.mobileNo,
          d1.telephoneNo,
          d1.roleId,
          d1.aclPolicy,
          d1.file,
          d1.role);
      setState(() {});
      setProfileData();
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(_context, e);
    }
  }

  Future<UserProfile> getProfileApi(String userid, String token) async {
    Response response = await Dio().post(
      AppUtils.UserLoginProfileUrl,
      data: {AppUtils.KEY_id: userid},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("profile data ${response.data}");
    print("token ${token}");
    return UserProfile.fromjson_profile(response.data);
  }

  Future<SampleCounter> getconuter(String token) async {
    try {
      SampleCounter d1 = await getNotificationCnt(token);
      print("Counter ${d1.data}");
      notificationCounter = d1.data;
      setState(() {});

      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(_context, e);
    }
  }

  Future<SampleCounter> getNotificationCnt(String token) async {
    Response response = await Dio().post(
      AppUtils.NotificationCounterUrl,
      data: {},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return SampleCounter.fromjson(response.data);
  }

//   void choose2()async{
//     PickedFile pickedFile = await ImagePicker().getImage(
//       source: ImageSource.gallery,
//       maxWidth: 1800,
//       maxHeight: 1800,
//     );
//     if (pickedFile != null) {
//       File imageFile = File(pickedFile.path);
//       print("imgg $imageFile");
//     }
//
//     print("pick img $pickedFile");
//     print("path ${pickedFile.path}");
//   }
//   void _choose(BuildContext ctx) async {
// //    file = await ImagePicker.pickImage(source: ImageSource.camera);
// // file = await ImagePicker.pickImage(source: ImageSource.gallery);
//
//
//     if (imageSource != null) {
//       PickedFile  file = await picker.getImage(source:imageSource);
//       final bytes = await file.readAsBytes();
//
//       if (file != null) {
//         setState(() => _pickedImage = File(file.path));
//         print("pathhhh $_pickedImage");
//       }
//
//
//       var path = await FlutterAbsolutePath.getAbsolutePath(file.path);
//       print("gjwdhkja $path");
//
//     }
//
// //    print(await _pickedImage.length());
// //    fileSizeKb = await _pickedImage.length() / 1000;
//     fileSizeB = await _pickedImage.length();
//     print("bytes $fileSizeB");
// //    double fileSizemb = await _pickedImage.length() / 1000000;
// //kb
// //    print(fileSizeKb);
//     //mb
// //    print(fileSizemb);
//
//     filename = basename(_pickedImage.path);
//
// //    getUploadUrl(filename,fileSizeKb);
//
// //     uploadUrl(filename, fileSizeB);
//   }
  _getImage() async {
    final picker = ImagePicker();
    final imageSource = await showDialog<ImageSource>(
        context: _context,
        builder: (context) => AlertDialog(
              title: Text(
                "Select option",
                style: TextStyle(color: Constants.COLOR_LOGIN_UPPER_BG),
              ),
              actions: <Widget>[
                MaterialButton(
                  child: Text("Camera",
                      style: TextStyle(color: Constants.COLOR_CARD_BUTTON)),
                  onPressed: () => Navigator.pop(context, ImageSource.camera),
                ),
                MaterialButton(
                  child: Text("Gallery",
                      style: TextStyle(color: Constants.COLOR_CARD_BUTTON)),
                  onPressed: () => Navigator.pop(context, ImageSource.gallery),
                )
              ],
            ));

    PickedFile imageFile = await picker.getImage(source: imageSource);
//If there is no image selected, return.
    if (imageFile == null) return;
//File created.
    File tmpFile = File(imageFile.path);
//it gives path to a directory - path_provider package.
    final appDir = await getApplicationDocumentsDirectory();
//filename - returns last part after the separator - path package.
    final fileName = basename(imageFile.path);
//copy the file to the specified directory and return File instance.
    tmpFile = await tmpFile.copy('${appDir.path}/$fileName');
//prints file location

    setState(() {
      _image = tmpFile;
    });
    var bytes = new File(_image.path);
    var enc = await bytes.readAsBytes();
    print('File path is :${tmpFile.path}');
    print('File name :${fileName}');
    print('File size :${enc.length}');
    String mimeStr = lookupMimeType(_image.path);
    //HERE NEED TO CHECK HEIC TYPE IN IOS FOR IMAGE IF ANY ERROR OCCURS IN IOS CHECK https://stackoverflow.com/questions/47429421/converting-an-image-from-heic-to-jpeg-jpg
    var fileType = mimeStr.split('/');
    print('file type ${fileType[1]}');
    print("img?? ${_image}");
    if (fileType[1] == "jpeg" || fileType[1] == "png") {
      print('file type ok');
      uploadUrl(fileName, enc.length);
    } else {
      AppUtils.showToastWithMsg(
          "Please select Image only", ToastGravity.CENTER);
    }

  }

  Future<getUrlModel> uploadUrl(String fileName, var filesize) async {
    try {
      getUrlModel d1 = await getUploadUrl(fileName, filesize);
      print("url to call ${d1.dataUrl}");
      print("file path ${d1.filePath}");
      String profpath = d1.filePath;
      // getUrlandCallAws(d1.dataUrl);
      getUrlandCallAws(d1.dataUrl, fileName, filesize, profpath);
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    // data: Stream.fromIterable(image.map((e) => [e])),
    return null;
  }

  Future<getUrlModel> getUploadUrl(String fileName, var filesize) async {
    print("flile name${fileName} size ${filesize}");
    Response response = await Dio().post(
      AppUtils.GeneralFileUploadUrl,
      data: {
        "status": "pending",
        AppUtils.KEY_name: fileName,
        "size": filesize,
        AppUtils.KEY_type: "image/jpeg",
        "directory": "user_profile"
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print(response.data);
    return getUrlModel.fromjson(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<int> getUrlandCallAws(
      String url, String fname, var fsize, String profPath) async {
    try {
      int d1 = await uploadOnaws(url, fname);
      print("status ${d1}");
      chanegProfileimg(fname, fsize, profPath);
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<int> uploadOnaws(String url, String filename) async {
    // print(" img ${_image.path}");
    // print(" uurl ${url}");
    // var bytes = new File(_image.path);
    // var enc = await bytes.readAsBytes();
    // File file = new File(_image.path);
    //
    // List<int> postData = await _image.readAsBytes();
    // Response response = await Dio().put(
    //   url,
    //   data: _image,
    //   options: Options(headers: {
    //     "Content-Type": "image/jpeg",
    //     "Content-Length":enc.length
    //   }),
    // );
    //
    // print("code1 ${response}");
    // print("code2 ${response.toString()}");
    // print("code3 ${response.statusMessage}");
    // print("code4 ${response.statusCode}");
    // return response.statusCode;

    // File file = new File(_image.path);
    // // // Uint8List image = File(slectedfile).readAsBytesSync();
    // // // Uint8List image = File(filename).readAsBytesSync();
    // // List<int> postData = await file.readAsBytes();
    // Response response = await Dio().put(
    //   url,
    //   data:_image.open(),
    //   options: Options(headers: {
    //     "Content-Type": "image/jpeg",
    //   }),
    // );
    //
    Uint8List image = File(_image.path).readAsBytesSync();
    var bytes = new File(_image.path);
    var enc = await bytes.readAsBytes();
    Options options = Options(contentType: "image/jpeg", headers: {
      'Accept': "*/*",
      'Content-Length': enc.length,
      'Connection': 'keep-alive',
      'User-Agent': 'ClinicPlush' //try to remove these things
    });

    Response response = await Dio().put(url,
        data: Stream.fromIterable(image.map((e) => [e])), options: options);
    // print("codejj1 ${response}");
    // print("codehh2 ${response.toString()}");
    // // print("code3 ${response.statusMessage}");
    // print("hhcode4 ${response.statusCode}");
    // return response.statusCode;
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();

    return response.statusCode;
  }

  Future<SampleModel> chanegProfileimg(
      String filename, var filesize, String profilepath) async {
    pr.show();
    try {
      SampleModel d1 =
          await getchanegProfileimgCall(filename, filesize, profilepath);

      print("img???");
      print(d1.message);
      getProfile(userid, token);
      AppUtils.showToastWithMsg(d1.message, ToastGravity.CENTER);
      pr.hide();
      setState(() {});

      return d1;
    } catch (e) {
      print(e);
    }
    // data: Stream.fromIterable(image.map((e) => [e])),
    return null;
  }

  Future<SampleModel> getchanegProfileimgCall(
      String filename, var filesize, String profilepath) async {
    Response response = await Dio().post(
      AppUtils.ChangeProfileImg,
      data: {
        "_id": userid,
        "file": {
          "status": "pending",
          "name": filename,
          "type": "image/jpeg",
          "size": filesize,
          "directory": "user_profile",
          "path": profilepath
        },
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("Img upload ${response.data}");
    return SampleModel.fromjson(response.data);
    //  return jsonResponse.map((job) => new Job.fromJson(job)).toList();
  }

  Future<bool> saveData(
      String id,
      String companyId,
      String firstname,
      String lastname,
      String email,
      String mobileNo,
      String telephoneNo,
      String roleId,
      // String licenseId,
      String aclPolicy,
      String file,
      String role
      // String license
      ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(Constants.pref_id, id);
    preferences.setString(Constants.pref_email, email);
    preferences.setString(Constants.pref_fname, firstname);
    preferences.setString(Constants.pref_lname, lastname);
    preferences.setString(Constants.pref_mob, mobileNo);
    preferences.setString(Constants.pref_image, file);
    preferences.setString(Constants.pref_role, role);
    // preferences.setString(Constants.pref_licensetype, license);

//    setProfileData();
  }

  Future<bool> onWillPop() {
    print("here drawer index $_selectedDrawerIndex");

    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      // if(_selectedDrawerIndex==0){
      AppUtils.showToastWithMsg(
          "Back Press again to Exit!!", ToastGravity.BOTTOM);
      // }
      return Future.value(false);
    }
    return Future.value(true);
  }
}

class SampleCounter {
  String flag;
  String message;
  int data;

  SampleCounter(this.data);

  static SampleCounter fromjson(Map<String, dynamic> json) {
    int data = json["data"];

    return new SampleCounter(data);
  }
}
//TODI-IMAGE PICKE4R ISSUE IMAGE NAME IS NOT PERFEXT CHECK THAT
//https://pub.flutter-io.cn/packages/image_picker/example
