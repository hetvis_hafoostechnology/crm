

import 'dart:typed_data';

//import 'package:barcode_scan/barcode_scan.dart';

import 'package:flutter/material.dart';

class qrExample extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return qrExampleState();
  }
}

class qrExampleState extends State<qrExample> {
  String result = "Hey there !";

//  Future _scanQR() async {
//    try {
//      String qrResult = await BarcodeScanner.scan();
//      setState(() {
//        result = qrResult;
//        print(result);
//      });
//    }  on FormatException {
//      setState(() {
//        result = "You pressed the back button before scanning anything";
//      });
//    } catch (ex) {
//      setState(() {
//        result = "Unknown Error $ex";
//      });
//    }
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Scanner"),
      ),
      body: Center(
        child: Text(
          result,
          style: new TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
        ),
      ),
//      floatingActionButton: FloatingActionButton.extended(
//        icon: Icon(Icons.camera_alt),
//        label: Text("Scan"),
////        onPressed: _scanQR,
//      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}