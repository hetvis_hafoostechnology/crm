abstract class GetNearestLatlongState {}

class GetNearestLatlongUninitialized extends GetNearestLatlongState {}

class GetNearestLatlongLoading extends GetNearestLatlongState {
  @override
  String toString() => "{latlongStateLoading}";
}

class GetNearestLatlongFailed extends GetNearestLatlongState {
  int resCode;
  String reason;

  GetNearestLatlongFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{latlongFailed reason: $reason}";
  }
}

class GetNearestLatlongSuccess extends GetNearestLatlongState {
  @override
  String toString() {
    return "{latlongSucess}";
  }
}

class SetGetNearestLatlongData extends GetNearestLatlongState{
  String latitude,longitude;

  SetGetNearestLatlongData(this.latitude, this.longitude);
}
