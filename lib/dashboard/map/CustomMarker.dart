import 'dart:async';
import 'dart:io' show Platform;
import 'package:crmflutternew/dashboard/checkin/CheckinForm.dart';
import 'package:crmflutternew/model/EndVisit.dart';
import 'package:crmflutternew/model/StartendVisit.dart';
import 'package:crmflutternew/model/Visit.dart';
import 'package:crmflutternew/model/latlongByAddress/GetNearestLatlongModel.dart';
import 'package:crmflutternew/dashboard/visit/VisitList.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';

var log = FimberLog("marker");

class CustomMarker extends StatefulWidget {
  @override
  _CustomMarkerState createState() => _CustomMarkerState();
}

class _CustomMarkerState extends State<CustomMarker> {
  Completer<GoogleMapController> _controller = Completer();
  var visitBtnText = "";
  String CurrentDateTIme,startCurrentDateTIme,endCurrentDateTIme;
  String platfrom_ios_android = "";
  String _deviceid;

  ProgressDialog pr;
  var visitid,coordinates,startCoordinates,endCoordinates, curLat,curLng,startClat, startClng,endCLat,endClng;
  var markerid;
  String userid = "", token = "";

  Position _currentPosition,_startCurrentPos,_endCurrentPos;

  String markerid_save = "markerId";
  var StartAddresses,StartFirst;
  var EndAddresses,EndFirst;

  void _onMapCreated(GoogleMapController controller) {
    DateTime now = DateTime.now();
    CurrentDateTIme = DateFormat('yyyy-MM-ddTkk:mm:ss').format(now);

    Marker cmarker = Marker(
      markerId: MarkerId("00"),
      position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
      icon: BitmapDescriptor.defaultMarker,
      infoWindow: InfoWindow(
        title: "Current Location",
      ),
    );
    markers.add(cmarker);
    _controller.complete(controller);
  }

  static const platform = const MethodChannel('flutter.native/helper');

  Set<Marker> markers = Set();

  Future<void> responseFromNativeCode(String vid) async {
    WidgetsFlutterBinding.ensureInitialized();

    print("here in native device id= ${_deviceid} and visit id $vid");
    String response;
    try {
      final String result = await platform.invokeMethod('helloFromNativeCode',
          {"token": token, "visitid": vid, "Deviceid": _deviceid});
      print("native res $result");
//      latlongModel llm = new latlongModel(lat: result, lng: result);
//      await llDbHelper.db.newLatlng(llm);
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }

    setState(() {});
  }

  Future<void> endNativeCode() async {
    WidgetsFlutterBinding.ensureInitialized();
    try {
      await platform.invokeMethod('endService');
    } on PlatformException catch (e) {
      print("Failed to Invoke: ${e.message}");
    }

    setState(() {});
  }

  Future<String> getdeviceinfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      platfrom_ios_android = StringUtils.platfromIos;

      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      platfrom_ios_android = StringUtils.platfromAndroid;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  @override
  Future<void> initState() {
    setState(() {
      _getCurrentLocation();
      setData();
    });
  }

//todo start visit api done,have to check end visit api ,ask for id in start api response which will be there in end api's body update hafoos project detail for 13 march add that yoy've updated start api,end api added location array in there body , added api named unfinishedvisit to check start adn end visit status¬
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );
    return Scaffold(
      body: Stack(
        children: <Widget>[
          new GoogleMap(
            onMapCreated: _onMapCreated,
            myLocationEnabled: true,
            initialCameraPosition: CameraPosition(target: LatLng(0.0, 0.0)),
            markers: markers,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 10.0, right: 15.0, left: 15.0, bottom: 10.0),
                child: SizedBox(
                  height: 40.0,
                  child: RaisedButton(
                    child: Text(visitBtnText, style: TextStyle(fontSize: 20.0)),
                    color: Constants.COLOR_CARD_BUTTON,
                    textColor: Constants.COLOR_TEXT,
                    onPressed: () {
                      setState(() {
                        // textChange();
                        if (visitBtnText.toString() == StringUtils.BtnStartVisit) {
                          getstartvisitApicall();
                        } else {
                          getendvisitApicall();
                        }
                      });

                    },
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 10.0, right: 15.0, left: 15.0, bottom: 10.0),
                child: SizedBox(
                  height: 40.0,
                  child: FloatingActionButton(
                    child: Icon(
                      Icons.list,
                      color: Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => VisitList()));
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<GetNearestLatlongModel> nearestLatlongApiCall() async {
    pr.show();
    try {
      GetNearestLatlongModel d1 = await getNearestLatlong();
      markers.clear();
      d1.datas.forEach((data) {
        markerid = data.id;
        Marker marker = Marker(
          markerId: MarkerId(data.id),
          position: LatLng(data.address.geoLatlong.latitude,
              data.address.geoLatlong.longitude),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          infoWindow: InfoWindow(
            title: data.address.addressLine1,
          ),
          onTap: () {
            saveData(markerid);
            _checkinAlert(data.address.addressLine1, data.name);
          },
        );
        markers.add(marker);
      });

      setState(() {});
      pr.hide();

      return d1;
    } catch (e) {
      print(e);
    }
  }

  void markerTapped() {
    SizedBox(
        width: double.maxFinite,
        height: 50.0,
        child: RaisedButton(
          child: Text('Sign In', style: TextStyle(fontSize: 20.0)),
          color: Constants.COLOR_CARD_BUTTON,
          textColor: Constants.COLOR_TEXT,
          onPressed: () {},
        ));
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {
      setState(() {
        _currentPosition = position;
        curLat = _currentPosition.latitude;
        curLng = _currentPosition.longitude;
//        loader(true);
        nearestLatlongApiCall();
      });

      coordinates = new Coordinates(_currentPosition.latitude, _currentPosition.longitude);
    }).catchError((e) {
      print(e);
    });
  }
  startCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {

        _startCurrentPos = position;
        startClat = _startCurrentPos.latitude;
        startClng = _startCurrentPos.longitude;
//        loader(true);


      startCoordinates = new Coordinates(_startCurrentPos.latitude, _startCurrentPos.longitude);
       StartAddresses = await Geocoder.local.findAddressesFromCoordinates(startCoordinates);
       StartFirst = StartAddresses.first;
       setState(() {

       });

    }).catchError((e) {
      print("here error? $e");
    });
  }
  endCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;//IOS?
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {

        _endCurrentPos = position;
        endCLat = _endCurrentPos.latitude;
        endClng = _endCurrentPos.longitude;
//        loader(true);



      endCoordinates = new Coordinates(_endCurrentPos.latitude, _endCurrentPos.longitude);
      print("end cord ${endCoordinates}");
      EndAddresses = await Geocoder.local.findAddressesFromCoordinates(endCoordinates);
      EndFirst= EndAddresses.first;
      setState(() {

      });
    }).catchError((e) {
      print(e);
    });
  }

  Future<void> _checkinAlert(String address, String name) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(name,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                    textAlign: TextAlign.center),
                Text(address,
                    style: TextStyle(fontSize: 18.0, color: Colors.black),
                    textAlign: TextAlign.center),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
                child: Text(StringUtils.TxtCheckIn,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Constants.COLOR_CARD_BUTTON),
                    textAlign: TextAlign.center),
                onPressed: () {
                  Navigator.pop(context);
                  if (visitBtnText.toString() == StringUtils.BtnStartVisit) {
                    _askForStartVisit(address, name);
                  } else {
                    print("in custom marker visit id ${visitid}");
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => CheckinForm(visitId: visitid)));
                  }
                }),
          ],
        );
      },
    );
  }

  void textChange() {

    setState(() {

    });
  }

  Future<void> _askForStartVisit(String address, String name) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(name,
                    style: TextStyle(fontSize: 20.0, color: Colors.black)),
                Text(StringUtils.TxtStartVisit),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
                child: Text(StringUtils.TxtOk),
                onPressed: () {
                  Navigator.pop(context);
                }),
          ],
        );
      },
    );
  }

  Future<StartendVisit> getstartvisitApicall() async {
    // visitBtnText = StringUtils.BtnEndVisit;

    try {
      // pr.show();
      startCurrentLocation();
      StartendVisit d1 = await startcall();
      //take just latlong and call this method at certain duration
      visitid = d1.visit;
      AppUtils.showToastWithMsg(
          StringUtils.VisitStartSuccess, ToastGravity.CENTER);
//      visitStartedCallNativeCode();

      setState(() {
        print("in visit start");
        visitBtnText = StringUtils.BtnEndVisit;
        responseFromNativeCode(d1.visit);
      });
      // pr.hide();
      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<StartendVisit> startcall() async {

    DateTime now = DateTime.now();
    startCurrentDateTIme = DateFormat('yyyy-MM-ddTkk:mm:ss').format(now);



    AppUtils.showToastWithMsg(
        "StartVisit add${StartFirst.subLocality} latlng${startClat}${startClng} time${startCurrentDateTIme}coord ${startCoordinates}", ToastGravity.CENTER);
    Response response = await Dio().post(
      AppUtils.VisitSaveUrl,
      data: {
        AppUtils.KEY_userId: userid,
        AppUtils.KEY_location: {
          AppUtils.KEY_addressLine1: StartFirst.subLocality,
          AppUtils.KEY_addressLine2: StartFirst.thoroughfare,
          AppUtils.KEY_addressLine3: StartFirst.subThoroughfare,
          AppUtils.KEY_city: StartFirst.subAdminArea,
          AppUtils.KEY_zipCode: StartFirst.postalCode,
          AppUtils.KEY_state: StartFirst.adminArea,
          AppUtils.KEY_country: StartFirst.countryName,
          AppUtils.KEY_geoLocation: {
            AppUtils.KEY_latitude: startClat,
            AppUtils.KEY_longitude: startClng
          }
        },
        AppUtils.KEY_time: startCurrentDateTIme,
        AppUtils.KEY_isFinished: false
//        "updatedBy":userid
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("in start visit$response");
    return StartendVisit.fromJson(response.data);
  }

  Future<EndVisit> getendvisitApicall() async {
    // visitBtnText = StringUtils.BtnStartVisit;
//    putStartStatus = false;

    try {
      endCurrentLocation();
      // pr.show();
      EndVisit d1 = await endcall();

      AppUtils.showToastWithMsg(
          StringUtils.VisitStopSuccess, ToastGravity.CENTER);
      setState(() {
        visitBtnText = StringUtils.BtnStartVisit;
        endNativeCode();
      });
      // pr.hide();
      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<EndVisit> endcall() async {
    // pr.show();

    DateTime now = DateTime.now();
    endCurrentDateTIme = DateFormat('yyyy-MM-ddTkk:mm:ss').format(now);



    AppUtils.showToastWithMsg("Endvisit add ${EndFirst.subLocality} latlng ${endCLat} ${endClng} time ${endCurrentDateTIme} coor${endCoordinates}", ToastGravity.CENTER);
    Response response = await Dio().post(
      AppUtils.VisitSaveUrl,
      data: {
        AppUtils.KEY_userId: userid,
        AppUtils.KEY_location: {
          AppUtils.KEY_addressLine1: EndFirst.subLocality,
          AppUtils.KEY_addressLine2: EndFirst.thoroughfare,
          AppUtils.KEY_addressLine3: EndFirst.subThoroughfare,
          AppUtils.KEY_city: EndFirst.subAdminArea,
          AppUtils.KEY_zipCode: EndFirst.postalCode,
          AppUtils.KEY_state: EndFirst.adminArea,
          AppUtils.KEY_country: EndFirst.countryName,
          AppUtils.KEY_geoLocation: {
            AppUtils.KEY_latitude: endCLat,
            AppUtils.KEY_longitude: endClng
          }
        },
        AppUtils.KEY_time: endCurrentDateTIme,
        AppUtils.KEY_id: visitid,
        AppUtils.KEY_isFinished: true,
        AppUtils.KEY_updatedBy: userid
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("finished viist$response");
    // pr.hide();
    return EndVisit.fromJson(response.data);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
//    gettingVal = preferences.getBool(_startVisitPrefs);
    userid = preferences.getString(AppUtils.SP_KEYLoginID);
    token = preferences.getString(AppUtils.SP_KEYLoginTOKEN);
    _deviceid = await getdeviceinfo();
    print("platfrom${platfrom_ios_android}&id${_deviceid}");
    return userid;
  }

//
  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
      });

      getVisitApiCall(userid, token);
    });
  }

  Future<GetNearestLatlongModel> getNearestLatlong() async {
    print("Get Nearest latlng from ${curLat}&${curLng}");
    Response response = await Dio().post(
      AppUtils.GetNearestAddUrl,
      data: {AppUtils.KEY_latitude: curLat, AppUtils.KEY_longitude: curLng},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("got nearest latlong list?${response.data}");
    return GetNearestLatlongModel.fromJsonArray(
        response.data[AppUtils.KEY_DATA]);
  }

  Future<Visit> getVisitApiCall(String user_id, String token) async {
    try {
      Visit d1 = await getVisitList(user_id, token);
      if (d1 == null) {
//        putStartStatus = false;
        visitBtnText = StringUtils.BtnStartVisit;
      } else {
        //already started
//        putStartStatus = true;

        visitBtnText = StringUtils.BtnEndVisit;
//        d1.datas.forEach((data) {
        visitid = d1.message;
        responseFromNativeCode(visitid);
//        });
      }
      setState(() {});
      return d1;
    } catch (e) {
      print(e);
    }
  }

  Future<Visit> getVisitList(String user_id, String token) async {
    Response response = await Dio().post(
      AppUtils.UnfinishedVisitUrl,
      data: {AppUtils.KEY_userId: user_id},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );

    return Visit.fromJsonResponse(response.data);
  }

  Future<bool> saveData(String markerid) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(markerid_save, markerid);
  }
}
