abstract class GetNearestLatlongEvent{}
class GetNearestLatlongButtonPressed extends GetNearestLatlongEvent{
  String latitude,longitude;

  GetNearestLatlongButtonPressed(this.latitude,this.longitude );

  @override
  String toString() => 'GetNearestLatlongButtonPressed { latitude: $latitude, longitude: $longitude }';
}
class GetNearestLatlongInitStateCalled extends GetNearestLatlongEvent{}
