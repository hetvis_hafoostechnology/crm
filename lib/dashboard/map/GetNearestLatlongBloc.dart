import 'package:bloc/bloc.dart';
import 'package:crmflutternew/dashboard/map/GetNearestLatlongEvent.dart';
import 'package:crmflutternew/dashboard/map/GetNearestLatlongState.dart';
import 'package:crmflutternew/model/latlongByAddress/Data.dart';
import 'package:crmflutternew/model/latlongByAddress/GetNearestLatlongModel.dart';
import 'package:crmflutternew/helper/DataManager.dart';
import 'package:fimber/fimber.dart';

class GetNearestLatlongBloc extends Bloc<GetNearestLatlongEvent, GetNearestLatlongState> {
  var log = FimberLog("latlongBloc");
  @override
  GetNearestLatlongState get initialState => GetNearestLatlongUninitialized();

  @override
  Stream<GetNearestLatlongState> mapEventToState(GetNearestLatlongEvent event) async* {
//    if (event is GetNearestLatlongButtonPressed) {
//      yield GetNearestLatlongLoading();
//      try {
//        GetNearestLatlongModel latlongModel = await DataManager.instance.nearestLatlongApiCall();
//        print("in bloc $latlongModel");
//        yield GetNearestLatlongSuccess();
//      } catch (e, stacktrace) {
//        log.e("something went wrong", ex: e, stacktrace: stacktrace);
//      }
//    }
//    else if (event is GetNearestLatlongInitStateCalled) {
//      var latitude = DataManager.instance.getString("latitude");
//      var longitude = DataManager.instance.getString("longitude");
//      yield SetGetNearestLatlongData(latitude, longitude);
//    }
  }
}