import 'package:crmflutternew/dashboard/map/CustomMarker.dart';
import 'package:crmflutternew/model/CheckIn.dart';
import 'package:crmflutternew/model/StartendVisit.dart';
import 'package:crmflutternew/model/assigned/AssignedDropdown.dart';
import 'package:crmflutternew/model/contact/ContactDropdown.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:crmflutternew/model/SampleModel.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckinForm extends StatefulWidget {

  String visitId;

  CheckinForm({Key key, @required this.visitId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CheckinFormState(getVisitId: visitId);
  }
}

class _CheckinFormState extends State<CheckinForm> {
  List<String> result;
  List<String> resultContact;
  List<String> idsAssign;
  List<String> idContact;
  List<String> contactEmail;
  List<String> contactMobile;
  String id;
  String cid;
  String userid;
  String getmarkerid;
  ProgressDialog pr;
  String name;
  String contactid;
  String contactname;
  String selectedcontactEmail = "";
  String selectedcontactMobile = "";
  String dropdownValue = null;
  var assignedName;
  String loginId = "Login_Id";
  String markerid_save = "markerId";

  var CurrentDateTIme;
  var currentimeaddedMore;
  var selectedid;
  String selectedName = null;
  var addressestoLatlong;
  Position _currentPosition;
  var clat;
  var coordinates;
  var clongi;

  TextEditingController subController,
      locationController,
      descController,
      eventAddController;
  String token;
  String loginToken = "Login_token";
  String getVisitId;



  _CheckinFormState({Key key2, @required this.getVisitId});
  @override
  void initState() {
    super.initState();
    setData();
    result = List();
    resultContact = List();
    DateTime startDate = DateTime.now(); //current date time
    DateTime endDate = startDate.add(new Duration(seconds: 120)); //added
//CurrentDateTIme = startDate.toIso8601String();
    CurrentDateTIme = DateFormat('yyyy-MM-ddTkk:mm:ss.sss').format(startDate);
    currentimeaddedMore = DateFormat('yyyy-MM-ddTkk:mm:ss.sss').format(endDate);
//YYYY-MM-DDTHH:MM:SSZ
    subController = TextEditingController();
    locationController = TextEditingController();
    descController = TextEditingController();
    eventAddController = TextEditingController();
print("get visit id checkin $getVisitId");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
//    getAssignedtoApicall();
    pr = new ProgressDialog(context);
    pr.style(
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
    );

    return Scaffold(
        appBar: AppBar(
            title: Text("Checkin Form"),
            backgroundColor: Constants.COLOR_LOGIN_UPPER_BG),
        body: Material(
            child: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.all(20.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
                child: Text(StringUtils.TitleSubject,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: subController,
                autofocus: false,
//                focusNode: _focusNodeloc,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.0)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, left: 0.0, bottom: 5.0),
                child: Text(StringUtils.TitleAssignedTo,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              callAssigneddropdown(),
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 15.0),
                child: Text(StringUtils.TitleLocation,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: locationController,
                autofocus: false,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.0)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 15.0),
                child: Text(StringUtils.TitleDescription,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: descController,
                autofocus: false,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.0)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, left: 0.0, bottom: 5.0),
                child: Text(StringUtils.TitleContact,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              callContactdropdown(),
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 15.0),
                child: Text(StringUtils.TitleMob,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  new Flexible(
//                    child: CountryPicker(
//                      dense: false,
//                      showFlag: true,
//
//                      //displays flag, true by default
//                      showDialingCode: true,
//                      //displays dialing code, false by default
//                      showName: false,
//                      //displays country name, true by default
//                      showCurrency: false,
//                      //eg. 'British pound'
//                      showCurrencyISO: false,
//                      //eg. 'GBP'
//                      onChanged: (Country country) {
//                        setState(() {
//                          _selected = country;
//                        });
//                      },
//                      selectedCountry: _selected,
//                    ),
//                    flex: 3,
//                  ),
//                  new Flexible(
//                    child: Container(
//                      padding: EdgeInsets.all(10.0),
//                      decoration: BoxDecoration(
//                          border: Border.all(color: Colors.grey)),
//                      child: Text(selectedcontactMobile),
//                    ),
////                    child: TextFormField(
////                      keyboardType: TextInputType.phone,
////                      readOnly: true,
////                      initialValue: selectedcontactMobile,
////                      autofocus: false,
////                      decoration: InputDecoration(
////                        contentPadding: EdgeInsets.all(10.0),
////                        border: OutlineInputBorder(
////                            borderRadius: BorderRadius.circular(0.0)),
////                      ),
////                    ),
//                    flex: 7,
//                  )
//                ],
//              ),
              Container(
                height: 50.0,
                padding: EdgeInsets.all(10.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(selectedcontactMobile),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 10.0),
                child: Text(StringUtils.TitleEmail,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),

              Container(
                height: 50.0,
                padding: EdgeInsets.all(10.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(selectedcontactEmail),
                ),
              ),
//              TextFormField(
//                keyboardType: TextInputType.emailAddress,
//                readOnly: true,
//                initialValue: selectedcontactEmail,
//                autofocus: false,
//                decoration: InputDecoration(
//                  contentPadding: EdgeInsets.all(10.0),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(0.0)),
//                ),
//              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0, bottom: 5.0, top: 15.0),
                child: Text(StringUtils.TitleEventAdd,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: eventAddController,
                autofocus: false,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.0)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding:
                        EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                    child: OutlineButton(
                      child: Text(
                        StringUtils.BtnCancel,
                        style:
                            TextStyle(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                      borderSide:
                          BorderSide(color: Constants.COLOR_CARD_SIGNIN_TITLE),
                      highlightElevation: 4.0,
//                      shape: new RoundedRectangleBorder(
//                          borderRadius: new BorderRadius.circular(30.0)
//                      )
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                    child: RaisedButton(
                      color: Constants.COLOR_CARD_SIGNIN_TITLE,
                      elevation: 4,
                      child: Text(
                        StringUtils.BtnSave,
                        style: TextStyle(color: Constants.COLOR_TEXT),
                      ),
                      splashColor: Constants.COLOR_CARD_FP,
                      onPressed: () {
                        if (_isValidData()) {
//                          pr.show();
                          getCheckInApicall();

//                          Navigator.of(context).push(
//                        MaterialPageRoute(
//                            builder: (context) => CustomMarker()));
                        }
//                    Navigator.of(context).push(
//                        MaterialPageRoute(
//                            builder: (context) => ChangePassword()));
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        )));
  }

  Future<ContactDropdown> getContactApicall() async {
    try {
      ContactDropdown d1 = await contactDropdown();
//      result = List();
      List<String> save1 = List();
      List<String> saveid1 = List();
      List<String> saveemail = List();
      List<String> samemob = List();
      d1.datas.forEach((data) {
        contactname = data.name;
        cid = data.id;
//        selectedcontactEmail = data.email;
//        selectedcontactMobile = data.mobile;

        save1.add(contactname);
        saveid1.add(cid);
        saveemail.add(data.email);
        samemob.add(data.mobile);

//        String fname = data.firstname;
//        String lname = data.lastname;
      });
      resultContact = save1;
      idContact = saveid1;
      contactEmail = saveemail;
      contactMobile = samemob;
      setState(() {});

      return d1;
    } catch (e) {
      print("here4$e");
    }
  }

  Future<AssignedDropdown> getAssignedtoApicall() async {
    try {
      AssignedDropdown d1 = await assignedDropdown();
//      result = List();
      List<String> save = List();
      List<String> saveid = List();
      d1.datas.forEach((data) {
        name = data.name;
        id = data.id;
        save.add(name);
        saveid.add(id);
        if (id == userid) {
          selectedid = id;
          selectedName = data.name;
          assignedName = id;
        }
        print("whatsid${assignedName}&${selectedid}");
//        String fname = data.firstname;
//        String lname = data.lastname;
      });
      result = save;
      idsAssign = saveid;

      setState(() {});

      return d1;
    } catch (e) {
      print("here1$e");
    }
  }

  Future<AssignedDropdown> assignedDropdown() async {
    print("tokrn$token");
    Response response = await Dio().post(
      AppUtils.UserDropDownUrl,
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("dropdown$response");
    return AssignedDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  callAssigneddropdown() {
    if (result != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: selectedName,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(color: Colors.black),
        onChanged: (String newValue) {
          setState(() {
            selectedName = newValue;
            int indexid = result.indexOf(newValue);
            assignedName = idsAssign.asMap()[indexid];
            print("here in $assignedName");
            //idsAssign.indexOf(indexid.toString());
          });
        },
        items: getAssignedItems(),
      );
    } else {
      return Container();
    }
  }

  getAssignedItems() {
    var dropdownItems = List<DropdownMenuItem<String>>();
    result.forEach((value) {
      dropdownItems.add(DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      ));
    });
    return dropdownItems;
  }

  Future<ContactDropdown> contactDropdown() async {
    print("ass$assignedName");
    print("sel$selectedid");
    Response response = await Dio().post(
      AppUtils.ActContactUrl,
      data: {AppUtils.KEY_accountId: assignedName},
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("getcontactitem$response");
    return ContactDropdown.fromJsonArray(response.data[AppUtils.KEY_DATA]);
  }

  callContactdropdown() {
    if (resultContact != null) {
      return DropdownButtonFormField<String>(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(3.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
        ),
        value: dropdownValue,
        icon: Icon(Icons.arrow_drop_down),
        iconSize: 24,
        style: TextStyle(color: Colors.black),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue = newValue;
            int indexid = resultContact.indexOf(newValue);

            contactname = idContact.asMap()[indexid];

            selectedcontactEmail = contactEmail.asMap()[indexid];
            selectedcontactMobile = contactMobile.asMap()[indexid];
          });
        },
        items: getContactItems(),
      );
    } else {
      return Container();
    }
  }

  getContactItems() {
    var dropdowncItems = List<DropdownMenuItem<String>>();
    resultContact.forEach((cvalue) {
      dropdowncItems.add(DropdownMenuItem<String>(
        value: cvalue,
        child: Text(cvalue),
      ));
    });
    return dropdowncItems;
  }

  bool _isValidData() {
    var subject = subController.text;
    var loc = locationController.text;
    var desc = descController.text;
    var evenAdd = eventAddController.text;

    if (subject == null || subject.length == 0) {
      AppUtils.showAlert(context, StringUtils.SubjectHint);
      return false;
    } else if (selectedName == null) {
      AppUtils.showAlert(context, StringUtils.SelectAssignedHint);
      return false;
    } else if (loc == null || loc.length == 0) {
      AppUtils.showAlert(context, StringUtils.LocationHint);
      return false;
    } else if (desc == null || desc.length == 0) {
      AppUtils.showAlert(context, StringUtils.DescHint);
      return false;
    } else if (dropdownValue == null) {
      AppUtils.showAlert(context, StringUtils.SelectContactHint);
      return false;
    } else if (evenAdd == null || evenAdd.length == 0) {
      AppUtils.showAlert(context, StringUtils.EventAddHint);
      return false;
    }
    return true;
  }

  Future<CheckIn> getCheckInApicall() async {
    pr.show();
    try {
      CheckIn d1 = await checkincall();
      print("checkin called ${d1.message} and id ${d1.data}");
      callSaveVisitForCheckin(d1.data);
      setState(() {});
      return d1;
    } catch (e) {
      AppUtils.sessionExpiredLoginCall(context, e);
    }
  }

  Future<CheckIn> checkincall() async {
    final coordinates = new Coordinates(clat, clongi);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    var currentadd = first.addressLine;

    Response response = await Dio().post(
      AppUtils.ActivityAddUrl,
      data: {
        AppUtils.KEY_id: null,
        "activityType": "event",
        "contactType": "contact",
        "contactId": contactname,
        "relatedType": "account",
        "relatedId": getmarkerid,
        AppUtils.KEY_accountId: getmarkerid,
        "assignedTo": assignedName,
        "subject": subController.text,
        "status": "5c80fed2b9d7af35c72e0b52",// 5c80fed2b9d7af35c72e0b52
        "priority": "5c80fe9ab9d7af35c72e0b4e",//5c80fe9ab9d7af35c72e0b4e
        "recurring": "5c86326d8d55a036c7b44b68",//5c86326d8d55a036c7b44b68
        "startDate": CurrentDateTIme,
        "endDate": currentimeaddedMore,
        "startTime": CurrentDateTIme,
        "endTime": currentimeaddedMore,
        AppUtils.KEY_Mobile: selectedcontactMobile,
        "description": descController.text,
        AppUtils.KEY_type: "5c9df31f8310b84a1b8f9e41",// 5c9df31f8310b84a1b8f9e41
        AppUtils.KEY_location: locationController.text,
        AppUtils.KEY_Email: selectedcontactEmail,
        "eventAddress": {
          AppUtils.KEY_address: currentadd,
          AppUtils.KEY_latitude: clat,
          AppUtils.KEY_longitude: clongi
        },
        AppUtils.KEY_updatedBy: userid
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("checkin$response");
    print("checkIn location ${clat} and ${clongi} address ${currentadd}");
    return CheckIn.fromJson(response.data);
  }
  Future<StartendVisit> callSaveVisitForCheckin(String eventid) async {
    print("in checkin visit  call event id $eventid");
    if(eventid==null){
      AppUtils.showToastWithMsg(StringUtils.CheckInFail, ToastGravity.BOTTOM);
    }
   else{
      try {

        StartendVisit d1 = await startcall(eventid);
        pr.hide();
        setState(() {
          AppUtils.showToastWithMsg(StringUtils.CheckInSuccess, ToastGravity.BOTTOM);
          Navigator.of(context).pop();

        });
        return d1;
      } catch (e) {
        print(e);
      }
    }
  }

  Future<StartendVisit> startcall(String eventid) async {

    print("in checkin visit call api ${getVisitId}");
    final coordinates = new Coordinates(clat, clongi);
    var addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    Response response = await Dio().post(
      AppUtils.VisitSaveUrl,
      data: {
        AppUtils.KEY_userId: userid,
        AppUtils.KEY_location: {
          AppUtils.KEY_addressLine1: first.subLocality,
          AppUtils.KEY_addressLine2: first.thoroughfare,
          AppUtils.KEY_addressLine3: first.subThoroughfare,
          AppUtils.KEY_city: first.subAdminArea,
          AppUtils.KEY_zipCode: first.postalCode,
          AppUtils.KEY_state: first.adminArea,
          AppUtils.KEY_country: first.countryName,
          AppUtils.KEY_geoLocation: {
            AppUtils.KEY_latitude: clat,
            AppUtils.KEY_longitude: clongi
          }
        },
        AppUtils.KEY_time: CurrentDateTIme,
        AppUtils.KEY_isFinished: false,
        AppUtils.KEY_id: getVisitId,
        AppUtils.KEY_updatedBy: userid,
        AppUtils.KEY_EventId: eventid,
      },
      options: Options(headers: {
        AppUtils.KEY_HeaderContent: AppUtils.Val_HeaderContent,
        AppUtils.KEY_AccessToken: token
      }),
    );
    print("in start visit$response");
    return StartendVisit.fromJson(response.data);
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {
      setState(() {
        _currentPosition = position;
        clat = _currentPosition.latitude;
        clongi = _currentPosition.longitude;
      });

      coordinates = new Coordinates(
          _currentPosition.latitude, _currentPosition.longitude);
    }).catchError((e) {
      print("here3$e");
    });
  }
//  Future<SampleModel> trackvisit() async {
//
//    Response response = await Dio().post(
//      AppUtils.BASE_URL + "visit/track/save",
//      data: {
//        "visitId": ,
//        "deviceId": ,
//        "platform": ,
//        "latitude": ,
//        AppUtils.KEY_longitude: ,
//
//      },
//      options: Options(headers: {
//        AppUtils.KEY_HeaderContent: Val_HeaderContent,
//        AppUtils.KEY_AccessToken: token
//      }),
//    );
//    print("trackvisit$response");
//    return SampleModel.fromjson(response.data);
//  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userid = preferences.getString(loginId);
    getmarkerid = preferences.getString(markerid_save);
    token = preferences.getString(loginToken);
    return userid;
  }

  setData() {
    loadData().then((value) {
      setState(() {
        userid = value;
        _getCurrentLocation();
        getAssignedtoApicall();
        getContactApicall();
      });
    });
  }
}
