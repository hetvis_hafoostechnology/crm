abstract class CheckInState {}

class CheckInUninitialized extends CheckInState {}

class CheckInLoading extends CheckInState {
  @override
  String toString() => "{CheckInStateLoading}";
}

class CheckInFailed extends CheckInState {
  int resCode;
  String reason;

  CheckInFailed({this.resCode, this.reason});

  @override
  String toString() {
    return "{CheckInFailed reason: $reason}";
  }
}

class CheckInSuccess extends CheckInState {
  @override
  String toString() {
    return "{CheckIn Sucess}";
  }
}


