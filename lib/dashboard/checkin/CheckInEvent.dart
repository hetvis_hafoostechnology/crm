abstract class CheckInEvent{}
class CheckInButtonPressed extends CheckInEvent{
  String email,password;
  bool isRememberPassword=false;
  CheckInButtonPressed(this.email, this.password);
  @override
  String toString() => 'CheckInButtonPressed { email: $email, password: $password }';
}
class CheckInInitStateCalled extends CheckInEvent{}
