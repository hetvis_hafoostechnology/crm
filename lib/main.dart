import 'dart:io';

import 'package:crmflutternew/dashboard/DashboardScreen.dart';

import 'package:crmflutternew/util/Constants.dart';
import 'package:crmflutternew/util/StringUtils.dart';
import 'package:crmflutternew/util/AppUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'login/LoginScreen.dart';
import 'dart:ui';
import 'package:permission_handler/permission_handler.dart';
import 'package:crmflutternew/model/MessageNotifi.dart';
import 'package:crmflutternew/dashboard/fragments/lead/LeadDetail.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
final BehaviorSubject<MessageNotifi> didReceiveLocalNotificationSubject =
    BehaviorSubject<MessageNotifi>();
final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print("hetetete bg data $message");
  if (message.containsKey('data')) {
    // Handle data message
    print("hetetete bg data");
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    print("hetetete bg not");
    // Handle  message
    final dynamic messagenoti = message['notification'];
  }
  print("hree in bg");
  // Or do other work.
}

String selectedNotificationPayload;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  print("hree in main");
  final NotificationAppLaunchDetails notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  String initialRoute = SplashScreen.routeName;
  if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
    selectedNotificationPayload = notificationAppLaunchDetails.payload;
    initialRoute = LeadDetail.routeName;
  }

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings(
          requestAlertPermission: false,
          requestBadgePermission: false,
          requestSoundPermission: false,
          onDidReceiveLocalNotification:
              (int id, String title, String body, String payload) async {
            print("datas ${title} & ${body} & ${payload}");
            didReceiveLocalNotificationSubject
                .add(MessageNotifi(title: title, body: body));
          });
  const MacOSInitializationSettings initializationSettingsMacOS =
      MacOSInitializationSettings(
          requestAlertPermission: false,
          requestBadgePermission: false,
          requestSoundPermission: false);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    print("datas ${payload}");
    if (payload != null) {
      debugPrint('notifi payload: $payload');
    }
    selectedNotificationPayload = payload;
    selectNotificationSubject.add(payload);
  });
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_){
    runApp(
      MaterialApp(
        initialRoute: initialRoute,
        routes: <String, WidgetBuilder>{
          SplashScreen.routeName: (_) =>
              SplashScreen(notificationAppLaunchDetails),
          LeadDetail.routeName: (_) => LeadDetail(leadId: selectedNotificationPayload)
        },
      ),
    );
  });

}

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: SplashScreen());
//   }
// }

class SplashScreen extends StatefulWidget {
  const SplashScreen(
    this.notificationAppLaunchDetails, {
    Key key,
  }) : super(key: key);

  static const String routeName = '/';

  final NotificationAppLaunchDetails notificationAppLaunchDetails;

  bool get didNotificationLaunchApp =>
      notificationAppLaunchDetails?.didNotificationLaunchApp ?? false;

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  String data = "";
  SharedPreferences prefs;
  bool allowSoundSp,
      allowVibrationSp,
      allowBadgeSp,
      allowContentSp,
      allowAllFeature;

  void checkPermission() async {
    final PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Future.delayed(Duration(seconds: 3), () {
        if (data == null) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              ModalRoute.withName("/LoginScreen"));
        } else {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => DashboardScreen(),
              ),
              ModalRoute.withName("/DashboardScreen"));
        }
      });
    } else {
      var hasOpened = openAppSettings();
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
                title: Text(StringUtils.TitlePermissionError),
                content: Text(StringUtils.TxtLOcationenable),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text(StringUtils.TxtOk),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  CupertinoDialogAction(
                    child: Text(StringUtils.TxtOpenSettings),
                    onPressed: () => openAppSettings(),
                  )
                ],
              ));
    }
  }

  @override
  void initState() {
    super.initState();

    final GlobalKey<NavigatorState> navigatorKey =
        GlobalKey(debugLabel: "Main Navigator");

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        // prefs = await SharedPreferences.getInstance();
        // allowSoundSp = prefs.getBool('allowNotification_sound') == null
        //     ? true
        //     : prefs.getBool('allowNotification_sound');
        // allowContentSp = prefs.getBool('allowNotification_content') == null
        //     ? true
        //     : prefs.getBool('allowNotification_content');
        // allowVibrationSp = prefs.getBool('allowNotification_vibration') == null
        //     ? true
        //     : prefs.getBool('allowNotification_vibration');
        // allowAllFeature = prefs.getBool('AllNotificationFeaturesAllow') == null
        //     ? true
        //     : prefs.getBool('AllNotificationFeaturesAllow');
        // print("onMessag ${message}");
        // print("check sound ${allowSoundSp} &content ${allowContentSp}");

        print(
            "user sel sound ${allowSoundSp} vibrate ${allowVibrationSp} content  ${allowContentSp} and all ${allowAllFeature}");
        if (allowContentSp == false ||
            allowVibrationSp == false ||
            allowSoundSp == false ||
            allowAllFeature == false) {
          if (allowAllFeature == false) {
            print("cancel");
            await flutterLocalNotificationsPlugin.cancelAll();
          } else {
            print("else");
            await notificationAsperUserSelection(
                message,
                "channelid$allowSoundSp$allowVibrationSp$allowContentSp",
                allowSoundSp,
                allowVibrationSp,
                allowContentSp);
          }
        } else {
          print("default noti $message");
          await _showNotification(message);
        }
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        _navigateToItemDetail(message);
        fcmMessageHandler(message, navigatorKey, context);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        fcmMessageHandler(message, navigatorKey, context);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      print("Push Messaging token: $token");
    });

    _requestPermissions();
    _configureDidReceiveLocalNotificationSubject();
    _configureSelectNotificationSubject();

    checkPermission();
    setData();
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    print(" in naviagte method");
  }

  void _requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  void fcmMessageHandler(msg, navigatorKey, context) {
    print("on click screen $msg");
    switch (msg['data']['screen']) {
      case "Lead":
        Navigator.of(context).pushNamed('/LeadDetail');
        break;
      default:
        Navigator.of(context).pushNamed('/LeadDetail',);
        break;
    }
  }

  void _configureDidReceiveLocalNotificationSubject() {
    print("hree in config rec not ");
    didReceiveLocalNotificationSubject.stream
        .listen((MessageNotifi receivedNotification) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: receivedNotification.title != null
              ? Text(receivedNotification.title)
              : null,
          content: receivedNotification.body != null
              ? Text(receivedNotification.body)
              : null,
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () async {
                print("pay load= $selectedNotificationPayload");
                Navigator.of(context, rootNavigator: true).pop();
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) =>
                        LeadDetail(leadId: selectedNotificationPayload,),
                  ),
                );
              },
              child: const Text('Ok'),
            )
          ],
        ),
      );
    });
  }

  void _configureSelectNotificationSubject() {
    print("hree in config seletc noti ");
    selectNotificationSubject.stream.listen((String payload) async {
      print("select notifciation ${payload}");
      await Navigator.pushNamed(context, '/LeadDetail');
    });
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Constants.COLOR_LOGIN_UPPER_BG
    ));
    return Scaffold(
      body: Center(
        child: new Container(
          alignment: Alignment.topCenter,
          child: Text(StringUtils.TitleAppName,
              key: Key(StringUtils.TitleAppName),
              style: TextStyle(
                  color: Constants.COLOR_CARD_BUTTON,
                  decoration: TextDecoration.none,
                  fontSize: 40.0)),
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .15),
        ),
      ),
    );
  }

  Future<void> _showNotification(Map<String, dynamic> message) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'default_notification_channel_id',
      'notification_channel_name',
      'notification_channel_description',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
    );
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        message["notification"]["title"],
        message["notification"]["body"],
        platformChannelSpecifics,
        payload: message["data"]["id"]);
    //for IOs
    // final bool result = await flutterLocalNotificationsPlugin
    //     .resolvePlatformSpecificImplementation<
    //     IOSFlutterLocalNotificationsPlugin>()
    //     ?.requestPermissions(
    //   alert: true,
    //   badge: true,
    //   sound: true,
    // );
  }

  Future<void> notificationAsperUserSelection(Map<String, dynamic> message,
      String channelid, bool sound, bool vibrate, bool content) async {
    print("user multiple selection");
    AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            channelid, 'user_channel_name', 'user_channel_description',
            playSound: sound,
            enableVibration: vibrate,
            styleInformation: DefaultStyleInformation(true, true));
    IOSNotificationDetails iOSPlatformChannelSpecifics =
        IOSNotificationDetails(presentSound: sound);
    MacOSNotificationDetails macOSPlatformChannelSpecifics =
        MacOSNotificationDetails(presentSound: sound);
    NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics,
        macOS: macOSPlatformChannelSpecifics);
    if (content == false) {
      await flutterLocalNotificationsPlugin.show(
          0,
          message["notification"]["title"],
          "Content Hidden",
          platformChannelSpecifics,
          payload: message["data"]["id"]);
    } else {
      await flutterLocalNotificationsPlugin.show(
          0,
          message["notification"]["title"],
          message["notification"]["body"],
          platformChannelSpecifics,
          payload: message["data"]["id"]);
    }
    // await flutterLocalNotificationsPlugin.show(
    //     0,
    //     message[""]["title"],
    //     message[""]["body"],
    //     platformChannelSpecifics,
    //     payload: message["data"]["content"]);
  }

  Future<String> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(AppUtils.SP_KEYLoginFname);
  }

  setData() {
    loadData().then((value) {
      setState(() {
        data = value;
//print("spalsh name is $data");
      });
    });
  }

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.locationAlways.status;
    //contact per. if needed  final PermissionStatus permission = await Permission.locationAlways.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.locationAlways].request();
      return permissionStatus[Permission.locationAlways] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }
}

